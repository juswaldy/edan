# The Datahub

```mermaid
graph LR
    subgraph TmsEPrd
        Jenzabar
        Housing
        SETL
        SDS
        AQSync
        TWU_AR
        TWU_RE
        Advising
        CourseCreator
        ARCH
        Kantech
        Causeview
        TWU_RPT
        eRecords
    end
    Jenzabar <--> Housing
    Jenzabar <--> SETL
    Jenzabar <--> SDS
    Jenzabar <--> AQSync
    TWU_AR --> Jenzabar
    TWU_RE --> Jenzabar
    Jenzabar <--> Advising
    Jenzabar <--> CourseCreator
    Jenzabar <--> ARCH
    Jenzabar <--> Kantech
    Causeview --> Jenzabar
    Jenzabar --> TWU_RPT
    eRecords --> Jenzabar
```

```mermaid
graph
    subgraph Datahub
        Receiving
        Staging
        Shipping
        Staging <--> MDM
    end
    subgraph Aqueduct
        AQSync
        AQReports
    end
    subgraph TmsEPrd
        Jenzabar
    end
    subgraph FinancialReporting
        Model1
        Model2
    end
    Receiving --> Staging
    Staging --> Shipping
    Receiving --> Shipping
    Shipping --> Housing
    Shipping --> SETL
    Shipping --> SDS
    Shipping --> ARCH
    Shipping --> Kantech
    Shipping --> TWU_RPT
    Shipping --> CourseCreator
    Shipping --> Advising
    Shipping --> AQSync
    Shipping --> Model1
    Shipping --> Model2
    TWU_AR --> Receiving
    TWU_RE --> Receiving
    Advising --> Receiving
    CourseCreator --> Receiving
    Causeview --> Receiving
    eRecords --> Receiving
    Shipping --> Jenzabar
    Shipping --> AQReports
    Jenzabar --> Receiving
    Model1 --> Receiving
    Model2 --> Receiving
    AQSync --> Receiving
    ARCH --> Receiving
```