USE Aqueduct
GO

-- Drop stored procedure if it already exists.
IF EXISTS (
    SELECT  *
    FROM    INFORMATION_SCHEMA.ROUTINES
    WHERE   SPECIFIC_SCHEMA = N'HumanResources'
    AND     SPECIFIC_NAME = N'DiffADPExport'
)
    DROP PROCEDURE HumanResources.DiffADPExport
GO

-- =============================================================================
-- Author:      Juswaldy Jusman
-- Create date: 2020-11-02
-- Description: Find ADP export diffs between 2 given datetimes. If parameters
--              are null, they will be defaulted to the last and current versions.
--              If toDatetime is null, it will be defaulted to the current version.
--              If fromDatetime is null, it will be defaulted to the version
--              previous to the given toDatetime.
-- Dependencies: TABLE - HumanResources.ADPExportVersion
--               TABLE - HumanResources.ADPExportVersionDetail
-- Returns: Table containing the diffs.
-- Updates:
-- 2020-11-09 JJJ Fix bug in the List report type: flip FromValue vs ToValue.
-- =============================================================================
CREATE PROCEDURE HumanResources.DiffADPExport
	@reportType CHAR(1) = 'L',     -- (L)ist or (T)able.
	@fromDatetime DATETIME = NULL, -- If null, it will be defaulted to the version previous to the toDatetime.
	@toDatetime DATETIME = NULL   -- If null, it will be defaulted to the current version.
AS
BEGIN

DECLARE
	@status_code INT = 0,
	@status_message VARCHAR(MAX) = NULL;

DECLARE
	@fromVersionId VARCHAR(8),
	@toVersionId VARCHAR(8),

	@metascript NVARCHAR(MAX) = NULL,
	@script NVARCHAR(MAX) = NULL,
	@eol CHAR(1) = CHAR(13),
	@tab CHAR(1) = CHAR(9),
	@idx INT = NULL,
	@schemaName VARCHAR(256) = 'HumanResources',
	@tableName VARCHAR(256) = 'ADPExportVersionDetail',
	@associateIdField VARCHAR(256) = 'AssociateId',
	@positionIdField VARCHAR(256) = 'PositionId',
	@fieldInfoTable VARCHAR(256) = '#_fieldInfo',
	@diffTable VARCHAR(256) = 'tempdb..##_diffResults',
	@datatypeField VARCHAR(16) = '--{DataType}--',
	@formatPrefix VARCHAR(16) = '/*FormatPrefix*/',
	@formatSuffix VARCHAR(16) = '/*FormatSuffix*/';

BEGIN TRY

	-- Make sure fromDatetime <= toDatetime.
	IF COALESCE(@fromDatetime, '1900-01-01') > COALESCE(@toDatetime, '9999-12-31') THROW 50001, N'Unable to translate parameters into diff versions', 1;

	-- If toDatetime is null, get the current version. Otherwise, get the last version before the specified toDatetime.
	SELECT @toVersionId = MAX(ADPExportVersionId) FROM HumanResources.ADPExportVersion WHERE (@toDatetime IS NULL AND 1=1) OR (Versiontime <= @toDatetime);

	-- If toVersionId is still null, get the current version.
	IF @toVersionId IS NULL SELECT @toVersionId = MAX(ADPExportVersionId) FROM HumanResources.ADPExportVersion;
		
	-- If fromDate is null, assign the version previous to the toVersionId. Otherwise, get the last version before the specified fromDatetime.
	SELECT @fromVersionId = MAX(ADPExportVersionId) FROM HumanResources.ADPExportVersion WHERE (@fromDatetime IS NULL AND ADPExportVersionId < @toVersionId) OR (Versiontime <= @fromDatetime);

	-- If fromVersionId is still null, get the first version.
	IF @fromVersionId IS NULL SELECT @fromVersionId = MIN(ADPExportVersionId) FROM HumanResources.ADPExportVersion;

	-- Set up diff fields table.
	CREATE TABLE #_fieldInfo (_name VARCHAR(256), _type VARCHAR(16));
	INSERT INTO #_fieldInfo VALUES
	('FirstName', 'VARCHAR(256)'),
	('LastName', 'VARCHAR(256)'),
	('JobTitleDescription', 'VARCHAR(256)'),
	('HomeDepartmentDescription', 'VARCHAR(256)'),
	('BusinessUnitDescription', 'VARCHAR(256)'),
	('LocationDescription', 'VARCHAR(256)'),
	('ReportsToAssociateId', 'VARCHAR(256)'),
	('ContractEndDate', 'DATETIME'),
	('TerminationDate', 'DATETIME');

	-- Prepare field creates, selects, formats, comparisons, and equal filters.
	DECLARE
		@creates NVARCHAR(MAX),
		@selects NVARCHAR(MAX),
		@formats NVARCHAR(MAX),
		@compares NVARCHAR(MAX),
		@equals NVARCHAR(MAX);
	WITH
	creates AS ( SELECT f.x FROM ( SELECT _name + ' VARCHAR(256), ' FROM #_fieldInfo FOR XML PATH ('') ) f(x) ),
	selects AS ( SELECT f.x FROM ( SELECT _name + ', ' FROM #_fieldInfo FOR XML PATH ('') ) f(x) ),
	formats AS ( SELECT f.x FROM ( SELECT CASE WHEN _type = 'DATETIME' THEN 'CONVERT(VARCHAR(10), ' + _name + ', 23)' ELSE _name END + ', ' FROM #_fieldInfo FOR XML PATH ('') ) f(x) ),
	compares AS ( SELECT f.x FROM ( SELECT 'CASE WHEN COALESCE(a.' + _name + ', '''') = COALESCE(b.' + _name + ', '''') THEN '''' ELSE ' + CASE WHEN _type = 'DATETIME' THEN 'COALESCE(CONVERT(VARCHAR(10), a.' + _name + ', 23), '''') + '' == '' + COALESCE(CONVERT(VARCHAR(10), b.' + _name + ', 23), '''')' ELSE 'COALESCE(a.' + _name + ', '''') + '' == '' + COALESCE(b.' + _name + ', '''')' END + ' END, ' FROM #_fieldInfo FOR XML PATH ('') ) f(x) ),
	equals AS ( SELECT f.x FROM ( SELECT 'COALESCE(a.' + _name + ', '''') = COALESCE(b.' + _name + ', '''') AND ' FROM #_fieldInfo FOR XML PATH ('') ) f(x) )
	SELECT
		@creates = LEFT(c.x, LEN(c.x)-1),
		@selects = LEFT(s.x, LEN(s.x)-1),
		@formats = LEFT(f.x, LEN(f.x)-1),
		@compares = REPLACE(LEFT(p.x, LEN(p.x)-1), '==', '=>'),
		@equals = LEFT(e.x, LEN(e.x)-4)
	FROM creates c
	CROSS JOIN selects s
	CROSS JOIN formats f
	CROSS JOIN compares p
	CROSS JOIN equals e;

	-- Set up the diff results table.
	SET @script = N'IF OBJECT_ID(@diffTable) IS NOT NULL DROP TABLE ' + @diffTable + '; CREATE TABLE ' + @diffTable;
	SET @script = @script + ' (OperationType VARCHAR(8), AssociateId VARCHAR(32), AssociateName VARCHAR(256),';
	IF @reportType = 'L'
		SET @script = @script + ' Field VARCHAR(256), FromValue VARCHAR(512), ToValue VARCHAR(512))';
	ELSE
		SET @script = @script + ' ' + @creates + ')';
	EXEC sp_executesql @script, N'@diffTable VARCHAR(256)', @diffTable = @diffTable;

	-- Make meta script for new and deleted associates.
	SET @metascript = N'WITH diffAssociates AS (
		SELECT AssociateId FROM HumanResources.ADPExportVersionDetail WHERE PrimaryPosition = ''Yes'' AND ADPExportVersionId = {VersionId1}
		EXCEPT
		SELECT AssociateId FROM HumanResources.ADPExportVersionDetail WHERE PrimaryPosition = ''Yes'' AND ADPExportVersionId = {VersionId2}
	)
	INSERT INTO ' + @diffTable + ' SELECT ''{OperationType}'', d.AssociateId, CONCAT(FirstName, '' '', LastName),';
	IF @reportType = 'L'
		SET @metascript = @metascript + ' ''All'', NULL, NULL';
	ELSE
		SET @metascript = @metascript + ' ' + @formats;
	SET @metascript = @metascript + N' FROM diffAssociates d JOIN HumanResources.ADPExportVersionDetail x ON d.AssociateId = x.AssociateId WHERE x.ADPExportVersionId = {VersionId1} ORDER BY 3';

	-- Get deleted associates.
	SET @script = REPLACE(@metascript, '{VersionId1}', @fromVersionId);
	SET @script = REPLACE(@script, '{VersionId2}', @toVersionId);
	SET @script = REPLACE(@script, '{OperationType}', 'Delete');
	EXEC sp_executesql @script;

	-- Get new associates.
	SET @script = REPLACE(@metascript, '{VersionId1}', @toVersionId);
	SET @script = REPLACE(@script, '{VersionId2}', @fromVersionId);
	SET @script = REPLACE(@script, '{OperationType}', 'Insert');
	EXEC sp_executesql @script;

	-- Updated associates.
	IF @reportType = 'L'
	BEGIN
		-- Make meta script for updated fields.
		SET @metascript = N'
			SELECT @script = N'''' + REPLACE(LEFT(f.x, LEN(f.x)-1), ''@'', @eol)
			FROM (SELECT 1 a) x
			CROSS APPLY (
				SELECT
					''INSERT INTO ' + @diffTable + '@''
					+ ''SELECT DISTINCT ''''Update'''', a.{AssociateId}, CONCAT(a.FirstName, '''' '''', a.LastName), '''''' + _name + '''''', '
						+ @formatPrefix + 'b.'' + _name + ''' + @formatSuffix + ', '
						+ @formatPrefix + 'a.'' + _name + ''' + @formatSuffix + ''' + ''@''
					+ ''FROM {SchemaName}.{TableName} a'' + ''@''
					+ ''LEFT JOIN {SchemaName}.{TableName} b ON a.AssociateId = b.AssociateId'' + ''@''
					+ ''WHERE a.PrimaryPosition = ''''Yes'''' AND a.ADPExportVersionId = ' + @toVersionId + ''' + ''@''
					+ ''AND b.PrimaryPosition = ''''Yes'''' AND b.ADPExportVersionId = ' + @fromVersionId + ''' + ''@''
					+ ''AND COALESCE(a.'' + _name + '', '''''''') != COALESCE(b.'' + _name + '', '''''''') @''
					+ ''ORDER BY 3 @''
				FROM #_fieldInfo
				WHERE _type LIKE ''' + @datatypeField + '''
				FOR XML PATH ('''')
			) f (x);
		';

		-- Foreach data type we care about: run metascript to get the actual script, and then run the actual script.
		-- Datetime fields.
		SET @script = REPLACE(@metascript, '{SchemaName}', @schemaName);
		SET @script = REPLACE(@script, '{TableName}', @tableName);
		SET @script = REPLACE(@script, '{AssociateId}', @associateIdField);
		SET @script = REPLACE(@script, '{PositionId}', @positionIdField);
		SET @script = REPLACE(@script, @datatypeField, 'datetime');
		SET @script = REPLACE(@script, @formatPrefix, 'CONVERT(VARCHAR(10), ');
		SET @script = REPLACE(@script, @formatSuffix, ', 23)');
		EXEC sp_executesql @script, N'@eol CHAR(1), @script NVARCHAR(MAX) OUTPUT', @eol = @eol, @script = @script OUTPUT;
		IF @script IS NOT NULL EXEC sp_executesql @script;

		-- Int fields.
		SET @script = REPLACE(@metascript, '{SchemaName}', @schemaName);
		SET @script = REPLACE(@script, '{TableName}', @tableName);
		SET @script = REPLACE(@script, '{AssociateId}', @associateIdField);
		SET @script = REPLACE(@script, '{PositionId}', @positionIdField);
		SET @script = REPLACE(@script, @datatypeField, 'int');
		SET @script = REPLACE(@script, @formatPrefix, 'CONVERT(VARCHAR(10), ');
		SET @script = REPLACE(@script, @formatSuffix, ')');
		EXEC sp_executesql @script, N'@eol CHAR(1), @script NVARCHAR(MAX) OUTPUT', @eol = @eol, @script = @script OUTPUT;
		IF @script IS NOT NULL EXEC sp_executesql @script;

		-- Char fields.
		SET @script = REPLACE(@metascript, '{SchemaName}', @schemaName);
		SET @script = REPLACE(@script, '{TableName}', @tableName);
		SET @script = REPLACE(@script, '{AssociateId}', @associateIdField);
		SET @script = REPLACE(@script, '{PositionId}', @positionIdField);
		SET @script = REPLACE(@script, @datatypeField, '%char%');
		EXEC sp_executesql @script, N'@eol CHAR(1), @script NVARCHAR(MAX) OUTPUT', @eol = @eol, @script = @script OUTPUT;
		IF @script IS NOT NULL EXEC sp_executesql @script;
	END
	ELSE
	BEGIN
		SET @script = '
			WITH fromValues AS (
				SELECT *
				FROM HumanResources.ADPExportVersionDetail
				WHERE PrimaryPosition = ''Yes''
				AND ADPExportVersionId = ' + CONVERT(VARCHAR(8), @fromVersionId) + '
			),
			toValues AS (
				SELECT *
				FROM HumanResources.ADPExportVersionDetail
				WHERE PrimaryPosition = ''Yes''
				AND ADPExportVersionId = ' + CONVERT(VARCHAR(8), @toVersionId) + '
			),
			combined AS (
				SELECT DISTINCT AssociateId FROM fromValues
				UNION
				SELECT DISTINCT AssociateId FROM toValues
			)
			INSERT INTO ' + @diffTable + ' ( OperationType, AssociateId, AssociateName, ' + @selects + ' )
			SELECT ''Update'', x.AssociateId, CONCAT(b.FirstName, '' '', b.LastName), ' + @compares + '
			FROM combined x
			LEFT JOIN fromValues a ON x.AssociateId = a.AssociateId
			LEFT JOIN toValues b ON x.AssociateId = b.AssociateId
			LEFT JOIN ' + @diffTable + ' d ON x.AssociateId = d.AssociateId
			WHERE d.AssociateId IS NULL AND NOT ( ' + @equals + ' )
			ORDER BY 3
		';
		EXEC sp_executesql @script;

	END

	-- Select result set.
	SELECT * FROM ##_diffResults;

	-- Clean up
	DROP TABLE ##_diffResults;
	DROP TABLE #_fieldInfo;
END TRY
BEGIN CATCH
	-- Log the error message.
	SET @status_code = ERROR_NUMBER() + 50000;
	SET @status_message = ERROR_MESSAGE();

	-- Throw error.
	THROW @status_code, @status_message, 1;
END CATCH;

END
GO

GRANT EXECUTE ON HumanResources.DiffADPExport TO cloveretl
GO

