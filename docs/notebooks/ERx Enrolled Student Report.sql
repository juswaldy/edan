/*
https://twu.my.salesforce.com/00O4v0000089QED

Filter Logic:(1 AND 2 AND 3 AND 4 AND (5 OR 6)) AND 7
1. School (Program Catalog) Display Name equals "Undergraduate"
2. Admissions Status equals "Enrolled"
3. Level of Study for Reporting contains "Traditional"
4. Term (HEDA) equals "Fall 2019,Fall 2018,Fall 2017,Fall 2016,Fall 2015,Fall 2014"
5. Entrance Type equals "New"
6. IB Diploma equals "Yes"
7. Region not equal to "School of Education Post Degree"
*/

SELECT
    c.EnrollmentrxRx__SIS_ID__c,
    t.Name TermHEDA,
    a.Display_Name__c SchoolProgramCatalogDisplayName,
    o.EnrollmentrxRx__Admissions_Status__c,
    NULL LevelOfStudyForReporting,
    o.Entrance_Type__c,
    o.IB_Diploma__c,
    r.Name Region
FROM EnrollmentrxRx__Enrollment_Opportunity__c o
JOIN Contact c ON o.EnrollmentrxRx__Applicant__c = c.Id
JOIN Account a ON o.Level_of_Study__c = a.Id
JOIN hed__Term__c t ON o.Term_HEDA__c = t.Id
JOIN TWURegion__c r ON o.Region__c = r.Id
JOIN Account prog ON o.Program_Of_Interest_HEDA__c = prog.Id
WHERE (
    a.Display_Name__c = 'Undergraduate' -- 1
    AND o.EnrollmentrxRx__Admissions_Status__c = 'Enrolled' -- 2
	-- 3 See below
    AND t.Name LIKE 'Fall 201[4-9]' -- 4
    AND (
        o.Entrance_Type__c = 'New' -- 5
        OR
        o.IB_Diploma__c = 'Yes' -- 6
    )
)
AND r.Name != 'School of Education Post Degree'; -- 7

/*
Level of Study for Reporting:

CASE(
	Level_of_Study__r.Name,
	"TLC", "TLC",
	"ESLI", "ESLI",
	"Graduate", "Graduate",
	"ACTS", "ACTS",
	"Extension", 
		IF(
			Program_Of_Interest_HEDA__r.Name = "Partners",
			"Partner Programs",
			"Expired Extension"
		),
	"Undergraduate", 
		IF(
			OR(
				AND(
					TEXT(Entrance_Type__c) != "Current",
					OR(
						ISBLANK(Program_Of_Interest_HEDA__c),
						AND(
							NOT(CONTAINS(Program_Of_Interest_HEDA__r.Name, "*Leadership")),
							NOT(CONTAINS(Program_Of_Interest_HEDA__r.Name, "Certificate")),
							NOT(CONTAINS(Program_Of_Interest_HEDA__r.Name, "Degree Completion")),
							Program_Of_Interest_HEDA__r.Name <> "MBA International Bridge Program",
							Program_Of_Interest_HEDA__r.Name <> "Partners"
						)
					),
					Program_Of_Interest_HEDA__r.Name <> "Partners"
				),
				ISPICKVAL(Current_Student_Status__c, "Visiting Student")
			),
			"Traditional Undergraduate",
			IF(
				Program_Of_Interest_HEDA__r.Name = "Partners",
				"Partner Programs",
				IF(
					OR(
						CONTAINS(Program_Of_Interest_HEDA__r.Name, "Degree Completion"),
						Program_Of_Interest_HEDA__r.Name = "MBA International Bridge Program",
						CONTAINS(Program_Of_Interest_HEDA__r.Name, "*Leadership"),
						CONTAINS(Program_Of_Interest_HEDA__r.Name, "Certificate"),
						AND(	
							OR(
								Entrance_Term_Name__c = "Fall 2019",
								VALUE(RIGHT(Entrance_Term_Name__c, 4)) > 2019
							)
						),
						Stream_Account_LU__r.Name = "UG International Degree Completion"
					),
					"Flexible Undergraduate",
					IF(
						TEXT(Entrance_Type__c) = "Current",
						"Current Undergraduate",
						null
					)
				)
			)
		),
	null
)
*/