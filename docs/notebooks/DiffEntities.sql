USE JUS_Hub
GO

-- Drop stored procedure if it already exists.
IF EXISTS (
    SELECT  *
    FROM    INFORMATION_SCHEMA.ROUTINES
    WHERE   SPECIFIC_SCHEMA = N'Staging'
    AND     SPECIFIC_NAME = N'DiffEntities'
)
    DROP PROCEDURE Staging.DiffEntities
GO

-- =============================================================================
-- Author:       Juswaldy Jusman
-- Create date:  2020-03-22
-- Description:  Create a Staging vs ODS diff of asset records for the given
--               domain, source, and key.
-- Dependencies: TABLE - JUS_Hub.Staging.<Source><Domain>
--               TABLE - JUS_ODS.<Domain>.<Source>
-- Returns: Tables containing the diffs.
-- =============================================================================
CREATE PROCEDURE Staging.DiffEntities
	@domain VARCHAR(256) = 'Assets',
	@source VARCHAR(256) = 'Telus',
	@key VARCHAR(MAX) = 'CONCAT(SubscriberName, PhoneNumber, Status)'
AS
BEGIN

DECLARE
	@status_code INT = 0,
	@status_message VARCHAR(MAX) = NULL;

DECLARE
	@metascript NVARCHAR(MAX) = NULL,
	@script NVARCHAR(MAX) = NULL,
	@eol CHAR(1) = CHAR(13),
	@tab CHAR(1) = CHAR(9),
	@idx INT = NULL,
	@fieldInfoTable VARCHAR(256) = '#_fieldInfo',
	@diffTable VARCHAR(256) = NULL,
	@diffField VARCHAR(256) = 'tempdb..#_diffField',
	@diffRow VARCHAR(256) = 'tempdb..#_diffRow',
	@datatypeField VARCHAR(16) = '--{DataType}--',
	@formatPrefix VARCHAR(16) = '/*FormatPrefix*/',
	@formatSuffix VARCHAR(16) = '/*FormatSuffix*/';

BEGIN TRY

	-- Set up diff fields table.
	CREATE TABLE #_fieldInfo (_name VARCHAR(256), _type VARCHAR(128));
	INSERT INTO #_fieldInfo
	SELECT
		COLUMN_NAME _name,
		CONCAT(
			UPPER(DATA_TYPE),
			CASE DATA_TYPE
			WHEN 'sql_variant' THEN ''
			WHEN 'text' THEN ''
			WHEN 'ntext' THEN ''
			WHEN 'xml' THEN ''
			WHEN 'decimal' THEN '(' + CAST(NUMERIC_PRECISION AS VARCHAR) + ', ' + CAST(NUMERIC_SCALE AS VARCHAR) + ')'
			WHEN 'numeric' THEN '(' + CAST(NUMERIC_PRECISION AS VARCHAR) + ', ' + CAST(NUMERIC_SCALE AS VARCHAR) + ')'
			ELSE COALESCE(
				CONCAT(
					'(',
					CASE
					WHEN CHARACTER_MAXIMUM_LENGTH = -1 THEN 'MAX'
					ELSE CAST(CHARACTER_MAXIMUM_LENGTH AS VARCHAR)
					END,
					')'
				),
				''
			)
			END
		) _type
	FROM JUS_ODS.INFORMATION_SCHEMA.COLUMNS
	WHERE TABLE_SCHEMA = @domain AND TABLE_NAME = @source;

	-- Only continue if field info was found.
	IF EXISTS (SELECT top 1 * FROM #_fieldInfo)
	BEGIN
		-- Prepare field creates, selects, formats, comparisons, and equal filters.
		DECLARE
			@creates NVARCHAR(MAX),
			@selects NVARCHAR(MAX),
			@formats NVARCHAR(MAX),
			@compares NVARCHAR(MAX),
			@equals NVARCHAR(MAX);
		WITH
		creates AS ( SELECT f.x FROM ( SELECT _name + ' VARCHAR(256), ' FROM #_fieldInfo FOR XML PATH ('') ) f(x) ),
		selects AS ( SELECT f.x FROM ( SELECT _name + ', ' FROM #_fieldInfo FOR XML PATH ('') ) f(x) ),
		formats AS ( SELECT f.x FROM ( SELECT CASE WHEN _type = 'DATETIME' THEN 'CONVERT(VARCHAR(10), ' + _name + ', 23)' ELSE _name END + ', ' FROM #_fieldInfo FOR XML PATH ('') ) f(x) ),
		compares AS ( SELECT f.x FROM ( SELECT 'CASE WHEN COALESCE(a.' + _name + ', '''') = COALESCE(b.' + _name + ', '''') THEN '''' ELSE ' + CASE WHEN _type = 'DATETIME' THEN 'COALESCE(CONVERT(VARCHAR(10), a.' + _name + ', 23), '''') + '' == '' + COALESCE(CONVERT(VARCHAR(10), b.' + _name + ', 23), '''')' ELSE 'COALESCE(a.' + _name + ', '''') + '' == '' + COALESCE(b.' + _name + ', '''')' END + ' END, ' FROM #_fieldInfo FOR XML PATH ('') ) f(x) ),
		equals AS ( SELECT f.x FROM ( SELECT 'COALESCE(a.' + _name + ', '''') = COALESCE(b.' + _name + ', '''') AND ' FROM #_fieldInfo FOR XML PATH ('') ) f(x) )
		SELECT
			@creates = LEFT(c.x, LEN(c.x)-1),
			@selects = LEFT(s.x, LEN(s.x)-1),
			@formats = LEFT(f.x, LEN(f.x)-1),
			@compares = REPLACE(LEFT(p.x, LEN(p.x)-1), '==', '=>'),
			@equals = LEFT(e.x, LEN(e.x)-4)
		FROM creates c
		CROSS JOIN selects s
		CROSS JOIN formats f
		CROSS JOIN compares p
		CROSS JOIN equals e;

SELECT * FROM #_fieldInfo;
SELECT @creates creates, @selects selects, @formats formats, @compares compares, @equals equals;

		-- Set up the diff results tables.
		SET @script = N'CREATE TABLE ' + @diffField;
		--SET @script = N'IF OBJECT_ID(@diffField) IS NOT NULL DROP TABLE ' + @diffField + '; CREATE TABLE ' + @diffField;
		SET @script = @script + ' (OperationType VARCHAR(8), Id VARCHAR(MAX),';
		SET @script = @script + ' Field VARCHAR(256), FromValue VARCHAR(512), ToValue VARCHAR(512));';
		SET @script = @script + 'IF OBJECT_ID(@diffRow) IS NOT NULL DROP TABLE ' + @diffRow + '; CREATE TABLE ' + @diffRow;
		SET @script = @script + ' (OperationType VARCHAR(8), Id VARCHAR(MAX),';
		SET @script = @script + ' ' + @creates + ')';
SELECT @script;
		EXEC sp_executesql @script, N'@diffField VARCHAR(256), @diffRow VARCHAR(256)', @diffField = @diffField, @diffRow = @diffRow;

		--------------------------------------------------------------------------------
		-- Make meta script for new and deleted entities.
		DECLARE @fromODS VARCHAR(256) = CONCAT('JUS_ODS.', @domain, '.', @source);
		DECLARE @fromSource VARCHAR(256) = CONCAT('JUS_Hub.Staging.', @source, @domain);

		SET @metascript = N'WITH diffEntities AS (
			SELECT {Key} Id FROM {Version1}
			EXCEPT
			SELECT {Key} Id FROM {Version2}
		)
		INSERT INTO ' + @diffField + ' SELECT ''{OperationType}'', Id,';
		SET @metascript = @metascript + ' ''All'', NULL FROM diffEntities;';
		SET @metascript = @metascript + 'WITH diffEntities AS (
			SELECT {Key} Id FROM {Version1}
			EXCEPT
			SELECT {Key} Id FROM {Version2}
		)
		INSERT INTO ' + @diffRow + ' SELECT ''{OperationType}'', Id,';
		SET @metascript = @metascript + @formats + N' FROM diffEntities d JOIN {Version1} x ON d.Id = CONCAT(x.SubscriberName, x.PhoneNumber, x.Status);';
		SET @metascript = REPLACE(@metascript, '{Key}', @key);

		-- Get deleted associates.
		SET @script = REPLACE(@metascript, '{Version1}', @fromODS);
		SET @script = REPLACE(@script, '{Version2}', @fromSource);
		SET @script = REPLACE(@script, '{OperationType}', 'Delete');
SELECT @script;
		EXEC sp_executesql @script;

		-- Get new associates.
		SET @script = REPLACE(@metascript, '{Version1}', @fromSource);
		SET @script = REPLACE(@script, '{Version2}', @fromODS);
		SET @script = REPLACE(@script, '{OperationType}', 'Insert');
SELECT @script;
		EXEC sp_executesql @script;

		--------------------------------------------------------------------------------
		-- Clean up
		SET @diffTable = @diffField;
		SET @script = N'IF OBJECT_ID(@diffTable) IS NOT NULL DROP TABLE ' + @diffTable + ';';
		EXEC sp_executesql @script, N'@diffTable VARCHAR(256)', @diffTable = @diffField;
		SET @diffTable = @diffRow;
		SET @script = N'IF OBJECT_ID(@diffTable) IS NOT NULL DROP TABLE ' + @diffTable + ';';
		EXEC sp_executesql @script, N'@diffTable VARCHAR(256)', @diffTable = @diffRow;
	END

	-- Clean up
	DROP TABLE #_fieldInfo;

END TRY
BEGIN CATCH
	-- Log the error message.
	SET @status_code = ERROR_NUMBER() + 50000;
	SET @status_message = ERROR_MESSAGE();

	-- Throw error.
	THROW @status_code, @status_message, 1;
END CATCH;

END
GO

GRANT EXECUTE ON Staging.DiffEntities TO cloveretl
GO

