DECLARE @frbranches TABLE (
    branch VARCHAR(256),
    dbobject VARCHAR(256)
);

INSERT INTO @frbranches (branch, dbobject) VALUES
('CollectBalancesToTables', 'FinancialReporting.sln'),
('CollectBalancesToTables', 'FinancialReportingCollectBalances/FRCollectBiographMasterBalances.dtsx'),
('CollectBalancesToTables', 'FinancialReportingCollectBalances/FRMasterCollectBalances.dtsx'),
('CollectBalancesToTables', 'FinancialReportingCollectBalances/FinancialReporting.conmgr'),
('CollectBalancesToTables', 'FinancialReportingCollectBalances/FinancialReportingCollectBalances.database'),
('CollectBalancesToTables', 'FinancialReportingCollectBalances/FinancialReportingCollectBalances.dtproj'),
('CollectBalancesToTables', 'FinancialReportingCollectBalances/JenzabarEX.conmgr'),
('CollectBalancesToTables', 'FinancialReportingCollectBalances/Project.params');

INSERT INTO @frbranches (branch, dbobject) VALUES
('viewRename', 'FinancialReporting.sqlproj'),
('viewRename', 'STAGING/Scripts/LOAD_VENA_DETAIL_StudentGroupFinancialAid.sql'),
('viewRename', 'STAGING/Scripts/LOAD_VENA_LID_StudentGroupCourseEnrolment.sql'),
('viewRename', 'STAGING/Views/COURSE_HIERARCHIES.sql'),
('viewRename', 'STAGING/Views/COURSE_HIERARCHIES_V.sql'),
('viewRename', 'STAGING/Views/COURSE_SECTION_HIERARCHY_With_Periods.sql'),
('viewRename', 'STAGING/Views/COURSE_SECTION_HIERARCHY_With_Periods_V.sql'),
('viewRename', 'STAGING/Views/GL_HIERARCHY_WITH_ATTRIBUTES.sql'),
('viewRename', 'STAGING/Views/GL_HIERARCHY_WITH_ATTRIBUTES_V.sql'),
('viewRename', 'STAGING/Views/GL_TRANSACTIONS_FOR_VENA_LOAD.sql'),
('viewRename', 'STAGING/Views/GL_TRANSACTIONS_FOR_VENA_LOAD_V.sql'),
('viewRename', 'STAGING/Views/STUDENT_GRP_HIERARCHY_Code2.sql'),
('viewRename', 'STAGING/Views/STUDENT_GRP_HIERARCHY_Code2_V.sql'),
('viewRename', 'STAGING/Views/STUDENT_GRP_HIERARCHY_Code.sql'),
('viewRename', 'STAGING/Views/STUDENT_GRP_HIERARCHY_Code_V.sql'),
('viewRename', 'STAGING/Views/TRANS_HIST_STUDENT_CHARGES.sql'),
('viewRename', 'STAGING/Views/TRANS_HIST_STUDENT_CHARGES_V.sql');

INSERT INTO @frbranches (branch, dbobject) VALUES
('awmRevisions', 'INPUT/Tables/PERIODS_BY_DAY.sql'),
('awmRevisions', 'SOURCE_JZ/Tables/FIN_SRVS_FLAGS_TWU.sql'),
('awmRevisions', 'SOURCE_JZ/Tables/SECTION_MASTER.sql'),
('awmRevisions', 'SOURCE_JZ/Tables/STUDENT_CRS_HIST.sql'),
('awmRevisions', 'SOURCE_JZ/Tables/TRANS_HIST.sql'),
('awmRevisions', 'STAGING/Tables/GL_TRIAL_BALANCE_BY_MONTH.sql'),
('awmRevisions', 'STAGING/Views/COURSE_HIERARCHIES.sql');

INSERT INTO @frbranches (branch, dbobject) VALUES
('dev', 'STAGING/Stored Procedures/LOAD_SOURCE_TABLES.SQL');

INSERT INTO @frbranches (branch, dbobject) VALUES
('v3', 'META/Scripts/Initialize_DATACUBE.sql'),
('v3', 'META/Scripts/Initialize_DIMENSION.sql'),
('v3', 'META/Tables/DATACUBE.sql'),
('v3', 'META/Tables/DIMENSION.sql'),
('v3', 'notebooks/Adding DATAMODEL_ID to META tables.ipynb'),
('v3', 'notebooks/FinancialReporting Current State.ipynb'),
('v3', 'notebooks/FinancialReporting.md'),
('v3', 'notebooks/Investigating FR Errors.ipynb'),
('v3', 'notebooks/Investigating Source Load failure.ipynb');

INSERT INTO @frbranches (branch, dbobject) VALUES
('unposted', 'META/Scripts/GENERATE_DETAIL_TRIAL_BALANCE_CMP5_UNPOSTED.sql'),
('unposted', 'META/Scripts/GENERATE_INTERSECTION_TRIAL_BALANCE_CMP4.sql'),
('unposted', 'META/Scripts/GENERATE_INTERSECTION_TRIAL_BALANCE_CMP5.sql'),
('unposted', 'META/Scripts/GENERATE_INTERSECTION_TRIAL_BALANCE_CMP5_UNPOSTED.sql'),
('unposted', 'META/Scripts/GENERATE_LID_TRIAL_BALANCE_CMP4.sql'),
('unposted', 'META/Scripts/Initialize_DATACUBE.sql'),
('unposted', 'META/Scripts/Initialize_DATACUBE_OUTPUT.sql'),
('unposted', 'META/Scripts/Initialize_DATACUBE_VENA_PARAMETER.sql'),
('unposted', 'META/Scripts/Initialize_SOURCE_TABLE.sql'),
('unposted', 'PRESENTATION/Tables/DETAIL_TRIAL_BALANCE_CMP5_UNPOSTED.sql'),
('unposted', 'PRESENTATION/Tables/INTERSECTION_TRIAL_BALANCE_CMP5_UNPOSTED.sql'),
('unposted', 'STAGING/Scripts/PREPARE_STAGING_GL_TRIAL_BALANCE_BY_MONTH.sql'),
('unposted', 'STAGING/Stored Procedures/LOAD_SOURCE_TABLES.sql'),
('unposted', 'STAGING/Stored Procedures/MAKE_CREATE_TABLE_SQL.sql'),
('unposted', 'STAGING/Tables/GL_TRIAL_BALANCE_BY_MONTH.sql'),
('unposted', 'notebooks/Adding Component 5 Unposted Load.ipynb'),
('unposted', 'notebooks/Freeing up some space on FinancialReporting.ipynb');

WITH c AS (SELECT DISTINCT dbobject FROM @frbranches WHERE branch = 'CollectBalancesToTables'),
r AS (SELECT DISTINCT dbobject FROM @frbranches WHERE branch = 'viewRename'),
a AS (SELECT DISTINCT dbobject FROM @frbranches WHERE branch = 'awmRevisions'),
d AS (SELECT DISTINCT dbobject FROM @frbranches WHERE branch = 'dev'),
v AS (SELECT DISTINCT dbobject FROM @frbranches WHERE branch = 'v3'),
u AS (SELECT DISTINCT dbobject FROM @frbranches WHERE branch = 'unposted'),
all_branches (dbobject) AS (SELECT dbobject FROM c UNION SELECT dbobject FROM r UNION SELECT dbobject FROM a UNION SELECT dbobject FROM d UNION SELECT dbobject FROM v UNION SELECT dbobject FROM u)
SELECT
    x.dbobject,
    CASE WHEN c.dbobject IS NULL THEN 0 ELSE 1 END AS CollectBalancesToTables,
    CASE WHEN r.dbobject IS NULL THEN 0 ELSE 1 END AS viewRename,
    CASE WHEN a.dbobject IS NULL THEN 0 ELSE 1 END AS awmRevisions,
    CASE WHEN d.dbobject IS NULL THEN 0 ELSE 1 END AS dev,
    CASE WHEN v.dbobject IS NULL THEN 0 ELSE 1 END AS v3,
    CASE WHEN u.dbobject IS NULL THEN 0 ELSE 1 END AS unposted
FROM all_branches x
LEFT JOIN c ON x.dbobject = c.dbobject
LEFT JOIN r ON x.dbobject = r.dbobject
LEFT JOIN a ON x.dbobject = a.dbobject
LEFT JOIN d ON x.dbobject = d.dbobject
LEFT JOIN v ON x.dbobject = v.dbobject
LEFT JOIN u ON x.dbobject = u.dbobject
