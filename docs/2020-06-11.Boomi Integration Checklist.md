# Boomi Integration Checklist



1. **The People**. As with planning any project, your an integration project starts with the people involved. Be sure to account for the owners and users of the applications and data. Find out how their insights and perspectives shape the business needs and technical requirements for the project.
2. **The Processes**. Processes are about how the data flows from one endpoint to another. The endpoints can be applications but could also be various stops along the way for staging, vetting, or modifying the data before it moves on to other applications. Map out how your data flows across the organization.
3. **The Apps**. Applications are at the heart of any integration project. This is where work gets done. And an integration project is no different. Take your time to understand the applications being integrated, their needs, and how they interact with each other.
4. **The Data**. Ultimately, it’s all about the data. This is the information that people need. Applications exist to provide data in the most useful way. To do their job, applications need the right data at the right place and at the right time.
5. **Salesforce Example**. While Salesforce integrations share common issues with other data and application integrations projects, they also have plenty of specific requirements and support needs, particular for cloud integration. Assess these requirements to determine whether you have in-house expertise or may benefit from the help of an experienced systems integrator. Your requirements may change based on your specific Salesforce instances, the level of customization in place or required, and your licensing model. It’s important to also understand how these things will impact your integration project.






## 1 The People



#### Identify who owns and uses the applications and the data involved
- How might their usage of the applications and data have implications for how you plan the integrations?
- What limitations are they running into with their applications?
- How might your project change the way they use their applications and data?



#### Identify who is responsible for managing the data
- Are they different than the owners? If so, why?



#### Understand how you need to control access to the relevant applications and their data
- Who can change the data?
- Who can only view the data?
- Talk to all stakeholders to understand their business goals



#### What are all the use cases relevant to your integration project? How do they relate?
- Do all stakeholders agree on how your integrations should be designed and managed? If there are differing opinions, what are they?



#### Understand your organization’s integration standards and best practices
- Who manages these programs?





## 2 The Processes



#### Identify the endpoints
- Where does the data live?
- Where does the data need to go?



#### Map what needs to happen to the data as it travels from one endpoint to another
- What are the key touchpoints or stops it will need to make?
- Does the data travel point-to-point or is it shared?
- In which direction is the flow of data?
- Will it need some sort of validation, access approval, etc.?
- Will the data need to be enriched with data from other sources to make it more useful?
- When and how is the data reconciled?



#### Determine the volume, frequency, and performance of data delivery required by the various applications
- Will you be moving a lot of data? If so, when will this need to happen?
- Do you have seasonal or periodic spikes in data volumes?
- What are the data performance requirements?
- How frequently does the data need to be updated? Real-time/near real-time or batch updates?



#### Establish what should happen if there are errors or problems during the integration
- Who should be notified for each type of error (data quality, network performance, etc.)



#### Account for the security, risk and compliance needs for the data as it travels inside and outside the organization
- Does the data need to be PCI-compliant, HIPAA-compliant, or meet other standards?





## 3 The Apps



#### Identify the applications involved
- Where do your applications live?
- How do they need to be connected?



#### Understand how each application might be dependent on the functions of other applications
- When and how does one application need updates from other applications?
- Which application is the process driver?
- Are the applications on-premises or cloud- based? How will these different kinds of applications need to interact?



#### Identify which applications are centralized and which are distributed
- Do you have different sets of applications performing the same functions across the organization (varying by regions or departments)?
- Will you need to deal with variables among the same types of data, such as different currencies or regional pricing?



#### Define the access controls you will need for each application and its data



#### Determine what rules you will need to guide how updates happen among all the applications
- Are there any non-permitted actions?
- Who is in charge of the updates?





## 4 The Data



#### Determine all the data that is associated with each application
- Is this data shared or similar to the data for other applications? If so, how?
- For each type of data set, which application is the system of record?
- How is the data currently reconciled?
- Who owns the maintenance of these data sets?
- Where do you run risks of duplicate or conflicting data?



#### Assess data quality
- How clean and consistent is the data?
- Will you need to consolidate or reconcile data before integrating?
- Will some data require a one-time migration or ongoing synchronization?



#### Determine the structure of the data for each application
- Is there a hierarchy or other structure to how the data interrelates?
- What are the fields, primary keys, data types, and validation rules?
- Is there a hierarchy or dependencies between different records?
- How does data structure and identification vary among the applications that will share data?
- Are the data fields and data labels consistent and appropriate for all application interactions?





## 5 Salesforce Example



#### Determine all the data that is associated with each application
- Is this data shared or similar to the data for other applications? If so, how?
- For each type of data set, which application is the system of record?
- How is the data currently reconciled?
- Who owns the maintenance of these data sets?
- Where do you run risks of duplicate or conflicting data?



#### Take your company’s cloud strategy into account
- Does your company have a cloud strategy? If so, what is it?
- How will your project need to fit into that strategy?
- Who is responsible for ensuring cloud management best practices?
- How does your organization monitor cloud functions?



#### Determine your Salesforce expertise
- What staff, tools, systems, and processes are already in place for structuring your Salesforce project?
- Do you have the in-house staff to make best use of Salesforce Connect and other Salesforce technologies?
- What external resources or consultants might you need to carry out your project?



#### Understand the specifics of your Salesforce edition
- What optional features or fields have you enabled in your Salesforce account?
- Have you added custom fields to objects to help maintain “state” information and help identify external applications accessing Salesforce?



#### Understand the customizations your organization has implemented in Salesforce
- Are you using custom fields and record types, validations and required fields, or scripting and workflows?
- How might these need to be accounted for in the integration plan and design?
- Do you need to view external data without replicating it in Salesforce?
- Do you have very large amounts of data to import or export with the Salesforce Bulk API?
- Do you have specific, quick integration tasks (such as near real-time order status updates) that could benefit from custom Salesforce Apex call invoking published APIs?
- Do you have event-based use cases that could benefit from Salesforce Outbound Events or Platform Events?



#### Understand the integration project’s licensing implications for your Salesforce account
- Will you use a separate, dedicated Salesforce user for the integrations?
- Will you need additional Salesforce licenses to support concurrent connections for high volume throughput and parallel processing?

