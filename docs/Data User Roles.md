# Data User Roles



Consumer

- Requires timely, trusted data
- Expects clean, consistent, accurate data
- Leverages data for decision support

Contributor

- Authors data, prefers it centralized
- Suffers with spreadsheets
- Collaborates, but keeps ownership

Steward

- Enforces quality: consistency, completeness, compliance
- Needs metrics for process and quality
- Requires lineage, audit, traceability

Champion

- Treats data as an asset
- Understands business context
- Performs data analysis to determine relationships and opportunities, support decisions



## Skills: (Tools + Features) x Need

On the scale of 1 to10.

| Tools                  | Consumer | Contributor | Steward | Champion |
| ---------------------- | :------: | :---------: | :-----: | :------: |
| SandDance              |    10    |     10      |   10    |    10    |
| Tableau                |    8     |      8      |   10    |    10    |
| OpenRefine             |    5     |      8      |   10    |    10    |
| Azure Data Studio      |          |      3      |    5    |    10    |
| SQL                    |          |      2      |    5    |    8     |
| pandas                 |          |             |    3    |    8     |
| Neo4j                  |          |             |         |    5     |
| d3.js                  |          |             |         |    5     |
| **Features**           |          |             |         |          |
| Read data profile      |    2     |      5      |   10    |    10    |
| Read data drift        |          |      5      |    8    |    10    |
| Read concept drift     |          |      2      |    5    |    10    |
| Read FMEA              |          |             |    5    |    8     |
| Generate data profile  |          |             |    5    |    8     |
| Generate data drift    |          |             |    3    |    8     |
| Generate concept drift |          |             |    2    |    5     |
| Generate FMEA          |          |             |         |    2     |

\* Failure Modes and Effects Analysis