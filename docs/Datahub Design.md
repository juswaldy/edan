# Datahub Design

# Financial Reporting

## Current State

```mermaid
graph LR
    subgraph Producer
        Jenzabar
        Aqueduct
    end
    subgraph FinancialReporting
        subgraph Hub
            Input
            Source
            Staging
        end
        subgraph Outlet
            Staging2[Staging]
            Presentation
        end
    end
    subgraph Consumer
        Vena
    end
    Input --> Staging
    Input --> Presentation
    Jenzabar --> Input
    Jenzabar --> Source
    Aqueduct --> Source
    Source --> Staging
    Staging --> Staging2
    Staging2 --> Presentation
    Source --> Presentation
    Presentation --> Vena
```

## Future State 1

```mermaid
graph LR
    subgraph Producer
        Jenzabar
        Aqueduct
    end
        subgraph Hub
            Input
            Source
            Staging
        end
        subgraph FinancialReporting
            Input2[Input]
            Staging2[Staging]
            Source2[Source]
            Presentation
        end
    subgraph Consumer
        Vena
    end
    Input --> Input2
    Input --> Staging
    Jenzabar --> Input
    Jenzabar --> Source
    Aqueduct --> Source
    Source --> Staging
    Staging --> Staging2
    Presentation --> Vena
    Staging --> Source2
    Input2 --> Staging2
    Staging2 --> Presentation
    Source2 --> Staging2
```

## Future State 2

```mermaid
graph LR
    subgraph Producer
        Jenzabar
        Aqueduct
    end
    subgraph Hub
        subgraph Incoming
            direction TB
            AreaI1[Area I1]
            AreaI2[Area I2]
        end
        subgraph Staging
            direction TB
            AreaS1[Staging 1]
            AreaS2[Staging 2]
        end
        subgraph Outgoing
            direction TB
            AreaO1[Area O1]
            AreaO2[Area O2]
        end
    end
    subgraph FR
        FinancialReporting
        FAST
    end
    subgraph Consumer
        Vena
        MSAccess
    end
    Jenzabar --> Incoming
    Aqueduct --> Incoming
    Incoming --> Staging
    Incoming --> Outgoing
    Staging --> Outgoing
    Outgoing --> FinancialReporting
    Outgoing --> FAST
    FinancialReporting --> Vena
    FAST --> MSAccess
```

# Systems

## Current State

```mermaid
graph
    subgraph cron1.twu.ca
        Services
        ScheduledTasks
    end
    subgraph prod-aqdb.twu.ca
        Aqueduct
        clover_db
        IntegrationHub
    end
    subgraph jenzabarsrv.twu.ca
        TmsEPrd
        ICS_NET
    end
    subgraph fast.twu.ca
        FinancialReporting
        FASTMCSL
    end
    subgraph cloveretl.twu.ca
        CloverDX
    end
    Aqueduct <--> Services
    TmsEPrd <--> Services
    Aqueduct <--> ScheduledTasks
    TmsEPrd <--> ScheduledTasks
    clover_db <--> CloverDX
    IntegrationHub <--> CloverDX
    Aqueduct  <--> CloverDX
    TmsEPrd <--> CloverDX
    FinancialReporting <--> CloverDX

    classDef red fill:#770000,stroke-width:4px,color:#fff
    classDef green fill:#007700,stroke-width:4px,color:#fff
    classDef blue fill:#000077,stroke-width:4px,color:#fff
    class clover_db,IntegrationHub,CloverDX blue

```

## Future State

```mermaid
graph
    subgraph cron1.twu.ca
        Services
        ScheduledTasks
    end
    subgraph prod-aqdb.twu.ca
        Aqueduct
    end
    subgraph jenzabarsrv.twu.ca
        TmsEPrd
        ICS_NET
    end
    subgraph fast.twu.ca
        FinancialReporting
        FASTMCSL
    end
    subgraph integrationhub.twu.ca
        clover_db
        CloverDX
        IntegrationHub
    end
    Aqueduct <--> Services
    TmsEPrd <--> Services
    Aqueduct <--> ScheduledTasks
    TmsEPrd <--> ScheduledTasks
    clover_db <--> CloverDX
    Aqueduct  <--> CloverDX
    IntegrationHub <--> CloverDX
    TmsEPrd <--> CloverDX
    FinancialReporting <--> CloverDX

    classDef red fill:#770000,stroke-width:4px,color:#fff
    classDef green fill:#007700,stroke-width:4px,color:#fff
    classDef blue fill:#000077,stroke-width:4px,color:#fff
    class clover_db,IntegrationHub,CloverDX blue
```