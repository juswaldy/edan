# DW Short Case Studies 01

(Wed, 17 Jun 2020 - J.Jusman)




## Aggregate: Calculate CRS_ENROLLMENT for SETL

We can't trust Jenzabar calculated fields like CRS_ENROLLMENT, so we have to calculate it ourselves. We just did this recently because we needed this number for SETL. What does the current solution look like, and how will this look in the future?

#### CURRENT

```mermaid
graph LR
	sch(STUDENT_CRS_HIST) -- COUNT Students BY<br/>Year-Term-Course --> Temp(Temp Table) --> SETL(TWU_SETL_COURSES)
```

#### FUTURE

```mermaid
graph LR
	subgraph Jenzabar
		sch(STUDENT_CRS_HIST)
		sm(SECTION_MASTER)
	end
	subgraph DW
		ODS(ODS)
		Period(Period)
		Course(Course)
		Student(Student)
		SCP(COUNT Students BY Period-Course)
	end
	subgraph Outlets
		SETL(SETL)
		Feedback(Feedback)
	end

	sch --> ODS
	ODS --> Student
	ODS --> Course
	ODS --> Period
	
	Student --> SCP
	Course --> SCP
	Period --> SCP
	
	SCP --> SETL
	SCP --> Feedback
	
	Feedback --> sm
	
```

#### Example COUNTs given the 3 dimensions

x = Input dimensions

y = Aggregated dimension

|                                      | Student | Course | Period | Explanation                                                  |
| ------------------------------------ | :-----: | :----: | :----: | ------------------------------------------------------------ |
| Periods BY Student-Course            |    x    |   x    |   y    | How many semesters did a Student take a Course?              |
| Students BY Period-CrosslistedCourse |    y    |   x    |   x    | How many Students were enrolled in a Class? A Class is made up of multiple crosslisted Courses. |
| Students BY Period-SETLGroupedCourse |    y    |   x    |   x    | How many Students were enrolled in a SETL Group of Courses? A SETL Group is made up of multiple Courses with the same Instructor. We need an Instructor dimension for this. |
| Courses BY Student-Period            |    x    |   y    |   x    | How many Courses did a Student take in each semester?        |
| Students BY Course                   |    y    |   x    |        | How many Students took a specific Course?                    |
| etc                                  |         |        |        |                                                              |





## Dependency: Housing Definition of `Is New Student`

We recently changed the definition of `Is New Student` field in the Housing Placement Report `TWU_SP_HOUSING_PLACEMENT_ARB_REPORT2` because the upstream Jenzabar tables for SESSIONS have changed.

#### BEFORE

```mermaid
graph BT
	subgraph Aqueduct
		Candidacy(Admissions.Candidacy)
	end
	subgraph Jenzabar
		Housing(STUD_SESS_ASSIGN)
		Session(SESS_TABLE)
	end
	
	fn(TWU_FN_HOUSING_SESSIONS) --> Housing
    fn --> Session
    fn --> Candidacy

	style Session fill:#cf0000,stroke-width:4px,color:#fff
	style fn fill:#3377cf,stroke-width:4px,color:#fff
```

#### AFTER

```mermaid
graph BT
	subgraph Aqueduct
		Candidacy(Admissions.Candidacy)
	end
	subgraph Jenzabar
		Housing(STUD_SESS_ASSIGN)
		Session(CM_SESSION_MSTR)
        YearTerm(YEAR_TERM_TABLE)
        Join(JOIN)
	end
	
	fn(TWU_FN_HOUSING_SESSIONS) --> Housing
	fn --> Join
    fn --> Candidacy
    Join --> Session
    Join --> YearTerm

	style Session fill:#cf0000,stroke-width:4px,color:#fff
	style YearTerm fill:#cf0000,stroke-width:4px,color:#fff
	style Join fill:#cf0000,stroke-width:4px,color:#fff
	style fn fill:#3377cf,stroke-width:4px,color:#fff
```

#### Downstream

```mermaid
graph BT
	Sources(Sources)
	fn(TWU_FN_HOUSING_SESSIONS) --> Sources
	vw(TWU_JJ_VW_HOUSING_SESSIONS) --> fn
	sp(TWU_SP_HOUSING_PLACEMENT_ARB_REPORT2) --> vw
	IsNewStudent(Is New Student) --> sp
	
	style Sources fill:#cf0000,stroke-width:4px,color:#fff
	style fn fill:#3377cf,stroke-width:4px,color:#fff
	style IsNewStudent fill:#cf0000,stroke-width:4px,color:#fff
```




## Dependency: JICS -> SETL

We updated SETL Project Code functionality and it broke JICS Landing Page for Shari Morehouse. How can we document and codify this knowledge so that when we update SETL Project Code functionality, we can test all the procedures and modules and apps that depend on it? And how can we automate monitoring and alerting for this?

```mermaid
graph LR
	Landing(Landing Page) --> Perm(Permissions and Roles)
	Landing --> Personal(Personal Settings)
	Landing --> JICS(JICS App)
	Personal --> JICS
	JICS --> Dropdown(Dropdown)
	Dropdown --> SETL(SETL Views)
	style Landing fill:#cf0000,stroke-width:4px,color:#fff
	style SETL fill:#cf0000,stroke-width:4px,color:#fff,stroke-dasharray:5px
```






## Data Flow: Housing Applications Spreadsheet

The spreadsheet used to depend on the view `TWU_JJ_VW_HOUSING_SESSIONS`, but we have reached a limit of performance/capability afforded by a view, so we had to migrate the functionality over to a stored procedure `TWU_SP_JJ_HOUSING_SESSIONS`.

#### BEFORE

```mermaid
graph LR
	subgraph jenzabarsrv
		jz(TmsEPrd)
		jics(ICS_NET)
		x(View)
	end
	subgraph db2
		aq(Aqueduct)
	end
	
	jz --> x
	jics --> x
    aq --> x

	filter(Term Filter) --> Excel
    x --> Excel(Excel)

```

#### AFTER

```mermaid
graph LR
	subgraph jenzabarsrv
		jz(TmsEPrd)
		jics(ICS_NET)
		x(Stored Procedure)
	end
	subgraph db2
		aq(Aqueduct)
	end
	
	filter(Term Filter) --> x
	jz --> x
	jics --> x
    aq --> x

	x --> Excel(Excel)
```

#### FUTURE

````mermaid
graph LR
	subgraph jenzabarsrv
		jz(TmsEPrd)
		jics(ICS_NET)
	end
	subgraph db2
		aq(Aqueduct)
	end
	subgraph DW
		ods(ODS)
		master(Master)
		Period(Period)
		Student(Student)
		HousingApp(HousingApp)
		Athlete(Athlete)
	end
	subgraph Outlets
		Housing(Housing)
	end
	
	jz --> ods(ODS)
	jics --> ods
    aq --> ods

	ods --> master
    master --> Period
    master --> Student
    master --> HousingApp
    master --> Athlete

	Period --> Housing
	Student --> Housing
	HousingApp --> Housing
	Athlete --> Housing

	Housing --> Excel(Excel)

````



