# SD Workflows

## Deforum discovery and relabeling

```mermaid
graph LR
    source --> select((select))
    select --> basename
    select --> mp4
    select --> settings
    mp4 --> judge((judge))
    settings --> judge
    judge -- reject --> to_remove
    judge -- accept --> to_keep
    to_keep --> rename((relabel))
    basename --> rename
    settings --> rename
    rename --> target
```