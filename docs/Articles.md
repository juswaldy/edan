



## Git: HOWTO Work With Multiple Accounts In GitHub Desktop

Thu, 21 May 2020



First, uninstall. And then, reinstall. And then after that, you can finally log in using the alternate account.

I kid you not, bro. They don't support multiple accounts. See for yourself here:

https://github.com/desktop/desktop/issues/3707

And here:

https://help.github.com/en/github/setting-up-and-managing-your-github-user-account/merging-multiple-user-accounts

But, what if I *do* need to use multiple accounts? Or, what if I was using a *throwaway account* and now I'm ready to use the *real one*, but I can't change my account anymore no matter what I do? like what actually happened to Jus?



### First, Uninstall

- Go to `Add or remove programs` in System settings.
- Search for `GitHub Desktop` and click `Uninstall`
- Wait a minute until `GitHub Desktop` disappears
- Delete the folder `C:\Users\<Your.Username>\AppData\Roaming\GitHub Desktop`



### And Then, Reinstall

- Reinstall



### That's It

To switch accounts, repeat those steps above.



------



## Data Warehousing: Introductory Articles

Wed, 27 May 2020



MDM/Golden Record:
https://info.semarchy.com/hubfs/collateral-v2/infographics/i-how-to-know-your-customers-better.jpg
https://blogs.informatica.com/2015/05/08/golden-record/
https://www.blumshapiro.com/insights/6-steps-for-creating-golden-records/
https://www.blumshapiro.com/insights/the-value-of-golden-records/
https://boomi.com/content/video/demo/mdm-success/

https://help.boomi.com/bundle/hub/page/c-mdm-system_9b7c31ec-922f-4eab-b4c0-61fd8d683c4b.html
https://en.wikipedia.org/wiki/Master_data
https://en.wikipedia.org/wiki/Master_data_management

https://www.red-gate.com/simple-talk/sql/database-delivery/master-data-management-mdm-using-sql-server/

https://www.red-gate.com/simple-talk/sql/database-delivery/master-data-services-basics/

https://profisee.com/definitive-guide-to-master-data-services/

ODS:
https://en.wikipedia.org/wiki/Operational_data_store

Data hub:
https://blog.semarchy.com/how-to-differentiate-a-data-hub-a-data-lake-and-a-data-warehouse
https://miktysh.com.au/data-lake-vs-data-warehouse-vs-data-hub-whats-the-difference/
https://blogs.dxc.technology/2017/07/24/data-lakes-hubs-and-warehouses-when-to-use-what/
https://en.wikipedia.org/wiki/Data_hub

Overview:
https://panoply.io/data-warehouse-guide/
https://www.javatpoint.com/data-warehouse-operational-data-stores

https://info.semarchy.com/i-cdo-the-new-seat-in-the-c-suite
https://dataform.co/blog/modern-data-stack
https://en.wikipedia.org/wiki/Architectural_pattern#Examples
https://panoply.io/analytics-stack-guide/

Star schema:
https://en.wikipedia.org/wiki/Star_schema
https://en.wikipedia.org/wiki/Dimensional_modeling

Tools:
https://blog.panoply.io/28-data-management-tools-5-ways-of-thinking-about-data-management
https://medium.com/@foundinblank/whats-a-data-stack-7c96f7a15fe8

Boomi Sync Strategies:

https://community.boomi.com/s/article/syncstrategiespart1onewaysyncs

https://community.boomi.com/s/article/syncstrategiespart2twowaysyncs

https://community.boomi.com/s/article/syncstrategiespart3realtimesyncs

https://community.boomi.com/s/article/syncstrategiespart4syncingmultipleapplications



---



## Diagrams: Using mermaidjs to make simple diagrams

Mon, 15 Jun 2020



Have you ever wished there was a robot that can ****\*automatically\***** diagram your code, your network, your notes on the kings of Israel and Judah, and even your thoughts about coffee? Well, I have. But since it hasn't reached GA yet (General Availability) I ask, is there a quick and dirty tool that can help me to quickly make a quick and dirty diagram of my thoughts?



I have asked this again and again over the years, and have googled it almost every year and recently found mermaidjs at https://mermaid-js.github.io/mermaid/#/



Here are some examples of what I mean by simple and quick and dirty.



### 1. Making Coffee with Aeropress

```
graph LR;
  Grind --> Water --> Press --> Drink;
```

![2020-06-15.01](Articles\2020-06-15.01.png)



### 2. Enjoying Coffee with Friend

```
graph BT;
    Grind(Grind Coffee) --> Press(Stir and Press);
    Water(Pour Water) --> Press;
    Press --> Drink(Enjoy with Friend);
```

![2020-06-15.02](Articles\2020-06-15.02.png)



### 3. More Quick and Dirty Stuff

```
graph TB;
    Terah --> Abraham
    Terah --> Nahor

    Abraham --> Isaac
    Sarah --> Isaac

    Nahor --> Bethuel
    Bethuel --> Rebekah
    Bethuel --> Laban 

    Isaac --> Jacob
    Rebekah --> Jacob 

    Jacob --> Judah
    Leah --> Judah 

    Laban --> Rachel
    Laban --> Leah
```

![2020-06-15.03](Articles\2020-06-15.03.png)



The screencaps are from a markdown editor called Typora. https://typora.io

There's another markdown editor that seems to support mermaidjs called Markdown Monster, but I haven't checked it out because you need an Evaluation license.



---



## Python: HOWTO Connect to SQL Server from Linux

Fri, 19 Jun 2020



Ingredients:

- Ubuntu 18.04
- Microsoft Linux ODBC Driver 17 for SQL Server
- pip 20.1.1
- pyodbc 4.0.30
- (optional) sqlalchemy 1.3.12 + jupyter 4.6 + ipython 7.11



Steps:

1. Install Microsoft driver
2. Install pyodbc
3. Create ODBC Data Source Name (DSN)
4. Register DSN with Microsoft ODBC driver
5. (optional) Install sqlalchemy + jupyter, and use sql magics



### 1. Install Microsoft driver

https://docs.microsoft.com/en-us/sql/connect/odbc/linux-mac/installing-the-microsoft-odbc-driver-for-sql-server

```
$ sudo su
$ curl https://packages.microsoft.com/keys/microsoft.asc | apt-key add -
$ curl https://packages.microsoft.com/config/ubuntu/18.04/prod.list > /etc/apt/sources.list.d/mssql-release.list
$ apt-get update
$ ACCEPT_EULA=Y apt-get install msodbcsql17
```



### 2. Install pyodbc

https://github.com/mkleehammer/pyodbc/wiki/Install

```
$ sudo -s
$ apt install python3-pip
$ apt install unixodbc-dev
$ apt install python3-dev
$ pip3 install pyodbc
```



### 3. Create ODBC Data Source Name (DSN)

https://github.com/mkleehammer/pyodbc/wiki/Connecting-to-SQL-Server-from-Linux

Check the ODBC Driver name:

```
$ cat /etc/odbcinst.ini
[ODBC Driver 17 for SQL Server]
Description=Microsoft ODBC Driver 17 for SQL Server
Driver=/opt/microsoft/msodbcsql17/lib64/libmsodbcsql-17.5.so.2.1
UsageCount=1
```



Create a temporary dsn file with content like the following. Make sure to match the Driver name with what is in `odbcinst.ini` above. Save the file (e.g. `mssql.dsn`).

```
[db6]
Driver    = ODBC Driver 17 for SQL Server
Description = Connect to db6 SQL Server instance
Trace    = No
Server    = db6.dev.twu.ca,1433
```



### 4. Register DSN with Microsoft ODBC driver

```
$ sudo odbcinst -i -s -f /path/to/your/temporary/dsn/file/mssql.dsn -l
```

Check the DSN registration:


`$ cat /etc/odbc.ini`

```
[db6]
Driver=ODBC Driver 17 for SQL Server
Description=Connect to db6 instance
Trace=No
Server=db6.dev.twu.ca,1433
```



### 5. (optional) Install sqlalchemy + jupyter

```
$ pip3 install SQLAlchemy
```



### And finally, how to connect to SQL Server with all that stuff

With pyodbc:

```
import pandas as pd
import pyodbc
conn = pyodbc.connect('DSN=db6;Database=TmsEPrd;UID=user;PWD=password', autocommit=True)
sql = """
SELECT TOP 9 *
FROM NAME_MASTER
ORDER BY NEWID()
"""
df = pd.read_sql(sql, conn)
```

With sql magics in a jupyter notebook:

```
%load_ext sql
%sql mssql+pyodbc://user:password@db6
%%sql
USE TmsEPrd;
SELECT TOP 9 *
FROM NAME_MASTER
ORDER BY NEWID()
```

For usage info, please see the pyodbc wiki at https://github.com/mkleehammer/pyodbc/wiki



---



## Archiving: False zip bombs from Salesforce Data Export weekly backup

Wed, 8 Jul 2020



### The Boring Part

We use Salesforce's Data Export facility to get our backup data from them. Once a week they make available the files through links on the Data Export page. These files are compressed using the `zip` format, containing both `SObject` data in `csv` format, and binary documents. The binary documents are described in `Attachment.csv` and `ContentDocumentLink.csv`. All these files are typically available only for 48 hours after they send us a notification email that they are ready. And the `SObject` data files are usually contained in the first `zip` file.

Our weekly backup process goes up and logs in to Salesforce, starting at the scheduled time of 1AM Friday. If it doesn't find any links available, it goes to sleep and tries again in 15 minutes. It keeps doing this until the links are published, or until 48 hours are up. As of today we have 208 files x 512MB to process each week. TODO: make the notification email trigger the process, instead of using a fixed schedule.

Last week we noticed that none of the `SObject` or the documents has been loaded since Fri, 12 June. Upon investigation, it turns out that Info-ZIP's `unzip` tool that we are using (version 6.0-21.el7) has been detecting "overlapped components" in the zip file and so treats the file as a zip bomb threat. As a result, none of the files has been properly loaded since that time.



### The Surprising Part

When I found out about this, I thought the solution would be simple. Just use the `-f` option to "`force` unzip, even with overlapped components". To my surprise, unlike most unix tools, there is no such option with `unzip`! So I tried its many different options by trial and error, and surprisingly, without success! Usually unix tools are not like this. They are usually very flexible and can handle specific cases with advanced usage by using detailed and specific parameters.

On googling, I was surprised again to find that this problem is still very common, even until now! My search terms were `unzip overlapped components`. And after reading some of the forum posts and bug reports and git commits, I was surprised again to find that the problems are not uniform. And neither are the solutions! There are reports from Gentoo, Red Hat, Debian, etc, with problems in Gradle, Adobe, etc. Version 6.0-24 even breaks lots of Java `jar` files!



### The Solving Part

I was already spending too much unplanned time on this, plus I was running out of time, plus I got tired of searching for the ideal solution because it seemed to be not so simple, so I decided to try 7zip at least for now. Not surprisingly, it worked. I installed version 16.02.

After reading more about it this week, looks like the vulnerability was published last year. And it was supposed to be fixed in version 6.0-25. But then there are already posts out there saying there are bugs in that version. I think we should stay with 7zip for now and upgrade later.



### References

| Title                              | Author          | Comment                                                      | Link                                                         |
| ---------------------------------- | --------------- | ------------------------------------------------------------ | ------------------------------------------------------------ |
| Export Backup Data from Salesforce | Salesforce Help | Documentation on how to use the Data Export facility         | https://help.salesforce.com/articleView?id=admin_exportdata.htm |
| Zip Bomb                           | Wikipedia       | Intro article about zip bombs                                | https://en.wikipedia.org/wiki/Zip_bomb                       |
| A Better Zip Bomb                  | David Fifield   | A technical article on how to build a better zip bomb. Has tons of excellent links | https://www.usenix.org/system/files/woot19-paper_fifield_0.pdf |
| CVE-2019-13232                     | Debian          | Publication of vulnerability. Has good links in the Notes section | https://security-tracker.debian.org/tracker/CVE-2019-13232   |
| Bugs in package unzip (v6.0-25)    | Debian          | Debian Bug report logs                                       | https://bugs.debian.org/cgi-bin/pkgreport.cgi?pkg=unzip;dist=unstable |



---



## AI: Break into NLP

Wed, 29 Jul 2020



Comments and insights from the "Break into NLP" seminar by deeplearning.ai.



### Presenters

- Ryan Keenan, Director of Product at deeplearning.ai
- Kenneth Church, Distinguished Scientist, Baidu USA
- Marti Hearst, Professor, School of Information and EECS Department, UC Berkeley
- Łukasz Kaiser, Staff Research Scientist at Google Brain
- Younes Mourri, Instructor of AI at Stanford University
- Andrew Ng, Founder of deeplearning.ai



### Natural Language Processing Background and History

- Performance of Machine Translation systems are scored using the [BLEU](https://en.wikipedia.org/wiki/BLEU) metric
- Humans score around 30 BLEU
- 2014: Phrase-based systems, scoring 20 BLEU
- 2015: larger and larger LSTMs, 24 BLEU
- 2016: Google Translate + seminal paper using RNNs released, 26 BLEU
  - Łukasz: But the training for the Google Translate model took 1 month @ 128 GPUs, no one could afford that except Google
  - Andrew: In 2010 we trained a neural network with 16000 CPU cores, and only Google could afford that. Today you can replicate our set up with probably less than $3000
- 2017: Transformer models, using no Recurrence, but an Attention mechanism, 28/29 BLEU
  - Andrew: "Attention is all you need" - Łukasz Kaiser
- 2019: OpenAI's GPT-2 blows away my toupée
  - Jus: there used to be a demo at [talktotransformer.com](https://talktotransformer.com), but he shut it down. Now there's one at [TextSynth](https://bellard.org/textsynth/)
- 2020: GPT-3 is the SOTA today, uses few-shot learning
  - Łukasz: Transformer now has Reformer which is a more efficient Transformer
  - Łukasz: GPT3 is the first model that shows you can do learning without gradient descent



### Talk about timing

- Andrew: Timing is very important: Leonardo da Vinci's helicopter, Apple's Newton, it was not time yet. Deep learning 30 years ago was not time yet, but 15 years ago YES. NLP timing now feels more "solid".
- Marti: Timing in terms of selecting a research project: look at an area where not a lot of people are working on. If you want to have satisfaction in an area of intellectual pursuit, ask "What are you interested in?" Be prepared to get a lot of rejection. Most of early papers are rejected. Know your own interests and strengths and do that, instead of what other people are doing.
- Ken: Timing is like the stock market "buy low sell high". The tendency is to do the wrong thing. It's not a good idea to jump into something everybody else is doing. Every 20 years there is an oscillation. Read "A Pendulum Swung Too Far".
- Łukasz: On the internet it might feel like everybody is doing NLP, but I feel the opposite is true. We're barely scratching the surface.



### Talk about modelling

- Marti: Deep learning is a foundational tool now. GPT3 model is too narrow, isn't equipped with robust enough modelling to solve harder problems in language and communication.
- Ken: There is a flood of of papers with worthless benchmarks, showing incremental progress on something that's not worth doing. Currently writing a soon to be released paper "Benchmarks and Goals".
- Andrew: "Yes at some point they are pointless." But I do a lot of pointless things. It's actually OK. It's an important part of your learning journey. If you stay at that level your whole life, then that's not great. "But go ahead and do useless projects."
- Younes: NLP models are getting larger and larger. Model reduction is a great research field to go into.



### When a super-great solution arrives, is everything else becoming irrelevant?

- Łukasz: You can't deploy just the model. You need regex, preprocessing, postprocessing. You need to learn how to use the model, when/where it works, how it works.
- Andrew: There are too many new ideas. But the percentage of ideas that have its roots in biology has diminished significantly in the last decade. Don't know if it's good or bad, but most ideas are from engineering/computer science principles, very rarely from neuroscience and cognitive science.
- Ken: Shiny Object won't be on top for long. Also, we usually try for average case performance, we should aim for something harder than average case but not as hard as worst case.



### Does it make sense to dive deep into a single topic, or to keep it broad?

- Marti: Like Andrew's statistics, the teachers can't even keep up with new stuff.
- Marti: Regardless, you have to get the foundations first.
- Marti: Get broad first, then dive deep. It's a "T".



### Memorable paraphrases and quotes

- Łukasz: It's never the end, you need to keep learning.
- Marti: Don't worry if you're weak in an area, you can learn it.
- Ken: "You shall know a word by the company it keeps." - John Rupert Firth
- Ken: Not all data is interchangeable.
- Younes: When you finish an assignment, there is already some newer technology being released, that you need to learn.
- Andrew: "Yes at some point they are pointless... But go ahead and do useless projects."



---



## Azure Data Studio: HOWTO Save results with 5K+ rows from a notebook

Tue, 11 Aug 2020



### The Problem

Currently, Azure Data Studio limits SQL notebooks to pull only a maximum of 5K rows of data. There is no configuration setting or option to remove this limit, so you can't save all the data and look at it somewhere else (Excel etc). This can be annoying if you have a pretty complex process to arrive at your final result. But no worries, save your final result in a global temp table.



### The Solution

1. In your notebook, save the results into a global temp table e.g. `SELECT ... INTO ##GlobalTemp FROM ....`
2. Open a new query and connect to the same database as the notebook
3. Pull results from the saved global temp table from #1 above e.g. `SELECT * FROM ##GlobalTemp`
4. Click save as CSV or Excel or XML or JSON
5. Clean up and `DROP TABLE ##GlobalTemp`



---



## Github: HOWTO render mermaidjs diagrams on your browser

Thu, 17 Sep 2020



```
tl;dr - Use Chrome to browse github after installing mermaid-diagrams extension
```



### Background

Mermaid diagrams are an excellent communication tool because they are very easy to read. But also, and perhaps more importantly, because they are very easy to write, compared to Visio. I think this is why very quickly a lot of people and companies are adopting it and [integrating](https://mermaid-js.github.io/mermaid/#/integrations) it into their workflows. I myself started using it extensively earlier this year, and since that time, I have noticed some new big names pop up on their list of adopters: Azure DevOps, MediaWiki, Atlassian, etc.

I make these diagrams mostly to describe data or process flow, component architecture, integration messaging, etc. And I check them in as READMEs into our code repositories on github, so that they will be versioned and shared immediately with our Devs and Ops.



### The Problem

Github, our online git provider, doesn't support Mermaid yet. Unlike gitlab, who supported it from the very beginning. This is very interesting (or should it be alarming??), because the people have requested this feature [since 2015](https://github.com/github/markup/issues/533). And they haven't stopped requesting it [until 3 days ago](https://github.community/t/feature-request-support-mermaid-markdown-graph-diagrams-in-md-files/1922). But github has largely just ignored these requests. So my problem is this: whatever diagram I check in just looks like gobbledygook code on a github page. If I want github to show the picture, then I need to first export it as an image file or PDF, and then check it in together with the markdown source. Not good.



### Solution #1: Easy (Recommended)

Use a ready-made browser extension for Mermaid. Like [mermaid-diagrams](https://chrome.google.com/webstore/detail/mermaid-diagrams/phfcghedmopjadpojhmmaffjmfiakfil) for Google Chrome. But you will be at the mercy of the maintainer of the extension. (Mermaid is growing quickly, and so far this developer has been pretty diligent in updating his extension. So this was the option that I chose)



### Solution #2: Harder (Recommended if you have spare time)

Develop your own extension and maintain it yourself. This way you are always in control of which Mermaid version to use on which browser. Upon googling, I found a pretty good candidate called [github-mermaid-extension](https://github.com/BackMarket/github-mermaid-extension) which seems to be have been abandoned by the original developer last year, but was forked by a few others. I think a good solution can be made by combining these forks:

- [ns8inc](https://github.com/ns8inc/github-mermaid-extension): Update dependencies
- [n8v-guy](https://github.com/n8v-guy/github-mermaid-extension): Eliminate excessive binding for mermaid API on rendering, or
- [tehprofessor](https://github.com/tehprofessor/github-mermaid-extension): Bind to target only if specified



### Solution #3: Very Hard (Not recommended)

Switch to gitlab. This will probably take $23M and 5 years to accomplish. (Numbers slightly exaggerated to illustrate level of difficulty)



---



## Visualization: Accidentally stumbling across patterns in data using SandDance

Tue, 22 Sep 2020



```
tl;dr Use visualization not only to tell others what you think, but also to let it show you new ideas that you've never articulated before.
```


### The Story

Brad enabled the [Deadlocks report](http://10.10.118.230:8123/iwc/database.iwc?db_id=2&repo_id=1&sdTab=4&iorw=r&pm=P&fullSQLText=&tab=4&chartType=) in solarwinds a couple of weeks ago because Finance was having deadlock problems in Jenzabar. That is a really useful report with lots of juicy details on each deadlock recorded. I got more interested in it and started collecting the data into a [spreadsheet](https://teams.microsoft.com/l/file/0CC420EB-6DAC-4084-9F16-81AFCDA5967D?tenantId=2b4ef155-1673-43ef-a480-230c3d483f16&fileType=xlsx&objectUrl=https%3A%2F%2Fmytwu.sharepoint.com%2Fsites%2FTWIT.Vids%2FShared Documents%2FJUMP - Technical%2Fsolarwinds.deadlocks.xlsx&baseUrl=https%3A%2F%2Fmytwu.sharepoint.com%2Fsites%2FTWIT.Vids&serviceName=teams&threadId=19:bb6806f6542b46d6b80a3e0b0ced7354@thread.skype&groupId=d13f852e-7ae4-43c3-b233-590e83a0b199), because I had no direct access to the database. I posted some initial analysis on the [Applications](https://teams.microsoft.com/l/channel/19%3abb6806f6542b46d6b80a3e0b0ced7354%40thread.skype/Applications?groupId=d13f852e-7ae4-43c3-b233-590e83a0b199&tenantId=2b4ef155-1673-43ef-a480-230c3d483f16) channel and left it at that.

This morning I was stuck with my data warehousing task, so I decided to take another look at the report data again. But this time, using SandDance in Azure Data Studio. (SandDance is a [visualization tool](https://sanddance.js.org) made by the Microsoft Research VIDA Group) And I saw an interesting pattern that I never saw in the spreadsheet: there's a pattern of deadlocks happening every 15 minutes!



### The Adventure

Here are the steps that enabled me to embark on this journey:

1. Save the spreadsheet in CSV format
2. Open Azure Data Studio and go to the Explorer <CTRL+SHIFT+E>
3. Right click on the CSV file and click "View in SandDance"
4. See below of how my accidents led me to stumble across a nice little gem in the mud pits



### A. View in SandDance

You can see that a lot of deadlocks happened on September 9th and 10th.

![2020-09-22.01](Articles\2020-09-22.01.png)



### B. Exclude Outliers

That is, you click the 2 outliers and click Exclude. I think this is one of the most helpful things offered by visualizations: the ability to quickly exclude or focus on outliers.

![2020-09-22.02](Articles\2020-09-22.02.png)



### C. Color by Cost, Apply Quantile Binning with 7 Bins

Another helpful technique is to use [quantile](https://en.wikipedia.org/wiki/Quantile) binning. Instead of thoughtlessly dividing the max value by 7 (number of bins) and not caring how many members are in each bin, quantiles allow you to keep the size of your bins roughly equal to each other. But don't forget that in some situations, thoughtless disregard is a more helpful technique.

![2020-09-22.03](Articles\2020-09-22.03.png)



### D. Facet by Minute, 12 Bins

Here is where the pattern is starting to emerge. It seems that the Red blocks are appearing at a regular interval. Blank-Blank-Red, Blank-Blank-Red, etc.

![2020-09-22.04](Articles\2020-09-22.04.png)



### E. Isolate by Cost >= 40 seconds

So let's isolate only the Red blocks, which are the expensive deadlocks (>= 40 seconds each). And here the pattern is becoming clear. You see a bunch of stuff happening in Minute 10-15, 25-30, 40-45, and 55-60, i.e. around every 15 minutes!

![2020-09-22.05](Articles\2020-09-22.05.png)



### F. Exclude SSMS and User jobs

Again, take out the outliers.

![2020-09-22.06](Articles\2020-09-22.06.png)



### G. Cross Facet by Program

Which Programs are included in this slice of data?

![2020-09-22.07](Articles\2020-09-22.07.png)

Here's the right side of the same graph, where you can see the Program names on the right edge.

![2020-09-22.08](Articles\2020-09-22.08.png)



### The Gem

Looks like this pattern is mostly by the `ex_app_pb, ex_app_vb` deadlock! And it shows that the deadlocks only happen on Minutes 11-13, 26-27, 41-42, and 56-57.



### H. Verify Conclusion by Isolating the Program

Now let's verify this by repeating steps B through E. But this time we color the data points by Program, and then Isolate `ex_app_pb, ex_app_vb`, and then facet by Minute. Boom. Voila.

![2020-09-22.09](Articles\2020-09-22.09.png)



### I. Take a quick look at these in a bigger picture with the other deadlocks

We see that the Blue blocks actually do occur only in those 4 blocks of Minute facets.

![2020-09-22.10](Articles\2020-09-22.10.png)



### The Lesson

This time the gem is not worth much at all, almost worthless like a fake plastic diamond. But who knows, next time it might be a real ruby or sapphire. Even though this finding is not at all that important, I think the practice of keeping an open mind and being teachable is very important. And I think using visualizations to look at data will probably help keep me being teachable. By the way, I have experienced visualization "accidents" like this many times before in the past. But this is the first time I have decided to write it down before I forget. So another lesson for me is: some accidents teach you a lot and are worth remembering.


```
tl;dr Some accidents teach you a lot and are worth remembering.
```


---



## Languages: Kusto Query Language

Thu, 29 Oct 2020



### Background

Last month's release of Azure Data Studio introduced the support of Kusto kernel in notebooks. When I saw it, I thought, what is this language, and why do we even need it? After exploring a [little](https://en.wikipedia.org/wiki/Azure_Data_Explorer) [bit](https://docs.microsoft.com/en-us/azure/data-explorer/kusto/query/) I was dumfounded by its simplicity, and quirkiness, and especially by its capabilities. It has windowing functions, regular expressions, geospatial, time series, user analytics, machine learning, etc etc. As an example of its awesomeness: it even renders tabular data into a boatload of graphs for you, simply by calling an [operator](https://docs.microsoft.com/en-us/azure/data-explorer/kusto/query/renderoperator) called `render`.

The system and the language was developed in 2014 by the Israeli R&D centre of Microsoft. Its name is supposed to be a reference to Jacques Cousteau, who explored the oceans. Kusto is made for "exploring the oceans of data". This product grew up to be Azure Data Explorer, and the query language is still called Kusto.



### Foreground

For those of you who are familiar with SQL and/or UNIX pipes, here are some comparisons:

| **SQL**                                                      | **Kusto**                                                    |
| ------------------------------------------------------------ | ------------------------------------------------------------ |
| SELECT TOP 100 city, province FROM address                   | address \| project city, province \| take 100                |
| SELECT * FROM address WHERE city LIKE 'Ab%'                  | address \| where city startsWith "Ab"address \| where city matches regex "^Ab.*" |
| SELECT COUNT(*) FROM address GROUP BY city HAVING COUNT(*) > 100 | address \| summarize Count = count() by city \| where Count > 100 |
| SELECT TOP 10 city, COUNT(*) CountFROM addressGROUP BY cityORDER BY Count DESC | address\| summarize Count = count() by city\| top 10 by Count desc |
| SELECT * FROM error_logWHERE timestamp > '2020-09-01'AND timestamp < '2020-10-01' | error_log\| where timestamp > datetime(2020-09-01) and timestamp < datetime(2020-10-01) |
| -- Get log entries from the week of Trump's birthday this year.DECLARE @trumps_birthday DATETIME = '2020-06-14'; SELECT * FROM error_log WHERE timestampBETWEEN-- Start of the week.DATEADD(DAY, -(DATEPART(WEEKDAY, @trumps_birthday)-1), DATEADD(DAY, DATEDIFF(DAY, 0, @trumps_birthday), 0)) AND-- End of the week.DATEADD(DAY, 7-(DATEPART(WEEKDAY, @trumps_birthday)), DATEADD(SECOND, -1, DATEADD(DAY, DATEDIFF(DAY, 0, @trumps_birthday) + 1, 0))) | let trumps_birthday = datetime(2020-06-14);error_log\| where timestamp between ( startofweek(trumps_birthday) .. endofweek(trumps_birthday)); |



### Update Wed, 3 Mar 2021

Looks like KQL is used in Azure Sentinel, Microsoft's security analytics tool. Look at these example queries for finding suspected [Hafnium](https://www.microsoft.com/security/blog/2021/03/02/hafnium-targeting-exchange-servers/) activities.

```
// This query looks for suspicious request patterns to Exchange servers that fit a pattern observed by HAFNIUM actors.
// The same query can be run on HTTPProxy logs from on-premise hosted Exchange servers.

let exchange_servers = (
    W3CIISLog
    | where TimeGenerated > ago(14d)
    | where sSiteName =~ "Exchange Back End"
    | summarize by Computer
);

W3CIISLog
| where TimeGenerated > ago(1d)
| where Computer in (exchange_servers)
| where csUriQuery startswith "t="
| project-reorder TimeGenerated, Computer, csUriStem, csUriQuery, csUserName, csUserAgent, cIP
| extend timestamp = TimeGenerated, AccountCustomEntity = csUserName, HostCustomEntity = Computer, IPCustomEntity = cIP
;
// This query looks for the Exchange server UM process writing suspicious files that may be indicative of webshells.

let scriptExtensions = dynamic([".php", ".jsp", ".js", ".aspx", ".asmx", ".asax", ".cfm", ".shtml"]);
union isfuzzy=true
(
    SecurityEvent
    | where EventID == 4663
    | where Process has_any ("umworkerprocess.exe", "UMService.exe")
    | where ObjectName has_any (scriptExtensions)
    | where AccessMask in ('0x2','0x100', '0x10', '0x4')
),
(
    DeviceFileEvents
    | where ActionType =~ "FileCreated"
    | where InitiatingProcessFileName has_any ("umworkerprocess.exe", "UMService.exe")
    | where FileName has_any(scriptExtensions)
)
| extend timestamp = TimeGenerated, AccountCustomEntity = Account, HostCustomEntity = Computer, IPCustomEntity = IpAddress
;
// This query looks for new processes being spawned by the Exchange UM service where that process has not previously been observed before.

let lookback = 14d;
let timeframe = 1d;
SecurityEvent
| where TimeGenerated > ago(lookback) and TimeGenerated < ago(timeframe)
| where EventID == 4688
| where ParentProcessName has_any ("umworkerprocess.exe", "UMService.exe")
| join kind=rightanti (
    SecurityEvent
    | where TimeGenerated > ago(timeframe)
    | where ParentProcessName has_any ("umworkerprocess.exe", "UMService.exe")
    | where EventID == 4688
) on NewProcessName
| extend timestamp = TimeGenerated, AccountCustomEntity = Account, HostCustomEntity = Computer, IPCustomEntity = IpAddress
;
```



---



## SQL Server: Graph Database

Fri, 27 Nov 2020


```
tl;dr "New" SQL Graph feature lets you connect tables together like nodes and edges in a graph.
```


###  What is [SQL Graph](https://docs.microsoft.com/en-us/sql/relational-databases/graphs/sql-graph-overview?view=sql-server-ver15)?


It is Microsoft's way of providing a conceptual Graph layer on top of its Relational model RDBMS. It is not a true graph database like Neo4j or Redis, because it converts the Graph operations and queries into their Relational equivalents. But its Graph capabilities look promising, sort of. Its [Match](https://docs.microsoft.com/en-us/sql/t-sql/queries/match-sql-graph?view=sql-server-ver15) command looks somewhat familiar, like something from Neo4j's [Cypher](https://neo4j.com/developer/cypher/). And from what I've read, its performance can be much better than if we just implement it using the Relational model.



###  Why do we want this?

When we have hierarchical data and/or complex relationships between entities, this feature can help simplify the queries. Especially if we need to analyze the interconnections between the entities and/or the relationships.



###  Why can't we just use the `hierarchyid` data type for the hierarchies?


Yes, we can use the regular Relational model and the `hierarchyid` data type. But then we have to manage the generation and assignment of the `hierarchyid` ourselves for each node. When the data grows and/or the rate of change in the data increases, then the complexity of management increases more and more rapidly. Also, the complexity of the queries will be greater, making it harder to optimize, and therefore also forcing a performance trade-off.



###  Why can't we just use Neo4j?


Yes, at this point, Neo4j is definitely better. Just use it.



###  What does it look like?


Let's say we have the following Entities and Relationships.

![2020-11-27.01](Articles\2020-11-27.01.png)


Then we can ask questions like these:



### People who order Hummus also order what?

> SELECT f2.Name
> FROM Orders o, includes i1, OrderDetails od1, includes odi1, Foods f1, includes i2, OrderDetails od2, includes odi2, Foods f2
> WHERE MATCH(f1<-(odi1)-od1<-(i1)-o-(i2)->od2-(odi2)->f2) -- Get "also orders what"
> AND f1.Name = 'Hummus'
> AND f1.Id != f2.Id 



### People who order from Popeye's Chicken also order from which Restaurants which are located at most 2 Neighborhoods away from me?

> SELECT DISTINCT r2.Name Restaurant, n3.Name Neighborhood
> FROM Restaurants r1, receives rcv1, Orders o1, Restaurants r2, receives rcv2, Orders o2, Customers c, places p1, places p2, Neighborhoods n1, Neighborhoods FOR PATH n2, nextTo FOR PATH nt, Neighborhoods n3, locatedIn li
> WHERE MATCH(r1-(rcv1)->o1<-(p1)-c-(p2)->o2<-(rcv2)-r2) -- Get "also from which Restaurants"
> AND r1.Name = 'Popeye''s Chicken'
> AND r1.Id != r2.Id
>
> AND n1.Name = 'Walnut Grove' -- Get "from me"
> AND MATCH(SHORTEST_PATH(n1(-(nt)->n2){​​​​​​1,2}​​​​​​)) -- At most 2 hops away
>
> AND MATCH(r2-(li)->n3)


```
tl;dr Relatively new SQL Graph feature lets you make a graph of nodes and edges using tables.
```


---



## Paradigms: Event Modeling

Thu, 24 Dec 2020



![2020-12-24.01](Articles\2020-12-24.01.jpg)

Recently, when researching tools and techniques for our analytics infrastructure, I stumbled across [onote.com](https://onote.com), which advertises something called [Event Modeling](https://eventmodeling.org/posts/what-is-event-modeling/). Hmmm, looks like a design and modeling paradigm, sounds interesting. After following a few articles and [examples](https://eventmodeling.org/posts/what-is-event-modeling/blueprint_large.jpg) on it, I thought wow very interesting idea. Not sure how practical this would be, but here's what I gathered so far.



### Historically

This idea is the latest iteration of a conglomeration of a few kindred ideas in the domain of process and project management/specifications. To get a taste of what this is all about, take a look at this timeline:

- 2003 Eric Evans publishes [Domain Driven Design](https://en.wikipedia.org/wiki/Domain-driven_design) book. (DDD)
- 2005 Martin Fowler writes [Event Sourcing](https://martinfowler.com/eaaDev/EventSourcing.html) article. (ES)
- 2007 Greg Young formalizes [Command and Query Responsibility Segregation](https://docs.microsoft.com/en-us/azure/architecture/patterns/cqrs) pattern [with ES](https://www.eventstore.com/blog/event-sourcing-and-cqrs). (CQRS/ES)
- 2008 Adam Dymitruk adds consensus to ES Scaling.
- 2010 Greg Young's workflow specifications adopted by [CQRS](https://martinfowler.com/bliki/CQRS.html) practitioners.
- 2012 Greg Young releases [Event Store DB](https://developers.eventstore.com/server/20.6/server/introduction/) v1.
- 2013 Alberto Brandolini develops [Event Storming](https://en.wikipedia.org/wiki/Event_storming).
- 2018 Adam Dymitruk formalizes [Event Modeling](https://eventmodeling.org/posts/what-is-event-modeling/).



### Theoretically

Current systems are optimized for an old problem that is quickly becoming totally irrelevant: insufficient storage space. As a result, our current tools, even though very mature, are good only for keeping *some* of the data, but not *all* of the data. This is true not only of the tools, but also of the thinking and the modeling of solutions utilizing these tools. The new emerging tools, quickly becoming the standard requirement of modern systems of the 21st century, are built with these things in mind. Event Modeling is supposed to be a new way of thinking and designing a system that takes these things into consideration. It's a way to design a blueprint for a system of any size or scale, and it's supposed to be done in a way that allows the clearest communication of the system's workings to the largest possible cross-section of roles in an organization. The system's completeness can be verified by following the single thread of data propagation through it, and its security by highlighting the edges where sensitive data crosses boundaries, i.e. the arrows that cross the swimlanes. As a result, project management and change management can be simplified a lot, and the cost of adding features can remain relatively constant when compared to traditional ways of development. In theory.

![2020-12-24.02](Articles\2020-12-24.02.jpg)

![2020-12-24.03](Articles\2020-12-24.03.jpg)



### Practically

The seven steps of Event Modeling:

1. Brainstorming: specify state-changing `Events`, use orange stickies.
2. Story-like: come up with the plot of "the movie," put Events in a single timeline.
3. The Storyboard: add wireframes and screens to illustrate the story, i.e. `Interfaces`.
4. Identify Inputs: these are called `Commands`.
5. Identify Outputs: these are called `Views` or Read-Models.
6. Swimlanes: organize events into swimlanes, aka "apply [Conway's Law](http://melconway.com/Home/Conways_Law.html)*."
7. Elaborate Scenes: write specs using `Given-When-Then` or `Given-Then` constructs.

\* Conway's Law: Any organization that designs a system will produce a design whose structure is a copy of the organization's communication structure.



### No Conclusions

I'm still chewing on this. Probably will try it out in one or two experimental projects.



---



## Visualization: brat

Fri, 29 Jan 2021



### Intro

While thinking about and looking around for visualization tools that we could use for our parsed SQL trees, I recently stumbled across [brat](https://github.com/nlplab/brat) (short for [brat rapid annotation tool](http://brat.nlplab.org/index.html)). It is a super awesome tool from the domain of text and natural language processing/annotation. It is not a perfect fit, but it's so cool that I have to write it down somewhere to remember it.

Take a look at some of these outputs it produced. (Current TWU Mission and Vision statements as interpreted by [Stanford CoreNLP](https://stanfordnlp.github.io/CoreNLP/))



### Parts of speech

![2021-01-29.01](Articles\2021-01-29.01.png)

### Parsed dependencies

![2021-01-29.02](Articles\2021-01-29.02.png)

![2021-01-29.03](Articles\2021-01-29.03.png)

### Constituency tree

![2021-01-29.04](Articles\2021-01-29.04.png)



---



## Metrics: Accuracy, Precision, and Recall

Thu, 25 Feb 2021



![2021-02-25.01](Articles\2021-02-25.01.png)



### Short Summary


When doing predictive modelling, `Accuracy` is a great measure. But most of the time it is _not enough_* for a model to be just highly accurate. There are 2 other measures which are practically as important as `Accuracy`: `Precision` and `Recall`.

![2021-02-25.02](Articles\2021-02-25.02.png)

\* Unless you can achieve 100% `Accuracy`, which means you will also have achieved 100% `Precision` and 100% `Recall`.



### Importance


Importance is a relative concept. If we have already produced a pretty accurate predictive model, which way should we go about improving it? Which measure should we focus on first? `Precision` or `Recall`? There is no simple answer to this, because it all depends on the use case and the business strategy. Let's look at a few examples to illustrate.



### 1. Spam Detector

`Recall`. Better let *a little* spam through, rather than block *any* actual non-spam emails. Better err on the side of allowing a pregnant mustachioed black man to visit the OB/GYN, rather than denying visit to the chunky lady who swears that the water gushing down is not her pee.



###  2. Search Autocomplete

`Precision`. You don't care much about avoiding false negatives, because you know what you are searching for. But you don't want too many false positives to show up, otherwise the autocomplete will be largely irrelevant, and pretty much useless. Like an OB/GYN office that's fully booked every day until Christmas by mustachioed black men.



###  3. Call Centre

`Recall`. You care *a lot* about false negatives. You don't want to ignore them. They are lost opportunities if you ignore them. Of course, better `Precision` means you will have fewer false positives, which means you will have fewer people to call. Yes it will save you time. But better `Recall` means you will have a better chance of catching the false negatives, even if it means you will have to call more people. How much Value is there in winning one extra customer?



###  4. Cancer Diagnosis

`Recall`. You totally want to go through practically *all* the negative predictions, and manually make sure they are true negatives. False positives are OK. Better err on the safe side, rather than misdiagnose people who are actually sick. So if possible, you want to minimize the number of false negatives. That way, you don't have to sift through too many of them manually.



###  5. [Hijackers](https://en.wikipedia.org/wiki/List_of_aircraft_hijackings) on [US Flights](https://www.bts.gov/newsroom/final-full-year-2019-traffic-data-us-airlines-and-foreign-airlines-us-flights)

Time period: 2000-2019
Total passengers: 20 years x 800 million people = 16 billion (very low estimate)
Total confirmed hijackers: 19


If you make a function called `isHarmless?(Passenger)` which always returns `TRUE`, its `Accuracy` and `Precision` would be practically 100%, and its `Recall` would be actually 100% (because it doesn't predict any Negative at all).

![2021-02-25.03](Articles\2021-02-25.03.png)



### 6. Quantum Superposition

I wonder if this is somewhat like that "spooky action at a distance" that Einstein talked about. Like our modern "and/or" conjunction. It's both and neither shallow and/or deep at the same time, simple and/or complicated, relative and/or absolute, etc. It's neither easy nor difficult and both, if you think about it, until you forgot that you were *actually* thinking and beginning to grasp that Value is an important and valuable concept to determine the relative Importance of an absolute decision.



### Further (and/or Closer) Readings

| **Title**                                                    | **Author**                                                   | **Comment**                                                  | **URL**                                                      |
| ------------------------------------------------------------ | ------------------------------------------------------------ | ------------------------------------------------------------ | ------------------------------------------------------------ |
| The Confusing Metrics of AP and mAP for Object Detection / Instance Segmentation | Yanfeng Liu, M.Sc. EE from University of Nebraska-Lincoln. SDE at AWS AI. | Short and sweet                                              | https://medium.com/@yanfengliux/the-confusing-metrics-of-ap-and-map-for-object-detection-3113ba0386ef |
| How to Calculate Precision, Recall, and F-Measure for Imbalanced Classification | Jason Brownlee, PhD Swinburne University of Technology       | Focus on Imbalanced Classification                           | https://machinelearningmastery.com/precision-recall-and-f-measure-for-imbalanced-classification/ |
| Precision vs. Recall – An Intuitive Guide for Every Machine Learning Person | Purva Huilgol                                                | Uses scikit-learn + Heart Disease Dataset from UCI for illustration | https://www.analyticsvidhya.com/blog/2020/09/precision-recall-machine-learning/ |
| Precision-recall                                             | scikit-learn                                                 | Short and sweet, plus usage examples of actual implementations | https://scikit-learn.org/stable/auto_examples/model_selection/plot_precision_recall.html |
| Precision and recall                                         | Wikipedia                                                    | Wikipedia                                                    | https://en.wikipedia.org/wiki/Precision_and_recall           |
| Confusion matrix                                             | Wikipedia                                                    | Wikipedia                                                    | https://en.wikipedia.org/wiki/Confusion_matrix               |
| F-score                                                      | Wikipedia                                                    | Wikipedia                                                    | https://en.wikipedia.org/wiki/F-score                        |
| Matthews correlation coefficient                             | Wikipedia                                                    | Wikipedia                                                    | https://en.wikipedia.org/wiki/Matthews_correlation_coefficient |
| Receiver operating characteristic (ROC)                      | Wikipedia                                                    | Wikipedia                                                    | https://en.wikipedia.org/wiki/Receiver_operating_characteristic |
| How to Use ROC Curves and Precision-Recall Curves for Classification in Python | Jason Brownlee                                               | Uses the scikit-learn implementation                         | https://machinelearningmastery.com/roc-curves-and-precision-recall-curves-for-classification-in-python/ |



---



## Azure Data Studio: HOWTO Multi-Cursor Editing

Thu, 31 Mar 2021



Maybe this is old stuff for you guys, but I just stumbled across this [cool feature](https://code.visualstudio.com/docs/getstarted/tips-and-tricks) in Azure Data Studio called Multi-Cursor Editing. (also works in Visual Studio Code, same code base). It enables you to define multiple cursors in your current document. Is this better than Find and Replace? Not really, especially if you always use regular expressions to do it. Like in [vim](https://www.vim.org). I'll just show you by examples:

- CTRL+D: Select the next instance of your item and make a cursor there
- ALT+Click: Create a new cursor at the place where you click
- Unlimited: Your item is not limited to just a word
- ALT+Double-click: Select any word and make a new cursor there



### CTRL+D

- Double-click a word
- CTRL+D
- Voila! Now you have 2 cursors

![2021-03-31.01](Articles\2021-03-31.01.gif)



### Nothing Special

- Nothing special here. Just continuing the editing from above

![2021-03-31.02](Articles\2021-03-31.02.gif)



### ALT+Click

- Click somewhere to place the cursor
- Click on another spot to make another cursor

![2021-03-31.03](Articles\2021-03-31.03.gif)



### Unlimited

- Hold down SHIFT to highlight an item. This item is not limited to just a word
- CTRL+D
- Voila! Now you have 2 cursors

![2021-03-31.04](Articles\2021-03-31.04.gif)



### ALT+Double-click

- Select an item
- Hold down ALT while double-clicking on another item
- Now you have cursors in 2 places without having to think about a regular expression that would capture them

![2021-03-31.05](Articles\2021-03-31.05.gif)



### Reference

| **Title**                          | **Author** | **Comment**                                                  | **URL**                                                      |
| ---------------------------------- | ---------- | ------------------------------------------------------------ | ------------------------------------------------------------ |
| Visual Studio Code Tips and Tricks | Microsoft  | Lots and lots of good stuff including keyboard shortcuts, task runner, customizations, etc. | https://code.visualstudio.com/docs/getstarted/tips-and-tricks |



---





## OpenRefine: Splitting a list into multiple columns for fun and profit

Fri, 30 April 2021



[OpenRefine](https://openrefine.org) is a really awesome tool for data exploration and cleaning.



### Let's say

Let's say that you found a random piece of data on one of our servers somewhere. It seems to be a list of column changes to a database. This list looks interesting and likely important, but it's a little too big to scrutinize by hand (~5K rows). It looks like this:

```
Column [dbo].[ACA_MEDIA_WRK].[SUFFIX] changed datatype from char to varchar
Column [dbo].[ACCT_CMP_1_DEF].[APPID] was added
Column [dbo].[ACCT_CMP_1_DEF].[APPROWVERSION] was added
Column [dbo].[ACCT_CMP_2_DEF].[APPID] was added
Column [dbo].[ACCT_CMP_2_DEF].[APPROWVERSION] was added
Column [dbo].[ACCT_CMP_3_DEF].[APPID] was added
Column [dbo].[ACCT_CMP_3_DEF].[APPROWVERSION] was added
...
```



### Time constraint

Since you don't have much time to explore, you decide to use [OpenRefine](http://206.12.209.2:3333/).



### What else is there besides `changed` and `added`?

- You see a bunch of rows at the top that says `changed` and `added`
- But you want to know, what else is there besides `changed` and `added`?
- So you add a new column which shows `true` if it's `changed` or `added`, and `false` otherwise
- Then you do a Text facet on the new column, and choose `false`
- Hey! You see `removed`!

![2021-04-30.01](Articles\2021-04-30.01.gif)



### What else is there besides `changed`, `added`, and `removed`?

- OK so you want to add `removed` to the list of words to watch for
- You go to the Undo/Redo tab and go Extract, hit `CTRL+C` to copy the last operation
- Then go back a step and `CTRL+V` to paste the operation
- Then edit the operation to include `removed`, and click Perform Operations
- Then you do Text facet again, and select `false` again
- Hey! You see a row that has a double `changed`!

![2021-04-30.02](Articles\2021-04-30.02.gif)



### What other double ops are there?

- You see a double `changed` in 2 rows, so let's update the filter to include double ops
- You remove the facet
- Go to copy the operation again, but this time update the filter to `>= 1` "greater or equal to 1"
- Go Text facet again
- Hey! Now you only see `true`, which means that everything has been captured by your filter!

![2021-04-30.03](Articles\2021-04-30.03.gif)



### Can we separate the rows by op type?

- Now that you know there are only 3 op types (`changed`, `added`, and `removed`), you want to separate them by this
- Go add another column and capture the first occurrence of the op in the string
- Go Text facet
- Hey! Those 3 types are the only ones showing up! (which is expected)

![2021-04-30.04](Articles\2021-04-30.04.gif)



### What other information can we get from the `changed` op?

- You select `changed` in the facet and you see `datatype` showing up at the top, but you ask, what else is there?
- You add another column for Change Type, capturing only `datatype` because that's the only thing you have so far
- Text facet again
- Select `blank`, which means something not captured by your filter
- Hey! You see `nullability` showing up!

![2021-04-30.05](Articles\2021-04-30.05.gif)



### What else is there besides `datatype` and `nullability`?

- Again, you copy the last operation
- You update the filter to include `nullability`, and apply the operation
- Text facet again to see what else is there
- Hey! There's nothing else, which means you have captured everything in `changed` with your filter!

![2021-04-30.06](Articles\2021-04-30.06.gif)

![2021-04-30.07](Articles\2021-04-30.07.gif)



### How about `added`? What's going on there?

- You go Text facet and select `added`
- Wow looks like the data is mostly `added` (>4K)
- But doesn't look like there's anything special there

![2021-04-30.08](Articles\2021-04-30.08.gif)

![2021-04-30.09](Articles\2021-04-30.09.gif)



### How about `removed`? Is there anything special there?

- Doesn't look like anything special here either

![2021-04-30.10](Articles\2021-04-30.10.gif)



### The [Cheat](https://www.youtube.com/watch?v=22ZLiDF_pTE)

Actually all this information has been gathered already by Andrew Menary into a [spreadsheet](https://teams.microsoft.com/l/file/505A2941-1F2E-45AC-983E-6D0C1C7C8684?tenantId=2b4ef155-1673-43ef-a480-230c3d483f16&fileType=xlsx&objectUrl=https%3A%2F%2Fmytwu.sharepoint.com%2Fsites%2FJenzabarUpgradeProject%2FShared Documents%2FIT%2FJZUp Old to New DB Schema Comparison 2021-04-20.xlsx&baseUrl=https%3A%2F%2Fmytwu.sharepoint.com%2Fsites%2FJenzabarUpgradeProject&serviceName=teams&threadId=19:b049ea03974f417dbc02d73712953a69@thread.tacv2&groupId=58888698-5239-419e-b0a4-35e29cfb6cb0) for us. And he even pulled this information programmatically using his [SQL script](https://mytwu.sharepoint.com/sites/JenzabarUpgradeProject/_layouts/15/Doc.aspx?sourcedoc={2fc26d9e-90fb-410b-be58-2f3865259047}&action=edit&wd=target(Schema Changes Overview|71352d46-0328-4bf2-b952-3d0e8558e41e%2F)). I'm just using that one column of data for illustration purposes only, being too lazy to search for actual data in the wild for this.



### UPDATE Mon, 31 May 2021

I didn't realize that the Teams Wiki had a limitation of <=200K for a gif, so what I posted in April didn't work at all. I had to break up each file (~1.5M) into smaller chunks using [PIL](https://pillow.readthedocs.io/en/stable/). I even had to scale down the images and limit the number of colors to 8 for it to be small enough to fit into 10 files!



---



## SQL Server: STRING_SPLIT is not Feature Complete

Wed, 30 Jun 2021



### Background

While working on integration tests during JUMP4a, Richard Bandsmer encountered a `QUERY_EXPRESSION_TOO_COMPLEX` error which got a few of us quite baffled. My first thought was, What? Wow! Great job Richard :p the elegance and complexity of your SQL queries have finally surpassed Microsoft's ability to handle them!

Brad Wiens gave us the [link](https://docs.microsoft.com/en-US/sql/relational-databases/errors-events/mssqlserver-8632-database-engine-error?view=sql-server-ver15) to this error's doc page, and immediately after reading I logged in to the test environment to see this awesome code that stumped the SQLServer engine. To my surprise, it wasn't that complex at all, nor elegant. (No wonder, it wasn't Richard's code :).



###  The Problem

The culprit is a function that parses course codes out of transaction descriptions, and it turns out that this is my own code from JUMP3! :O `TWU_FN_PARSE_CRSCDE_FROM_TRANSHIST_HYPHENATED`. And what's baffling is, it only breaks when it is called from a trigger! `TWU_TORCH_AMT` on `TRANS_HIST` for `INSERT`. It doesn't break when we call it from views, functions, or stored procedures!? Even weirder is, it only breaks at DB Compatibility Level 150!?? (Works fine with levels 100-140, I [verified](https://mytwu.sharepoint.com/sites/TWIT.Vids/Shared Documents/General/Investigating Error MSSQLSERVER_8632.ipynb) it myself) Is it a Microsoft bug?? (UPDATE Tue 6 Jul 2021: It's a *feature*, not a bug. See next article)



###  The Solution

Anyway, following their recommended action of simplifying the query, I updated the function to use Microsoft's long awaited [STRING_SPLIT](https://docs.microsoft.com/en-us/sql/t-sql/functions/string-split-transact-sql?view=sql-server-ver15) function (available since Compatibility Level 130), which splits a string using the specified delimiter into a table of substrings. And this fixed the "issue" for Compatibility Level 150.



###  The Solution's Problem

When working on the solution though, I found one thing that irked me about this `STRING_SPLIT`: it doesn't give you the sequence/order number for the substrings. I thought, well, that's still OK because the ordering will still match the input string. Right? No. Guess again. WHAATT?? You mean the ordering might not match the order of the substrings in the input string??? Yes. That's right. From the [documentation](https://docs.microsoft.com/en-us/sql/t-sql/functions/string-split-transact-sql?view=sql-server-ver15#remarks): (emphasis not mine)

> The output rows might be in any order. The order is *not* guaranteed to match the order of the substrings in the input string.



###  A Possibly Better Solution

Write our own `TWU_FN_STRING_SPLIT` function. This is in my TODO list.



###  References

| **Title**                                                  | **Author**     | **Comment**                                                  | **URL**                                                      |
| ---------------------------------------------------------- | -------------- | ------------------------------------------------------------ | ------------------------------------------------------------ |
| MSSQLSERVER_8632                                           | Microsoft      | Error documentation                                          | https://docs.microsoft.com/en-US/sql/relational-databases/errors-events/mssqlserver-8632-database-engine-error |
| STRING_SPLIT (Transact-SQL)                                | Microsoft      | Function documentation                                       | https://docs.microsoft.com/en-us/sql/t-sql/functions/string-split-transact-sql |
| Investigating Error MSSQLSERVER_8632.ipynb                 | Jus            | Notebook to test different Compatibility Levels              | [https://mytwu.sharepoint.com/sites/TWIT.Vids/Shared%20Documents/General/Investigating%20Error%20MSSQLSERVER_8632.ipynb](https://mytwu.sharepoint.com/sites/TWIT.Vids/Shared Documents/General/Investigating Error MSSQLSERVER_8632.ipynb) |
| Add row position column to STRING_SPLIT                    | Various        | Some of the complaints from users, since 5 years ago         | https://feedback.azure.com/forums/908035-sql-server/suggestions/32902852-string-split-is-not-feature-complete |
| Please help with STRING_SPLIT improvements                 | Aaron Bertrand | Short and sweet review of problem scenarios and business cases | https://sqlperformance.com/2020/03/t-sql-queries/please-help-string-split-improvements |
| When STRING_SPLIT() falls short                            | Andy Mallon    | Anecdotal problem with performance                           | https://am2.co/2020/03/when-string_split-falls-short/        |
| SQL Server Split String Replacement Code with STRING_SPLIT | Aaron Bertrand | Guide for making interim solutions before the Microsoft Native STRING_SPLIT is fixed | https://www.mssqltips.com/sqlservertip/6390/sql-server-split-string-replacement-code-with-stringsplit/ |



---



## SQL Server: Compatibility Level 150 Features That Might Break Your App

Fri, 30 Jul 2021



### 1. Inlining of User Defined Scalar Functions

This feature helps us a lot by increasing the performance of our scalar UDFs to the next level. BUT, this is also what broke our `TRANS_HIST` insert trigger noted in the previous article, because it calls the function`TWU_FN_PARSE_CRSCDE_FROM_TRANSHIST_HYPHENATED`, which is "not inlineable" according to Microsoft's Intelligent Query Processing. There are [33 rules](https://docs.microsoft.com/en-us/sql/relational-databases/user-defined-functions/scalar-udf-inlining?view=sql-server-ver15#inlineable-scalar-udfs-requirements) that you need to follow to ensure that a scalar UDF will be inlineable. Someone with > 500GB RAM tried a "not inlineable" function, and it consumed all that memory even with a function that does not access table data at all. The workaround:`ALTER DATABASE SCOPED CONFIGURATION SET TSQL_SCALAR_UDF_INLINING = OFF;`



### 2. Cardinality Estimation and the Query Store

The Query Store is a feature available since Compatibility Level 130. (The version when `STRING_SPLIT()` was introduced). The idea is to be smart and monitor query plan data, so that we can tweak query performance. But some people who have not fixed queries with regression found it decreased the performance by 6-7 times, e.g. from 4 min to 39 min, from 1 hour to 6 hours. (Some people even said 100-200 times slower, but I think that's just GRRR talking, not actual measurements). The workaround for this is to turn on Legacy Cardinality Estimation, i.e. `ALTER DATABASE SCOPED CONFIGURATION SET LEGACY_CARDINALITY_ESTIMATION = ON;`.



### 3. Other Features

Not sure what else is there, but pretty sure they are there. It's impossible to know about every single feature :p



### References

| **Title**                                                    | **Site**         | **Comment**                                                  | **URL**                                                      |
| ------------------------------------------------------------ | ---------------- | ------------------------------------------------------------ | ------------------------------------------------------------ |
| Scalar UDF Inlining                                          | Microsoft        | Microsoft documentation                                      | https://docs.microsoft.com/en-us/sql/relational-databases/user-defined-functions/scalar-udf-inlining |
| Intelligent query processing in SQL databases                | Microsoft        | Overview of Intelligent Query Processing                     | https://docs.microsoft.com/en-us/sql/relational-databases/performance/intelligent-query-processing |
| Cardinality Estimation (SQL Server)                          | Microsoft        | Concept article                                              | https://docs.microsoft.com/en-us/sql/relational-databases/performance/cardinality-estimation-sql-server |
| Change the Database Compatibility Level and use the Query Store | Microsoft        | An alternative option to take other than Query Tuning Assistant | https://docs.microsoft.com/en-us/sql/database-engine/install-windows/change-the-database-compatibility-mode-and-use-the-query-store |
| Changing to compatibility level 150 makes SQL Server consume all available memory | StackExchange    | Scalar UDF Inlining consumes all memory                      | https://dba.stackexchange.com/questions/259543/changing-to-compatibility-level-150-makes-sql-server-consume-all-available-memor |
| SQL Server 2019 performance worse than 2012… am I missing something? | StackExchange    | Cardinality Estimator slows things down                      | https://dba.stackexchange.com/questions/268099/sql-server-2019-performance-worse-than-2012-am-i-missing-something |
| Performance Issue After SQL 2019 Upgrade                     | SQLServerCentral | Cardinality Estimator issue                                  | https://www.sqlservercentral.com/forums/topic/performance-issue-after-sql-2019-upgrade |
| Performance issue on SQL 2019 Standard edition               | SQLServerCentral | Cardinality Estimator issue                                  | https://ask.sqlservercentral.com/questions/157106/performance-issue-on-sql-2019-standard-edition.html |
| Non-yielding scheduler on SQL Server 2019                    | MSDN Forum       | Scalar UDF Inlining issue                                    | https://social.msdn.microsoft.com/Forums/sqlserver/en-US/7d6df408-4c65-42f4-9666-2db0623100f8/nonyielding-scheduler-on-sql-server-2019?forum=sqldatabaseengine#64a6b8cd-e6c0-4471-b717-4ce2ea247842 |
| SQL SERVER 2019 – Disabling Scalar UDF Inlining              | SQLAuthority     | Looks like Indian Ben :p                                     | https://blog.sqlauthority.com/2020/08/01/sql-server-2019-disabling-scalar-udf-inlining/ |



---



## Making Fizzy Fruit Juice with Ginger Bug

Thu, 30 Sep 2021



During our Zoomcafe meeting a few months ago, Fred told me about one of his delicate experiments in his advanced microbiology lab. He was making carbonated fruit juices using "ginger bug". That sounded tasty and weird and interesting, and he said it was very easy to do, so one day I [decided](https://www.youtube.com/watch?v=bbgd-RS_tJ0) [to](https://www.youtube.com/watch?v=wtb1BvRUWmI) [try](https://www.youtube.com/watch?v=CtpM1ouPt8k) it too. Since that time, I have never stopped making it, always trying it with different fruit juices. It's totally yummy!



### Making the Ginger Bug

1. Put 3 cups distilled water in a quart or litre jar
2. Peel and chop 1 tablespoon of ginger and add into jar
3. Add 1 tablespoon of sugar
4. Stir vigorously to add oxygen into the liquid
5. Leave at room temperature and wait 1 day
6. Repeat steps 2-5 for 4 more times, totalling about 5 days
7. On day 6, you should see the bubbles coming up, put the jar in the fridge



### Making the Fizzy Juice

1. Buy juice containing no preservatives, especially sulfites
2. Our favourites so far are Welch's Grape Juice and Grown Right Organic Mango Orange Juice
3. Stir ginger bug
4. Mix 1/8 oz ginger bug per 500 ml juice in jar or flip-top beer bottles like Grolsch Pilsner
5. Leave at room temperature 2-3 days with unsealed cap
6. Seal the cap and leave at room temperature 1-3 more days, not too long because it might become sulfury
7. Put it in the fridge and enjoy



### Keeping the Ginger Bug

1. Add 1/2 tablespoon sugar every week
2. Take out old ginger every 2-3 weeks
3. Add a tablespoon of fresh ginger to replace the old ginger
4. Top up with distilled water



---



## Automating VAERS Dataset Pull

Fri, 31 Dec 2021



![2021-12-31.01](Articles\2021-12-31.01.jpg)


```
tl;dr If you look really really closely at the data, you are bound to find a unicorn that fits the pattern very very well.
```


### Background


The US CDC VAERS (United States Centers for Disease Control and Prevention Vaccine Adverse Event Reporting System) is notorious for its usability problems. But it suffers from a bigger problem: unclean and unreliable data. So I tried to stay as far away [as](https://www.medrxiv.org/content/10.1101/2021.08.30.21262866v1.full.pdf) [possible](https://www.medrxiv.org/search/vaers?numresults%3A75&sort%3Arelevance-rank) [from](https://arxiv.org/search/?query=vaers&searchtype=all) [it](https://arxiv.org/abs/2007.02266). Yet, when I heard a rumor that one day roughly 50K rows got removed from the database, that piqued my interest. I mean, who can resist a Data Conspiracy Theory? Maybe you can, but I certainly was not able to! I thought, hmmm, can I just watch the row count and nothing else, eh? I finally gave in and abandoned all hope and entered the website.



### The journey through the underworld

The front doors to the VAERS Dataset is located here: https://vaers.hhs.gov/data.html. It has a long disclaimer, which ends with something to the effect of "this is junky data; the better data is not available to the public". At the bottom there's a checkbox for acknowledging that you have read and understood it.

![2021-12-31.02](Articles\2021-12-31.02.png)



If you just click the Download button without checking the box, a window pops up telling you to please check the box.

![2021-12-31.03](Articles\2021-12-31.03.png)



Once you check the box, and then click Download, it brings you to the Dataset page located here: https://vaers.hhs.gov/data/datasets.html. Amazingly, you can just visit that page directly, without going through the previous pages!

![2021-12-31.04](Articles\2021-12-31.04.png)



There you can see, neatly laid up in a table, all the links to all the files to all the years, since 1990. And at the top, there's a link to a ZIP that contains all these files: https://vaers.hhs.gov/eSubDownload/index.jsp?fn=AllVAERSDataCSVS.zip. When you click it, it brings you to a verification page. Amazingly, you can just visit that page directly, without going through the previous pages!

![2021-12-31.05](Articles\2021-12-31.05.png)



This page shows you a captcha* image, and you're supposed to type the characters shown in the image, into the box. Upon inspection, I found that the verification code is always 6 alphanumeric characters. At least so far. And also, the image is accessible from this location: https://vaers.hhs.gov/eSubDownload/captchaImage. Amazingly, you can just visit that page directly, without going through the previous pages! You can try it out yourself. I promise this is not a l<n0wBe4 test.



### Where the straight way was lost

After this series of amazing discoveries, I thought, hmmm can I just automate that last step so I don't have to pull it manually every week? (I found out later that they *usually* update the data every week on Friday AM) Then I tried it and found that you *can* directly download a file from the datasets page if you (1) supply a valid verification code from a recent captcha image, and (2) use the cookies downloaded from the verification page e.g. https://vaers.hhs.gov/eSubDownload/index.jsp?fn=AllVAERSDataCSVS.zip&verificationCode=cfFEfF, so yes theoretically you can automate this. So, using HP/Google's [tesseract](https://github.com/tesseract-ocr/tesseract) to OCR the verification captcha image, I made a script to do it.

![2021-12-31.06](Articles\2021-12-31.06.png)


```
* tl;dr I learned that captcha stands for Completely Automated Public Turing test to tell Computers and Humans Apart.
```


---

## Notebooks: Colab and Binder

Thu, 31 Mar 2022
 
 
### Background
 
[Notebooks](https://jupyter.org/try-jupyter/retro/notebooks/?path=notebooks/Intro.ipynb) are a very useful interactive tool for making experiments with, and even documenting, your code. Besides the actual running code, you can type up and include your problem statements, your questions, guesses, solutions, strategies, partial solutions, links to StackOverflow answers, pics of unicorns, etc. And you can format your text nicely in Markdown format (hehe I said format three times in a sentence). In a sense, all this enables you to "spice up" and "enhance" the meat, which is the code itself. Plus, you can slice up "the meat" into bite-sized chunks so that it's easier for other people to digest, or for your older self to revisit and to reminisce and/or cringe: you separate the code into smaller blocks. Easier to chew in case your teeth are all gone.
 
Ever since I discovered notebooks during my Pro-D, I started using them right away. And very soon afterwards, I started incorporating them into my workflows. I mean, there are many tasks now that I do, where the first step is: Number 1. Open the notebook...
 
 
### Colab
 
[Colab](https://colab.research.google.com/) is a free* cloud notebook environment provided by Google Research. If you want to experiment with something quickly, but don't want to install anything, this is awesome. Especially if you want to use it to collaborate with somebody from Ecuador for example.
 
\* well, mostly free. Even with GPU! But it's limited to only ~6 consecutive hours, which is plenty of time. But that's why they also offer a paid version where you can use their bigger GPUs for more than 6 hours, and get more RAM, or even use their TPUs if you do your project in Tensorflow. Tensor Processing Unit ([TPU](https://www.hpcwire.com/2021/05/20/google-launches-tpu-v4-ai-chips)) is one of the recent cases of hardware being built especially to serve the existing software, not the other way around.
 
 
### Binder
 
[Binder](https://mybinder.org/) is a free environment builder where your git repo is automatically containerized and deployed, so you can publish working and/or production notebooks to the cloud almost immediately. This is super awesome and I hope we can make use of this at TWU in the near future. Here are a couple of examples: [manim](https://try.manim.community/) (math animation library), [bokeh](https://mybinder.org/v2/gh/binder-examples/bokeh/master?urlpath=/proxy/5006/bokeh-app) (data visualization).
 
 
```
tl;dr Find the "hidden" links and click them.
``` 

---
 
## VS Code: File diff

 
Fri, 30 Jun 2022
 
 
### Expectations
 
When dealing with versions of text, it's very helpful to have a good diff tool. And since I use [vim](https://www.vim.org/) as my text editor, it's natural to just use vimdiff for diff-ing (comparing the versions). It doesn't look so pretty (the comments I have heard about it: "looks like dog food", "it looks 8 bit", "it's like froot loops"), but it does the job well.
 
![2022-06-30.01](Articles\2022-06-30.01.png)

Recently I switched from straight vim to using [VS Code](https://code.visualstudio.com/), with vim [extension](https://marketplace.visualstudio.com/items?itemName=vscodevim.vim), because there was a technical preview of [GitHub Copilot](https://github.com/features/copilot/). (We should totally get it, this tool is totally worth it. Now it's out of beta and is available as a 60-day free trial). But I was so used to the benefits of vimdiff, that I go out of the VS Code environment to a bash shell, and then run vimdiff there, just for the purpose of checking diffs.
 
 
### Delight
 
Last week when working on the db2 Upgrade project, I finally decided to venture out of my comfort zone, and try to find something to replace it inside VS Code. To my utter delight, it already comes with it! Their diff tool is as good as (or even better than??) vimdiff, and very easy to use. In vim normal mode, you just jump to the Explorer window and find the first file to compare, and then `CTRL+SHIFT+P` (Command Palette) and type `compare`, choosing `File: Compare Active File With...` That's it!
 
With a mouse:
1. Left-click on the first file in Explorer
1. CTRL-Left-click on the second file
1. Right-click on one of the files and choose Compare Selected
1. Done!
 
### 1. Left-click on the first file in Explorer

![2022-06-30.02](Articles\2022-06-30.02.png)

### 2. CTRL-Left-click on the second file

![2022-06-30.03](Articles\2022-06-30.03.png)

### 3. Right-click on one of the files and choose Compare Selected

![2022-06-30.04](Articles\2022-06-30.04.png)

### 4. Done!

![2022-06-30.05](Articles\2022-06-30.05.png)

---