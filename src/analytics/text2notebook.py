import json
import nbformat

def makecell(cell_type, source, metadata=None):
    ''' Create a notebook cell dictionary '''
    cell = {'cell_type': cell_type, 'source': source}
    if metadata:
        cell['metadata'] = metadata
    return cell

def text2cellsource(text):
    ''' Convert text to a notebook cell source '''
    return text.splitlines

WINDOWS_LINE_ENDING = b'\r\n'
UNIX_LINE_ENDING = b'\n'
textfile = './db2Upgrade/db2Upgrade.ps1'
notebookfile = './db2Upgrade/db2Upgrade.ipynb'
title = 'db2 Upgrade'
notebook = {
    'metadata': {
        'kernelspec': {
            'name': 'powershell',
            'display_name': 'PowerShell',
            'language': 'powershell'
        },
        'language_info': {
            'name': 'powershell',
            'codemirror_mode': 'shell',
            'mimetype': 'text/x-sh',
            'file_extension': '.ps1'
        }
    },
    'nbformat': 4,
    'nbformat_minor': 2,
    'cells': []
}

#title = makecell('markdown', [f'# {title}'])
#notebook['cells'].append(title)
#with open(textfile, 'rb') as f:
#    lines = f.readlines()
#    for line in lines:
#        notebook['cells'].append(makecell('code', line.replace(UNIX_LINE_ENDING, WINDOWS_LINE_ENDING)))
#
#with open(notebookfile, 'w') as f:
#    json.dump(notebook, f)

#infile = convert.read(open(textfile, 'r'), 'ps')
#convert.write(infile, open(notebookfile, 'w'), 'ipynb')

infile = nbformat.read(open(textfile, 'r'), nbformat.current_nbformat)
nbformat.write(infile, notebookfile, nbformat.NO_CONVERT)
