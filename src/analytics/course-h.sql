WITH base AS (
    SELECT
        _member_name,
        SUBSTRING(_member_name, 1, 5) comp1,
        SUBSTRING(_member_name, 6, 5) comp2,
        SUBSTRING(_member_name, 11, 5) third5chars,
        SUBSTRING(_member_name, 16, 5) fourth5chars,
        CHARINDEX(' - ', SUBSTRING(_member_name, 11, 5)) AS dash3,
        CHARINDEX(' - ', SUBSTRING(_member_name, 16, 5)) AS dash4,
        RIGHT(_member_name, 8) hier
    FROM STAGING.HIERARCHY_PATH
    WHERE _dim = 'FLEX03' AND hierarchy_version_id = 643 AND is_leaf = 1
    AND _member_name LIKE '____ ___  % - __:__:__'
),
components AS (
    SELECT *,
        CASE
        WHEN CHARINDEX(' - ', third5chars) > 0 THEN SUBSTRING(third5chars, 1, dash3)
        ELSE third5chars
        END AS comp3,
        CASE
        WHEN CHARINDEX(' - ', fourth5chars) > 0 THEN SUBSTRING(fourth5chars, 1, dash4)
        ELSE NULL
        END AS comp4,
        SUBSTRING(hier, 1, 2) AS hier1,
        SUBSTRING(hier, 1, 5) AS hier2,
        SUBSTRING(hier, 1, 8) AS hier3,
        CONCAT( '01_', SUBSTRING(hier, 1, 2)) AS h1,
        CONCAT( '02_', SUBSTRING(hier, 4, 2)) AS h2,
        CONCAT( '03_', SUBSTRING(hier, 7, 2)) AS h3
    FROM base
)
SELECT
    _member_name, hier,
    comp1, comp2, comp3, comp4, hier1, hier2, hier3, h1, h2, h3
    INTO #jjj1
FROM components

-- SELECT hier1, hier2, hier3, COUNT(*) Count FROM components GROUP BY hier1, hier2, hier3 ORDER BY hier1, hier2, hier3
SELECT hier1, hier2, COUNT(*) Count FROM components GROUP BY hier1, hier2
UNION
SELECT hier2, hier3, COUNT(*) Count FROM components GROUP BY hier2, hier3
ORDER BY hier1, hier2
-- SELECT hier1, COUNT(*) Count FROM hierarchies GROUP BY hier1
-- SELECT hier2, COUNT(*) Count FROM hierarchies GROUP BY hier2
-- SELECT hier3, COUNT(*) Count FROM hierarchies GROUP BY hier3

SELECT parent, child, Count FROM (
    SELECT 1 AS level, 'root' AS parent, h1+'1' AS child, COUNT(*) Count FROM components GROUP BY h1
    UNION
    SELECT 2, h1+'1', h2+'2', COUNT(*) Count FROM components GROUP BY h1, h2
    UNION
    SELECT 3, h2+'2', h3+'3', COUNT(*) Count FROM components GROUP BY h2, h3
) x
ORDER BY level, parent, child

SELECT comp1 FROM components GROUP BY comp1 HAVING COUNT(*) = 1
SELECT comp2 FROM components GROUP BY comp2 HAVING COUNT(*) = 1
SELECT * FROM components WHERE comp2 NOT LIKE '[0-9][0-9][0-9]'
SELECT comp3 FROM components GROUP BY comp3 HAVING COUNT(*) = 1

