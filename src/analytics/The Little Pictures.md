# The Little Pictures

## Metadata

### Incoming

```mermaid
graph LR
    subgraph source
        app
    end
    subgraph hub
        receiving
        staging
    end
    subgraph warehouse
        meta
    end
    app -- source<br>format --> receiving
    receiving -- intermediate<br>format --> staging
    staging -- internal<br>format --> meta
```

### Outgoing

```mermaid
graph LR
    subgraph warehouse
        meta
    end
    subgraph hub
        staging
        shipping
    end
    subgraph target
        app
    end
    meta -- internal<br>format --> staging
    staging -- target<br>format --> shipping
    shipping --> app
```

## Data

### Incoming

```mermaid
graph LR
    subgraph warehouse
        meta
        ods
    end
    subgraph source
        app
    end
    subgraph hub
        receiving
        staging
    end
    app -- 1 Read --> receiving
    receiving -- 2 Preprocess --> staging
    meta -- 3 Join --> staging
    staging -- 4 Load --> ods

```

### Outgoing

```mermaid
graph LR
    subgraph warehouse
        meta
        ods
    end
    subgraph hub
        staging
        shipping
    end
    subgraph target
        app
    end
    ods -- 1 Read --> staging
    meta -- 2 Join --> staging
    staging -- 3 Process --> shipping
    shipping -- 4 Load --> app
```