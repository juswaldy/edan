from typing import Tuple
from dbconfig import *
import pyodbc
import os
import glob
import re
import csv
import json
import argparse
from unicodedata import category
import numpy as np
# from tangled_up_in_unicode import uppercase

DEBUGGING = True

""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
""" Configs """
class Configs:
    def __init__(self, **kwargs):

        """ SQL Exporter/Importer """
        self.batchsize = 100 # Number of rows per INSERT statement.

""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
""" Argument parsing and checking """
def parse_args() -> argparse.Namespace:
    desc = 'FTP Project GL Number Tools'
    parser = argparse.ArgumentParser(description=desc)
    parser.add_argument('--fn', type=str, default=None, help='sankey_gl, tree_components, sankey_course', required=True)
    parser.add_argument('--inputfolder', type=str, default=None, help='Input folder', required=False)
    parser.add_argument('--inputfile', type=str, default=None, help='Input file', required=False)
    parser.add_argument('--outputfile', type=str, default=None, help='Output file', required=False)
    parser.add_argument('--istrue', action='store_true', help='Flag as true?', required=False)
    return check_args(parser.parse_args())
def check_args(args: argparse.Namespace) -> argparse.Namespace:
    """ No checks for now. """
    return args

""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
""" Helper functions """

""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
""" Viz functions """

""" Sankey diagram for GL numbers """
def sankey_gl(temptable: str, sourcetable: str, sourcefield: str, outputprefix: str) -> None:

    # Generate the components.
    # Add first, middle, and last 10 digit components.
    components = {}
    for i in range(1, 7):
        c = f'comp{str(i)}'
        cfrom = (i-1)*5 + 1
        cto = (i-1)*5 + 5
        components[c] = [cfrom, cto]
    components['first10'] = [ 1, 10 ]
    components['middle10'] = [ 11, 20 ]
    components['last10'] = [ 21, 30 ]

    # Connect to the database.
    conn = pyodbc.connect(glconv, autocommit=True)
    cursor = conn.cursor()

    # Rebuild the mapping table.
    sql = f"IF OBJECT_ID('tempdb..{temptable}') IS NOT NULL DROP TABLE {temptable};\n"
    sql += 'SELECT\n\t'
    sql += ',\n\t'.join([ f'SUBSTRING({sourcefield}, {i:02d}, 1) c{i:02d}' for i in range(1, 31) ])
    sql += f'''
        INTO {temptable}
        FROM {sourcetable};
    '''
    print(sql) if DEBUGGING else None
    cursor.execute(sql)
    cursor.commit()

    # Go through each component and generate the nodes and links.
    for c in components.items():
        comp = c[0]
        comp_from, comp_to = c[1]

        # Create nodes table.
        sql = "IF OBJECT_ID('tempdb..#nodes') IS NOT NULL DROP TABLE #nodes;"
        sql += 'WITH base AS (\n\t'
        sql += '\n\tUNION '.join([ f"SELECT DISTINCT CONCAT('{i:02d}_', c{i:02d}) id, c{i:02d} name FROM {temptable}" for i in range(comp_from, comp_to+1) ])
        sql += f'''
            )
            SELECT id, name, ROW_NUMBER() OVER (ORDER BY id) AS node_id
            INTO #nodes
            FROM base
            WHERE CONVERT(INT, SUBSTRING(id, 1, 2)) BETWEEN {comp_from} AND {comp_to}
            ORDER BY id;
        '''
        print(sql) if DEBUGGING else None
        cursor.execute(sql)
        cursor.commit()

        # Retrieve the nodes.
        sql = """
            SELECT '_0' id, '' name
            UNION
            SELECT id, name FROM #nodes ORDER BY id;
        """
        print(sql) if DEBUGGING else None
        cursor.execute(sql)
        nodes = cursor.fetchall()
        nodes = [ { 'id': row[0], 'name': row[1] } for row in nodes ]
        print(nodes) if DEBUGGING else None

        # Retrieve the links.
        sql = 'WITH base AS (\n\t'
        sql += '\n\tUNION '.join([ f"SELECT s.node_id source, t.node_id target, COUNT(*) value FROM {temptable} m JOIN #nodes s ON CONCAT('{i:02d}_', m.c{i:02d}) = s.id JOIN #nodes t ON CONCAT('{i+1:02d}_', m.c{i+1:02d}) = t.id GROUP BY s.node_id, t.node_id" for i in range(comp_from, comp_to) ])
        sql += '''
            )
            SELECT *
            FROM base
            ORDER BY source, target;
        '''
        print(sql) if DEBUGGING else None
        cursor.execute(sql)
        links = cursor.fetchall()
        links = [ { 'source': row[0], 'target': row[1], 'value': row[2] } for row in links ]
        print(links) if DEBUGGING else None

        # Compose JSON and save to file.
        combined = { 'nodes': nodes, 'links': links }
        with open(f'{outputprefix}{comp}.json', 'w') as f:
            json.dump(combined, f)

""" Tree diagram for GL numbers """
def tree_components(temptable: str, sourcetable: str, sourcefield:str, outputprefix: str) -> None:

    comp_from, comp_to = 1, 6

    # Connect to the database.
    conn = pyodbc.connect(glconv, autocommit=True)
    cursor = conn.cursor()

    # Rebuild the mapping table.
    sql = f"IF OBJECT_ID('tempdb..{temptable}') IS NOT NULL DROP TABLE {temptable};\n"
    sql += 'SELECT\n\t'
    sql += ',\n\t'.join([ f'COMP{i} c{i:02d}' for i in range(comp_from, comp_to+1) ])
    sql += f'''
        INTO {temptable}
        FROM {sourcetable};
    '''
    print(sql) if DEBUGGING else None
    cursor.execute(sql)
    cursor.commit()

    # Create nodes table.
    sql = "IF OBJECT_ID('tempdb..#nodes') IS NOT NULL DROP TABLE #nodes;"
    sql += 'WITH base AS (\n\t'
    sql += '\n\tUNION '.join([ "SELECT DISTINCT CONCAT('{:02d}_', c{:02d}) id, c{:02d} name FROM #comp_dec13".format(i, i, i) for i in range(comp_from, comp_to+1) ])
    sql += f'''
        )
        SELECT id, name, ROW_NUMBER() OVER (ORDER BY id) AS node_id
        INTO #nodes
        FROM base
        ORDER BY id;
    '''
    print(sql) if DEBUGGING else None
    cursor.execute(sql)
    cursor.commit()

    # Retrieve the nodes.
    sql = """
        SELECT '_0' id, '' name
        UNION
        SELECT id, name FROM #nodes ORDER BY id;
    """
    print(sql) if DEBUGGING else None
    cursor.execute(sql)
    nodes = cursor.fetchall()
    nodes = [ { 'id': row[0], 'name': row[1] } for row in nodes ]
    print(nodes) if DEBUGGING else None

    # Retrieve the links.
    sql = 'WITH base AS (\n\t'
    sql += '\n\tUNION '.join([ "SELECT s.node_id source, t.node_id target, COUNT(*) value FROM #comp_dec13 m JOIN #nodes s ON CONCAT('{:02d}_', m.c{:02d}) = s.id JOIN #nodes t ON CONCAT('{:02d}_', m.c{:02d}) = t.id GROUP BY s.node_id, t.node_id".format(i, i, i+1, i+1) for i in range(comp_from, comp_to) ])
    sql += '''
        )
        SELECT *
        FROM base
        ORDER BY source, target;
    '''
    print(sql) if DEBUGGING else None
    cursor.execute(sql)
    links = cursor.fetchall()
    links = [ { 'source': row[0], 'target': row[1], 'value': row[2] } for row in links ]
    print(links) if DEBUGGING else None

    # Compose JSON and save to file.
    combined = { 'nodes': nodes, 'links': links }
    with open(f'tree.json', 'w') as f:
        json.dump(combined, f)

""" Sankey diagram for course components """
def sankey_course(leafnodes: str, nodetable: str, linktable: str) -> None:

    # Connect to the database.
    conn = pyodbc.connect(prod_frdb, autocommit=True)
    cursor = conn.cursor()

    # Get leaf nodes.
    sql = f"IF OBJECT_ID('tempdb..{leafnodes}') IS NOT NULL DROP TABLE {leafnodes};\n"
    sql += f"""WITH base AS (
            SELECT
                _member_name,
                SUBSTRING(_member_name, 1, 5) comp1,
                SUBSTRING(_member_name, 6, 5) comp2,
                SUBSTRING(_member_name, 11, 5) third5chars,
                SUBSTRING(_member_name, 16, 5) fourth5chars,
                CHARINDEX(' - ', SUBSTRING(_member_name, 11, 5)) AS dash3,
                CHARINDEX(' - ', SUBSTRING(_member_name, 16, 5)) AS dash4,
                RIGHT(_member_name, 8) hier
            FROM STAGING.HIERARCHY_PATH
            WHERE _dim = 'FLEX03' AND hierarchy_version_id = 643 AND is_leaf = 1
            AND _member_name LIKE '____ ___  % - __:__:__'
        ),
        components AS (
            SELECT *,
                CASE
                WHEN CHARINDEX(' - ', third5chars) > 0 THEN SUBSTRING(third5chars, 1, dash3)
                ELSE third5chars
                END AS comp3,
                CASE
                WHEN CHARINDEX(' - ', fourth5chars) > 0 THEN SUBSTRING(fourth5chars, 1, dash4)
                ELSE NULL
                END AS comp4,
                SUBSTRING(hier, 1, 2) AS hier1,
                SUBSTRING(hier, 1, 5) AS hier2,
                SUBSTRING(hier, 1, 8) AS hier3,
                CONCAT('01_', SUBSTRING(hier, 1, 2)) AS h1,
                CONCAT('02_', SUBSTRING(hier, 4, 2)) AS h2,
                CONCAT('03_', SUBSTRING(hier, 7, 2)) AS h3
            FROM base
        )
        SELECT
            _member_name, hier,
            comp1, comp2, comp3, comp4, hier1, hier2, hier3, h1, h2, h3
        INTO {leafnodes}
        FROM components
    """
    print(sql) if DEBUGGING else None
    cursor.execute(sql)
    cursor.commit()

    # Create nodes table.
    sql = f"IF OBJECT_ID('tempdb..{nodetable}') IS NOT NULL DROP TABLE {nodetable};\n"
    sql += f"""
        WITH base AS ( 
            """ + '\n\tUNION '.join([ f"SELECT DISTINCT h{i} id FROM {leafnodes}" for i in range(1, 4) ]) + f"""
        )
        SELECT
            id,
            ROW_NUMBER() OVER (ORDER BY id) AS nodeid
        INTO {nodetable}
        FROM base
    """
    print(sql) if DEBUGGING else None
    cursor.execute(sql)
    cursor.commit()

    # Generate the nodes.
    nodes = []
    nodes.append({ 'id': '_0', 'name': '' })
    sql = f"SELECT id, nodeid FROM {nodetable} ORDER BY nodeid"
    print(sql) if DEBUGGING else None
    cursor.execute(sql)
    rows = cursor.fetchall()
    for r in rows:
        node = { 'id': r[0], 'name': r[0][3:] }
        nodes.append(node)
    print(nodes) if DEBUGGING else None

    # Generate the links.
    links = []
    for level in range(1, 3):
        sql = f"""SELECT x.h{level}, x.h{level+1}, id{level}.nodeid source, id{level+1}.nodeid target, COUNT(*) value
            FROM {leafnodes} x
            LEFT JOIN {nodetable} id{level} ON x.h{level} = id{level}.id
            LEFT JOIN {nodetable} id{level+1} ON x.h{level+1} = id{level+1}.id
            GROUP BY h{level}, h{level+1}, id{level}.nodeid, id{level+1}.nodeid
            ORDER BY h{level}, h{level+1}, id{level}.nodeid, id{level+1}.nodeid"""
        print(sql) if DEBUGGING else None
        cursor.execute(sql)
        rows = cursor.fetchall()
        for r in rows:
            link = { 'source': r[2], 'target': r[3], 'value': r[4] }
            links.append(link)
    print(links) if DEBUGGING else None

    # Compose JSON and save to file.
    combined = { 'nodes': nodes, 'links': links }
    with open(f'course.json', 'w') as f:
        json.dump(combined, f)

    return

""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
""" Main """
def main():
    args = parse_args()
    if args is None:
        print('Problem!')
        exit()

    if args.fn == 'sankey_gl':
        sankey_gl(
            temptable='#old_dec13',
            sourcetable='TmsEPrd.dbo.GL_MASTER',
            sourcefield='ACCT_CDE',
            outputprefix='old_'
        )
        sankey_gl(
            temptable='#new_dec13',
            sourcetable='glconv.dbo.GL_MASTER_DEC13',
            sourcefield='ACCT_CDE',
            outputprefix='new_'
        )
    elif args.fn == 'tree_components':
        tree_components(
            temptable='#new_dec13',
            sourcetable='glconv.dbo.GL_MASTER_DEC13',
            sourcefield='ACCT_CDE',
            outputprefix='new_'
        )
    elif args.fn == 'sankey_course':
        sankey_course(
            leafnodes='#leafnodes',
            nodetable='#nodes',
            linktable='#links'
        )

    return True

if __name__ == '__main__':
    config = Configs()
    main()