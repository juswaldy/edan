# Integration/Data Hub

This module provides the following functionalities:

- Messaging queue (publisher-subscriber pattern)
- Mapping of entities between systems

