-- ================================================
-- Template generated from Template Explorer using:
-- Create DataChange Trigger
--
-- Use the Specify Values for Template Parameters 
-- command (Ctrl-Shift-M) to fill in the parameter 
-- values below.
--
-- See additional Create Trigger templates for more
-- examples of different Trigger statements.
--
-- This block of comments will not be included in
-- the definition of the function.
-- ================================================
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ================================================
-- Author:      Juswaldy Jusman
-- Create date: 2016-07-28
-- Description: Send an outbound message from TmsEPrd
--              whenever STUDENT_MASTER.RESID_COMMUTER_STS
--              changes.
-- ================================================

USE [TmsEPrd]
GO

IF OBJECT_ID ('dbo.STUDENT_MASTER_HousingChangeTrigger','TR') IS NOT NULL
	DROP TRIGGER dbo.STUDENT_MASTER_HousingChangeTrigger
GO

CREATE TRIGGER [dbo].[STUDENT_MASTER_HousingChangeTrigger] ON [dbo].[STUDENT_MASTER]
AFTER INSERT, UPDATE
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	DECLARE @message XML;
	DECLARE @inserted NVARCHAR(MAX) = '';
	DECLARE @deleted NVARCHAR(MAX) = '';
	DECLARE @eventType CHAR(6) = 'Insert';
	DECLARE @documentName NVARCHAR(128) = N'JenzabarDataChange';

	-- Only do something if there is a change in Housing status.
	IF EXISTS (
			SELECT    i.*
			FROM      inserted i
			LEFT JOIN deleted d ON i.ID_NUM = d.ID_NUM
			WHERE     i.USER_NAME NOT IN (SELECT Username FROM dbo.TWU_FN_GET_SYNC_USERNAMES())
			AND (
				d.ID_NUM IS NULL
				OR
				COALESCE(i.RESID_COMMUTER_STS, '') != COALESCE(d.RESID_COMMUTER_STS, '')
			)
	)
	BEGIN

	BEGIN TRY
		-- Set @eventType based on whether inserted and deleted contain records or not
		IF EXISTS(SELECT * FROM deleted)
		BEGIN
			SET @eventType = CASE WHEN EXISTS(SELECT * FROM inserted) THEN 'Update' ELSE 'Delete' END;
		END

		-- Create @inserted message component from contents of inserted
		-- Only if there is a change in Housing status.
		IF EXISTS(SELECT * FROM inserted)
		BEGIN
			SELECT  @inserted = (
				SELECT    i.*
				FROM      inserted i
				LEFT JOIN deleted d ON i.ID_NUM = d.ID_NUM
				WHERE     i.USER_NAME NOT IN (SELECT Username FROM dbo.TWU_FN_GET_SYNC_USERNAMES())
				AND       COALESCE(i.RESID_COMMUTER_STS, '') != COALESCE(d.RESID_COMMUTER_STS, '')
				FOR XML PATH('STUDENT_MASTER'), ELEMENTS, ROOT('Inserted')
			)
		END

		-- Create @deleted message component from contents of deleted
		IF @eventType = 'Update' AND EXISTS(SELECT * FROM inserted WHERE USER_NAME NOT IN (SELECT Username FROM dbo.TWU_FN_GET_SYNC_USERNAMES()))
		BEGIN
			-- If an Update event, join with inserted rows whose USER_NAME is not a Sync User
			-- Also do this only if there is a change in Housing status, matching the @inserted component.
			SELECT  @deleted = (
				SELECT  d.*
				FROM    deleted d
				JOIN    inserted i ON d.ID_NUM = i.ID_NUM
				WHERE   i.USER_NAME NOT IN (SELECT Username FROM dbo.TWU_FN_GET_SYNC_USERNAMES())
				AND     COALESCE(i.RESID_COMMUTER_STS, '') != COALESCE(d.RESID_COMMUTER_STS, '')
				FOR XML PATH('STUDENT_MASTER'), ELEMENTS, ROOT('Deleted')
			)
		END
		ELSE
		BEGIN
			-- Otherwise, only create @deleted component if USER_NAME is not a Sync User
			IF @eventType = 'Delete' AND EXISTS(SELECT * FROM deleted WHERE USER_NAME NOT IN (SELECT Username FROM dbo.TWU_FN_GET_SYNC_USERNAMES()))
			BEGIN
				SELECT  @deleted = (
					SELECT  *
					FROM    deleted
					WHERE   USER_NAME NOT IN (SELECT Username FROM dbo.TWU_FN_GET_SYNC_USERNAMES())
					FOR XML PATH('STUDENT_MASTER'), ELEMENTS, ROOT('Deleted')
				)
			END
		END

		-- If an actual user made a change then send a message,
		-- however, if a sync system made a change then do nothing further.
		IF (NOT(@inserted = '' AND @deleted = ''))
		BEGIN
			-- Build the @message
			SELECT  @message = N'<' + @documentName + N'><Topic>'
			                 + N'Jenzabar.HousingStatus.' + @eventType
			                 + N'</Topic><'
			                 + N'STUDENT_MASTERs'
			                 + N'>' + @inserted + @deleted + N'</'
			                 + N'STUDENT_MASTERs'
			                 + N'></' + @documentName + N'>';

			-- Start a Service Broker conversation dialog
			DECLARE @h UNIQUEIDENTIFIER;
			BEGIN DIALOG @h
				FROM SERVICE [JenzabarDataChangeStatusSubscriber]
				TO SERVICE N'JenzabarDataChangeSubscriber'
				ON CONTRACT [//www.twu.ca/integration/contracts/JenzabarDataChange/v1.0]
				WITH ENCRYPTION = OFF;

			-- Send the message through Service Broker
			SEND ON CONVERSATION @h
				MESSAGE TYPE [//www.twu.ca/integration/messages/JenzabarDataChange]
				(@message);
		END;
	END TRY
	BEGIN CATCH
		-- Supress any errors that result from our Service Broker conversation
		-- Execute standard error handler
	END CATCH;

	END;
END;
GO

