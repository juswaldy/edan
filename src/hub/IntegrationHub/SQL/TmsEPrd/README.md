# We have moved to a new location!

These scripts have been moved to the JenzabarEX repo: [https://github.com/TrinityWestern/JenzabarEX/tree/master/JenzabarEX/JenzabarEX/dbo](https://github.com/TrinityWestern/JenzabarEX/tree/master/JenzabarEX/JenzabarEX/dbo)
- CREATE SERVICE JenzabarDataChangeStatusSubscriber ON QUEUE Sync.JenzabarDataChangeStatusSubscriberQueue.sql
- CREATE TRIGGER dbo.STUD_SESS_ASSIGN_HousingChangeTrigger ON dbo.STUD_SESS_ASSIGN AFTER UPDATE INSERT.sql
- CREATE TRIGGER dbo.SUBSID_MASTER_FirstTuitionPaymentChangeTrigger ON dbo.SUBSID_MASTER AFTER INSERT UPDATE.sql

The only script left in here is `CREATE TRIGGER dbo.STUDENT_MASTER_HousingChangeTrigger ON dbo.STUDENT_MASTER AFTER UPDATE INSERT.sql`, for historical background. This trigger has been moved from the table `STUDENT_MASTER` to `STUD_SESS_ASSIGN`.

