-- ===================================
-- Create Unique Nonclustered Index
-- ===================================
USE IntegrationHub
GO

SET QUOTED_IDENTIFIER ON
GO

IF EXISTS (SELECT * FROM sys.indexes WHERE name = N'AK_Mapping_Note_CvwTaskId')
	DROP INDEX AK_Mapping_Note_CvwTaskId
	ON Mapping.Note
GO

CREATE UNIQUE NONCLUSTERED INDEX AK_Mapping_Note_CvwTaskId
ON Mapping.Note
(
	CvwTaskId ASC
)
WHERE
	CvwTaskId IS NOT NULL
WITH
(
	SORT_IN_TEMPDB = OFF,
	DROP_EXISTING = OFF
)
GO

EXEC sys.sp_addextendedproperty
	@name=N'MS_Description',
	@value=N'Ensure uniqueness of Causeview Task identifiers.',
	@level0type=N'SCHEMA',
	@level0name=N'Mapping',
	@level1type=N'TABLE',
	@level1name=N'Note',
	@level2type=N'INDEX',
	@level2name=N'AK_Mapping_Note_CvwTaskId'
GO
