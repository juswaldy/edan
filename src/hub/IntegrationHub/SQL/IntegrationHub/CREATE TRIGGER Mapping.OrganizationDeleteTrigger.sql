/*
 * Trigger:     IntegrationHub.Mapping.OrganizationDeleteTrigger
 *
 * Author:      Juswaldy Jusman
 * Created:     2016-06-07
 * Description: A trigger to copy deleted Organization records to the version archive.
 *
 */
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ================================================
-- Author:      Juswaldy Jusman
-- Create date: 2016-06-07
-- Description: Copy deleted Organization records to the version archive table.
-- ================================================
USE [IntegrationHub]
GO

IF OBJECT_ID ('Mapping.OrganizationDeleteTrigger','TR') IS NOT NULL
	DROP TRIGGER Mapping.OrganizationDeleteTrigger
GO

CREATE TRIGGER [Mapping].[OrganizationDeleteTrigger] ON [Mapping].[Organization]
FOR DELETE
AS
DECLARE @OperatorId NVARCHAR(254)
BEGIN
	SELECT  @OperatorId = LastModifier FROM DELETED
	INSERT INTO Mapping.OrganizationVersionArchive 
	([OrganizationId], [AQOrganizationId], [ERxSchoolId], [CvwAccountId], [Name], [LastModified], [LastModifier], Operation, OperatorId, OperationDate)
	SELECT  [OrganizationId], [AQOrganizationId], [ERxSchoolId], [CvwAccountId], [Name], [LastModified], [LastModifier], 'Delete', @OperatorId, CURRENT_TIMESTAMP
	FROM    DELETED
END
GO
