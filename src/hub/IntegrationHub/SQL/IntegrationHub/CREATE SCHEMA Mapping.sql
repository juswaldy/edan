/*
 * Schema:      IntegrationHub.Mapping
 *
 * Author:      Andrew Menary
 * Created:     2016-05-19
 * Description: A schema to collect together objects related to data mapping between systems.
 *
 */

USE IntegrationHub
GO

IF NOT EXISTS (SELECT * FROM sys.schemas WHERE name = N'Mapping')
BEGIN
	DECLARE @sql NVARCHAR(256) = N'CREATE SCHEMA [Mapping];'
	EXECUTE sp_executesql @sql;

	IF NOT EXISTS (SELECT * FROM sys.schemas WHERE name = N'Mapping')
		RAISERROR('*** FAILED CREATING SCHEMA %s ***', 10, 1, '[Mapping]') WITH NOWAIT
	ELSE
		RAISERROR('*** CREATED SCHEMA %s ***', 10, 1, '[Mapping]') WITH NOWAIT
END
ELSE
	RAISERROR('*** SKIPPED CREATING EXISTING SCHEMA %s ***', 10, 1, '[Mapping]') WITH NOWAIT
GO
