/*
 * Procedure: IntegrationHub.Mapping.MapNote_AQ_ERx
 */

USE IntegrationHub
GO

-- Drop stored procedure if it already exists
IF EXISTS (
    SELECT  *
    FROM    INFORMATION_SCHEMA.ROUTINES
    WHERE   SPECIFIC_SCHEMA = N'Mapping'
    AND     SPECIFIC_NAME = N'MapNote_AQ_ERx'
)
    DROP PROCEDURE Mapping.MapNote_AQ_ERx
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================================================
-- Author:      Juswaldy Jusman
-- Create date: 2018-15
-- Description: Create or update a mapping between Aqueduct Note and ERx Task.
-- Returns:     Aqueduct Note Id, ERx Task Id, Return Code.
--              Return Code 0 means nothing was changed.
--              Return Code 1 means a new mapping was created.
--              Return Code 2 means an existing mapping was updated.
-- 2017-02-07 JJJ Allow for remapping of IDs.
-- =============================================================================
CREATE PROCEDURE Mapping.MapNote_AQ_ERx
	@AQNoteId     INT,
	@ERxTaskId    VARCHAR(18),
	@LastModifier NVARCHAR(254)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	-- SET XACT_ABORT ON to ensure any existing transaction
	-- and not just the current statement is rolled back if
	-- there is a run-time error.
	SET XACT_ABORT ON;

	DECLARE	@Now               DATETIME,
			@ReturnCode        INT = 0, -- Default to success.
			@ErrorNumber       INT = NULL,
			@ErrorMessage      VARCHAR(MAX) = NULL,
			@NumberOfMatches   INT = 0,

			@_NoteId           BIGINT,
			@_AQNoteId         INT,
			@_ERxTaskId        VARCHAR(18)
			;

	SET @Now = GETUTCDATE();

	BEGIN TRY
		DECLARE @hasOuterTransaction BIT = CASE WHEN @@TRANCOUNT > 0 THEN 1 ELSE 0 END;
		DECLARE @rollbackPoint NCHAR(32) = REPLACE(CONVERT(NCHAR(36), NEWID()), N'-', N'');

		IF @hasOuterTransaction = 1
		BEGIN
			SAVE TRANSACTION @rollbackPoint;
		END
		ELSE
		BEGIN
			BEGIN TRANSACTION @rollbackPoint;
		END;

		-- Trim and convert blank strings to null.
		IF RTRIM(@ERxTaskId) = '' SET @ERxTaskId = NULL ELSE SET @ERxTaskId = RTRIM(LTRIM(@ERxTaskId));
		IF RTRIM(@LastModifier) = '' SET @LastModifier = NULL ELSE SET @LastModifier = RTRIM(LTRIM(@LastModifier));

		-- Validate parameters.
		IF @AQNoteId IS NULL AND @ERxTaskId IS NULL
			THROW 50001, N'Bad mapping: Both ID''s are null', 1;

		IF @LastModifier IS NULL
			THROW 50001, N'Bad mapping: LastModifier parameter is null', 1;

		-- Remap a Note if necessary.
		IF @AQNoteId IS NOT NULL
		BEGIN
			-- Count the number of ID matches.
			SELECT @NumberOfMatches = COUNT(*)
			FROM Mapping.Note
			WHERE AQNoteId = @AQNoteId
			OR ERxTaskId = @ERxTaskId;

			-- If exactly 2 matches, remap.
			IF @NumberOfMatches = 2
			BEGIN
				-- Blank out existing mapping.
				UPDATE Mapping.Note SET AQNoteId = NULL
				WHERE AQNoteId = @AQNoteId;

				-- Map to a different person.
				UPDATE Mapping.Note SET AQNoteId = @AQNoteId
				WHERE ERxTaskId = @ERxTaskId;
			END
		END

		-- Count the number of ID matches.
		SELECT @NumberOfMatches = COUNT(*)
		FROM Mapping.Note
		WHERE AQNoteId = @AQNoteId
		OR ERxTaskId = @ERxTaskId;

		-- If more than one matches.
		IF @NumberOfMatches > 1
		BEGIN
			-- If more than one matches, throw error.
			THROW 50002, N'Bad mapping: Matched more than one ID''s', 1;
		END

		-- If no mapping exists.
		IF @NumberOfMatches = 0
		BEGIN
			-- If no mapping exists, then insert a new mapping row.
			INSERT INTO Mapping.Note
				(AQNoteId, ERxTaskId, LastModified, LastModifier)
			VALUES
				(@AQNoteId, @ERxTaskId, @Now, @LastModifier);

			-- Return Code 1 means a new mapping was created.
			SET @ReturnCode = 1;
		END
		ELSE IF @NumberOfMatches = 1
		BEGIN
			-- If a single mapping exists, retrieve it.
			SELECT
				@_NoteId = NoteId,
				@_AQNoteId = AQNoteId,
				@_ERxTaskId = ERxTaskId
			FROM Mapping.Note
			WHERE AQNoteId = @AQNoteId
			OR ERxTaskId = @ERxTaskId;

			-- Diff with input params.
			IF COALESCE(@_AQNoteId, 0) = COALESCE(@AQNoteId, 0)
				AND COALESCE(@_ERxTaskId, '') = COALESCE(@ERxTaskId, '')
			BEGIN
				-- If no changes, do nothing.
				SET @ReturnCode = 0 -- Return Code 0 means nothing was changed.
			END
			ELSE
-- 2017-02-07 JJJ Allow for remapping of IDs.
--			-- Otherwise, one of these conditions has to be true:
--			-- 1. Both the ID's must match, or
--			-- 2. One of the database ID's must be null, or
--			-- 3. One of the supplied ID's must be null
--			IF (@_AQNoteId = @AQNoteId AND @_ERxTaskId = @ERxTaskId)
--				OR @_AQNoteId IS NULL
--				OR @_ERxTaskId IS NULL
--				OR @AQNoteId IS NULL
--				OR @ERxTaskId IS NULL
			BEGIN
				-- Set null parameters to db values.
				IF @AQNoteId IS NULL SET @AQNoteId = @_AQNoteId
				IF @ERxTaskId IS NULL SET @ERxTaskId = @_ERxTaskId

				-- Update the mapping table.
				UPDATE Mapping.Note SET
					AQNoteId = @AQNoteId,
					ERxTaskId = @ERxTaskId,
					LastModified = @Now,
					LastModifier = @LastModifier
				WHERE NoteId = @_NoteId;

				-- Return Code 2 means an existing mapping was updated.
				SET @ReturnCode = 2;
			END
-- 2017-02-07 JJJ Allow for remapping of IDs.
--			ELSE
--			BEGIN
--				-- If one or more ID's don't match, throw error.
--				THROW 50003, N'Bad mapping: ID values have changed', 1;
--			END
		END
		ELSE
		BEGIN
			-- The number of matches is negative, throw error.
			THROW 50004, N'Bad mapping: Negative number of matches found', 1;
		END

		IF @hasOuterTransaction = 0
		BEGIN
			COMMIT TRANSACTION;
		END;
	END TRY
	BEGIN CATCH
		IF XACT_STATE() = 1
		BEGIN
			ROLLBACK TRANSACTION @rollbackPoint;
		END;

		-- Execute standard error handler
		SET @ErrorNumber = ERROR_NUMBER() + 50000;
		SET @ErrorMessage = 'MapNote_AQ_ERx: ' + ERROR_MESSAGE();
		THROW @ErrorNumber, @ErrorMessage, 1;
	END CATCH;

	-- Return result set.
	SELECT @AQNoteId, @ERxTaskId, @ReturnCode;
END
GO

/* If stored procedure created successfully then add permissions */
IF EXISTS (
    SELECT  *
    FROM    INFORMATION_SCHEMA.ROUTINES
    WHERE   SPECIFIC_SCHEMA = N'Mapping'
    AND     SPECIFIC_NAME = N'MapNote_AQ_ERx'
)
BEGIN
	IF EXISTS (SELECT * FROM sys.sysusers WHERE name = N'SalesforceSyncDEV')
	GRANT EXECUTE ON Mapping.MapNote_AQ_ERx TO SalesforceSyncDEV

	IF EXISTS (SELECT * FROM sys.sysusers WHERE name = N'SalesforceSyncTEST')
	GRANT EXECUTE ON Mapping.MapNote_AQ_ERx TO SalesforceSyncTEST
 
	IF EXISTS (SELECT * FROM sys.sysusers WHERE name = N'SalesforceSync')
	GRANT EXECUTE ON Mapping.MapNote_AQ_ERx TO SalesforceSync

	IF EXISTS (SELECT * FROM sys.sysusers WHERE name = N'cloveretl')
	GRANT EXECUTE ON Mapping.MapNote_AQ_ERx TO cloveretl
END
GO
