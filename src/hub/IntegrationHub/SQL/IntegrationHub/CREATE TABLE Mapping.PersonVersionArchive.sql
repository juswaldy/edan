/*
 * Table:       IntegrationHub.Mapping.PersonVersionArchive
 *
 * Author:      Andrew Menary
 * Created:     2016-05-20
 * Description: A version archive of the mapping table for Person identifiers across multiple systems.
 *
 */

USE IntegrationHub
GO

IF OBJECT_ID(N'[Mapping].[PersonVersionArchive]', N'U') IS NULL
BEGIN
	CREATE TABLE [Mapping].[PersonVersionArchive](
	        [PersonVersionArchiveId] BIGINT NOT NULL IDENTITY(1,1),
	        [PersonId]               BIGINT NOT NULL,
	        [AQPersonId]             INT NULL,
	        [ERxContactId]           CHAR(18) NULL,
	        [CvwIndividualId]        CHAR(18) NULL,
	        [Name]                   NVARCHAR(254) NULL,
	        [LastModified]           DATETIME NOT NULL,
	        [LastModifier]           NVARCHAR(254) NOT NULL,
	        [Operation]              VARCHAR(7) NOT NULL,
	        [OperatorId]             NVARCHAR(254) NOT NULL,
	        [OperationDate]          DATETIME NOT NULL
	                CONSTRAINT DF_MappingPersonVersionArchive_OperationDate DEFAULT (CURRENT_TIMESTAMP),

	CONSTRAINT [PK_MappingPersonVersionArchive]
	        PRIMARY KEY CLUSTERED ([PersonVersionArchiveId]) ON [HISTORIC]
	) ON [HISTORIC]

	IF OBJECT_ID(N'[Mapping].[PersonVersionArchive]', N'U') IS NOT NULL
		RAISERROR('*** CREATED TABLE %s.%s ***', 10, 1, '[Mapping]', '[PersonVersionArchive]') WITH NOWAIT
	ELSE
		RAISERROR('*** FAILED CREATING TABLE %s.%s ***', 10, 1, '[Mapping]', '[PersonVersionArchive]') WITH NOWAIT

	IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description', N'SCHEMA', N'Mapping', N'TABLE', N'PersonVersionArchive', NULL, NULL))
		EXEC sys.sp_addextendedproperty @name=N'MS_Description',
		        @value=N'A version archive of the mapping table for Person identifiers across multiple systems.',
		        @level0type=N'SCHEMA', @level0name=N'Mapping',
		        @level1type=N'TABLE', @level1name=N'PersonVersionArchive'
END
ELSE
	RAISERROR('*** SKIPPED CREATING EXISTING TABLE %s.%s ***', 10, 1, '[Mapping]', '[PersonVersionArchive]') WITH NOWAIT
GO
