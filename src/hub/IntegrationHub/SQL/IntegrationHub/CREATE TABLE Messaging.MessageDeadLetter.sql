/*
 * Table:       IntegrationHub.Messaging.MessageDeadLetter
 *
 * Author:      Andrew Menary
 * Created:     2016-05-20
 * Description: Store for data synchronization messages that expired
 *              or otherwise could not be processed.
 *
 */

USE IntegrationHub
GO

IF OBJECT_ID(N'[Messaging].[MessageDeadLetter]', N'U') IS NULL
BEGIN
	CREATE TABLE [Messaging].[MessageDeadLetter](
	        [DeadLetterId]  BIGINT NOT NULL IDENTITY(1,1),
	        [Id]            BIGINT NOT NULL,
	        [CorrelationId] BIGINT NOT NULL,
	        [Due]           DATETIME NOT NULL,
	        [Expires]       DATETIME NOT NULL,
	        [Deceased]      DATETIME NOT NULL
	                CONSTRAINT DF_MessagingMessageDeadLetter_Deceased DEFAULT (GETUTCDATE()),
	        [Topic]         VARCHAR(254) NULL,
	        [Payload]       XML,

	CONSTRAINT [PK_MessagingDeadLetterId]
	        PRIMARY KEY CLUSTERED ([DeadLetterId]) ON [HISTORIC]
	) ON [HISTORIC]

	IF OBJECT_ID(N'[Messaging].[MessageDeadLetter]', N'U') IS NOT NULL
	BEGIN
		RAISERROR('*** CREATED TABLE %s.%s ***', 10, 1, '[Messaging]', '[MessageDeadLetter]') WITH NOWAIT
	END
	ELSE
		RAISERROR('*** FAILED CREATING TABLE %s.%s ***', 10, 1, '[Messaging]', '[MessageDeadLetter]') WITH NOWAIT

	IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description', N'SCHEMA', N'Messaging', N'TABLE', N'MessageDeadLetter', NULL, NULL))
		EXECUTE sys.sp_addextendedproperty @name=N'MS_Description',
		        @value=N'Store for data synchronization messages that expired or otherwise could not be processed',
		        @level0type=N'SCHEMA', @level0name=N'Messaging',
		        @level1type=N'TABLE', @level1name=N'MessageDeadLetter'
END
ELSE
	RAISERROR('*** SKIPPED CREATING EXISTING TABLE %s.%s ***', 10, 1, '[Messaging]', '[MessageDeadLetter]') WITH NOWAIT
GO
