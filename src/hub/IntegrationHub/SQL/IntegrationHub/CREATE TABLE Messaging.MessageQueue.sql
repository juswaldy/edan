/*
 * Table:       IntegrationHub.Messaging.MessageQueue
 *
 * Author:      Andrew Menary
 * Created:     2016-05-20
 * Description: Message queue for pending data synchronization messages from/to connected systems
 *
 */

USE IntegrationHub
GO

IF OBJECT_ID(N'[Messaging].[MessageQueue]', N'U') IS NULL
BEGIN
	CREATE TABLE [Messaging].[MessageQueue](
	        [Id]            BIGINT NOT NULL IDENTITY(1,1),
	        [CorrelationId] BIGINT NOT NULL,
	        [Due]           DATETIME NOT NULL
	                CONSTRAINT DF_MessagingMessageQueue_Due DEFAULT (GETUTCDATE()),
	        [Expires]       DATETIME NOT NULL
	                CONSTRAINT DF_MessagingMessageQueue_Expires DEFAULT (DATEADD(day, 1, GETUTCDATE())),
	        [Topic]         VARCHAR(254) NULL,
	        [Payload]       XML
	)

	IF OBJECT_ID(N'[Messaging].[MessageQueue]', N'U') IS NOT NULL
	BEGIN
		CREATE CLUSTERED INDEX [IX_MessagingMessageQueue_Due_Topic] ON [Messaging].[MessageQueue] ([Due], [Topic])
		RAISERROR('*** CREATED TABLE %s.%s ***', 10, 1, '[Messaging]', '[MessageQueue]') WITH NOWAIT
	END
	ELSE
		RAISERROR('*** FAILED CREATING TABLE %s.%s ***', 10, 1, '[Messaging]', '[MessageQueue]') WITH NOWAIT

	IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description', N'SCHEMA', N'Messaging', N'TABLE', N'MessageQueue', NULL, NULL))
		EXECUTE sys.sp_addextendedproperty @name=N'MS_Description',
		        @value=N'Message queue for pending data synchronization messages from/to connected systems',
		        @level0type=N'SCHEMA', @level0name=N'Messaging',
		        @level1type=N'TABLE', @level1name=N'MessageQueue'
END
ELSE
	RAISERROR('*** SKIPPED CREATING EXISTING TABLE %s.%s ***', 10, 1, '[Messaging]', '[MessageQueue]') WITH NOWAIT
GO
