-- ================================================
-- Template generated from Template Explorer using:
-- Create Procedure (New Menu).SQL
--
-- Use the Specify Values for Template Parameters 
-- command (Ctrl-Shift-M) to fill in the parameter 
-- values below.
--
-- This block of comments will not be included in
-- the definition of the procedure.
-- ================================================

USE IntegrationHub
GO

-- Drop stored procedure if it already exists
IF EXISTS (
  SELECT *
    FROM INFORMATION_SCHEMA.ROUTINES
   WHERE SPECIFIC_SCHEMA = N'Logging'
     AND SPECIFIC_NAME = N'ClearTrackingAqueductDataChangeMessage'
)
   DROP PROCEDURE Logging.ClearTrackingAqueductDataChangeMessage
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:      Andrew Menary
-- Create date: 2016-06-15
-- Description: Clear the tracking count for failed
--              messages used to handle poison messages.
-- =============================================
CREATE PROCEDURE Logging.ClearTrackingAqueductDataChangeMessage
	@dialog UNIQUEIDENTIFIER,
	@poisoned BIT = 0
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	-- If it is a poisoned message preserve it in the poisoned message table
	-- either way, remove it from the failed message table.
	IF (@poisoned > 0)
	BEGIN
		WITH cte AS (
			SELECT  Dialog,
					MessageType,
					MessageBody,
					LastModified
			FROM    Logging.AqueductDataChangeFailedMessage WITH (ROWLOCK, READPAST)
			WHERE   Dialog = @dialog
		)
		DELETE FROM cte
			OUTPUT  deleted.Dialog,
					deleted.MessageType,
					deleted.MessageBody,
					deleted.LastModified
			INTO    Logging.AqueductDataChangePoisonedMessage
	END
	ELSE
	BEGIN
		DELETE FROM Logging.AqueductDataChangeFailedMessage
		WHERE   Dialog = @dialog
	END
END
GO
