/*
 * Table:       IntegrationHub.Mapping.Application
 *
 * Author:      Juswaldy Jusman
 * Created:     2016-06-09
 * Description: A mapping table for Admissions Application identifiers across multiple systems.
 * Updates:
 * 2016-07-29 JJJ Add AQCandidacyId field.
 *
 */

USE IntegrationHub
GO

IF OBJECT_ID(N'[Mapping].[Application]', N'U') IS NULL
BEGIN
	CREATE TABLE [Mapping].[Application](
	        [ApplicationId]    BIGINT NOT NULL IDENTITY(1,1),
	        [AQApplicationId]  INT NULL,
	        [AQCandidacyId]    INT NULL,
	        [ERxApplicationId] CHAR(18) NULL,
	        [ERxApplicantId]   CHAR(18) NULL,
	        [Name]             NVARCHAR(254) NULL,
	        [LastModified]     DATETIME NOT NULL
	                CONSTRAINT DF_MappingApplication_LastModified DEFAULT (CURRENT_TIMESTAMP),
	        [LastModifier]     NVARCHAR(254) NOT NULL

	CONSTRAINT [PK_MappingApplication]
	        PRIMARY KEY CLUSTERED ([ApplicationId])
	)

	IF OBJECT_ID(N'[Mapping].[Application]', N'U') IS NOT NULL
		RAISERROR('*** CREATED TABLE %s.%s ***', 10, 1, '[Mapping]', '[Application]') WITH NOWAIT
	ELSE
		RAISERROR('*** FAILED CREATING TABLE %s.%s ***', 10, 1, '[Mapping]', '[Application]') WITH NOWAIT

	IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description', N'SCHEMA', N'Mapping', N'TABLE', N'Application', NULL, NULL))
		EXEC sys.sp_addextendedproperty @name=N'MS_Description',
		        @value=N'A mapping table for Application identifiers across multiple systems.',
		        @level0type=N'SCHEMA', @level0name=N'Mapping',
		        @level1type=N'TABLE', @level1name=N'Application'
END
ELSE
	RAISERROR('*** SKIPPED CREATING EXISTING TABLE %s.%s ***', 10, 1, '[Mapping]', '[Application]') WITH NOWAIT
GO
