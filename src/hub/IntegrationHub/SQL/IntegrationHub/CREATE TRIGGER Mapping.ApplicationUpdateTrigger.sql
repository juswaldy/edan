/*
 * Trigger:     IntegrationHub.Mapping.ApplicationUpdateTrigger
 *
 * Author:      Juswaldy Jusman
 * Created:     2016-06-09
 * Description: A trigger to copy updated Application mappings to the version archive.
 *
 */
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ================================================
-- Author:      Juswaldy Jusman
-- Create date: 2016-06-09
-- Description: Copy updated Application mappings to the version archive table.
-- ================================================
USE [IntegrationHub]
GO

IF OBJECT_ID ('Mapping.ApplicationUpdateTrigger','TR') IS NOT NULL
	DROP TRIGGER Mapping.ApplicationUpdateTrigger
GO

CREATE TRIGGER [Mapping].[ApplicationUpdateTrigger] ON [Mapping].[Application]
FOR UPDATE
AS
DECLARE @OperatorId NVARCHAR(254)
BEGIN
	SELECT  @OperatorId = LastModifier FROM INSERTED
	INSERT INTO Mapping.ApplicationVersionArchive
	([ApplicationId], [AQApplicationId], [AQCandidacyId], [ERxApplicationId], [ERxApplicantId], [Name], [LastModified], [LastModifier], Operation, OperatorId, OperationDate)
	SELECT  [ApplicationId], [AQApplicationId], [AQCandidacyId], [ERxApplicationId], [ERxApplicantId], [Name], [LastModified], [LastModifier], 'Update', @OperatorId, CURRENT_TIMESTAMP
	FROM    DELETED
END
GO
