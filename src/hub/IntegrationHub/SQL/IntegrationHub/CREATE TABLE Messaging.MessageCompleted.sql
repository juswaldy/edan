/*
 * Table:       IntegrationHub.Messaging.MessageCompleted
 *
 * Author:      Andrew Menary
 * Created:     2016-05-20
 * Description: Message store for data synchronization messages from/to connected systems that have been handled
 *
 */

USE IntegrationHub
GO

IF OBJECT_ID(N'[Messaging].[MessageCompleted]', N'U') IS NULL
BEGIN
	CREATE TABLE [Messaging].[MessageCompleted](
	        [CompletedId]   BIGINT NOT NULL IDENTITY(1,1),
	        [Id]            BIGINT NOT NULL,
	        [CorrelationId] BIGINT NOT NULL,
	        [Due]           DATETIME NOT NULL,
	        [Expires]       DATETIME NOT NULL,
	        [Locked]        DATETIME NOT NULL,
	        [Ended]         DATETIME NOT NULL
	                CONSTRAINT DF_MessagingMessageCompleted_Ended DEFAULT (GETUTCDATE()),
	        [RunId]         VARCHAR(255) NULL,
	        [ErrorNumber]   INTEGER NULL,
	        [ErrorMessage]  NVARCHAR(MAX) NULL,
	        [Topic]         VARCHAR(255) NULL,
	        [Payload]       XML,

	CONSTRAINT [PK_MessagingMessageCompleted]
	        PRIMARY KEY CLUSTERED ([CompletedId])
	) ON HISTORIC

	IF OBJECT_ID(N'[Messaging].[MessageCompleted]', N'U') IS NOT NULL
		RAISERROR('*** CREATED TABLE %s.%s ***', 10, 1, '[Messaging]', '[MessageCompleted]') WITH NOWAIT
	ELSE
		RAISERROR('*** FAILED CREATING TABLE %s.%s ***', 10, 1, '[Messaging]', '[MessageCompleted]') WITH NOWAIT

	IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description', N'SCHEMA', N'Messaging', N'TABLE', N'MessageCompleted', NULL, NULL))
		EXECUTE sys.sp_addextendedproperty @name=N'MS_Description',
		        @value=N'Message store for data synchronization messages from/to connected systems that have been handled',
		        @level0type=N'SCHEMA', @level0name=N'Messaging',
		        @level1type=N'TABLE', @level1name=N'MessageCompleted'
END
ELSE
	RAISERROR('*** SKIPPED CREATING EXISTING TABLE %s.%s ***', 10, 1, '[Messaging]', '[MessageCompleted]') WITH NOWAIT
GO
