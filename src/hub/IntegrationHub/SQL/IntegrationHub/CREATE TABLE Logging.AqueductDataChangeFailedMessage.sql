/*
 * Table:       IntegrationHub.Logging.AqueductDataChangeFailedMessage
 *
 * Author:      Andrew Menary
 * Created:     2016-06-13
 * Description: A table to count the failed messages for poison message handling.
 *
 */

USE IntegrationHub
GO

IF OBJECT_ID(N'[Logging].[AqueductDataChangeFailedMessage]', N'U') IS NULL
BEGIN
	CREATE TABLE [Logging].[AqueductDataChangeFailedMessage](
	        [Dialog]       [UNIQUEIDENTIFIER] NOT NULL,
	        [MessageType]  [SYSNAME] NULL,
	        [MessageBody]  [XML] NULL,
	        [Count]        [SMALLINT] NOT NULL
	                CONSTRAINT DF_AqueductDataChangeFailedMessage_Count DEFAULT (0),
	        [LastModified] [DATETIME] NOT NULL
	                CONSTRAINT DF_AqueductDataChangeFailedMessage_LastModified DEFAULT (CURRENT_TIMESTAMP),

	CONSTRAINT [PK_LoggingAqueductDataChangeFailedMessage]
	        PRIMARY KEY CLUSTERED ([Dialog])
	)

	IF OBJECT_ID(N'[Logging].[AqueductDataChangeFailedMessage]', N'U') IS NOT NULL
		RAISERROR('*** CREATED TABLE %s.%s ***', 10, 1, '[Logging]', '[AqueductDataChangeFailedMessage]') WITH NOWAIT
	ELSE
		RAISERROR('*** FAILED CREATING TABLE %s.%s ***', 10, 1, '[Logging]', '[AqueductDataChangeFailedMessage]') WITH NOWAIT

	IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description', N'SCHEMA', N'Logging', N'TABLE', N'AqueductDataChangeFailedMessage', NULL, NULL))
		EXEC sys.sp_addextendedproperty @name=N'MS_Description',
		        @value=N'A table to count the failed messages for poison message handling.',
		        @level0type=N'SCHEMA', @level0name=N'Logging',
		        @level1type=N'TABLE', @level1name=N'AqueductDataChangeFailedMessage'
END
ELSE
	RAISERROR('*** SKIPPED CREATING EXISTING TABLE %s.%s ***', 10, 1, '[Logging]', '[AqueductDataChangeFailedMessage]') WITH NOWAIT
GO
