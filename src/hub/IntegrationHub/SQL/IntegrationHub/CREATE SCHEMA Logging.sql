/*
 * Schema:      IntegrationHub.Logging
 *
 * Author:      Andrew Menary
 * Created:     2016-06-13
 * Description: A schema for objects related to logging system activity.
 *
 */

USE IntegrationHub
GO

IF NOT EXISTS (SELECT * FROM sys.schemas WHERE name = N'Logging')
BEGIN
	DECLARE @sql NVARCHAR(256) = N'CREATE SCHEMA [Logging];'
	EXECUTE sp_executesql @sql;

	IF NOT EXISTS (SELECT * FROM sys.schemas WHERE name = N'Logging')
		RAISERROR('*** FAILED CREATING SCHEMA %s ***', 10, 1, '[Logging]') WITH NOWAIT
	ELSE
		RAISERROR('*** CREATED SCHEMA %s ***', 10, 1, '[Logging]') WITH NOWAIT
END
ELSE
	RAISERROR('*** SKIPPED CREATING EXISTING SCHEMA %s ***', 10, 1, '[Logging]') WITH NOWAIT
GO
