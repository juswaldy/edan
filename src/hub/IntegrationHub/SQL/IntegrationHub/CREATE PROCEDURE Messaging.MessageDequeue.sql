/*
 * Procedure: IntegrationHub.Messaging.MessageDequeue
 */

USE IntegrationHub
GO

-- Drop stored procedure if it already exists
IF EXISTS (
    SELECT  *
    FROM    INFORMATION_SCHEMA.ROUTINES
    WHERE   SPECIFIC_SCHEMA = N'Messaging'
    AND     SPECIFIC_NAME = N'MessageDequeue'
)
    DROP PROCEDURE Messaging.MessageDequeue
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:      Andrew Menary
-- Create date: 2016-05-20
-- Description: Pop a data synchronization message off the queue.
-- =============================================
CREATE PROCEDURE Messaging.MessageDequeue
	@Topic         VARCHAR(254),
	@RunId         VARCHAR(254) = NULL
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	-- SET XACT_ABORT ON to ensure any existing transaction
	-- and not just the current statement is rolled back if
	-- there is a run-time error.
	SET XACT_ABORT ON;

	DECLARE @Now          DATETIME,
	        @ReturnCode   INT = 0, -- Default to success.
	        @ErrorNumber  INT = NULL,
	        @ErrorMessage VARCHAR(MAX) = NULL;

	SET @Now = GETUTCDATE();

	BEGIN TRY
		DECLARE @hasOuterTransaction BIT = CASE WHEN @@TRANCOUNT > 0 THEN 1 ELSE 0 END;
		DECLARE @rollbackPoint NCHAR(32) = REPLACE(CONVERT(NCHAR(36), NEWID()), N'-', N'');

		IF @hasOuterTransaction = 1
		BEGIN
			SAVE TRANSACTION @rollbackPoint;
		END
		ELSE
		BEGIN
			BEGIN TRANSACTION @rollbackPoint;
		END;

		WITH cte AS (
			SELECT  TOP(1) Id,
			        CorrelationId,
			        Due,
			        Expires,
			        Topic,
			        Payload
			FROM    Messaging.MessageQueue WITH (ROWLOCK, READPAST)
			WHERE   Due < @Now
			AND     ISNULL(Topic, 'N/A') = ISNULL(@Topic, 'N/A')
			ORDER BY Due, Topic
		)
		DELETE FROM cte
			OUTPUT  deleted.Id,
			        deleted.CorrelationId,
			        deleted.Due,
			        deleted.Expires,
			        @Now AS Locked,
			        @RunId AS RunId,
			        deleted.Topic,
			        deleted.Payload
			INTO    Messaging.MessageInProcessQueue
			OUTPUT  @ReturnCode AS ReturnCode,
			        deleted.Id,
			        deleted.CorrelationId,
			        deleted.Topic,
			        deleted.Payload

		IF @hasOuterTransaction = 0
		BEGIN
			COMMIT TRANSACTION;
		END;
	END TRY
	BEGIN CATCH
		IF XACT_STATE() = 1
		BEGIN
			ROLLBACK TRANSACTION @rollbackPoint;
		END;

		-- Execute standard error handler
		SET @ErrorNumber = ERROR_NUMBER() + 50000;
		SET @ErrorMessage = ERROR_MESSAGE();
		THROW @ErrorNumber, @ErrorMessage, 1;
	END CATCH;
END
GO

/* If stored procedure created successfully then add permissions */
IF EXISTS (
    SELECT  *
    FROM    INFORMATION_SCHEMA.ROUTINES
    WHERE   SPECIFIC_SCHEMA = N'Messaging'
    AND     SPECIFIC_NAME = N'MessageDequeue'
)
BEGIN
	IF EXISTS (SELECT * FROM sys.sysusers WHERE name = N'SalesforceSyncDEV')
	GRANT EXECUTE ON Messaging.MessageDequeue TO SalesforceSyncDEV

	IF EXISTS (SELECT * FROM sys.sysusers WHERE name = N'SalesforceSyncTEST')
	GRANT EXECUTE ON Messaging.MessageDequeue TO SalesforceSyncTEST
 
	IF EXISTS (SELECT * FROM sys.sysusers WHERE name = N'SalesforceSync')
	GRANT EXECUTE ON Messaging.MessageDequeue TO SalesforceSync

	IF EXISTS (SELECT * FROM sys.sysusers WHERE name = N'cloveretl')
	GRANT EXECUTE ON Messaging.MessageDequeue TO cloveretl
END
GO
