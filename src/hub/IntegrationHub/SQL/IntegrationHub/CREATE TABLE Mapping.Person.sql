/*
 * Table:       IntegrationHub.Mapping.Person
 *
 * Author:      Andrew Menary
 * Created:     2016-05-19
 * Description: A mapping table for Person identifiers across multiple systems.
 *
 */

USE IntegrationHub
GO

IF OBJECT_ID(N'[Mapping].[Person]', N'U') IS NULL
BEGIN
	CREATE TABLE [Mapping].[Person](
	        [PersonId]        BIGINT NOT NULL IDENTITY(1,1),
	        [AQPersonId]      INT NULL,
	        [ERxContactId]    CHAR(18) NULL,
	        [CvwIndividualId] CHAR(18) NULL,
	        [Name]            NVARCHAR(254) NULL,
	        [LastModified]    DATETIME NOT NULL
	                CONSTRAINT DF_MappingPerson_LastModified DEFAULT (CURRENT_TIMESTAMP),
	        [LastModifier]    NVARCHAR(254) NOT NULL

	CONSTRAINT [PK_MappingPerson]
	        PRIMARY KEY CLUSTERED ([PersonId])
	)

	IF OBJECT_ID(N'[Mapping].[Person]', N'U') IS NOT NULL
		RAISERROR('*** CREATED TABLE %s.%s ***', 10, 1, '[Mapping]', '[Person]') WITH NOWAIT
	ELSE
		RAISERROR('*** FAILED CREATING TABLE %s.%s ***', 10, 1, '[Mapping]', '[Person]') WITH NOWAIT

	IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description', N'SCHEMA', N'Mapping', N'TABLE', N'Person', NULL, NULL))
		EXEC sys.sp_addextendedproperty @name=N'MS_Description',
		        @value=N'A mapping table for Person identifiers across multiple systems.',
		        @level0type=N'SCHEMA', @level0name=N'Mapping',
		        @level1type=N'TABLE', @level1name=N'Person'
END
ELSE
	RAISERROR('*** SKIPPED CREATING EXISTING TABLE %s.%s ***', 10, 1, '[Mapping]', '[Person]') WITH NOWAIT
GO
