/*
 * Table:       IntegrationHub.Mapping.Person
 *
 * Author:      Juswaldy Jusman
 * Created:     2017-05-05
 * Description: A mapping table for Person Document identifiers across multiple systems.
 *
 */

USE IntegrationHub
GO

IF OBJECT_ID(N'[Mapping].[Document]', N'U') IS NULL
BEGIN
	CREATE TABLE [Mapping].[Document](
	        [DocumentId]      BIGINT NOT NULL IDENTITY(1,1),
	        [AQPersonFileId]  INT NULL,
	        [ERxAttachmentId] CHAR(18) NULL,
	        [CvwAttachmentId] CHAR(18) NULL,
	        [LastModified]    DATETIME NOT NULL
	                CONSTRAINT DF_MappingDocument_LastModified DEFAULT (CURRENT_TIMESTAMP),
	        [LastModifier]    NVARCHAR(254) NOT NULL

	CONSTRAINT [PK_MappingDocument]
	        PRIMARY KEY CLUSTERED ([DocumentId])
	)

	IF OBJECT_ID(N'[Mapping].[Document]', N'U') IS NOT NULL
		RAISERROR('*** CREATED TABLE %s.%s ***', 10, 1, '[Mapping]', '[Document]') WITH NOWAIT
	ELSE
		RAISERROR('*** FAILED CREATING TABLE %s.%s ***', 10, 1, '[Mapping]', '[Document]') WITH NOWAIT

	IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description', N'SCHEMA', N'Mapping', N'TABLE', N'Document', NULL, NULL))
		EXEC sys.sp_addextendedproperty @name=N'MS_Description',
		        @value=N'A mapping table for Person Document identifiers across multiple systems.',
		        @level0type=N'SCHEMA', @level0name=N'Mapping',
		        @level1type=N'TABLE', @level1name=N'Document'
END
ELSE
	RAISERROR('*** SKIPPED CREATING EXISTING TABLE %s.%s ***', 10, 1, '[Mapping]', '[Document]') WITH NOWAIT
GO
