/*
 * Trigger:     IntegrationHub.Mapping.PersonInsertTrigger
 *
 * Author:      Andrew Menary
 * Created:     2016-05-20
 * Description: A trigger to copy new person records to the version archive.
 *
 */
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ================================================
-- Author:      Andrew Menary
-- Create date: 2016-05-20
-- Description: Copy new person records to the version archive table.
-- ================================================
USE [IntegrationHub]
GO

IF OBJECT_ID ('Mapping.PersonInsertTrigger','TR') IS NOT NULL
	DROP TRIGGER Mapping.PersonInsertTrigger 
GO

CREATE TRIGGER [Mapping].[PersonInsertTrigger] ON [Mapping].[Person]
FOR INSERT
AS
DECLARE @OperatorId NVARCHAR(254)
BEGIN
	SELECT  @OperatorId = LastModifier FROM INSERTED
	INSERT INTO Mapping.PersonVersionArchive
	([PersonId], [AQPersonId], [ERxContactId], [CvwIndividualId], [Name], [LastModified], [LastModifier], Operation, OperatorId, OperationDate)
	SELECT  [PersonId], [AQPersonId], [ERxContactId], [CvwIndividualId], [Name], [LastModified], [LastModifier], 'Insert', @OperatorId, CURRENT_TIMESTAMP
	FROM    INSERTED
END
GO
