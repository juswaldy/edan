-- ================================================
-- Author:      Andrew Menary
-- Create date: 2016-06-13
-- Description: Create a Service Broker Service to
--              receive AqueductDataChange messages.
-- ================================================

USE master
GO

-- If the database is not enabled for service broker
IF NOT EXISTS (SELECT 1 FROM sys.databases WHERE database_id = DB_ID(N'IntegrationHub') AND is_broker_enabled = 1)

	-- then enable it with this statement:
	ALTER DATABASE IntegrationHub
	    SET ENABLE_BROKER
	    WITH ROLLBACK IMMEDIATE;
GO

-- To disable service broker:
--ALTER DATABASE IntegrationHub
--    SET DISABLE_BROKER;
--GO

-- If the database is not marked as trustworthy
IF NOT EXISTS (SELECT 1 FROM sys.databases WHERE database_id = DB_ID(N'IntegrationHub') AND is_trustworthy_on = 1)

	-- then mark the database as TRUSTWORTHY so it can be allowed
	-- to access resources beyond the database itself
	ALTER DATABASE IntegrationHub
	    SET TRUSTWORTHY ON;
GO

-- Create the required meta-data
--** Target (IntegrationHub) Database ** --

USE IntegrationHub
GO

-- Create message types for valid XML messages to be sent and received
CREATE MESSAGE TYPE [//www.twu.ca/integration/messages/AqueductDataChange]
    VALIDATION = WELL_FORMED_XML;

CREATE MESSAGE TYPE [//www.twu.ca/integration/messages/AqueductDataChangeStatus]
    VALIDATION = WELL_FORMED_XML;
GO

-- Create a contract which will be used by the service to validate what
-- types of messages are allowed for initiator and target
CREATE CONTRACT [//www.twu.ca/integration/contracts/AqueductDataChange/v1.0] (
    [//www.twu.ca/integration/messages/AqueductDataChange] SENT BY INITIATOR,
    [//www.twu.ca/integration/messages/AqueductDataChangeStatus] SENT BY TARGET
);
GO

-- Create a queue which is an internal physical table to hold the messages
-- passed to the service.  You can SELECT from this table, but you can't
-- INSERT, UPDATE, or DELETE.  You need to use SEND and RECEIVE commands
-- to send messages to the queue and receive messages from it.
CREATE QUEUE [Messaging].[AqueductDataChangeSubscriberQueue]
    WITH STATUS = ON,
    ACTIVATION (
       PROCEDURE_NAME = [Messaging].[AqueductDataChangeSubscriberQueueActivator],
       MAX_QUEUE_READERS = 1,
       EXECUTE AS OWNER)
GO

-- Create a service, which is a logical endpoint sitting atop a queue on
-- which messages are either sent or received.  With service creation
-- you also specify the contract used to validate messages on the service.
CREATE SERVICE [AqueductDataChangeSubscriber]
    ON QUEUE Messaging.AqueductDataChangeSubscriberQueue (
        [//www.twu.ca/integration/contracts/AqueductDataChange/v1.0]
    );
GO

--ALTER QUEUE [Messaging].[AqueductDataChangeSubscriberQueue] WITH ACTIVATION (STATUS = OFF);
--GO

--ALTER QUEUE [Messaging].[AqueductDataChangeSubscriberQueue] WITH ACTIVATION (STATUS = ON);
--GO

--USE IntegrationHub
--GO

---- Remove all service broker database objects related to AqueductDataChange
--DROP SERVICE [AqueductDataChangeSubscriber]
--DROP QUEUE [Messaging].[AqueductDataChangeSubscriberQueue]
--DROP CONTRACT [//www.twu.ca/integration/contracts/AqueductDataChange/v1.0]
--DROP MESSAGE TYPE [//www.twu.ca/integration/messages/AqueductDataChange]
--DROP MESSAGE TYPE [//www.twu.ca/integration/messages/AqueductDataChangeStatus]
--IF EXISTS (SELECT 1 FROM sys.procedures WHERE [object_id] = OBJECT_ID(N'[Messaging].[AqueductDataChangeSubscriberQueueActivator]'))
--DROP PROCEDURE [Messaging].[AqueductDataChangeSubscriberQueueActivator]
--GO
