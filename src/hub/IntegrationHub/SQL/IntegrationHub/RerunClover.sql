USE IntegrationHub
GO

-- Drop stored procedure if it already exists
IF EXISTS (
    SELECT  *
    FROM    INFORMATION_SCHEMA.ROUTINES
    WHERE   SPECIFIC_SCHEMA = N'Messaging'
    AND     SPECIFIC_NAME = N'RerunClover'
)
    DROP PROCEDURE Messaging.RerunClover
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================================================
-- Author:      Juswaldy Jusman
-- Create date: 2016-10-13
-- Description: Rerun Clover on the first message of the specified topic.
-- =============================================================================
CREATE PROCEDURE Messaging.RerunClover
	@Topic VARCHAR(MAX) = NULL
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	-- SET XACT_ABORT ON to ensure any existing transaction
	-- and not just the current statement is rolled back if
	-- there is a run-time error.
	SET XACT_ABORT ON;

	DECLARE
		@ErrorNumber INT = NULL,
		@ErrorMessage VARCHAR(MAX) = NULL,
		@RunId BIGINT;

	BEGIN TRY

		DECLARE RerunCursor CURSOR FOR
		SELECT TOP 1 Topic
		FROM Messaging.MessageQueue
		WHERE ((@Topic IS NULL AND 1=1) OR Topic = @Topic)
		AND Topic NOT LIKE 'Unknown.%'
		AND Topic NOT LIKE 'Unmapped.%'
		ORDER BY Due ASC;

		OPEN RerunCursor;
		FETCH NEXT FROM RerunCursor INTO @Topic
		WHILE @@FETCH_STATUS = 0
		BEGIN
			EXEC Messaging.RunCloverGraph @graphName = @Topic, @runId = @RunId;
			-- WAITFOR DELAY '00:00:01' -- Sleep 1 second. In case we ever want to rerun multiple messages.
			FETCH NEXT FROM RerunCursor INTO @Topic
		END
		CLOSE RerunCursor;
		DEALLOCATE RerunCursor;

	END TRY
	BEGIN CATCH
		-- Execute standard error handler
		SET @ErrorNumber = ERROR_NUMBER() + 50000;
		SET @ErrorMessage = 'RerunClover: ' + ERROR_MESSAGE();
		THROW @ErrorNumber, @ErrorMessage, 1;
	END CATCH;

END
GO
