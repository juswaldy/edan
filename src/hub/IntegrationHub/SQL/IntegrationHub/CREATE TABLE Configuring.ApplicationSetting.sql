/*
 * Table:       IntegrationHub.Configuring.ApplicationSettings
 *
 * Author:      Andrew Menary
 * Created:     2016-06-28
 * Description: A table to store configuration settings.
 *
 */

USE IntegrationHub
GO

IF OBJECT_ID(N'[Configuring].[ApplicationSetting]', N'U') IS NULL
BEGIN
	CREATE TABLE [Configuring].[ApplicationSetting](
	        [Name] [NVARCHAR](30) NOT NULL,
	        [Value] [NVARCHAR](MAX) NULL
	CONSTRAINT [PK_ConfiguringApplicationSetting]
	        PRIMARY KEY CLUSTERED ([Name])
	)

	IF OBJECT_ID(N'[Configuring].[ApplicationSetting]', N'U') IS NOT NULL
		RAISERROR('*** CREATED TABLE %s.%s ***', 10, 1, '[Configuring]', '[ApplicationSetting]') WITH NOWAIT
	ELSE
		RAISERROR('*** FAILED CREATING TABLE %s.%s ***', 10, 1, '[Configuring]', '[ApplicationSetting]') WITH NOWAIT

	IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description', N'SCHEMA', N'Configuring', N'TABLE', N'ApplicationSetting', NULL, NULL))
		EXEC sys.sp_addextendedproperty @name=N'MS_Description',
		        @value=N'A table to store configuration settings.',
		        @level0type=N'SCHEMA', @level0name=N'Configuring',
		        @level1type=N'TABLE', @level1name=N'ApplicationSetting'
END
ELSE
	RAISERROR('*** SKIPPED CREATING EXISTING TABLE %s.%s ***', 10, 1, '[Configuring]', '[ApplicationSetting]') WITH NOWAIT
GO
