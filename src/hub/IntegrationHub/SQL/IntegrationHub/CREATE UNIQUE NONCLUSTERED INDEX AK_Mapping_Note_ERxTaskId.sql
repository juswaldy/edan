-- ===================================
-- Create Unique Nonclustered Index
-- ===================================
USE IntegrationHub
GO

SET QUOTED_IDENTIFIER ON
GO

IF EXISTS (SELECT * FROM sys.indexes WHERE name = N'AK_Mapping_Note_ERxTaskId')
	DROP INDEX AK_Mapping_Note_ERxTaskId
	ON Mapping.Note
GO

CREATE UNIQUE NONCLUSTERED INDEX AK_Mapping_Note_ERxTaskId
ON Mapping.Note
(
	ERxTaskId ASC
)
WHERE
	ERxTaskId IS NOT NULL
WITH
(
	SORT_IN_TEMPDB = OFF,
	DROP_EXISTING = OFF
)
GO

EXEC sys.sp_addextendedproperty
	@name=N'MS_Description',
	@value=N'Ensure uniqueness of Enrollment Rx Task identifiers.',
	@level0type=N'SCHEMA',
	@level0name=N'Mapping',
	@level1type=N'TABLE',
	@level1name=N'Note',
	@level2type=N'INDEX',
	@level2name=N'AK_Mapping_Note_ERxTaskId'
GO
