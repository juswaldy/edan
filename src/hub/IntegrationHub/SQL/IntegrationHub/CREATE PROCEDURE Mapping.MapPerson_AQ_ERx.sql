/*
 * Procedure: IntegrationHub.Mapping.MapPerson_AQ_ERx
 */

USE IntegrationHub
GO

-- Drop stored procedure if it already exists
IF EXISTS (
    SELECT  *
    FROM    INFORMATION_SCHEMA.ROUTINES
    WHERE   SPECIFIC_SCHEMA = N'Mapping'
    AND     SPECIFIC_NAME = N'MapPerson_AQ_ERx'
)
    DROP PROCEDURE Mapping.MapPerson_AQ_ERx
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================================================
-- Author:      Juswaldy Jusman
-- Create date: 2016-06-07
-- Description: Create or update a mapping between Aqueduct Person and ERx Contact.
-- Returns:     Aqueduct Person Id, ERx Contact Id, Return Code.
--              Return Code 0 means nothing was changed.
--              Return Code 1 means a new mapping was created.
--              Return Code 2 means an existing mapping was updated.
-- 2017-02-07 JJJ Allow for remapping of IDs.
-- =============================================================================
CREATE PROCEDURE Mapping.MapPerson_AQ_ERx
	@AQPersonId   INT,
	@ERxContactId VARCHAR(18),
	@Name         NVARCHAR(254),
	@LastModifier NVARCHAR(254)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	-- SET XACT_ABORT ON to ensure any existing transaction
	-- and not just the current statement is rolled back if
	-- there is a run-time error.
	SET XACT_ABORT ON;

	DECLARE	@Now               DATETIME,
			@ReturnCode        INT = 0, -- Default to success.
			@ErrorNumber       INT = NULL,
			@ErrorMessage      VARCHAR(MAX) = NULL,
			@NumberOfMatches   INT = 0,

			@_PersonId         BIGINT,
			@_AQPersonId       INT,
			@_ERxContactId     VARCHAR(18),
			@_Name             VARCHAR(254)
			;

	SET @Now = GETUTCDATE();

	BEGIN TRY
		DECLARE @hasOuterTransaction BIT = CASE WHEN @@TRANCOUNT > 0 THEN 1 ELSE 0 END;
		DECLARE @rollbackPoint NCHAR(32) = REPLACE(CONVERT(NCHAR(36), NEWID()), N'-', N'');

		IF @hasOuterTransaction = 1
		BEGIN
			SAVE TRANSACTION @rollbackPoint;
		END
		ELSE
		BEGIN
			BEGIN TRANSACTION @rollbackPoint;
		END;

		-- Trim and convert blank strings to null.
		IF RTRIM(@ERxContactId) = '' SET @ERxContactId = NULL ELSE SET @ERxContactId = RTRIM(LTRIM(@ERxContactId));
		IF RTRIM(@Name) = '' SET @Name = NULL ELSE SET @Name = RTRIM(LTRIM(@Name));
		IF RTRIM(@LastModifier) = '' SET @LastModifier = NULL ELSE SET @LastModifier = RTRIM(LTRIM(@LastModifier));

		-- Validate parameters.
		IF @AQPersonID IS NULL AND @ERxContactID IS NULL
			THROW 50001, N'Bad mapping: Both ID''s are null', 1;

		IF @LastModifier IS NULL
			THROW 50001, N'Bad mapping: LastModifier parameter is null', 1;

		-- Remap a Person if necessary.
		IF @AQPersonId IS NOT NULL
		BEGIN
			-- Count the number of ID matches.
			SELECT @NumberOfMatches = COUNT(*)
			FROM Mapping.Person
			WHERE AQPersonId = @AQPersonId
			OR ERxContactId = @ERxContactId;

			-- If exactly 2 matches, remap.
			IF @NumberOfMatches = 2
			BEGIN
				-- Blank out existing mapping.
				UPDATE Mapping.Person SET AQPersonId = NULL
				WHERE AQPersonId = @AQPersonId;

				-- Map to a different person.
				UPDATE Mapping.Person SET AQPersonId = @AQPersonId
				WHERE ERxContactId = @ERxContactId;
			END
		END

		-- Count the number of ID matches.
		SELECT @NumberOfMatches = COUNT(*)
		FROM Mapping.Person
		WHERE AQPersonId = @AQPersonId
		OR ERxContactId = @ERxContactId;

		-- If more than one matches.
		IF @NumberOfMatches > 1
		BEGIN
			-- If more than one matches, throw error.
			THROW 50002, N'Bad mapping: Matched more than one ID''s', 1;
		END

		-- If no mapping exists.
		IF @NumberOfMatches = 0
		BEGIN
			-- If no mapping exists, then insert a new mapping row.
			INSERT INTO Mapping.Person
				(AQPersonId, ERxContactId, Name, LastModified, LastModifier)
			VALUES
				(@AQPersonId, @ERxContactId, @Name, @Now, @LastModifier);

			-- Return Code 1 means a new mapping was created.
			SET @ReturnCode = 1;
		END
		ELSE IF @NumberOfMatches = 1
		BEGIN
			-- If a single mapping exists, retrieve it.
			SELECT
				@_PersonId = PersonId,
				@_AQPersonId = AQPersonId,
				@_ERxContactId = ERxContactId,
				@_Name = Name
			FROM Mapping.Person
			WHERE AQPersonId = @AQPersonId
			OR ERxContactId = @ERxContactId;

			-- Diff with input params.
			IF COALESCE(@_AQPersonId, 0) = COALESCE(@AQPersonId, 0)
				AND COALESCE(@_ERxContactId, '') = COALESCE(@ERxContactId, '')
				AND COALESCE(@_Name, '') = COALESCE(@Name, '')
			BEGIN
				-- If no changes, do nothing.
				SET @_AQPersonId = @AQPersonId
			END
			ELSE
-- 2017-02-07 JJJ Allow for remapping of IDs.
--			-- Otherwise, one of these conditions has to be true:
--			-- 1. Both the ID's must match, or
--			-- 2. One of the database ID's must be null, or
--			-- 3. One of the supplied ID's must be null
--			IF (@_AQPersonId = @AQPersonId AND @_ERxContactId = @ERxContactId)
--				OR @_AQPersonId IS NULL
--				OR @_ERxContactId IS NULL
--				OR @AQPersonId IS NULL
--				OR @ERxContactId IS NULL
			BEGIN
				-- Set null parameters to db values.
				IF @AQPersonId IS NULL SET @AQPersonId = @_AQPersonId
				IF @ERxContactId IS NULL SET @ERxContactId = @_ERxContactId
				IF @Name IS NULL SET @Name = @_Name

				-- Update the mapping table.
				UPDATE Mapping.Person SET
					AQPersonId = @AQPersonId,
					ERxContactId = @ERxContactId,
					Name = @Name,
					LastModified = @Now,
					LastModifier = @LastModifier
				WHERE PersonId = @_PersonId;

				-- Return Code 2 means an existing mapping was updated.
				SET @ReturnCode = 2;
			END
-- 2017-02-07 JJJ Allow for remapping of IDs.
--			ELSE
--			BEGIN
--				-- If one or more ID's don't match, throw error.
--				THROW 50003, N'Bad mapping: ID values have changed', 1;
--			END
		END
		ELSE
		BEGIN
			-- The number of matches is negative, throw error.
			THROW 50004, N'Bad mapping: Negative number of matches found', 1;
		END

		IF @hasOuterTransaction = 0
		BEGIN
			COMMIT TRANSACTION;
		END;
	END TRY
	BEGIN CATCH
		IF XACT_STATE() = 1
		BEGIN
			ROLLBACK TRANSACTION @rollbackPoint;
		END;

		-- Execute standard error handler
		SET @ErrorNumber = ERROR_NUMBER() + 50000;
		SET @ErrorMessage = 'MapPerson_AQ_ERx: ' + ERROR_MESSAGE();
		THROW @ErrorNumber, @ErrorMessage, 1;
	END CATCH;

	-- Return result set.
	SELECT @AQPersonId, @ERxContactId, @ReturnCode;
END
GO

/* If stored procedure created successfully then add permissions */
IF EXISTS (
    SELECT  *
    FROM    INFORMATION_SCHEMA.ROUTINES
    WHERE   SPECIFIC_SCHEMA = N'Mapping'
    AND     SPECIFIC_NAME = N'MapPerson_AQ_ERx'
)
BEGIN
	IF EXISTS (SELECT * FROM sys.sysusers WHERE name = N'SalesforceSyncDEV')
	GRANT EXECUTE ON Mapping.MapPerson_AQ_ERx TO SalesforceSyncDEV

	IF EXISTS (SELECT * FROM sys.sysusers WHERE name = N'SalesforceSyncTEST')
	GRANT EXECUTE ON Mapping.MapPerson_AQ_ERx TO SalesforceSyncTEST
 
	IF EXISTS (SELECT * FROM sys.sysusers WHERE name = N'SalesforceSync')
	GRANT EXECUTE ON Mapping.MapPerson_AQ_ERx TO SalesforceSync

	IF EXISTS (SELECT * FROM sys.sysusers WHERE name = N'cloveretl')
	GRANT EXECUTE ON Mapping.MapPerson_AQ_ERx TO cloveretl
END
GO
