/*
 * Trigger:     IntegrationHub.Mapping.NoteUpdateTrigger
 *
 * Author:      Juswaldy Jusman
 * Created:     2016-08-15
 * Description: A trigger to copy updated Note records to the version archive.
 *
 */
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ================================================
-- Author:      Juswaldy Jusman
-- Create date: 2016-08-15
-- Description: Copy updated Note records to the version archive table.
-- ================================================
USE [IntegrationHub]
GO

IF OBJECT_ID ('Mapping.NoteUpdateTrigger','TR') IS NOT NULL
	DROP TRIGGER Mapping.NoteUpdateTrigger
GO

CREATE TRIGGER [Mapping].[NoteUpdateTrigger] ON [Mapping].[Note]
FOR UPDATE
AS
DECLARE @OperatorId NVARCHAR(254)
BEGIN
	SELECT  @OperatorId = LastModifier FROM INSERTED
	INSERT INTO Mapping.NoteVersionArchive
	([NoteId], [AQNoteId], [ERxTaskId], [CvwTaskId], [LastModified], [LastModifier], Operation, OperatorId, OperationDate)
	SELECT  [NoteId], [AQNoteId], [ERxTaskId], [CvwTaskId], [LastModified], [LastModifier], 'Update', @OperatorId, CURRENT_TIMESTAMP
	FROM    DELETED
END
GO
