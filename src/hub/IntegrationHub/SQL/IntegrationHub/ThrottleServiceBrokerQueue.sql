USE IntegrationHub;

-- Drop stored procedure if it already exists
IF EXISTS (
    SELECT  *
    FROM    INFORMATION_SCHEMA.ROUTINES
    WHERE   SPECIFIC_SCHEMA = N'Messaging'
    AND     SPECIFIC_NAME = N'ThrottleServiceBrokerQueue'
)
    DROP PROCEDURE Messaging.ThrottleServiceBrokerQueue
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================================================
-- Author:      Juswaldy Jusman
-- Create date: 2016-10-13
-- Description: Throttle the given Service Broker Queue so Clover doesn't get hammered.
-- =============================================================================
CREATE PROCEDURE Messaging.ThrottleServiceBrokerQueue
	@QueueName       VARCHAR(MAX),
	@MaxInProcess    INT = 7,
	@MaxRetries      INT = 70
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	-- SET XACT_ABORT ON to ensure any existing transaction
	-- and not just the current statement is rolled back if
	-- there is a run-time error.
	SET XACT_ABORT ON;

	DECLARE
		@ErrorNumber        INT = NULL,
		@ErrorMessage       VARCHAR(MAX) = NULL,
		@AlterStatement     VARCHAR(MAX) = 'ALTER QUEUE Messaging.' + @QueueName + ' WITH STATUS = ',
		@CurrentlyInProcess INT = 0,
		@NumberOfRetries    INT = 0,
		@ToggleOnOff        BIT = 0,
		@KeepGoing          BIT = 1;

	BEGIN TRY

		WHILE @KeepGoing = 1
		BEGIN
			-- Get the number of messages which are still in the InProcess queue.
			SELECT @CurrentlyInProcess = COUNT(*)
			FROM Messaging.MessageInProcessQueue;

			-- If the number is still under the max specified, then keep the service broker queue enabled.
			-- Otherwise, turn it off for now.
			IF @CurrentlyInProcess < @MaxInProcess
			BEGIN
				-- Turn the queue on.
				IF @ToggleOnOff = 0
				BEGIN
					EXEC (@AlterStatement + 'ON');
					SET @ToggleOnOff = (@ToggleOnOff + 1) % 2;
					RAISERROR('Turning Queue ON', 0, 1) WITH NOWAIT;
				END

				-- Reset number of tries.
				SET @NumberOfRetries = 0;
				RAISERROR('OK', 0, 1) WITH NOWAIT;
			END
			ELSE
			BEGIN
				-- Turn the queue off.
				IF @ToggleOnOff = 1
				BEGIN
					EXEC (@AlterStatement + 'OFF');
					SET @ToggleOnOff = (@ToggleOnOff + 1) % 2;
					RAISERROR('OOPS! TOO MUCH! Turning Queue OFF', 0, 1) WITH NOWAIT;
				END

				-- Increase number of tries.
				SET @NumberOfRetries = @NumberOfRetries + 1;
				SET @ErrorMessage = 'Number of retries: ' + CONVERT(VARCHAR(MAX), @NumberOfRetries);
				RAISERROR(@ErrorMessage, 0, 1) WITH NOWAIT;

				-- Quit if we have exceeded max tries.
				IF @NumberOfRetries > @MaxRetries
					SET @KeepGoing = 0;
			END

			-- Wait throttle seconds.
			WAITFOR DELAY '00:00:01';
		END

	END TRY
	BEGIN CATCH
		-- Execute standard error handler
		SET @ErrorNumber = ERROR_NUMBER() + 50000;
		SET @ErrorMessage = 'ThrottleServiceBrokerQueue: ' + ERROR_MESSAGE();
		THROW @ErrorNumber, @ErrorMessage, 1;
	END CATCH;

END
GO
