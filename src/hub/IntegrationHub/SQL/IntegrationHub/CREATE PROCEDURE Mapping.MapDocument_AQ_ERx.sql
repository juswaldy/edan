/*
 * Procedure: IntegrationHub.Mapping.MapDocument_AQ_ERx
 */

USE IntegrationHub
GO

-- Drop stored procedure if it already exists
IF EXISTS (
    SELECT  *
    FROM    INFORMATION_SCHEMA.ROUTINES
    WHERE   SPECIFIC_SCHEMA = N'Mapping'
    AND     SPECIFIC_NAME = N'MapDocument_AQ_ERx'
)
    DROP PROCEDURE Mapping.MapDocument_AQ_ERx
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================================================
-- Author:      Juswaldy Jusman
-- Create date: 2017-05-05
-- Description: Create or update a mapping between Aqueduct PersonFile and ERx Attachment.
-- Returns:     Aqueduct PersonFile Id, ERx Attachment Id, Return Code.
--              Return Code 0 means nothing was changed.
--              Return Code 1 means a new mapping was created.
--              Return Code 2 means an existing mapping was updated.
-- =============================================================================
CREATE PROCEDURE Mapping.MapDocument_AQ_ERx
	@AQPersonFileId  INT,
	@ERxAttachmentId VARCHAR(18),
	@LastModifier    NVARCHAR(254)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	-- SET XACT_ABORT ON to ensure any existing transaction
	-- and not just the current statement is rolled back if
	-- there is a run-time error.
	SET XACT_ABORT ON;

	DECLARE	@Now               DATETIME,
			@ReturnCode        INT = 0, -- Default to success.
			@ErrorNumber       INT = NULL,
			@ErrorMessage      VARCHAR(MAX) = NULL,
			@NumberOfMatches   INT = 0,

			@_DocumentId       BIGINT,
			@_AQPersonFileId   INT,
			@_ERxAttachmentId  VARCHAR(18)
			;

	SET @Now = GETUTCDATE();

	BEGIN TRY
		DECLARE @hasOuterTransaction BIT = CASE WHEN @@TRANCOUNT > 0 THEN 1 ELSE 0 END;
		DECLARE @rollbackPoint NCHAR(32) = REPLACE(CONVERT(NCHAR(36), NEWID()), N'-', N'');

		IF @hasOuterTransaction = 1
		BEGIN
			SAVE TRANSACTION @rollbackPoint;
		END
		ELSE
		BEGIN
			BEGIN TRANSACTION @rollbackPoint;
		END;

		-- Trim and convert blank strings to null.
		IF RTRIM(@ERxAttachmentId) = '' SET @ERxAttachmentId = NULL ELSE SET @ERxAttachmentId = RTRIM(LTRIM(@ERxAttachmentId));
		IF RTRIM(@LastModifier) = '' SET @LastModifier = NULL ELSE SET @LastModifier = RTRIM(LTRIM(@LastModifier));

		-- Validate parameters.
		IF @AQPersonFileId IS NULL AND @ERxAttachmentId IS NULL
			THROW 50001, N'Bad mapping: Both ID''s are null', 1;

		IF @LastModifier IS NULL
			THROW 50001, N'Bad mapping: LastModifier parameter is null', 1;

		-- Count the number of ID matches.
		SELECT @NumberOfMatches = COUNT(*)
		FROM Mapping.Document
		WHERE AQPersonFileId = @AQPersonFileId
		OR ERxAttachmentId = @ERxAttachmentId;

		-- If more than one matches.
		IF @NumberOfMatches > 1
		BEGIN
			-- If more than one matches, throw error.
			THROW 50002, N'Bad mapping: Matched more than one ID''s', 1;
		END

		-- If no mapping exists.
		IF @NumberOfMatches = 0
		BEGIN
			-- If no mapping exists, then insert a new mapping row.
			INSERT INTO Mapping.Document
				(AQPersonFileId, ERxAttachmentId, LastModified, LastModifier)
			VALUES
				(@AQPersonFileId, @ERxAttachmentId, @Now, @LastModifier);

			-- Return Code 1 means a new mapping was created.
			SET @ReturnCode = 1;
		END
		ELSE IF @NumberOfMatches = 1
		BEGIN
			-- If a single mapping exists, retrieve it.
			SELECT
				@_DocumentId = DocumentId,
				@_AQPersonFileId = AQPersonFileId,
				@_ERxAttachmentId = ERxAttachmentId
			FROM Mapping.Document
			WHERE AQPersonFileId = @AQPersonFileId
			OR ERxAttachmentId = @ERxAttachmentId;

			-- Diff with input params.
			IF COALESCE(@_AQPersonFileId, 0) = COALESCE(@AQPersonFileId, 0)
				AND COALESCE(@_ERxAttachmentId, '') = COALESCE(@ERxAttachmentId, '')
			BEGIN
				-- If no changes, do nothing.
				SET @ReturnCode = 0 -- Return Code 0 means nothing was changed.
			END
			ELSE
			-- Otherwise, one of these conditions has to be true:
			-- 1. Both the ID's must match, or
			-- 2. One of the database ID's must be null, or
			-- 3. One of the supplied ID's must be null
			IF (@_AQPersonFileId = @AQPersonFileId AND @_ERxAttachmentId = @ERxAttachmentId)
				OR @_AQPersonFileId IS NULL
				OR @_ERxAttachmentId IS NULL
				OR @AQPersonFileId IS NULL
				OR @ERxAttachmentId IS NULL
			BEGIN
				-- Set null parameters to db values.
				IF @AQPersonFileId IS NULL SET @AQPersonFileId = @_AQPersonFileId
				IF @ERxAttachmentId IS NULL SET @ERxAttachmentId = @_ERxAttachmentId

				-- Update the mapping table.
				UPDATE Mapping.Document SET
					AQPersonFileId = @AQPersonFileId,
					ERxAttachmentId = @ERxAttachmentId,
					LastModified = @Now,
					LastModifier = @LastModifier
				WHERE DocumentId = @_DocumentId;

				-- Return Code 2 means an existing mapping was updated.
				SET @ReturnCode = 2;
			END
			ELSE
			BEGIN
				-- If one or more ID's don't match, throw error.
				THROW 50003, N'Bad mapping: ID values have changed', 1;
			END
		END
		ELSE
		BEGIN
			-- The number of matches is negative, throw error.
			THROW 50004, N'Bad mapping: Negative number of matches found', 1;
		END

		IF @hasOuterTransaction = 0
		BEGIN
			COMMIT TRANSACTION;
		END;
	END TRY
	BEGIN CATCH
		IF XACT_STATE() = 1
		BEGIN
			ROLLBACK TRANSACTION @rollbackPoint;
		END;

		-- Execute standard error handler
		SET @ErrorNumber = ERROR_NUMBER() + 50000;
		SET @ErrorMessage = 'MapDocument_AQ_ERx: ' + ERROR_MESSAGE();
		THROW @ErrorNumber, @ErrorMessage, 1;
	END CATCH;

	-- Return result set.
	SELECT @AQPersonFileId, @ERxAttachmentId, @ReturnCode;
END
GO

/* If stored procedure created successfully then add permissions */
IF EXISTS (
    SELECT  *
    FROM    INFORMATION_SCHEMA.ROUTINES
    WHERE   SPECIFIC_SCHEMA = N'Mapping'
    AND     SPECIFIC_NAME = N'MapDocument_AQ_ERx'
)
BEGIN
	IF EXISTS (SELECT * FROM sys.sysusers WHERE name = N'SalesforceSyncDEV')
	GRANT EXECUTE ON Mapping.MapDocument_AQ_ERx TO SalesforceSyncDEV

	IF EXISTS (SELECT * FROM sys.sysusers WHERE name = N'SalesforceSyncTEST')
	GRANT EXECUTE ON Mapping.MapDocument_AQ_ERx TO SalesforceSyncTEST
 
	IF EXISTS (SELECT * FROM sys.sysusers WHERE name = N'SalesforceSync')
	GRANT EXECUTE ON Mapping.MapDocument_AQ_ERx TO SalesforceSync

	IF EXISTS (SELECT * FROM sys.sysusers WHERE name = N'cloveretl')
	GRANT EXECUTE ON Mapping.MapDocument_AQ_ERx TO cloveretl
END
GO
