/*
 * Schema:      IntegrationHub.Messaging
 *
 * Author:      Andrew Menary
 * Created:     2016-05-20
 * Description: Groups together objects related to messages from and to connected systems
 *
 */

USE IntegrationHub
GO

IF NOT EXISTS (SELECT * FROM sys.schemas WHERE name = N'Messaging')
BEGIN
	DECLARE @sql NVARCHAR(256) = N'CREATE SCHEMA [Messaging];'
	EXECUTE sp_executesql @sql;

	IF NOT EXISTS (SELECT * FROM sys.schemas WHERE name = N'Messaging')
		RAISERROR('*** FAILED CREATING SCHEMA %s ***', 10, 1, '[Messaging]') WITH NOWAIT
	ELSE
		RAISERROR('*** CREATED SCHEMA %s ***', 10, 1, '[Messaging]') WITH NOWAIT
END
ELSE
	RAISERROR('*** SKIPPED CREATING EXISTING SCHEMA %s ***', 10, 1, '[Messaging]') WITH NOWAIT
GO
