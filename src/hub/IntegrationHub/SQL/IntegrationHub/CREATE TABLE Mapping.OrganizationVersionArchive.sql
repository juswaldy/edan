/*
 * Table:       IntegrationHub.Mapping.OrganizationVersionArchive
 *
 * Author:      Juswaldy Jusman
 * Created:     2016-06-07
 * Description: A version archive of the mapping table for Organization identifiers across multiple systems.
 *
 */

USE IntegrationHub
GO

IF OBJECT_ID(N'[Mapping].[OrganizationVersionArchive]', N'U') IS NULL
BEGIN
	CREATE TABLE [Mapping].[OrganizationVersionArchive](
	        [OrganizationVersionArchiveId] BIGINT NOT NULL IDENTITY(1,1),
	        [OrganizationId]               BIGINT NOT NULL,
	        [AQOrganizationId]             INT NULL,
	        [ERxSchoolId]            CHAR(18) NULL,
	        [CvwAccountId]           CHAR(18) NULL,
	        [Name]                   NVARCHAR(254) NULL,
	        [LastModified]           DATETIME NOT NULL,
	        [LastModifier]           NVARCHAR(254) NOT NULL,
	        [Operation]              VARCHAR(7) NOT NULL,
	        [OperatorId]             NVARCHAR(254) NOT NULL,
	        [OperationDate]          DATETIME NOT NULL
	                CONSTRAINT DF_MappingOrganizationVersionArchive_OperationDate DEFAULT (CURRENT_TIMESTAMP),

	CONSTRAINT [PK_MappingOrganizationVersionArchive]
	        PRIMARY KEY CLUSTERED ([OrganizationVersionArchiveId]) ON [HISTORIC]
	) ON [HISTORIC]

	IF OBJECT_ID(N'[Mapping].[OrganizationVersionArchive]', N'U') IS NOT NULL
		RAISERROR('*** CREATED TABLE %s.%s ***', 10, 1, '[Mapping]', '[OrganizationVersionArchive]') WITH NOWAIT
	ELSE
		RAISERROR('*** FAILED CREATING TABLE %s.%s ***', 10, 1, '[Mapping]', '[OrganizationVersionArchive]') WITH NOWAIT

	IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description', N'SCHEMA', N'Mapping', N'TABLE', N'OrganizationVersionArchive', NULL, NULL))
		EXEC sys.sp_addextendedproperty @name=N'MS_Description',
		        @value=N'A version archive of the mapping table for Organization identifiers across multiple systems.',
		        @level0type=N'SCHEMA', @level0name=N'Mapping',
		        @level1type=N'TABLE', @level1name=N'OrganizationVersionArchive'
END
ELSE
	RAISERROR('*** SKIPPED CREATING EXISTING TABLE %s.%s ***', 10, 1, '[Mapping]', '[OrganizationVersionArchive]') WITH NOWAIT
GO
