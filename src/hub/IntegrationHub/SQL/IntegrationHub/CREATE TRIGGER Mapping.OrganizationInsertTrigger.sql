/*
 * Trigger:     IntegrationHub.Mapping.OrganizationInsertTrigger
 *
 * Author:      Juswaldy Jusman
 * Created:     2016-06-07
 * Description: A trigger to copy new Organization records to the version archive.
 *
 */
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ================================================
-- Author:      Juswaldy Jusman
-- Create date: 2016-06-07
-- Description: Copy new Organization records to the version archive table
-- ================================================
USE [IntegrationHub]
GO

IF OBJECT_ID ('Mapping.OrganizationInsertTrigger','TR') IS NOT NULL
	DROP TRIGGER Mapping.OrganizationInsertTrigger 
GO

CREATE TRIGGER [Mapping].[OrganizationInsertTrigger] ON [Mapping].[Organization]
FOR INSERT
AS
DECLARE @OperatorId NVARCHAR(254)
BEGIN
	SELECT  @OperatorId = LastModifier FROM INSERTED
	INSERT INTO Mapping.OrganizationVersionArchive
	([OrganizationId], [AQOrganizationId], [ERxSchoolId], [CvwAccountId], [Name], [LastModified], [LastModifier], Operation, OperatorId, OperationDate)
	SELECT  [OrganizationId], [AQOrganizationId], [ERxSchoolId], [CvwAccountId], [Name], [LastModified], [LastModifier], 'Insert', @OperatorId, CURRENT_TIMESTAMP
	FROM    INSERTED
END
GO
