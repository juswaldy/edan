/*
 * Table:       IntegrationHub.Mapping.Organization
 *
 * Author:      Juswaldy Jusman
 * Created:     2016-06-07
 * Description: A mapping table for Organization identifiers across multiple systems.
 *
 */

USE IntegrationHub
GO

IF OBJECT_ID(N'[Mapping].[Organization]', N'U') IS NULL
BEGIN
	CREATE TABLE [Mapping].[Organization](
	        [OrganizationId]        BIGINT NOT NULL IDENTITY(1,1),
	        [AQOrganizationId]      INT NULL,
	        [ERxSchoolId]     CHAR(18) NULL,
	        [CvwAccountId]    CHAR(18) NULL,
	        [Name]            NVARCHAR(254) NULL,
	        [LastModified]    DATETIME NOT NULL
	                CONSTRAINT DF_MappingOrganization_LastModified DEFAULT (CURRENT_TIMESTAMP),
	        [LastModifier]    NVARCHAR(254) NOT NULL

	CONSTRAINT [PK_MappingOrganization]
	        PRIMARY KEY CLUSTERED ([OrganizationId])
	)

	IF OBJECT_ID(N'[Mapping].[Organization]', N'U') IS NOT NULL
		RAISERROR('*** CREATED TABLE %s.%s ***', 10, 1, '[Mapping]', '[Organization]') WITH NOWAIT
	ELSE
		RAISERROR('*** FAILED CREATING TABLE %s.%s ***', 10, 1, '[Mapping]', '[Organization]') WITH NOWAIT

	IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description', N'SCHEMA', N'Mapping', N'TABLE', N'Organization', NULL, NULL))
		EXEC sys.sp_addextendedproperty @name=N'MS_Description',
		        @value=N'A mapping table for Organization identifiers across multiple systems.',
		        @level0type=N'SCHEMA', @level0name=N'Mapping',
		        @level1type=N'TABLE', @level1name=N'Organization'
END
ELSE
	RAISERROR('*** SKIPPED CREATING EXISTING TABLE %s.%s ***', 10, 1, '[Mapping]', '[Organization]') WITH NOWAIT
GO
