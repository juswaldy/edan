/*
 * Table:       IntegrationHub.Messaging.MessageInProcessQueue
 *
 * Author:      Andrew Menary
 * Created:     2016-05-20
 * Description: Message queue for data synchronization messages from/to connected systems that are currently being handled
 *
 */

USE IntegrationHub
GO

IF OBJECT_ID(N'[Messaging].[MessageInProcessQueue]', N'U') IS NULL
BEGIN
	CREATE TABLE [Messaging].[MessageInProcessQueue](
	        [Id]            BIGINT NOT NULL,
	        [CorrelationId] BIGINT NOT NULL,
	        [Due]           DATETIME NOT NULL,
	        [Expires]       DATETIME NOT NULL,
	        [Locked]        DATETIME NOT NULL
	                CONSTRAINT DF_MessagingMessageInProcessQueue_Locked DEFAULT (GETUTCDATE()),
	        [RunId]         VARCHAR(254) NULL,
	        [Topic]         VARCHAR(254) NULL,
	        [Payload]       XML
	)

	IF OBJECT_ID(N'[Messaging].[MessageInProcessQueue]', N'U') IS NOT NULL
	BEGIN
		CREATE CLUSTERED INDEX [IX_MessagingMessageInProcessQueue_Locked_Topic] ON [Messaging].[MessageInProcessQueue] ([Locked], [Topic])
		RAISERROR('*** CREATED TABLE %s.%s ***', 10, 1, '[Messaging]', '[MessageInProcessQueue]') WITH NOWAIT
	END
	ELSE
		RAISERROR('*** FAILED CREATING TABLE %s.%s ***', 10, 1, '[Messaging]', '[MessageInProcessQueue]') WITH NOWAIT

	IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description', N'SCHEMA', N'Messaging', N'TABLE', N'MessageInProcessQueue', NULL, NULL))
		EXECUTE sys.sp_addextendedproperty @name=N'MS_Description',
		        @value=N'Message queue for data synchronization messages from/to connected systems that are currently being handled',
		        @level0type=N'SCHEMA', @level0name=N'Messaging',
		        @level1type=N'TABLE', @level1name=N'MessageInProcessQueue'
END
ELSE
	RAISERROR('*** SKIPPED CREATING EXISTING TABLE %s.%s ***', 10, 1, '[Messaging]', '[MessageInProcessQueue]') WITH NOWAIT
GO
