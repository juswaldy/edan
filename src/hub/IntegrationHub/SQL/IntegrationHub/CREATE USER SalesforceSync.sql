-- ===================================
-- Create Data Reader/Data Writer User template
-- ===================================
USE IntegrationHub
GO

IF EXISTS (SELECT * FROM master.sys.syslogins WHERE name = N'SalesforceSyncDEV')
   AND NOT EXISTS (SELECT * FROM sys.sysusers WHERE name = N'SalesforceSyncDEV')
BEGIN
	-- Add the user to the database using their login name and the user name
	CREATE USER [SalesforceSyncDEV]
		FOR LOGIN [SalesforceSyncDEV]
		WITH DEFAULT_SCHEMA = dbo
END
GO

IF EXISTS (SELECT * FROM sys.sysusers WHERE name = N'SalesforceSyncDEV')
BEGIN
	-- Add user to db_datareader role
	ALTER ROLE [db_datareader]
	ADD MEMBER [SalesforceSyncDEV]

	-- Add user to db_datawriter role
	ALTER ROLE [db_datawriter]
	ADD MEMBER [SalesforceSyncDEV]
END
GO

IF EXISTS (SELECT * FROM master.sys.syslogins WHERE name = N'SalesforceSyncTEST')
   AND NOT EXISTS (SELECT * FROM sys.sysusers WHERE name = N'SalesforceSyncTEST')
BEGIN
	-- Add the user to the database using their login name and the user name
	CREATE USER [SalesforceSyncTEST]
		FOR LOGIN [SalesforceSyncTEST]
		WITH DEFAULT_SCHEMA = dbo
END
GO

IF EXISTS (SELECT * FROM sys.sysusers WHERE name = N'SalesforceSyncTEST')
BEGIN
	-- Add user to db_datareader role
	ALTER ROLE [db_datareader]
	ADD MEMBER [SalesforceSyncTEST]

	-- Add user to db_datawriter role
	ALTER ROLE [db_datawriter]
	ADD MEMBER [SalesforceSyncTEST]
END
GO

IF EXISTS (SELECT * FROM master.sys.syslogins WHERE name = N'SalesforceSync')
   AND NOT EXISTS (SELECT * FROM sys.sysusers WHERE name = N'SalesforceSync')
BEGIN
	-- Add the user to the database using their login name and the user name
	CREATE USER [SalesforceSync]
		FOR LOGIN [SalesforceSync]
		WITH DEFAULT_SCHEMA = dbo
END
GO

IF EXISTS (SELECT * FROM sys.sysusers WHERE name = N'SalesforceSync')
BEGIN
	-- Add user to db_datareader role
	ALTER ROLE [db_datareader]
	ADD MEMBER [SalesforceSync]

	-- Add user to db_datawriter role
	ALTER ROLE [db_datawriter]
	ADD MEMBER [SalesforceSync]
END
GO
