/*
 * Trigger:     IntegrationHub.Mapping.OrganizationUpdateTrigger
 *
 * Author:      Juswaldy Jusman
 * Created:     2016-06-07
 * Description: A trigger to copy updated Organization records to the version archive.
 *
 */
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ================================================
-- Author:      Juswaldy Jusman
-- Create date: 2016-06-07
-- Description: Copy updated Organization records to the version archive table.
-- ================================================
USE [IntegrationHub]
GO

IF OBJECT_ID ('Mapping.OrganizationUpdateTrigger','TR') IS NOT NULL
	DROP TRIGGER Mapping.OrganizationUpdateTrigger
GO

CREATE TRIGGER [Mapping].[OrganizationUpdateTrigger] ON [Mapping].[Organization]
FOR UPDATE
AS
DECLARE @OperatorId NVARCHAR(254)
BEGIN
	SELECT  @OperatorId = LastModifier FROM INSERTED
	INSERT INTO Mapping.OrganizationVersionArchive
	([OrganizationId], [AQOrganizationId], [ERxSchoolId], [CvwAccountId], [Name], [LastModified], [LastModifier], Operation, OperatorId, OperationDate)
	SELECT  [OrganizationId], [AQOrganizationId], [ERxSchoolId], [CvwAccountId], [Name], [LastModified], [LastModifier], 'Update', @OperatorId, CURRENT_TIMESTAMP
	FROM    DELETED
END
GO
