/*
 * Build script
 *
 * Requires SQLCMD mode be enabled.
 * To enable SQLCMD mode, select SQLCMD Mode from the Query menu.
 */

-- Create DATABASE
:r "C:\Repositories\Integration\IntegrationHub\SQL\IntegrationHub\CREATE DATABASE IntegrationHub.sql"

-- CREATE USERS
:r "C:\Repositories\Integration\IntegrationHub\SQL\IntegrationHub\CREATE USER cloveretl.sql"
:r "C:\Repositories\Integration\IntegrationHub\SQL\IntegrationHub\CREATE USER SalesforceSync.sql"
:r "C:\Repositories\Integration\IntegrationHub\SQL\IntegrationHub\CREATE USER Trinet_Juan.Fajardo.sql"

-- CREATE Logging Objects
:r "C:\Repositories\Integration\IntegrationHub\SQL\IntegrationHub\CREATE SCHEMA Logging.sql"
:r "C:\Repositories\Integration\IntegrationHub\SQL\IntegrationHub\CREATE TABLE Logging.AqueductDataChangeFailedMessage.sql"
:r "C:\Repositories\Integration\IntegrationHub\SQL\IntegrationHub\CREATE TABLE Logging.AqueductDataChangePoisonedMessage.sql"
:r "C:\Repositories\Integration\IntegrationHub\SQL\IntegrationHub\CREATE PROCEDURE Logging.ClearTrackingAqueductDataChangeMessage.sql"
:r "C:\Repositories\Integration\IntegrationHub\SQL\IntegrationHub\CREATE PROCEDURE Logging.TrackAqueductDataChangeMessage.sql"

-- CREATE Mapping Objects
:r "C:\Repositories\Integration\IntegrationHub\SQL\IntegrationHub\CREATE SCHEMA Mapping.sql"
:r "C:\Repositories\Integration\IntegrationHub\SQL\IntegrationHub\CREATE TABLE Mapping.Person.sql"
:r "C:\Repositories\Integration\IntegrationHub\SQL\IntegrationHub\CREATE TABLE Mapping.PersonVersionArchive.sql"
:r "C:\Repositories\Integration\IntegrationHub\SQL\IntegrationHub\CREATE UNIQUE NONCLUSTERED INDEX AK_Mapping_Person_AQPersonId.sql"
:r "C:\Repositories\Integration\IntegrationHub\SQL\IntegrationHub\CREATE UNIQUE NONCLUSTERED INDEX AK_Mapping_Person_CvwIndividualId.sql"
:r "C:\Repositories\Integration\IntegrationHub\SQL\IntegrationHub\CREATE UNIQUE NONCLUSTERED INDEX AK_Mapping_Person_ERxContactId.sql"
:r "C:\Repositories\Integration\IntegrationHub\SQL\IntegrationHub\CREATE TRIGGER Mapping.PersonDeleteTrigger.sql"
:r "C:\Repositories\Integration\IntegrationHub\SQL\IntegrationHub\CREATE TRIGGER Mapping.PersonInsertTrigger.sql"
:r "C:\Repositories\Integration\IntegrationHub\SQL\IntegrationHub\CREATE TRIGGER Mapping.PersonUpdateTrigger.sql"

-- CREATE Messaging Objects
:r "C:\Repositories\Integration\IntegrationHub\SQL\IntegrationHub\CREATE SCHEMA Messaging.sql"
:r "C:\Repositories\Integration\IntegrationHub\SQL\IntegrationHub\CREATE TABLE Messaging.MessageCompleted.sql"
:r "C:\Repositories\Integration\IntegrationHub\SQL\IntegrationHub\CREATE TABLE Messaging.MessageDeadLetter.sql"
:r "C:\Repositories\Integration\IntegrationHub\SQL\IntegrationHub\CREATE TABLE Messaging.MessageInProcessQueue.sql"
:r "C:\Repositories\Integration\IntegrationHub\SQL\IntegrationHub\CREATE TABLE Messaging.MessageQueue.sql"
:r "C:\Repositories\Integration\IntegrationHub\SQL\IntegrationHub\CREATE PROCEDURE Messaging.MessageEnqueue.sql"
:r "C:\Repositories\Integration\IntegrationHub\SQL\IntegrationHub\CREATE PROCEDURE Messaging.MessageDequeue.sql"
:r "C:\Repositories\Integration\IntegrationHub\SQL\IntegrationHub\CREATE PROCEDURE Messaging.MessageAckDone.sql"
:r "C:\Repositories\Integration\IntegrationHub\SQL\IntegrationHub\CREATE PROCEDURE Messaging.MessageDiscardDeadLetter.sql"
:r "C:\Repositories\Integration\IntegrationHub\SQL\IntegrationHub\CREATE PROCEDURE Messaging.MonitorMessageQueues.sql"

-- CREATE Service Broker Objects
:r "C:\Repositories\Integration\IntegrationHub\SQL\IntegrationHub\CREATE PROCEDURE Messaging.AqueductDataChangeSubscriberQueueActivator.sql"
:r "C:\Repositories\Integration\IntegrationHub\SQL\IntegrationHub\CREATE SERVICE AqueductDataChangeSubscriber ON QUEUE Messaging.AqueductDataChangeSubscriberQueue.sql"

-- Done
