/*
 * Schema:      IntegrationHub.Configuring
 *
 * Author:      Andrew Menary
 * Created:     2016-06-28
 * Description: A schema to organize all objects related to configuration data.
 *
 */

USE IntegrationHub
GO

IF NOT EXISTS (SELECT * FROM sys.schemas WHERE name = N'Configuring')
BEGIN
	DECLARE @sql NVARCHAR(256) = N'CREATE SCHEMA [Configuring];'
	EXECUTE sp_executesql @sql;

	IF NOT EXISTS (SELECT * FROM sys.schemas WHERE name = N'Configuring')
		RAISERROR('*** FAILED CREATING SCHEMA %s ***', 10, 1, '[Configuring]') WITH NOWAIT
	ELSE
		RAISERROR('*** CREATED SCHEMA %s ***', 10, 1, '[Configuring]') WITH NOWAIT
END
ELSE
	RAISERROR('*** SKIPPED CREATING EXISTING SCHEMA %s ***', 10, 1, '[Configuring]') WITH NOWAIT
GO
