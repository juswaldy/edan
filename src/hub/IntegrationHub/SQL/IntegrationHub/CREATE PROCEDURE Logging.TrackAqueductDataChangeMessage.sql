-- ================================================
-- Template generated from Template Explorer using:
-- Create Procedure (New Menu).SQL
--
-- Use the Specify Values for Template Parameters 
-- command (Ctrl-Shift-M) to fill in the parameter 
-- values below.
--
-- This block of comments will not be included in
-- the definition of the procedure.
-- ================================================

USE IntegrationHub
GO

-- Drop stored procedure if it already exists
IF EXISTS (
  SELECT *
    FROM INFORMATION_SCHEMA.ROUTINES
   WHERE SPECIFIC_SCHEMA = N'Logging'
     AND SPECIFIC_NAME = N'TrackAqueductDataChangeMessage'
)
   DROP PROCEDURE Logging.TrackAqueductDataChangeMessage
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:      Andrew Menary
-- Create date: 2016-06-14
-- Description: Update a counter whenever an AqueductDataChange
--              message fails to be handled correctly.
-- =============================================
CREATE PROCEDURE Logging.TrackAqueductDataChangeMessage
	@dialog UNIQUEIDENTIFIER,
	@messageType SYSNAME = NULL,
	@messageBody XML = NULL
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	IF @dialog IS NULL
		RETURN;

	DECLARE @count INT = NULL;
	SET @count = (
		SELECT  [Count]
		FROM    Logging.AqueductDataChangeFailedMessage
		WHERE   Dialog = @dialog
	);

	IF @count IS NULL
	BEGIN
		INSERT INTO Logging.AqueductDataChangeFailedMessage (
			Dialog,
			MessageType,
			MessageBody,
			[Count]
		) VALUES (
			@dialog,
			@messageType,
			@messageBody,
			1
		);
	END;

	IF @count > 3
	BEGIN
		EXEC Logging.ClearTrackingAqueductDataChangeMessage @dialog, true;
		BEGIN TRY
			END CONVERSATION @dialog
			    WITH ERROR = 500
			    DESCRIPTION = 'Unable to process message.';
		END TRY
		BEGIN CATCH
			-- There might not be a conversation if we are testing.
		END CATCH
	END
	ELSE
	BEGIN
		UPDATE  Logging.AqueductDataChangeFailedMessage
		SET     [Count] = [Count] + 1
		WHERE   Dialog = @dialog;
	END;
END
GO
