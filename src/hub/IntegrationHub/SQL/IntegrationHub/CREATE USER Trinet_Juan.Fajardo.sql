-- ===================================
-- Create Data Reader User template
-- ===================================
USE IntegrationHub
GO

IF EXISTS (SELECT * FROM master.sys.syslogins WHERE name = N'TRINET\Juan.Fajardo')
   AND NOT EXISTS (SELECT * FROM sys.sysusers WHERE name = N'TRINET\Juan.Fajardo')
BEGIN
	-- Add the user to the database using their login name and the user name
	CREATE USER [TRINET\Juan.Fajardo]
		FOR LOGIN [TRINET\Juan.Fajardo]
		WITH DEFAULT_SCHEMA = dbo
END
GO

IF EXISTS (SELECT * FROM sys.sysusers WHERE name = N'TRINET\Juan.Fajardo')
BEGIN
	-- Add user to db_datareader role
	ALTER ROLE [db_datareader]
	ADD MEMBER [TRINET\Juan.Fajardo]
END
GO
