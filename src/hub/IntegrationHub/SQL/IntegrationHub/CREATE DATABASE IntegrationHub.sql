-- =============================================
-- Create database IntegrationHub
-- =============================================
USE master
GO

-- Drop the database if it already exists
IF  EXISTS (SELECT name FROM sys.databases WHERE name = N'IntegrationHub')
DROP DATABASE IntegrationHub
GO

-- This next bit requires SQL Server 2012 or later
DECLARE @InstanceDefaultDataPath NVARCHAR(MAX) = CONVERT(NVARCHAR(MAX), SERVERPROPERTY('InstanceDefaultDataPath')),
        @InstanceDefaultLogPath NVARCHAR(MAX) = CONVERT(NVARCHAR(MAX), SERVERPROPERTY('InstanceDefaultLogPath'));

DECLARE @Sql NVARCHAR(MAX);

SET @Sql = N'
CREATE DATABASE IntegrationHub
ON
PRIMARY
	(NAME = IntegrationHubData1, FILENAME = ''' + @InstanceDefaultDataPath + 'IntegrationHubData1.mdf'', SIZE = 20MB, FILEGROWTH = 10MB),
FILEGROUP MAIN DEFAULT
	(NAME = IntegrationHubData2, FILENAME = ''' + @InstanceDefaultDataPath + 'IntegrationHubData2.ndf'', SIZE = 400MB, FILEGROWTH = 100MB),
FILEGROUP HISTORIC
	(NAME = IntegrationHubData3, FILENAME = ''' + @InstanceDefaultDataPath + 'IntegrationHubData3.ndf'', SIZE = 800MB, FILEGROWTH = 800MB)
LOG ON
	(NAME = IntegrationHubLog, FILENAME = ''' + @InstanceDefaultLogPath + 'IntegrationHubLog.ldf'', SIZE = 200MB);
'

EXECUTE sp_executesql @Sql
GO

-- Databases are created by current user, but for security reasons ownership should be switched to [sa]
IF  EXISTS (SELECT name FROM sys.databases WHERE name = N'IntegrationHub')
	ALTER AUTHORIZATION ON DATABASE::[IntegrationHub] TO [sa]
GO

IF  EXISTS (SELECT name FROM sys.databases WHERE name = N'IntegrationHub')
	RAISERROR('*** CREATED DATABASE %s ***', 10, 1, N'[IntegrationHub]') WITH NOWAIT
ELSE
	RAISERROR('*** FAILED CREATING DATABASE %s ***', 10, 1, N'[IntegrationHub]') WITH NOWAIT
