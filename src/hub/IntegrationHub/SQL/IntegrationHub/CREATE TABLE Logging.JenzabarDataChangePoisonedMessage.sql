/*
 * Table:       IntegrationHub.Logging.JenzabarDataChangePoisonedMessage
 *
 * Author:      Andrew Menary
 * Created:     2016-06-13
 * Description: A table to capture poisoned messages for debugging.
 *
 */

USE IntegrationHub
GO

IF OBJECT_ID(N'[Logging].[JenzabarDataChangePoisonedMessage]', N'U') IS NULL
BEGIN
	CREATE TABLE [Logging].[JenzabarDataChangePoisonedMessage](
	        [PoisonedMessageId] [BIGINT] IDENTITY(1,1) NOT NULL,
	        [Dialog]            [UNIQUEIDENTIFIER] NOT NULL,
	        [MessageType]       [SYSNAME] NULL,
	        [MessageBody]       [XML] NULL,
	        [LastModified] [DATETIME] NOT NULL
	                CONSTRAINT DF_JenzabarDataChangePoisonedMessage_LastModified DEFAULT (CURRENT_TIMESTAMP),

	CONSTRAINT [PK_LoggingJenzabarDataChangePoisonedMessage]
	        PRIMARY KEY CLUSTERED ([PoisonedMessageId])
	) ON HISTORIC

	IF OBJECT_ID(N'[Logging].[JenzabarDataChangePoisonedMessage]', N'U') IS NOT NULL
		RAISERROR('*** CREATED TABLE %s.%s ***', 10, 1, '[Logging]', '[JenzabarDataChangePoisonedMessage]') WITH NOWAIT
	ELSE
		RAISERROR('*** FAILED CREATING TABLE %s.%s ***', 10, 1, '[Logging]', '[JenzabarDataChangePoisonedMessage]') WITH NOWAIT

	IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description', N'SCHEMA', N'Logging', N'TABLE', N'JenzabarDataChangePoisonedMessage', NULL, NULL))
		EXEC sys.sp_addextendedproperty @name=N'MS_Description',
		        @value=N'A table to capture poisoned messages for debugging',
		        @level0type=N'SCHEMA', @level0name=N'Logging',
		        @level1type=N'TABLE', @level1name=N'JenzabarDataChangePoisonedMessage'
END
ELSE
	RAISERROR('*** SKIPPED CREATING EXISTING TABLE %s.%s ***', 10, 1, '[Logging]', '[JenzabarDataChangePoisonedMessage]') WITH NOWAIT
GO
