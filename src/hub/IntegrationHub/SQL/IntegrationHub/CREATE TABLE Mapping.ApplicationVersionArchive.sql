/*
 * Table:       IntegrationHub.Mapping.ApplicationVersionArchive
 *
 * Author:      Juswaldy Jusman
 * Created:     2016-06-09
 * Description: A version archive of the mapping table for Admissions Application identifiers across multiple systems.
 * Updates:
 * 2016-07-29 JJJ Add AQCandidacyId field.
 *
 */

USE IntegrationHub
GO

IF OBJECT_ID(N'[Mapping].[ApplicationVersionArchive]', N'U') IS NULL
BEGIN
	CREATE TABLE [Mapping].[ApplicationVersionArchive](
	        [ApplicationVersionArchiveId] BIGINT NOT NULL IDENTITY(1,1),
	        [ApplicationId]          BIGINT NOT NULL,
	        [AQApplicationId]        INT NULL,
	        [AQCandidacyId]          INT NULL,
	        [ERxApplicationId]       CHAR(18) NULL,
	        [ERxApplicantId]         CHAR(18) NULL,
	        [Name]                   NVARCHAR(254) NULL,
	        [LastModified]           DATETIME NOT NULL,
	        [LastModifier]           NVARCHAR(254) NOT NULL,
	        [Operation]              VARCHAR(7) NOT NULL,
	        [OperatorId]             NVARCHAR(254) NOT NULL,
	        [OperationDate]          DATETIME NOT NULL
	                CONSTRAINT DF_MappingApplicationVersionArchive_OperationDate DEFAULT (CURRENT_TIMESTAMP),

	CONSTRAINT [PK_MappingApplicationVersionArchive]
	        PRIMARY KEY CLUSTERED ([ApplicationVersionArchiveId]) ON [HISTORIC]
	) ON [HISTORIC]

	IF OBJECT_ID(N'[Mapping].[ApplicationVersionArchive]', N'U') IS NOT NULL
		RAISERROR('*** CREATED TABLE %s.%s ***', 10, 1, '[Mapping]', '[ApplicationVersionArchive]') WITH NOWAIT
	ELSE
		RAISERROR('*** FAILED CREATING TABLE %s.%s ***', 10, 1, '[Mapping]', '[ApplicationVersionArchive]') WITH NOWAIT

	IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description', N'SCHEMA', N'Mapping', N'TABLE', N'ApplicationVersionArchive', NULL, NULL))
		EXEC sys.sp_addextendedproperty @name=N'MS_Description',
		        @value=N'A version archive of the mapping table for Application identifiers across multiple systems.',
		        @level0type=N'SCHEMA', @level0name=N'Mapping',
		        @level1type=N'TABLE', @level1name=N'ApplicationVersionArchive'
END
ELSE
	RAISERROR('*** SKIPPED CREATING EXISTING TABLE %s.%s ***', 10, 1, '[Mapping]', '[ApplicationVersionArchive]') WITH NOWAIT
GO
