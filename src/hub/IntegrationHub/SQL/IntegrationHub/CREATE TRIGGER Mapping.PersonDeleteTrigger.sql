/*
 * Trigger:     IntegrationHub.Mapping.PersonDeleteTrigger
 *
 * Author:      Andrew Menary
 * Created:     2016-05-20
 * Description: A trigger to copy deleted person records to the version archive.
 *
 */
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ================================================
-- Author:      Andrew Menary
-- Create date: 2016-05-20
-- Description: Copy deleted person records to the version archive table.
-- ================================================
USE [IntegrationHub]
GO

IF OBJECT_ID ('Mapping.PersonDeleteTrigger','TR') IS NOT NULL
	DROP TRIGGER Mapping.PersonDeleteTrigger
GO

CREATE TRIGGER [Mapping].[PersonDeleteTrigger] ON [Mapping].[Person]
FOR DELETE
AS
DECLARE @OperatorId NVARCHAR(254)
BEGIN
	SELECT  @OperatorId = LastModifier FROM DELETED
	INSERT INTO Mapping.PersonVersionArchive 
	([PersonId], [AQPersonId], [ERxContactId], [CvwIndividualId], [Name], [LastModified], [LastModifier], Operation, OperatorId, OperationDate)
	SELECT  [PersonId], [AQPersonId], [ERxContactId], [CvwIndividualId], [Name], [LastModified], [LastModifier], 'Delete', @OperatorId, CURRENT_TIMESTAMP
	FROM    DELETED
END
GO
