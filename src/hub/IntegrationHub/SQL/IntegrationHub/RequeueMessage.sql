USE IntegrationHub
GO

-- Drop stored procedure if it already exists
IF EXISTS (
    SELECT  *
    FROM    INFORMATION_SCHEMA.ROUTINES
    WHERE   SPECIFIC_SCHEMA = N'Messaging'
    AND     SPECIFIC_NAME = N'RequeueMessage'
)
    DROP PROCEDURE Messaging.RequeueMessage
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================================================
-- Author:      Juswaldy Jusman
-- Create date: 2016-10-13
-- Description: Requeue a message for reprocessing.
-- =============================================================================
CREATE PROCEDURE Messaging.RequeueMessage
	@QueueName VARCHAR(MAX), -- MessageInProcessQueue or MessageCompleted or MessageDeadLetter
	@Topic VARCHAR(MAX) = NULL, -- Only for a specific topic, leave null for any topic
	@Id INT = NULL -- Only for a specific message, leave null for any message
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	-- SET XACT_ABORT ON to ensure any existing transaction
	-- and not just the current statement is rolled back if
	-- there is a run-time error.
	SET XACT_ABORT ON;

	DECLARE
		@ErrorNumber INT = NULL,
		@ErrorMessage VARCHAR(MAX) = NULL,
		@Script NVARCHAR(MAX) = NULL,
		@WhereClause VARCHAR(MAX) = NULL;

	DECLARE
		@Payload XML = NULL,
		@Due DATETIME = NULL,
		@Expires DATETIME = NULL,
		@CorrelationId BIGINT = NULL;

	BEGIN TRY

		-- Validate parameters.
		IF COALESCE(@QueueName, '') = '' THROW 50001, N'Parameter @QueueName must be specified', 1;
		IF @Topic IS NULL AND @Id IS NULL THROW 50001, N'Parameter @Topic and @Id cannot both be null', 1; 
		IF @Id IS NULL AND @QueueName != 'InProcessQueue' THROW 50001, N'Only the InProcessQueue will accept mass reprocessing of a Topic', 1; 

		-- Prepare the where clause.
		IF @Id IS NULL
		BEGIN
			SET @WhereClause = 'Topic = ' + @Topic;
		END
		ELSE
		BEGIN
			IF @QueueName = 'MessageInProcessQueue' SET @WhereClause = 'RunId';
			ELSE IF @QueueName = 'MessageCompleted' SET @WhereClause = 'CompletedId';
			ELSE IF @QueueName = 'MessageDeadLetter' SET @WhereClause = 'DeadLetterId';
			SET @WhereClause = @WhereClause + ' = ' + CONVERT(VARCHAR(MAX), @Id);

			IF @Topic IS NOT NULL
			BEGIN
				SET @WhereClause = 'Topic = ' + @Topic + ' AND ' + @WhereClause;
			END
		END

		-- Declare cursor dynamically.
		SET @Script = 'DECLARE RequeueCursor CURSOR FOR
			SELECT Topic, Payload, Due, Expires, CorrelationId
			FROM Messaging.' + @QueueName + '
			WHERE ' + @WhereClause + '
			ORDER BY Due ASC;'
		EXEC sp_executesql @script;

		OPEN RequeueCursor;
		FETCH NEXT FROM RequeueCursor INTO @Topic, @Payload, @Due, @Expires, @CorrelationId;
		WHILE @@FETCH_STATUS = 0
		BEGIN
			FETCH NEXT FROM RequeueCursor INTO @Topic, @Payload, @Due, @Expires, @CorrelationId;
			EXEC Messaging.MessageEnqueue @Topic, @Payload, @Due, @Expires, @CorrelationId;
		END
		CLOSE RequeueCursor;
		DEALLOCATE RequeueCursor;

	END TRY
	BEGIN CATCH
		-- Execute standard error handler
		SET @ErrorNumber = ERROR_NUMBER() + 50000;
		SET @ErrorMessage = 'RequeueMessage: ' + ERROR_MESSAGE();
		THROW @ErrorNumber, @ErrorMessage, 1;
	END CATCH;

END
GO
