/*
 * Procedure: IntegrationHub.Messaging.MonitorMessageQueues
 */

USE IntegrationHub
GO

-- Drop stored procedure if it already exists
IF EXISTS (
    SELECT  *
    FROM    INFORMATION_SCHEMA.ROUTINES
    WHERE   SPECIFIC_SCHEMA = N'Messaging'
    AND     SPECIFIC_NAME = N'MonitorMessageQueues'
)
    DROP PROCEDURE Messaging.MonitorMessageQueues
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:      Andrew Menary
-- Create date: 2016-05-20
-- Description: Examine data synchronization message queues to find stuck
--              or expired messages and deal with them appropriately.
-- =============================================
CREATE PROCEDURE Messaging.MonitorMessageQueues
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	-- SET XACT_ABORT ON to ensure any existing transaction
	-- and not just the current statement is rolled back if
	-- there is a run-time error.
	SET XACT_ABORT ON;

	DECLARE @Now           DATETIME,
	        @ReturnCode    INT = 0, -- Default to success.
	        @Id            BIGINT,
	        @CorrelationId BIGINT,
	        @Topic         VARCHAR(254),
	        @Payload       XML,
	        @Due           DATETIME = NULL,
	        @Expires       DATETIME = NULL,
	        @ErrorNumber   INTEGER = 0,
	        @ErrorMessage  NVARCHAR(MAX) = ''

	SET @Now = GETUTCDATE();

	BEGIN TRY
		DECLARE @hasOuterTransaction BIT = CASE WHEN @@TRANCOUNT > 0 THEN 1 ELSE 0 END;
		DECLARE @rollbackPoint NCHAR(32) = REPLACE(CONVERT(NCHAR(36), NEWID()), N'-', N'');

		IF @hasOuterTransaction = 1
		BEGIN
			SAVE TRANSACTION @rollbackPoint;
		END
		ELSE
		BEGIN
			BEGIN TRANSACTION @rollbackPoint;
		END;

		-- Remove Dead Letters
		SELECT  Id,
		        CorrelationId
		INTO    #DeadLettersPending
		FROM    Messaging.MessageQueue
		WHERE   Expires < @Now

		SELECT  Id,
		        CorrelationId
		INTO    #DeadLettersProcessing
		FROM    Messaging.MessageInProcessQueue
		WHERE   Expires < @Now

		DECLARE deadLetterCursor CURSOR LOCAL FAST_FORWARD
			FOR SELECT Id, CorrelationId FROM #DeadLettersPending
			    UNION
			    SELECT Id, CorrelationId FROM #DeadLettersProcessing;
		OPEN deadLetterCursor;
		FETCH NEXT FROM deadLetterCursor
		INTO @Id, @CorrelationId;

		WHILE @@FETCH_STATUS = 0
		BEGIN
			EXECUTE Messaging.MessageDiscardDeadLetter @Id = @Id, @CorrelationId = @CorrelationId;

			FETCH NEXT FROM deadLetterCursor
			INTO @Id, @CorrelationId;
		END

		CLOSE deadLetterCursor;
		DEALLOCATE deadLetterCursor;

		-- Resubmit Stuck Messages
		-- (when they have been stuck for more than 15 minutes)
		SELECT  Id,
		        CorrelationId,
		        Expires,
		        Topic,
		        Payload
		INTO    #StuckMessages
		FROM    Messaging.MessageInProcessQueue
		WHERE   DATEDIFF(minute, Locked, @now) > 15

		DECLARE stuckMessageCursor CURSOR LOCAL FAST_FORWARD
			FOR SELECT Id, CorrelationId, Expires, Topic, Payload FROM #StuckMessages;
		OPEN stuckMessageCursor;
		FETCH NEXT FROM stuckMessageCursor
		INTO @Id, @CorrelationId, @Expires, @Topic, @Payload;

		WHILE @@FETCH_STATUS = 0
		BEGIN
			SELECT  @Due = DATEADD(minute, 5, @Now);
			EXECUTE Messaging.MessageEnqueue @Topic = @Topic, @Payload = @Payload, @Due = @Due, @Expires = @Expires, @CorrelationId = @CorrelationId;
			EXECUTE Messaging.MessageAckDone @Id = @Id, @CorrelationId = @CorrelationId, @ErrorMessage = 'Message was stuck in processing queue and has been re-enqueued.';

			FETCH NEXT FROM stuckMessageCursor
			INTO @Id, @CorrelationId
		END

		CLOSE stuckMessageCursor;
		DEALLOCATE stuckMessageCursor;

		IF @hasOuterTransaction = 0
		BEGIN
			COMMIT TRANSACTION;
		END;

		-- Report
		DECLARE @Report TABLE (
		        Msg VARCHAR(MAX)
		)

		IF EXISTS(SELECT * FROM #DeadLettersPending)
		BEGIN
			INSERT  @Report (Msg)
			SELECT  Msg = 'There were ' + COUNT(*) + 'new dead letters in the pending queue.'
			FROM    #DeadLettersPending
		END

		IF EXISTS(SELECT * FROM #DeadLettersProcessing)
		BEGIN
			INSERT  @Report (Msg)
			SELECT  Msg = 'There were ' + COUNT(*) + 'new dead letters in the processing queue.'
			FROM    #DeadLettersProcessing
		END

		IF EXISTS(SELECT * FROM #StuckMessages)
		BEGIN
			INSERT  @Report (Msg)
			SELECT  Msg = 'There were ' + COUNT(*) + 'stuck messages in the processing queue.'
			FROM    #StuckMessages
		END

		IF EXISTS(SELECT * FROM @Report)
		BEGIN
			DECLARE @ReportHTML NVARCHAR(MAX);

			SET @ReportHTML =
				N'<h1>Message Queue Monitor Report</h1>' +
				N'<table border="0">' +
				CAST ((
					SELECT  td = Msg
					FROM    @Report
					FOR XML PATH('tr'), TYPE
				) AS NVARCHAR(MAX)) +
				N'</table>';

			BEGIN TRY
				EXEC msdb.dbo.sp_send_dbmail
					@profile_name = 'TWU',
					@recipients = 'dev@twu.ca',
					@subject = 'IntegrationHub Message Queue Monitor Report',
					@body = @ReportHTML,
					@body_format = 'HTML';
			END TRY
			BEGIN CATCH
				-- Eat it, so we don't rollback for the sake of an undeliverable report
			END CATCH;
		END
	END TRY
	BEGIN CATCH
		IF XACT_STATE() = 1
		BEGIN
			ROLLBACK TRANSACTION @rollbackPoint;
		END;

		-- Execute standard error handler
		SET @ErrorNumber = ERROR_NUMBER() + 50000;
		SET @ErrorMessage = ERROR_MESSAGE();
		THROW @ErrorNumber, @ErrorMessage, 1;
	END CATCH;
END
GO

/* If stored procedure created successfully then add permissions */
IF EXISTS (
    SELECT  *
    FROM    INFORMATION_SCHEMA.ROUTINES
    WHERE   SPECIFIC_SCHEMA = N'Messaging'
    AND     SPECIFIC_NAME = N'MonitorMessageQueues'
)
BEGIN
	IF EXISTS (SELECT * FROM sys.sysusers WHERE name = N'SalesforceSyncDEV')
	GRANT EXECUTE ON Messaging.MonitorMessageQueues TO SalesforceSyncDEV

	IF EXISTS (SELECT * FROM sys.sysusers WHERE name = N'SalesforceSyncTEST')
	GRANT EXECUTE ON Messaging.MonitorMessageQueues TO SalesforceSyncTEST
 
	IF EXISTS (SELECT * FROM sys.sysusers WHERE name = N'SalesforceSync')
	GRANT EXECUTE ON Messaging.MonitorMessageQueues TO SalesforceSync

	IF EXISTS (SELECT * FROM sys.sysusers WHERE name = N'cloveretl')
	GRANT EXECUTE ON Messaging.MonitorMessageQueues TO cloveretl
END
GO
