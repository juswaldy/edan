-- ===================================
-- Create Unique Nonclustered Index
-- ===================================
USE IntegrationHub
GO

SET QUOTED_IDENTIFIER ON
GO

IF EXISTS (SELECT * FROM sys.indexes WHERE name = N'AK_Mapping_Organization_ERxSchoolId')
	DROP INDEX AK_Mapping_Organization_ERxSchoolId
	ON Mapping.Organization
GO

CREATE UNIQUE NONCLUSTERED INDEX AK_Mapping_Organization_ERxSchoolId
ON Mapping.Organization
(
	ERxSchoolId ASC
)
WHERE
	ERxSchoolId IS NOT NULL
WITH
(
	SORT_IN_TEMPDB = OFF,
	DROP_EXISTING = OFF
)
GO

EXEC sys.sp_addextendedproperty
	@name=N'MS_Description',
	@value=N'Ensure uniqueness of Enrollment Rx School identifiers.',
	@level0type=N'SCHEMA',
	@level0name=N'Mapping',
	@level1type=N'TABLE',
	@level1name=N'Organization',
	@level2type=N'INDEX',
	@level2name=N'AK_Mapping_Organization_ERxSchoolId'
GO
