/*
 * Trigger:     IntegrationHub.Mapping.NoteDeleteTrigger
 *
 * Author:      Juswaldy Jusman
 * Created:     2016-08-15
 * Description: A trigger to copy deleted Note records to the version archive.
 *
 */
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ================================================
-- Author:      Juswaldy Jusman
-- Create date: 2016-08-15
-- Description: Copy deleted Note records to the version archive table.
-- ================================================
USE [IntegrationHub]
GO

IF OBJECT_ID ('Mapping.NoteDeleteTrigger','TR') IS NOT NULL
	DROP TRIGGER Mapping.NoteDeleteTrigger
GO

CREATE TRIGGER [Mapping].[NoteDeleteTrigger] ON [Mapping].[Note]
FOR DELETE
AS
DECLARE @OperatorId NVARCHAR(254)
BEGIN
	SELECT  @OperatorId = LastModifier FROM DELETED
	INSERT INTO Mapping.NoteVersionArchive 
	([NoteId], [AQNoteId], [ERxTaskId], [CvwTaskId], [LastModified], [LastModifier], Operation, OperatorId, OperationDate)
	SELECT  [NoteId], [AQNoteId], [ERxTaskId], [CvwTaskId], [LastModified], [LastModifier], 'Delete', @OperatorId, CURRENT_TIMESTAMP
	FROM    DELETED
END
GO
