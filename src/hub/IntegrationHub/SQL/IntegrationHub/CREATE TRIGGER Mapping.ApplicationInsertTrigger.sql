/*
 * Trigger:     IntegrationHub.Mapping.ApplicationInsertTrigger
 *
 * Author:      Juswaldy Jusman
 * Created:     2016-06-09
 * Description: A trigger to copy new Application mappings to the version archive.
 *
 */
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ================================================
-- Author:      Juswaldy Jusman
-- Create date: 2016-06-09
-- Description: Copy new Application mappings to the version archive table
-- ================================================
USE [IntegrationHub]
GO

IF OBJECT_ID ('Mapping.ApplicationInsertTrigger','TR') IS NOT NULL
	DROP TRIGGER Mapping.ApplicationInsertTrigger 
GO

CREATE TRIGGER [Mapping].[ApplicationInsertTrigger] ON [Mapping].[Application]
FOR INSERT
AS
DECLARE @OperatorId NVARCHAR(254)
BEGIN
	SELECT  @OperatorId = LastModifier FROM INSERTED
	INSERT INTO Mapping.ApplicationVersionArchive
	([ApplicationId], [AQApplicationId], [AQCandidacyId], [ERxApplicationId], [ERxApplicantId], [Name], [LastModified], [LastModifier], Operation, OperatorId, OperationDate)
	SELECT  [ApplicationId], [AQApplicationId], [AQCandidacyId], [ERxApplicationId], [ERxApplicantId], [Name], [LastModified], [LastModifier], 'Insert', @OperatorId, CURRENT_TIMESTAMP
	FROM    INSERTED
END
GO
