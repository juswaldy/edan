USE IntegrationHub;

-- Drop stored procedure if it already exists
IF EXISTS (
    SELECT  *
    FROM    INFORMATION_SCHEMA.ROUTINES
    WHERE   SPECIFIC_SCHEMA = N'Messaging'
    AND     SPECIFIC_NAME = N'ThrottleMessageQueue'
)
    DROP PROCEDURE Messaging.ThrottleMessageQueue
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================================================
-- Author:      Juswaldy Jusman
-- Create date: 2017-05-11
-- Description: Throttle the inbound Message Queue and call Clover.
-- =============================================================================
CREATE PROCEDURE Messaging.ThrottleMessageQueue
	@Topic VARCHAR(MAX) = NULL,
	@MaxInProcess    INT = 7,
	@MaxRetries      INT = 70
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	-- SET XACT_ABORT ON to ensure any existing transaction
	-- and not just the current statement is rolled back if
	-- there is a run-time error.
	SET XACT_ABORT ON;

	DECLARE
		@ErrorNumber        INT = NULL,
		@ErrorMessage       VARCHAR(MAX) = NULL,
		@CurrentlyInProcess INT = 0,
		@NumberOfRetries    INT = 0,
		@ToggleOnOff        BIT = 0,
		@KeepGoing          BIT = 1,
		@RunId BIGINT;

	BEGIN TRY

		DECLARE RerunCursor CURSOR FOR
			SELECT Topic
			FROM Messaging.MessageQueue
			WHERE ((@Topic IS NULL AND 1=1) OR Topic = @Topic)
			AND Topic NOT LIKE 'Unknown.%'
			AND Topic NOT LIKE 'Unmapped.%'
			ORDER BY Due ASC;
		OPEN RerunCursor;

		WHILE @KeepGoing = 1
		BEGIN
			-- Get the number of messages which are still in the in-process queue.
			SELECT @CurrentlyInProcess = COUNT(*)
			FROM Messaging.MessageInProcessQueue;

			-- If the number is still under the max specified, then keep calling Clover for messages in the inbound queue.
			-- Otherwise, skip.
			IF @CurrentlyInProcess < @MaxInProcess
			BEGIN
				-- Turn the toggle on.
				IF @ToggleOnOff = 0
				BEGIN
					SET @ToggleOnOff = (@ToggleOnOff + 1) % 2;
					RAISERROR('Turning throttler ON', 0, 1) WITH NOWAIT;
				END

				-- Get the next item and call Clover.
				FETCH NEXT FROM RerunCursor INTO @Topic;
				IF @@FETCH_STATUS = 0
				BEGIN
					EXEC Messaging.RunCloverGraph @graphName = @Topic, @runId = @RunId;
				END
				ELSE
				BEGIN
					SET @KeepGoing = 0;
					RAISERROR('No more messages. Exiting', 0, 1) WITH NOWAIT;
				END

				-- Reset number of tries.
				SET @NumberOfRetries = 0;
			END
			ELSE
			BEGIN
				-- Turn the toggle off.
				IF @ToggleOnOff = 1
				BEGIN
					SET @ToggleOnOff = (@ToggleOnOff + 1) % 2;
					RAISERROR('OOPS! TOO MUCH! Turning throttler OFF', 0, 1) WITH NOWAIT;
				END

				-- Increase number of tries.
				SET @NumberOfRetries = @NumberOfRetries + 1;
				SET @ErrorMessage = 'Number of retries: ' + CONVERT(VARCHAR(MAX), @NumberOfRetries);
				RAISERROR(@ErrorMessage, 0, 1) WITH NOWAIT;

				-- Quit if we have exceeded max tries.
				IF @NumberOfRetries > @MaxRetries
					SET @KeepGoing = 0;
			END

			-- Wait throttle seconds.
			WAITFOR DELAY '00:00:01';
		END

		CLOSE RerunCursor;
		DEALLOCATE RerunCursor;

	END TRY
	BEGIN CATCH
		-- Execute standard error handler
		SET @ErrorNumber = ERROR_NUMBER() + 50000;
		SET @ErrorMessage = 'ThrottleMessageQueue: ' + ERROR_MESSAGE();
		THROW @ErrorNumber, @ErrorMessage, 1;
	END CATCH;

END
GO
