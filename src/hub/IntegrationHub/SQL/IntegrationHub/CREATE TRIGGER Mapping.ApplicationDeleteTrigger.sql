/*
 * Trigger:     IntegrationHub.Mapping.ApplicationDeleteTrigger
 *
 * Author:      Juswaldy Jusman
 * Created:     2016-06-09
 * Description: A trigger to copy deleted Application mappings to the version archive.
 *
 */
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ================================================
-- Author:      Juswaldy Jusman
-- Create date: 2016-06-09
-- Description: Copy deleted Application mappings to the version archive table.
-- ================================================
USE [IntegrationHub]
GO

IF OBJECT_ID ('Mapping.ApplicationDeleteTrigger','TR') IS NOT NULL
	DROP TRIGGER Mapping.ApplicationDeleteTrigger
GO

CREATE TRIGGER [Mapping].[ApplicationDeleteTrigger] ON [Mapping].[Application]
FOR DELETE
AS
DECLARE @OperatorId NVARCHAR(254)
BEGIN
	SELECT  @OperatorId = LastModifier FROM DELETED
	INSERT INTO Mapping.ApplicationVersionArchive 
	([ApplicationId], [AQApplicationId], [AQCandidacyId], [ERxApplicationId], [ERxApplicantId], [Name], [LastModified], [LastModifier], Operation, OperatorId, OperationDate)
	SELECT  [ApplicationId], [AQApplicationId], [AQCandidacyId], [ERxApplicationId], [ERxApplicantId], [Name], [LastModified], [LastModifier], 'Delete', @OperatorId, CURRENT_TIMESTAMP
	FROM    DELETED
END
GO
