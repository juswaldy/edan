/*
 * Trigger:     IntegrationHub.Mapping.NoteInsertTrigger
 *
 * Author:      Juswaldy Jusman
 * Created:     2016-08-15
 * Description: A trigger to copy new Note records to the version archive.
 *
 */
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ================================================
-- Author:      Juswaldy Jusman
-- Create date: 2016-08-15
-- Description: Copy new Note records to the version archive table.
-- ================================================
USE [IntegrationHub]
GO

IF OBJECT_ID ('Mapping.NoteInsertTrigger','TR') IS NOT NULL
	DROP TRIGGER Mapping.NoteInsertTrigger 
GO

CREATE TRIGGER [Mapping].[NoteInsertTrigger] ON [Mapping].[Note]
FOR INSERT
AS
DECLARE @OperatorId NVARCHAR(254)
BEGIN
	SELECT  @OperatorId = LastModifier FROM INSERTED
	INSERT INTO Mapping.NoteVersionArchive
	([NoteId], [AQNoteId], [ERxTaskId], [CvwTaskId], [LastModified], [LastModifier], Operation, OperatorId, OperationDate)
	SELECT  [NoteId], [AQNoteId], [ERxTaskId], [CvwTaskId], [LastModified], [LastModifier], 'Insert', @OperatorId, CURRENT_TIMESTAMP
	FROM    INSERTED
END
GO
