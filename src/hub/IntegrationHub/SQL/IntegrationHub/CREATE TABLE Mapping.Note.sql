/*
 * Table:       IntegrationHub.Mapping.Note
 *
 * Author:      Juswaldy Jusman
 * Created:     2016-08-15
 * Description: A mapping table for Note identifiers across multiple systems.
 *
 */

USE IntegrationHub
GO

IF OBJECT_ID(N'[Mapping].[Note]', N'U') IS NULL
BEGIN
	CREATE TABLE [Mapping].[Note](
	        [NoteId]          BIGINT NOT NULL IDENTITY(1,1),
	        [AQNoteId]        INT NULL,
	        [ERxTaskId]       CHAR(18) NULL,
	        [CvwTaskId]       CHAR(18) NULL,
	        [LastModified]    DATETIME NOT NULL
	                CONSTRAINT DF_MappingNote_LastModified DEFAULT (CURRENT_TIMESTAMP),
	        [LastModifier]    NVARCHAR(254) NOT NULL

	CONSTRAINT [PK_MappingNote]
	        PRIMARY KEY CLUSTERED ([NoteId])
	)

	IF OBJECT_ID(N'[Mapping].[Note]', N'U') IS NOT NULL
		RAISERROR('*** CREATED TABLE %s.%s ***', 10, 1, '[Mapping]', '[Note]') WITH NOWAIT
	ELSE
		RAISERROR('*** FAILED CREATING TABLE %s.%s ***', 10, 1, '[Mapping]', '[Note]') WITH NOWAIT

	IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description', N'SCHEMA', N'Mapping', N'TABLE', N'Note', NULL, NULL))
		EXEC sys.sp_addextendedproperty @name=N'MS_Description',
		        @value=N'A mapping table for Note identifiers across multiple systems.',
		        @level0type=N'SCHEMA', @level0name=N'Mapping',
		        @level1type=N'TABLE', @level1name=N'Note'
END
ELSE
	RAISERROR('*** SKIPPED CREATING EXISTING TABLE %s.%s ***', 10, 1, '[Mapping]', '[Note]') WITH NOWAIT
GO
