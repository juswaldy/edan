-- ================================================
-- Author:      Juswaldy Jusman
-- Create date: 2016-07-28
-- Description: Create a Service Broker Service to
--              receive JenzabarDataChange messages.
-- ================================================

USE master
GO

-- If the database is not enabled for service broker
IF NOT EXISTS (SELECT 1 FROM sys.databases WHERE database_id = DB_ID(N'IntegrationHub') AND is_broker_enabled = 1)

	-- then enable it with this statement:
	ALTER DATABASE IntegrationHub
	    SET ENABLE_BROKER
	    WITH ROLLBACK IMMEDIATE;
GO

-- To disable service broker:
--ALTER DATABASE IntegrationHub
--    SET DISABLE_BROKER;
--GO

-- If the database is not marked as trustworthy
IF NOT EXISTS (SELECT 1 FROM sys.databases WHERE database_id = DB_ID(N'IntegrationHub') AND is_trustworthy_on = 1)

	-- then mark the database as TRUSTWORTHY so it can be allowed
	-- to access resources beyond the database itself
	ALTER DATABASE IntegrationHub
	    SET TRUSTWORTHY ON;
GO

-- Create the required meta-data
--** Target (IntegrationHub) Database ** --

USE IntegrationHub
GO

-- Create message types for valid XML messages to be sent and received
CREATE MESSAGE TYPE [//www.twu.ca/integration/messages/JenzabarDataChange]
    VALIDATION = WELL_FORMED_XML;

CREATE MESSAGE TYPE [//www.twu.ca/integration/messages/JenzabarDataChangeStatus]
    VALIDATION = WELL_FORMED_XML;
GO

-- Create a contract which will be used by the service to validate what
-- types of messages are allowed for initiator and target
CREATE CONTRACT [//www.twu.ca/integration/contracts/JenzabarDataChange/v1.0] (
    [//www.twu.ca/integration/messages/JenzabarDataChange] SENT BY INITIATOR,
    [//www.twu.ca/integration/messages/JenzabarDataChangeStatus] SENT BY TARGET
);
GO

-- Create a queue which is an internal physical table to hold the messages
-- passed to the service.  You can SELECT from this table, but you can't
-- INSERT, UPDATE, or DELETE.  You need to use SEND and RECEIVE commands
-- to send messages to the queue and receive messages from it.
CREATE QUEUE [Messaging].[JenzabarDataChangeSubscriberQueue]
    WITH STATUS = ON,
    ACTIVATION (
       PROCEDURE_NAME = [Messaging].[JenzabarDataChangeSubscriberQueueActivator],
       MAX_QUEUE_READERS = 1,
       EXECUTE AS OWNER)
GO

-- Create a service, which is a logical endpoint sitting atop a queue on
-- which messages are either sent or received.  With service creation
-- you also specify the contract used to validate messages on the service.
CREATE SERVICE [JenzabarDataChangeSubscriber]
    ON QUEUE Messaging.JenzabarDataChangeSubscriberQueue (
        [//www.twu.ca/integration/contracts/JenzabarDataChange/v1.0]
    );
GO

--ALTER QUEUE [Messaging].[JenzabarDataChangeSubscriberQueue] WITH ACTIVATION (STATUS = OFF);
--GO

--ALTER QUEUE [Messaging].[JenzabarDataChangeSubscriberQueue] WITH ACTIVATION (STATUS = ON);
--GO

--USE IntegrationHub
--GO

---- Remove all service broker database objects related to JenzabarDataChange
--DROP SERVICE [JenzabarDataChangeSubscriber]
--DROP QUEUE [Messaging].[JenzabarDataChangeSubscriberQueue]
--DROP CONTRACT [//www.twu.ca/integration/contracts/JenzabarDataChange/v1.0]
--DROP MESSAGE TYPE [//www.twu.ca/integration/messages/JenzabarDataChange]
--DROP MESSAGE TYPE [//www.twu.ca/integration/messages/JenzabarDataChangeStatus]
--IF EXISTS (SELECT 1 FROM sys.procedures WHERE [object_id] = OBJECT_ID(N'[Messaging].[JenzabarDataChangeSubscriberQueueActivator]'))
--DROP PROCEDURE [Messaging].[JenzabarDataChangeSubscriberQueueActivator]
--GO
