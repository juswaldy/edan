/*
 * Trigger:     IntegrationHub.Mapping.PersonUpdateTrigger
 *
 * Author:      Andrew Menary
 * Created:     2016-05-20
 * Description: A trigger to copy updated person records to the version archive.
 *
 */
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ================================================
-- Author:      Andrew Menary
-- Create date: 2016-05-20
-- Description: Copy updated person records to the version archive table.
-- ================================================
USE [IntegrationHub]
GO

IF OBJECT_ID ('Mapping.PersonUpdateTrigger','TR') IS NOT NULL
	DROP TRIGGER Mapping.PersonUpdateTrigger
GO

CREATE TRIGGER [Mapping].[PersonUpdateTrigger] ON [Mapping].[Person]
FOR UPDATE
AS
DECLARE @OperatorId NVARCHAR(254)
BEGIN
	SELECT  @OperatorId = LastModifier FROM INSERTED
	INSERT INTO Mapping.PersonVersionArchive
	([PersonId], [AQPersonId], [ERxContactId], [CvwIndividualId], [Name], [LastModified], [LastModifier], Operation, OperatorId, OperationDate)
	SELECT  [PersonId], [AQPersonId], [ERxContactId], [CvwIndividualId], [Name], [LastModified], [LastModifier], 'Update', @OperatorId, CURRENT_TIMESTAMP
	FROM    DELETED
END
GO
