/*
 * Procedure: IntegrationHub.Messaging.MessageEnqueue
 */

USE IntegrationHub
GO

-- Drop stored procedure if it already exists
IF EXISTS (
    SELECT  *
    FROM    INFORMATION_SCHEMA.ROUTINES
    WHERE   SPECIFIC_SCHEMA = N'Messaging'
    AND     SPECIFIC_NAME = N'MessageEnqueue'
)
    DROP PROCEDURE Messaging.MessageEnqueue
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:      Andrew Menary
-- Create date: 2016-05-20
-- Description: Push a new data synchronization message onto the queue.
-- =============================================
CREATE PROCEDURE Messaging.MessageEnqueue
	@Topic         VARCHAR(254),
	@Payload       XML,
	@Due           DATETIME = NULL,
	@Expires       DATETIME = NULL,
	@CorrelationId BIGINT = 0
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	-- SET XACT_ABORT ON to ensure any existing transaction
	-- and not just the current statement is rolled back if
	-- there is a run-time error.
	SET XACT_ABORT ON;

	DECLARE @Id           BIGINT,
	        @ReturnCode   INT = 0, -- Default to success.
	        @ErrorNumber  INT = NULL,
	        @ErrorMessage VARCHAR(MAX) = NULL;

	-- Validate parameters
	IF @Due IS NULL
		SET @Due = GETUTCDATE();

	IF @Expires IS NULL
		SET @Expires = DATEADD(day, 1, GETUTCDATE());

	BEGIN TRY
		DECLARE @hasOuterTransaction BIT = CASE WHEN @@TRANCOUNT > 0 THEN 1 ELSE 0 END;
		DECLARE @rollbackPoint NCHAR(32) = REPLACE(CONVERT(NCHAR(36), NEWID()), N'-', N'');

		IF @hasOuterTransaction = 1
		BEGIN
			SAVE TRANSACTION @rollbackPoint;
		END
		ELSE
		BEGIN
			BEGIN TRANSACTION @rollbackPoint;
		END;

		INSERT INTO Messaging.MessageQueue (
		        CorrelationId,
		        Due,
		        Expires,
		        Topic,
		        Payload
		) VALUES (
		        @CorrelationId,
		        @Due,
		        @Expires,
		        @Topic,
		        @Payload
		);

		SELECT  @Id = SCOPE_IDENTITY();

		IF @CorrelationId = 0
		BEGIN
			SET @CorrelationId = @Id;

			UPDATE  Messaging.MessageQueue
			SET     CorrelationId = @CorrelationId
			WHERE   Due = @Due
			AND     Topic = @Topic
			AND     Id = @Id;
		END

		SELECT  @ReturnCode, @Id, @CorrelationId;

		IF @hasOuterTransaction = 0
		BEGIN
			COMMIT TRANSACTION;
		END;
	END TRY
	BEGIN CATCH
		IF XACT_STATE() = 1
		BEGIN
			ROLLBACK TRANSACTION @rollbackPoint;
		END;

		-- Execute standard error handler
		SET @ErrorNumber = ERROR_NUMBER() + 50000;
		SET @ErrorMessage = ERROR_MESSAGE();
		THROW @ErrorNumber, @ErrorMessage, 1;
	END CATCH;
END
GO

/* If stored procedure created successfully then add permissions */
IF EXISTS (
    SELECT  *
    FROM    INFORMATION_SCHEMA.ROUTINES
    WHERE   SPECIFIC_SCHEMA = N'Messaging'
    AND     SPECIFIC_NAME = N'MessageEnqueue'
)
BEGIN
	IF EXISTS (SELECT * FROM sys.sysusers WHERE name = N'SalesforceSyncDEV')
	GRANT EXECUTE ON Messaging.MessageEnqueue TO SalesforceSyncDEV

	IF EXISTS (SELECT * FROM sys.sysusers WHERE name = N'SalesforceSyncTEST')
	GRANT EXECUTE ON Messaging.MessageEnqueue TO SalesforceSyncTEST
 
	IF EXISTS (SELECT * FROM sys.sysusers WHERE name = N'SalesforceSync')
	GRANT EXECUTE ON Messaging.MessageEnqueue TO SalesforceSync

	IF EXISTS (SELECT * FROM sys.sysusers WHERE name = N'cloveretl')
	GRANT EXECUTE ON Messaging.MessageEnqueue TO cloveretl
END
GO
