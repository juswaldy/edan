-- ===================================
-- Create Data Reader/Data Writer User template
-- ===================================
USE IntegrationHub
GO

IF EXISTS (SELECT * FROM master.sys.syslogins WHERE name = N'cloveretl')
   AND NOT EXISTS (SELECT * FROM sys.sysusers WHERE name = N'cloveretl')
BEGIN
	-- Add the user to the database using their login name and the user name
	CREATE USER [cloveretl]
		FOR LOGIN [cloveretl]
		WITH DEFAULT_SCHEMA = dbo
END
GO

IF EXISTS (SELECT * FROM sys.sysusers WHERE name = N'cloveretl')
BEGIN
	-- Add user to db_datareader role
	ALTER ROLE [db_datareader]
	ADD MEMBER [cloveretl]

	-- Add user to db_datawriter role
	ALTER ROLE [db_datawriter]
	ADD MEMBER [cloveretl]
END
GO
