-- ===================================
-- Create Unique Nonclustered Index
-- ===================================
USE IntegrationHub
GO

SET QUOTED_IDENTIFIER ON
GO

IF EXISTS (SELECT * FROM sys.indexes WHERE name = N'AK_Mapping_Document_ERxAttachmentId')
	DROP INDEX AK_Mapping_Document_ERxAttachmentId
	ON Mapping.Document
GO

CREATE UNIQUE NONCLUSTERED INDEX AK_Mapping_Document_ERxAttachmentId
ON Mapping.Document 
(
	ERxAttachmentId ASC
)
WHERE
	ERxAttachmentId IS NOT NULL
WITH
(
	SORT_IN_TEMPDB = OFF,
	DROP_EXISTING = OFF
)
GO

EXEC sys.sp_addextendedproperty
	@name=N'MS_Description',
	@value=N'Ensure uniqueness of ERx Attachment identifiers.',
	@level0type=N'SCHEMA',
	@level0name=N'Mapping',
	@level1type=N'TABLE',
	@level1name=N'Document',
	@level2type=N'INDEX',
	@level2name=N'AK_Mapping_Document_ERxAttachmentId'
GO
