/*
 * Table:       IntegrationHub.Mapping.NoteVersionArchive
 *
 * Author:      Juswaldy Jusman
 * Created:     2016-08-15
 * Description: A version archive of the mapping table for Note identifiers across multiple systems.
 *
 */

USE IntegrationHub
GO

IF OBJECT_ID(N'[Mapping].[NoteVersionArchive]', N'U') IS NULL
BEGIN
	CREATE TABLE [Mapping].[NoteVersionArchive](
	        [NoteVersionArchiveId] BIGINT NOT NULL IDENTITY(1,1),
	        [NoteId]                 BIGINT NOT NULL,
	        [AQNoteId]               INT NULL,
	        [ERxTaskId]              CHAR(18) NULL,
	        [CvwTaskId]              CHAR(18) NULL,
	        [LastModified]           DATETIME NOT NULL,
	        [LastModifier]           NVARCHAR(254) NOT NULL,
	        [Operation]              VARCHAR(7) NOT NULL,
	        [OperatorId]             NVARCHAR(254) NOT NULL,
	        [OperationDate]          DATETIME NOT NULL
	                CONSTRAINT DF_MappingNoteVersionArchive_OperationDate DEFAULT (CURRENT_TIMESTAMP),

	CONSTRAINT [PK_MappingNoteVersionArchive]
	        PRIMARY KEY CLUSTERED ([NoteVersionArchiveId]) ON [HISTORIC]
	) ON [HISTORIC]

	IF OBJECT_ID(N'[Mapping].[NoteVersionArchive]', N'U') IS NOT NULL
		RAISERROR('*** CREATED TABLE %s.%s ***', 10, 1, '[Mapping]', '[NoteVersionArchive]') WITH NOWAIT
	ELSE
		RAISERROR('*** FAILED CREATING TABLE %s.%s ***', 10, 1, '[Mapping]', '[NoteVersionArchive]') WITH NOWAIT

	IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description', N'SCHEMA', N'Mapping', N'TABLE', N'NoteVersionArchive', NULL, NULL))
		EXEC sys.sp_addextendedproperty @name=N'MS_Description',
		        @value=N'A version archive of the mapping table for Note identifiers across multiple systems.',
		        @level0type=N'SCHEMA', @level0name=N'Mapping',
		        @level1type=N'TABLE', @level1name=N'NoteVersionArchive'
END
ELSE
	RAISERROR('*** SKIPPED CREATING EXISTING TABLE %s.%s ***', 10, 1, '[Mapping]', '[NoteVersionArchive]') WITH NOWAIT
GO
