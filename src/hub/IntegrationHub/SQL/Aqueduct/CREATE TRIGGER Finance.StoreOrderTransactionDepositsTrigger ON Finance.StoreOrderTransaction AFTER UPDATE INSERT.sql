-- ================================================
-- Template generated from Template Explorer using:
-- Create DataChange Trigger
--
-- Use the Specify Values for Template Parameters 
-- command (Ctrl-Shift-M) to fill in the parameter 
-- values below.
--
-- See additional Create Trigger templates for more
-- examples of different Trigger statements.
--
-- This block of comments will not be included in
-- the definition of the function.
-- ================================================
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ================================================
-- Author:      Juswaldy Jusman
-- Create date: 2016-06-24
-- Description: Send an outbound message from Aqueduct
--              whenever Finance.StoreOrderTransaction
--              changes. We only care about changes
--              for StoreFrontId = 11 "Admission and
--              Housing Deposit".
--              Also, we don't care about deletes.
-- ================================================

USE [Aqueduct]
GO

IF OBJECT_ID ('Finance.StoreOrderTransactionDepositsTrigger','TR') IS NOT NULL
	DROP TRIGGER Finance.StoreOrderTransactionDepositsTrigger
GO

CREATE TRIGGER [Finance].[StoreOrderTransactionDepositsTrigger] ON [Finance].[StoreOrderTransaction]
AFTER INSERT, UPDATE
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	DECLARE @message XML;
	DECLARE @inserted NVARCHAR(MAX) = '';
	DECLARE @deleted NVARCHAR(MAX) = '';
	DECLARE @eventType CHAR(6) = 'Insert';
	DECLARE @documentName NVARCHAR(128) = N'AqueductDataChange';

	BEGIN TRY
		-- Set @eventType based on whether inserted and deleted contain records or not
		IF EXISTS(SELECT * FROM deleted)
		BEGIN
			SET @eventType = 'Update'
		END

		-- If there is any successful Deposits
		IF EXISTS(	SELECT *
					FROM inserted i
					JOIN Finance.StoreOrder so ON i.StoreOrderId = so.StoreOrderId
					WHERE i.Message LIKE 'APPROVED%'
					AND so.StoreFrontId = 11 -- "Admission and Housing Deposit"
				)
		BEGIN
			-- Create @inserted message component from contents of inserted
			SELECT  @inserted = (
				SELECT  *
				FROM    inserted
				FOR XML PATH('StoreOrderTransaction'), ELEMENTS, ROOT('Inserted')
			)

			-- Create @deleted message component from contents of deleted
			IF @eventType = 'Update'
			BEGIN
				SELECT  @deleted = (
					SELECT  *
					FROM    deleted
					FOR XML PATH('StoreOrderTransaction'), ELEMENTS, ROOT('Deleted')
				)
			END
		END

		-- If an actual user made a change then send a message,
		-- however, if a sync system made a change then do nothing further.
		IF (NOT(@inserted = '' AND @deleted = ''))
		BEGIN
			-- Build the @message
			SELECT  @message = N'<' + @documentName + N'><Topic>'
			                 + N'Aqueduct.StoreOrderTransaction.' + @eventType
			                 + N'</Topic><'
			                 + N'StoreOrderTransactions'
			                 + N'>' + @inserted + @deleted + N'</'
			                 + N'StoreOrderTransactions'
			                 + N'></' + @documentName + N'>';

			-- Start a Service Broker conversation dialog
			DECLARE @h UNIQUEIDENTIFIER;
			BEGIN DIALOG @h
				FROM SERVICE [AqueductDataChangeStatusSubscriber]
				TO SERVICE N'AqueductDataChangeSubscriber'
				ON CONTRACT [//www.twu.ca/integration/contracts/AqueductDataChange/v1.0]
				WITH ENCRYPTION = OFF;

			-- Send the message through Service Broker
			SEND ON CONVERSATION @h
				MESSAGE TYPE [//www.twu.ca/integration/messages/AqueductDataChange]
				(@message);
		END;
	END TRY
	BEGIN CATCH
		-- Supress any errors that result from our Service Broker conversation
		-- Execute standard error handler
	END CATCH;
END;
GO

