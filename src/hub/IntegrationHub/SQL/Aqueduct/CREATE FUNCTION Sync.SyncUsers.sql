--================================================
--  Create Inline Table-valued Function template
--================================================

-- ================================================
-- Author:      Andrew Menary
-- Create date: 2016-06-17
-- Description: A table valued function to provide
--              a list of PersonIds used by the
--              Sync/Integration processes.  This
--              is used to filter out rows which
--              do not need to be synced further
--              and thereby prevent a storm of sync
--              messages.
-- ================================================
USE Aqueduct
GO

IF OBJECT_ID (N'Sync.SyncUsers') IS NOT NULL
    DROP FUNCTION Sync.SyncUsers
GO

CREATE FUNCTION Sync.SyncUsers()
RETURNS TABLE
AS RETURN
(
	SELECT  527127 [PersonId] -- Causeview Sync
	UNION
	SELECT  572126 [PersonId] -- ERx Sync
)
GO
