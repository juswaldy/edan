/*
 * Build script
 *
 * Requires SQLCMD mode be enabled.
 * To enable SQLCMD mode, select SQLCMD Mode from the Query menu.
 */

-- CREATE Service Broker Objects
:r "C:\Repositories\Integration\IntegrationHub\SQL\Aqueduct\CREATE PROCEDURE Sync.AqueductDataChangeStatusSubscriberQueueActivator.sql"
:r "C:\Repositories\Integration\IntegrationHub\SQL\Aqueduct\CREATE SERVICE AqueductDataChangeStatusSubscriber ON QUEUE Sync.AqueductDataChangeStatusSubscriberQueue.sql"

-- CREATE Functions
:r "C:\Repositories\Integration\IntegrationHub\SQL\Aqueduct\CREATE FUNCTION Sync.SyncUsers.sql"

-- CREATE Triggers
:r "C:\Repositories\Integration\IntegrationHub\SQL\Aqueduct\CREATE TRIGGER Common.PersonDataChangeTrigger ON Common.Person AFTER UPDATE INSERT DELETE.sql"
:r "C:\Repositories\Integration\IntegrationHub\SQL\Aqueduct\CREATE TRIGGER Common.PersonEmailDataChangeTrigger ON Common.PersonEmail AFTER UPDATE INSERT DELETE.sql"
:r "C:\Repositories\Integration\IntegrationHub\SQL\Aqueduct\CREATE TRIGGER Common.PersonPhoneDataChangeTrigger ON Common.PersonPhone AFTER UPDATE INSERT DELETE.sql"
:r "C:\Repositories\Integration\IntegrationHub\SQL\Aqueduct\CREATE TRIGGER Common.PersonAddressDataChangeTrigger ON Common.PersonAddress AFTER UPDATE INSERT DELETE.sql"
:r "C:\Repositories\Integration\IntegrationHub\SQL\Aqueduct\CREATE TRIGGER Common.OrganizationDataChangeTrigger ON Common.Organization AFTER UPDATE INSERT DELETE.sql"
:r "C:\Repositories\Integration\IntegrationHub\SQL\Aqueduct\CREATE TRIGGER Common.OrganizationPhoneDataChangeTrigger ON Common.OrganizationPhone AFTER UPDATE INSERT DELETE.sql"
:r "C:\Repositories\Integration\IntegrationHub\SQL\Aqueduct\CREATE TRIGGER Common.OrganizationAddressDataChangeTrigger ON Common.OrganizationAddress AFTER UPDATE INSERT DELETE.sql"
:r "C:\Repositories\Integration\IntegrationHub\SQL\Aqueduct\CREATE TRIGGER Admissions.CandidacyDataChangeTrigger ON Admissions.Candidacy AFTER UPDATE INSERT DELETE.sql"

-- Done
