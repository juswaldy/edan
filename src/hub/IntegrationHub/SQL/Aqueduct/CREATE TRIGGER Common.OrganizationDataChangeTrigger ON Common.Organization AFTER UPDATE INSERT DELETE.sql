-- ================================================
-- Template generated from Template Explorer using:
-- Create DataChange Trigger
--
-- Use the Specify Values for Template Parameters 
-- command (Ctrl-Shift-M) to fill in the parameter 
-- values below.
--
-- See additional Create Trigger templates for more
-- examples of different Trigger statements.
--
-- This block of comments will not be included in
-- the definition of the function.
-- ================================================
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ================================================
-- Author:      Andrew Menary
-- Create date: 2016-06-17
-- Description: Send an outbound message from Aqueduct
--              whenever Common.Organization changes.
-- Updates:
-- 2016-06-24 JJJ Take care of Update and Delete cases.
-- ================================================

USE [Aqueduct]
GO

IF OBJECT_ID ('Common.OrganizationDataChangeTrigger','TR') IS NOT NULL
	DROP TRIGGER Common.OrganizationDataChangeTrigger
GO

CREATE TRIGGER [Common].[OrganizationDataChangeTrigger] ON [Common].[Organization]
AFTER INSERT, UPDATE, DELETE
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	DECLARE @message XML;
	DECLARE @inserted NVARCHAR(MAX) = '';
	DECLARE @deleted NVARCHAR(MAX) = '';
	DECLARE @eventType CHAR(6) = 'Insert';
	DECLARE @documentName NVARCHAR(128) = N'AqueductDataChange';

	BEGIN TRY
		-- Set @eventType based on whether inserted and deleted contain records or not
		IF EXISTS(SELECT * FROM deleted)
		BEGIN
			SET @eventType = CASE WHEN EXISTS(SELECT * FROM inserted) THEN 'Update' ELSE 'Delete' END;
		END

		-- Create @inserted message component from contents of inserted
		IF EXISTS(SELECT * FROM inserted WHERE LastModifier NOT IN (SELECT PersonId [LastModifier] FROM Sync.SyncUsers()))
		BEGIN
			SELECT  @inserted = (
				SELECT  *
				FROM    inserted
				WHERE   LastModifier NOT IN (SELECT PersonId [LastModifier] FROM Sync.SyncUsers())
				FOR XML PATH('Organization'), ELEMENTS, ROOT('Inserted')
			)
		END

		-- Create @deleted message component from contents of deleted
		IF @eventType = 'Update' AND EXISTS(SELECT * FROM inserted WHERE LastModifier NOT IN (SELECT PersonId [LastModifier] FROM Sync.SyncUsers()))
		BEGIN
			-- If an Update event, join with inserted rows whose LastModifier is not a Sync User
			SELECT  @deleted = (
				SELECT  d.*
				FROM    deleted d
				JOIN    inserted i ON d.OrganizationId = i.OrganizationId
				WHERE   i.LastModifier NOT IN (SELECT PersonId [LastModifier] FROM Sync.SyncUsers())
				FOR XML PATH('Organization'), ELEMENTS, ROOT('Deleted')
			)
		END
		ELSE
		BEGIN
			-- Otherwise, only create @deleted component if LastModifier is not a Sync User
			IF @eventType = 'Delete' AND EXISTS(SELECT * FROM deleted WHERE LastModifier NOT IN (SELECT PersonId [LastModifier] FROM Sync.SyncUsers()))
			BEGIN
				SELECT  @deleted = (
					SELECT  *
					FROM    deleted
					WHERE   LastModifier NOT IN (SELECT PersonId [LastModifier] FROM Sync.SyncUsers())
					FOR XML PATH('Organization'), ELEMENTS, ROOT('Deleted')
				)
			END
		END

		-- If an actual user made a change then send a message,
		-- however, if a sync system made a change then do nothing further.
		IF (NOT(@inserted = '' AND @deleted = ''))
		BEGIN
			-- Build the @message
			SELECT  @message = N'<' + @documentName + N'><Topic>'
			                 + N'Aqueduct.Organization.' + @eventType
			                 + N'</Topic><'
			                 + N'Organizations'
			                 + N'>' + @inserted + @deleted + N'</'
			                 + N'Organizations'
			                 + N'></' + @documentName + N'>';

			-- Start a Service Broker conversation dialog
			DECLARE @h UNIQUEIDENTIFIER;
			BEGIN DIALOG @h
				FROM SERVICE [AqueductDataChangeStatusSubscriber]
				TO SERVICE N'AqueductDataChangeSubscriber'
				ON CONTRACT [//www.twu.ca/integration/contracts/AqueductDataChange/v1.0]
				WITH ENCRYPTION = OFF;

			-- Send the message through Service Broker
			SEND ON CONVERSATION @h
				MESSAGE TYPE [//www.twu.ca/integration/messages/AqueductDataChange]
				(@message);
		END;
	END TRY
	BEGIN CATCH
		-- Supress any errors that result from our Service Broker conversation
		-- Execute standard error handler
	END CATCH;
END;
GO
