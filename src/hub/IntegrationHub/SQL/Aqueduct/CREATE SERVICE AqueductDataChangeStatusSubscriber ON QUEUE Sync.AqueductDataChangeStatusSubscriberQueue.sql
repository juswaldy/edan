-- ================================================
-- Author:      Andrew Menary
-- Create date: 2016-06-16
-- Description: Create a Service Broker Service to
--              receive AqueductDataChangeStatus messages.
-- ================================================

USE master
GO

-- If the database is not enabled for service broker
IF NOT EXISTS (SELECT 1 FROM sys.databases WHERE database_id = DB_ID(N'Aqueduct') AND is_broker_enabled = 1)

	-- then enable it with this statement:
	ALTER DATABASE Aqueduct
	    SET ENABLE_BROKER
	    WITH ROLLBACK IMMEDIATE;
GO

-- To disable service broker:
--ALTER DATABASE Aqueduct
--    SET DISABLE_BROKER;
--GO

-- If the database is not marked as trustworthy
IF NOT EXISTS (SELECT 1 FROM sys.databases WHERE database_id = DB_ID(N'Aqueduct') AND is_trustworthy_on = 1)

	-- then mark the database as TRUSTWORTHY so it can be allowed
	-- to access resources beyond the database itself
	ALTER DATABASE Aqueduct
	    SET TRUSTWORTHY ON;
GO

-- Create the required meta-data
-- ** Initiator (Aqueduct) Database ** --

USE Aqueduct
GO

-- Create message types for valid XML messages to be sent and received
CREATE MESSAGE TYPE [//www.twu.ca/integration/messages/AqueductDataChange]
    VALIDATION = WELL_FORMED_XML;

CREATE MESSAGE TYPE [//www.twu.ca/integration/messages/AqueductDataChangeStatus]
    VALIDATION = WELL_FORMED_XML;
GO

-- Create a contract which will be used by the service to validate what
-- types of messages are allowed for initiator and target
CREATE CONTRACT [//www.twu.ca/integration/contracts/AqueductDataChange/v1.0] (
    [//www.twu.ca/integration/messages/AqueductDataChange] SENT BY INITIATOR,
    [//www.twu.ca/integration/messages/AqueductDataChangeStatus] SENT BY TARGET
);
GO

-- A target can send back status messages to the initiator hence
-- we need a queue for the initiator also
CREATE QUEUE [Sync].[AqueductDataChangeStatusSubscriberQueue]
    WITH STATUS = ON,
    ACTIVATION (
        PROCEDURE_NAME = [Sync].[AqueductDataChangeStatusSubscriberQueueActivator],
        MAX_QUEUE_READERS = 1,
        EXECUTE AS OWNER
    );
GO

-- Likewise we need a service which will sit atop the initiator queue
-- and be used by the target to send status messages to the initiator
CREATE SERVICE [AqueductDataChangeStatusSubscriber]
    ON QUEUE [Sync].[AqueductDataChangeStatusSubscriberQueue]
GO

--ALTER QUEUE [Sync].[AqueductDataChangeStatusSubscriberQueue] WITH ACTIVATION (STATUS = OFF);
--GO

--ALTER QUEUE [Sync].[AqueductDataChangeStatusSubscriberQueue] WITH ACTIVATION (STATUS = ON);
--GO

--USE Aqueduct
--GO

---- Remove all service broker database objects related to AqueductDataChange
--DROP SERVICE [AqueductDataChangeStatusSubscriber]
--DROP QUEUE [Sync].[AqueductDataChangeStatusSubscriberQueue]
--DROP CONTRACT [//www.twu.ca/integration/contracts/AqueductDataChange/v1.0]
--DROP MESSAGE TYPE [//www.twu.ca/integration/messages/AqueductDataChange]
--DROP MESSAGE TYPE [//www.twu.ca/integration/messages/AqueductDataChangeStatus]
--IF EXISTS (SELECT 1 FROM sys.procedures WHERE [object_id] = OBJECT_ID(N'[Sync].[AqueductDataChangeStatusSubscriberQueueActivator]'))
--DROP PROCEDURE [Sync].[AqueductDataChangeStatusSubscriberQueueActivator]
--GO
