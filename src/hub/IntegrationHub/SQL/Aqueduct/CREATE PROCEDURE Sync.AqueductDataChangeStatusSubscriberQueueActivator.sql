-- ================================================
-- Template generated from Template Explorer using:
-- Create Procedure (New Menu).SQL
--
-- Use the Specify Values for Template Parameters 
-- command (Ctrl-Shift-M) to fill in the parameter 
-- values below.
--
-- This block of comments will not be included in
-- the definition of the procedure.
-- ================================================

USE Aqueduct
GO

-- Drop stored procedure if it already exists
IF EXISTS (
  SELECT *
    FROM INFORMATION_SCHEMA.ROUTINES
   WHERE SPECIFIC_SCHEMA = N'Sync'
     AND SPECIFIC_NAME = N'AqueductDataChangeStatusSubscriberQueueActivator'
)
   DROP PROCEDURE Sync.AqueductDataChangeStatusSubscriberQueueActivator
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:      Andrew Menary
-- Create date: 2016-06-16
-- Description: An internal activation procedure to
--              receive messages on a service broker
--              queue and handle them.
--
--              For the monolog pattern the sender
--              must have an activator proc the sole
--              job of which is to END CONVERSATION
--              on the sending side
-- =============================================
CREATE PROCEDURE Sync.AqueductDataChangeStatusSubscriberQueueActivator
AS
BEGIN
    SET NOCOUNT ON;

    DECLARE @message_type INT,
            @dialog UNIQUEIDENTIFIER;

    WHILE (1 = 1)
    BEGIN
        BEGIN TRANSACTION;

        --Receive the next available message from the queue
        WAITFOR (
            RECEIVE TOP(1) -- just handle one message at a time
                @message_type = message_type_id,  --the type of message received
                @dialog = CONVERSATION_HANDLE   -- the identifier of the dialog this message was received on
            FROM [Sync].[AqueductDataChangeStatusSubscriberQueue]
        ), TIMEOUT 1000;  -- if the queue is empty for one second, give UPDATE and go away

        --If we didn't get anything, bail out
        IF (@@ROWCOUNT = 0)
        BEGIN
            ROLLBACK TRANSACTION;
            BREAK;
        END

        --Check for the End Dialog message so we can end the conversation.
        -- Any other message type will be ignored (and discarded)
        IF (@message_type = 2) -- End dialog message
        BEGIN
            END CONVERSATION @dialog;
        END

        --Commit the transaction.  At any point before this, we could roll
        --back and the received message would be back on the queue.
        COMMIT TRANSACTION
    END;
END
GO
