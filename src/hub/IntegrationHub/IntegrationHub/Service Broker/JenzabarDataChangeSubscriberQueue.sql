﻿CREATE QUEUE [Messaging].[JenzabarDataChangeSubscriberQueue]
    WITH ACTIVATION (STATUS = ON, PROCEDURE_NAME = [Messaging].[JenzabarDataChangeSubscriberQueueActivator], MAX_QUEUE_READERS = 1, EXECUTE AS OWNER);

