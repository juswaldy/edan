﻿CREATE SERVICE [AqueductDataChangeSubscriber]
    AUTHORIZATION [dbo]
    ON QUEUE [Messaging].[AqueductDataChangeSubscriberQueue]
    ([//www.twu.ca/integration/contracts/AqueductDataChange/v1.0]);

