﻿CREATE MESSAGE TYPE [//www.twu.ca/integration/messages/AqueductDataChangeStatus]
    AUTHORIZATION [dbo]
    VALIDATION = WELL_FORMED_XML;

