﻿CREATE QUEUE [Messaging].[AqueductDataChangeSubscriberQueue]
    WITH ACTIVATION (STATUS = ON, PROCEDURE_NAME = [Messaging].[AqueductDataChangeSubscriberQueueActivator], MAX_QUEUE_READERS = 1, EXECUTE AS OWNER);

