﻿CREATE SERVICE [JenzabarDataChangeSubscriber]
    AUTHORIZATION [dbo]
    ON QUEUE [Messaging].[JenzabarDataChangeSubscriberQueue]
    ([//www.twu.ca/integration/contracts/JenzabarDataChange/v1.0]);

