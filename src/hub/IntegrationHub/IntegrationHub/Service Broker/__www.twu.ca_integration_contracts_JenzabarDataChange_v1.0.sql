﻿CREATE CONTRACT [//www.twu.ca/integration/contracts/JenzabarDataChange/v1.0]
    AUTHORIZATION [dbo]
    ([//www.twu.ca/integration/messages/JenzabarDataChange] SENT BY INITIATOR, [//www.twu.ca/integration/messages/JenzabarDataChangeStatus] SENT BY TARGET);

