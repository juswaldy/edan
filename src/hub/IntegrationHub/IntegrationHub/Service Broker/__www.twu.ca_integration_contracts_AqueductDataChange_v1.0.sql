﻿CREATE CONTRACT [//www.twu.ca/integration/contracts/AqueductDataChange/v1.0]
    AUTHORIZATION [dbo]
    ([//www.twu.ca/integration/messages/AqueductDataChange] SENT BY INITIATOR, [//www.twu.ca/integration/messages/AqueductDataChangeStatus] SENT BY TARGET);

