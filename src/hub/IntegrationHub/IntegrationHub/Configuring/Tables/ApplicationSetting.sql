﻿CREATE TABLE [Configuring].[ApplicationSetting] (
    [Name]  NVARCHAR (30)  NOT NULL,
    [Value] NVARCHAR (MAX) NULL,
    CONSTRAINT [PK_ConfiguringApplicationSetting] PRIMARY KEY CLUSTERED ([Name] ASC)
);


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'A table to store configuration settings.', @level0type = N'SCHEMA', @level0name = N'Configuring', @level1type = N'TABLE', @level1name = N'ApplicationSetting';

