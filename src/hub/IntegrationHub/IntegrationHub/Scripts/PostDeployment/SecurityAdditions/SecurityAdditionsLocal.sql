﻿PRINT 'Create role permissions for $(DeployType)';

PRINT 'Create users for $(DeployType)';
:r .\Users\SalesforceSyncDEV.user.sql
:r .\Users\cloveretl.user.sql

PRINT 'Create role memberships for $(DeployType)';
:r .\RolePermissions\Developers.sql
:r .\RolePermissions\SyncServices.sql
