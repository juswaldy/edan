﻿IF EXISTS (select * from sys.database_principals where name = 'cloveretl')
  EXEC sp_addrolemember N'SyncServices', N'cloveretl';

IF EXISTS (select * from sys.database_principals where name = 'SalesforceSync')
  EXEC sp_addrolemember N'SyncServices', N'SalesforceSync';

IF EXISTS (select * from sys.database_principals where name = 'SalesforceSyncDEV')
  EXEC sp_addrolemember N'SyncServices', N'SalesforceSyncDEV';

IF EXISTS (select * from sys.database_principals where name = 'SalesforceSyncTEST')
  EXEC sp_addrolemember N'SyncServices', N'SalesforceSyncTEST';
