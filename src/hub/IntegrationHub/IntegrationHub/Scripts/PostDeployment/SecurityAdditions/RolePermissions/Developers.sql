﻿IF EXISTS (select * from sys.database_principals where name = 'TRINET\Andrew.Menary')
  EXEC sp_addrolemember N'Developers', N'TRINET\Andrew.Menary';

IF EXISTS (select * from sys.database_principals where name = 'TRINET\Jonathan.Babbitt')
  EXEC sp_addrolemember N'Developers', N'TRINET\Jonathan.Babbitt';

IF EXISTS (select * from sys.database_principals where name = 'TRINET\Juswaldy.Jusman')
  EXEC sp_addrolemember N'Developers', N'TRINET\Juswaldy.Jusman';

IF EXISTS (select * from sys.database_principals where name = 'TRINET\Richard.Bandsmer')
  EXEC sp_addrolemember N'Developers', N'TRINET\Richard.Bandsmer';
