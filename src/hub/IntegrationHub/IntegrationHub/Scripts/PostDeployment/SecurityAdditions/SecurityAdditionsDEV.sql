﻿PRINT 'Create role permissions for $(DeployType)';

PRINT 'Create users for $(DeployType)';
:r .\Users\SalesforceSyncDEV.user.sql
:r .\Users\cloveretl.user.sql
:r .\Users\TRINET_Andrew.Menary.user.sql
:r .\Users\TRINET_Juswaldy.Jusman.user.sql
:r .\Users\TRINET_Jonathan.Babbitt.user.sql
:r .\Users\TRINET_Richard.Bandsmer.user.sql

PRINT 'Create role memberships for $(DeployType)';
:r .\RolePermissions\Developers.sql
:r .\RolePermissions\SyncServices.sql
