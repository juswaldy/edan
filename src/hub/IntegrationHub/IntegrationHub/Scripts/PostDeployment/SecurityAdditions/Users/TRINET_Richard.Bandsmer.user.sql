IF NOT EXISTS (SELECT * FROM sys.database_principals WHERE name = 'TRINET\Richard.Bandsmer') AND EXISTS (select 'x' from master.dbo.syslogins where name = 'TRINET\Richard.Bandsmer')
CREATE USER [TRINET\Richard.Bandsmer] FOR LOGIN [TRINET\Richard.Bandsmer] WITH DEFAULT_SCHEMA=[dbo];
