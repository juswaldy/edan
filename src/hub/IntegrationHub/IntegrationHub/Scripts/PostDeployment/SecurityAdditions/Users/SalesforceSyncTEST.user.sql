IF NOT EXISTS (SELECT * FROM sys.database_principals WHERE name = 'SalesforceSyncTEST') AND EXISTS (select 'x' from master.dbo.syslogins where name = 'SalesforceSyncTEST')
CREATE USER [SalesforceSyncTEST] FOR LOGIN [SalesforceSyncTEST] WITH DEFAULT_SCHEMA=[dbo];
