IF NOT EXISTS (SELECT * FROM sys.database_principals WHERE name = 'SalesforceSync') AND EXISTS (select 'x' from master.dbo.syslogins where name = 'SalesforceSync')
CREATE USER [SalesforceSync] FOR LOGIN [SalesforceSync] WITH DEFAULT_SCHEMA=[dbo];
