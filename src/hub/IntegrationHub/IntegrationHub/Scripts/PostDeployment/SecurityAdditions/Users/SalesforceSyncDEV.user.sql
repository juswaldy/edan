IF NOT EXISTS (SELECT * FROM sys.database_principals WHERE name = 'SalesforceSyncDEV') AND EXISTS (select 'x' from master.dbo.syslogins where name = 'SalesforceSyncDEV')
CREATE USER [SalesforceSyncDEV] FOR LOGIN [SalesforceSyncDEV] WITH DEFAULT_SCHEMA=[dbo];
