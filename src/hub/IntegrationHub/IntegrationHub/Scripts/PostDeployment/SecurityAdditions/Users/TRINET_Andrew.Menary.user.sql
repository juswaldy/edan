IF NOT EXISTS (SELECT * FROM sys.database_principals WHERE name = 'TRINET\Andrew.Menary') AND EXISTS (select 'x' from master.dbo.syslogins where name = 'TRINET\Andrew.Menary')
CREATE USER [TRINET\Andrew.Menary] FOR LOGIN [TRINET\Andrew.Menary] WITH DEFAULT_SCHEMA=[dbo];
