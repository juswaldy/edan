IF NOT EXISTS (SELECT * FROM sys.database_principals WHERE name = 'TRINET\Jonathan.Babbitt') AND EXISTS (select 'x' from master.dbo.syslogins where name = 'TRINET\Jonathan.Babbitt')
CREATE USER [TRINET\Jonathan.Babbitt] FOR LOGIN [TRINET\Jonathan.Babbitt] WITH DEFAULT_SCHEMA=[dbo];
