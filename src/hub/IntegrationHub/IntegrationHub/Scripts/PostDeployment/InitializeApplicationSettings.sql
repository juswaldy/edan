﻿/*
 * Where configuration data is missing, load a default value
 */
IF NOT EXISTS (SELECT * FROM Configuring.ApplicationSetting WHERE Name = N'graphEndpoint')
	INSERT INTO Configuring.ApplicationSetting (Name, Value) VALUES (N'graphEndpoint', N'http://cloveretl-test.twu.ca:8080/clover/simpleHttpApi/graph_run');

IF NOT EXISTS (SELECT * FROM Configuring.ApplicationSetting WHERE Name = N'graphSandbox')
	INSERT INTO Configuring.ApplicationSetting (Name, Value) VALUES (N'graphSandbox', N'IntegrationHub_Dev');

IF NOT EXISTS (SELECT * FROM Configuring.ApplicationSetting WHERE Name = N'graphUsername')
	INSERT INTO Configuring.ApplicationSetting (Name, Value) VALUES (N'graphUsername', N'wgetter');

IF NOT EXISTS (SELECT * FROM Configuring.ApplicationSetting WHERE Name = N'graphPassword')
	INSERT INTO Configuring.ApplicationSetting (Name, Value) VALUES (N'graphPassword', N'superCupcake$$');
