﻿-- =============================================
-- Pushes security based on the current environment variable setting
-- =============================================

PRINT '******************************* Deplying Permissions for DeployType = $(DeployType) ************************************'
--:on error ignore

IF ( '$(DeployType)' = 'Dev')
BEGIN
 :r .\SecurityAdditions\SecurityAdditionsDEV.sql
END
ELSE IF ( '$(DeployType)' = 'Test')
BEGIN
 :r .\SecurityAdditions\SecurityAdditionsTEST.sql
END
ELSE IF ( '$(DeployType)' = 'Prod')
BEGIN
 :r .\SecurityAdditions\SecurityAdditionsPROD.sql
END
ELSE IF ( '$(DeployType)' = 'Local')
BEGIN
 :r .\SecurityAdditions\SecurityAdditionsLocal.sql
END
ELSE
BEGIN
 :r .\SecurityAdditions\SecurityAdditionsDefault.sql
END
