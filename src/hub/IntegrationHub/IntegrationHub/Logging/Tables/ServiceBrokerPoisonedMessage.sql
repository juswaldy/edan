﻿CREATE TABLE [Logging].[ServiceBrokerPoisonedMessage] (
    [PoisonedMessageId] BIGINT           IDENTITY (1, 1) NOT NULL,
    [Dialog]            UNIQUEIDENTIFIER NOT NULL,
    [SourceSystem]      [sysname]        NULL,
    [MessageType]       [sysname]        NULL,
    [MessageBody]       XML              NULL,
    [LastModified]      DATETIME         CONSTRAINT [DF_ServiceBrokerPoisonedMessage_LastModified] DEFAULT (getdate()) NOT NULL,
    CONSTRAINT [PK_LoggingServiceBrokerPoisonedMessage] PRIMARY KEY CLUSTERED ([PoisonedMessageId] ASC) ON [HISTORIC]
) TEXTIMAGE_ON [HISTORIC];


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'A table to capture poisoned messages for debugging', @level0type = N'SCHEMA', @level0name = N'Logging', @level1type = N'TABLE', @level1name = N'ServiceBrokerPoisonedMessage';

