﻿CREATE TABLE [Logging].[ServiceBrokerFailedMessage] (
    [Dialog]       UNIQUEIDENTIFIER NOT NULL,
    [SourceSystem] [sysname]        NULL,
    [MessageType]  [sysname]        NULL,
    [MessageBody]  XML              NULL,
    [Count]        SMALLINT         CONSTRAINT [DF_ServiceBrokerFailedMessage_Count] DEFAULT ((0)) NOT NULL,
    [LastModified] DATETIME         CONSTRAINT [DF_ServiceBrokerFailedMessage_LastModified] DEFAULT (getdate()) NOT NULL,
    CONSTRAINT [PK_LoggingServiceBrokerFailedMessage] PRIMARY KEY CLUSTERED ([Dialog] ASC)
);


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'A table to count the failed messages for poison message handling.', @level0type = N'SCHEMA', @level0name = N'Logging', @level1type = N'TABLE', @level1name = N'ServiceBrokerFailedMessage';

