﻿-- =============================================
-- Author:      Andrew Menary
-- Create date: 2016-06-14
-- Description: Update a counter whenever an Service Broker
--              message fails to be handled correctly.
-- =============================================
CREATE PROCEDURE Logging.TrackServiceBrokerMessage
	@dialog UNIQUEIDENTIFIER,
	@sourceSystem SYSNAME = NULL,
	@messageType SYSNAME = NULL,
	@messageBody XML = NULL
AS
BEGIN
	-- GUID: {982C6678-3BF3-47A9-AA6F-3CB0FC6F3C8E}

	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	IF @dialog IS NULL
		RETURN;

	DECLARE @count INT = NULL;
	SET @count = (
		SELECT  [Count]
		FROM    Logging.[ServiceBrokerFailedMessage]
		WHERE   Dialog = @dialog
	);

	IF @count IS NULL
	BEGIN
		INSERT INTO Logging.[ServiceBrokerFailedMessage] (
		        Dialog,
		        SourceSystem,
		        MessageType,
		        MessageBody,
		        [Count]
		) VALUES (
		        @dialog,
		        @sourceSystem,
		        @messageType,
		        @messageBody,
		        1
		);
	END;

	IF @count > 3
	BEGIN
		EXEC Logging.ClearTrackingServiceBrokerMessage @dialog, true;
		BEGIN TRY
			END CONVERSATION @dialog
			    WITH ERROR = 500
			    DESCRIPTION = 'Unable to process message.';
		END TRY
		BEGIN CATCH
			-- There might not be a conversation if we are testing.
		END CATCH
	END
	ELSE
	BEGIN
		UPDATE  Logging.[ServiceBrokerFailedMessage]
		SET     [Count] = [Count] + 1
		WHERE   Dialog = @dialog;
	END;
END
