﻿-- =============================================
-- Author:      Andrew Menary
-- Create date: 2016-06-15
-- Description: Clear the tracking count for failed
--              messages used to handle poison messages.
-- =============================================
CREATE PROCEDURE Logging.ClearTrackingServiceBrokerMessage
	@dialog UNIQUEIDENTIFIER,
	@poisoned BIT = 0
AS
BEGIN
	-- GUID: {4EB61C5F-CF78-4EDB-B64C-7B2ABEEA4C6D}

	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	-- If it is a poisoned message preserve it in the poisoned message table
	-- either way, remove it from the failed message table.
	IF (@poisoned > 0)
	BEGIN
		WITH cte AS (
			SELECT  Dialog,
			        SourceSystem,
			        MessageType,
			        MessageBody,
			        LastModified
			FROM    Logging.[ServiceBrokerFailedMessage] WITH (ROWLOCK, READPAST)
			WHERE   Dialog = @dialog
		)
		DELETE FROM cte
			OUTPUT  deleted.Dialog,
			        deleted.SourceSystem,
			        deleted.MessageType,
			        deleted.MessageBody,
			        deleted.LastModified
			INTO    Logging.[ServiceBrokerPoisonedMessage]
	END
	ELSE
	BEGIN
		DELETE FROM Logging.[ServiceBrokerFailedMessage]
		WHERE   Dialog = @dialog
	END
END
