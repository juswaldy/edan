﻿CREATE TABLE [Mapping].[Application] (
    [ApplicationId]    BIGINT         IDENTITY (1, 1) NOT NULL,
    [AQApplicationId]  INT            NULL,
    [AQCandidacyId]    INT            NULL,
    [ERxApplicationId] CHAR (18)      NULL,
    [ERxApplicantId]   CHAR (18)      NULL,
    [Name]             NVARCHAR (254) NULL,
    [LastModified]     DATETIME       CONSTRAINT [DF_MappingApplication_LastModified] DEFAULT (getdate()) NOT NULL,
    [LastModifier]     NVARCHAR (254) NOT NULL,
    CONSTRAINT [PK_MappingApplication] PRIMARY KEY CLUSTERED ([ApplicationId] ASC)
);


GO
CREATE UNIQUE NONCLUSTERED INDEX [AK_Mapping_Application_AQApplicationId]
    ON [Mapping].[Application]([AQApplicationId] ASC) WHERE ([AQApplicationId] IS NOT NULL);


GO
CREATE UNIQUE NONCLUSTERED INDEX [AK_Mapping_Application_AQCandidacyId]
    ON [Mapping].[Application]([AQCandidacyId] ASC) WHERE ([AQCandidacyId] IS NOT NULL);


GO
CREATE UNIQUE NONCLUSTERED INDEX [AK_Mapping_Application_ERxApplicationId]
    ON [Mapping].[Application]([ERxApplicationId] ASC) WHERE ([ERxApplicationId] IS NOT NULL);


GO

CREATE TRIGGER [Mapping].[ApplicationDeleteTrigger] ON [Mapping].[Application]
FOR DELETE
AS
DECLARE @OperatorId NVARCHAR(254)
BEGIN
	SELECT  @OperatorId = LastModifier FROM DELETED
	INSERT INTO Mapping.ApplicationVersionArchive 
	([ApplicationId], [AQApplicationId], [AQCandidacyId], [ERxApplicationId], [ERxApplicantId], [Name], [LastModified], [LastModifier], Operation, OperatorId, OperationDate)
	SELECT  [ApplicationId], [AQApplicationId], [AQCandidacyId], [ERxApplicationId], [ERxApplicantId], [Name], [LastModified], [LastModifier], 'Delete', @OperatorId, CURRENT_TIMESTAMP
	FROM    DELETED
END
GO

CREATE TRIGGER [Mapping].[ApplicationInsertTrigger] ON [Mapping].[Application]
FOR INSERT
AS
DECLARE @OperatorId NVARCHAR(254)
BEGIN
	SELECT  @OperatorId = LastModifier FROM INSERTED
	INSERT INTO Mapping.ApplicationVersionArchive
	([ApplicationId], [AQApplicationId], [AQCandidacyId], [ERxApplicationId], [ERxApplicantId], [Name], [LastModified], [LastModifier], Operation, OperatorId, OperationDate)
	SELECT  [ApplicationId], [AQApplicationId], [AQCandidacyId], [ERxApplicationId], [ERxApplicantId], [Name], [LastModified], [LastModifier], 'Insert', @OperatorId, CURRENT_TIMESTAMP
	FROM    INSERTED
END
GO

CREATE TRIGGER [Mapping].[ApplicationUpdateTrigger] ON [Mapping].[Application]
FOR UPDATE
AS
DECLARE @OperatorId NVARCHAR(254)
BEGIN
	SELECT  @OperatorId = LastModifier FROM INSERTED
	INSERT INTO Mapping.ApplicationVersionArchive
	([ApplicationId], [AQApplicationId], [AQCandidacyId], [ERxApplicationId], [ERxApplicantId], [Name], [LastModified], [LastModifier], Operation, OperatorId, OperationDate)
	SELECT  [ApplicationId], [AQApplicationId], [AQCandidacyId], [ERxApplicationId], [ERxApplicantId], [Name], [LastModified], [LastModifier], 'Update', @OperatorId, CURRENT_TIMESTAMP
	FROM    DELETED
END
GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Ensure uniqueness of Aqueduct Application identifiers.', @level0type = N'SCHEMA', @level0name = N'Mapping', @level1type = N'TABLE', @level1name = N'Application', @level2type = N'INDEX', @level2name = N'AK_Mapping_Application_AQApplicationId';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Ensure uniqueness of Aqueduct Candidacy identifiers.', @level0type = N'SCHEMA', @level0name = N'Mapping', @level1type = N'TABLE', @level1name = N'Application', @level2type = N'INDEX', @level2name = N'AK_Mapping_Application_AQCandidacyId';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Ensure uniqueness of Enrollment Rx Application identifiers.', @level0type = N'SCHEMA', @level0name = N'Mapping', @level1type = N'TABLE', @level1name = N'Application', @level2type = N'INDEX', @level2name = N'AK_Mapping_Application_ERxApplicationId';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'A mapping table for Application identifiers across multiple systems.', @level0type = N'SCHEMA', @level0name = N'Mapping', @level1type = N'TABLE', @level1name = N'Application';


GO
