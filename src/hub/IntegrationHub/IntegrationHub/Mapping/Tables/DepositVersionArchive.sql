﻿CREATE TABLE [Mapping].[DepositVersionArchive] (
    [DepositVersionArchiveId] BIGINT         IDENTITY (1, 1) NOT NULL,
    [DepositId]               BIGINT         NOT NULL,
    [AQDepositId]             INT            NULL,
    [ERxDepositId]            CHAR (18)      NULL,
    [ERxApplicationId]        CHAR (18)      NULL,
    [LastModified]            DATETIME       NOT NULL,
    [LastModifier]            NVARCHAR (254) NOT NULL,
    [Operation]               VARCHAR (7)    NOT NULL,
    [OperatorId]              NVARCHAR (254) NOT NULL,
    [OperationDate]           DATETIME       CONSTRAINT [DF_MappingDepositVersionArchive_OperationDate] DEFAULT (getdate()) NOT NULL,
    CONSTRAINT [PK_MappingDepositVersionArchive] PRIMARY KEY CLUSTERED ([DepositVersionArchiveId] ASC) ON [HISTORIC]
);
GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'A version archive of the mapping table for Deposit identifiers across multiple systems.', @level0type = N'SCHEMA', @level0name = N'Mapping', @level1type = N'TABLE', @level1name = N'DepositVersionArchive';
GO
