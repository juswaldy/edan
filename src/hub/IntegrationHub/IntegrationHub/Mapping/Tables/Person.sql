﻿CREATE TABLE [Mapping].[Person] (
    [PersonId]        BIGINT         IDENTITY (1, 1) NOT NULL,
    [AQPersonId]      INT            NULL,
    [ERxContactId]    CHAR (18)      NULL,
    [CvwIndividualId] CHAR (18)      NULL,
    [Name]            NVARCHAR (254) NULL,
    [LastModified]    DATETIME       CONSTRAINT [DF_MappingPerson_LastModified] DEFAULT (getdate()) NOT NULL,
    [LastModifier]    NVARCHAR (254) NOT NULL,
    CONSTRAINT [PK_MappingPerson] PRIMARY KEY CLUSTERED ([PersonId] ASC)
);


GO
CREATE UNIQUE NONCLUSTERED INDEX [AK_Mapping_Person_AQPersonId]
    ON [Mapping].[Person]([AQPersonId] ASC)
    WHERE [AQPersonId] IS NOT NULL;


GO
CREATE UNIQUE NONCLUSTERED INDEX [AK_Mapping_Person_CvwIndividualId]
    ON [Mapping].[Person]([CvwIndividualId] ASC)
    WHERE [CvwIndividualId] IS NOT NULL;


GO
CREATE UNIQUE NONCLUSTERED INDEX [AK_Mapping_Person_ERxContactId]
    ON [Mapping].[Person]([ERxContactId] ASC)
    WHERE [ERxContactId] IS NOT NULL;


GO

CREATE TRIGGER [Mapping].[PersonInsertTrigger] ON [Mapping].[Person]
FOR INSERT
AS
DECLARE @OperatorId NVARCHAR(254)
BEGIN
	SELECT  @OperatorId = LastModifier FROM INSERTED
	INSERT INTO Mapping.PersonVersionArchive
	([PersonId], [AQPersonId], [ERxContactId], [CvwIndividualId], [Name], [LastModified], [LastModifier], Operation, OperatorId, OperationDate)
	SELECT  [PersonId], [AQPersonId], [ERxContactId], [CvwIndividualId], [Name], [LastModified], [LastModifier], 'Insert', @OperatorId, CURRENT_TIMESTAMP
	FROM    INSERTED
END

GO

CREATE TRIGGER [Mapping].[PersonUpdateTrigger] ON [Mapping].[Person]
FOR UPDATE
AS
DECLARE @OperatorId NVARCHAR(254)
BEGIN
	SELECT  @OperatorId = LastModifier FROM INSERTED
	INSERT INTO Mapping.PersonVersionArchive
	([PersonId], [AQPersonId], [ERxContactId], [CvwIndividualId], [Name], [LastModified], [LastModifier], Operation, OperatorId, OperationDate)
	SELECT  [PersonId], [AQPersonId], [ERxContactId], [CvwIndividualId], [Name], [LastModified], [LastModifier], 'Update', @OperatorId, CURRENT_TIMESTAMP
	FROM    DELETED
END

GO

CREATE TRIGGER [Mapping].[PersonDeleteTrigger] ON [Mapping].[Person]
FOR DELETE
AS
DECLARE @OperatorId NVARCHAR(254)
BEGIN
	SELECT  @OperatorId = LastModifier FROM DELETED
	INSERT INTO Mapping.PersonVersionArchive 
	([PersonId], [AQPersonId], [ERxContactId], [CvwIndividualId], [Name], [LastModified], [LastModifier], Operation, OperatorId, OperationDate)
	SELECT  [PersonId], [AQPersonId], [ERxContactId], [CvwIndividualId], [Name], [LastModified], [LastModifier], 'Delete', @OperatorId, CURRENT_TIMESTAMP
	FROM    DELETED
END

GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Ensure uniqueness of Aqueduct Person identifiers.', @level0type = N'SCHEMA', @level0name = N'Mapping', @level1type = N'TABLE', @level1name = N'Person', @level2type = N'INDEX', @level2name = N'AK_Mapping_Person_AQPersonId';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Ensure uniqueness of Causeview Individual identifiers.', @level0type = N'SCHEMA', @level0name = N'Mapping', @level1type = N'TABLE', @level1name = N'Person', @level2type = N'INDEX', @level2name = N'AK_Mapping_Person_CvwIndividualId';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Ensure uniqueness of Enrollment Rx Contact identifiers.', @level0type = N'SCHEMA', @level0name = N'Mapping', @level1type = N'TABLE', @level1name = N'Person', @level2type = N'INDEX', @level2name = N'AK_Mapping_Person_ERxContactId';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'A mapping table for Person identifiers across multiple systems.', @level0type = N'SCHEMA', @level0name = N'Mapping', @level1type = N'TABLE', @level1name = N'Person';

