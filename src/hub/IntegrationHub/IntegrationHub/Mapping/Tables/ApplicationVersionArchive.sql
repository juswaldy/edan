﻿CREATE TABLE [Mapping].[ApplicationVersionArchive] (
    [ApplicationVersionArchiveId] BIGINT         IDENTITY (1, 1) NOT NULL,
    [ApplicationId]               BIGINT         NOT NULL,
    [AQApplicationId]             INT            NULL,
    [AQCandidacyId]               INT            NULL,
    [ERxApplicationId]            CHAR (18)      NULL,
    [ERxApplicantId]              CHAR (18)      NULL,
    [Name]                        NVARCHAR (254) NULL,
    [LastModified]                DATETIME       NOT NULL,
    [LastModifier]                NVARCHAR (254) NOT NULL,
    [Operation]                   VARCHAR (7)    NOT NULL,
    [OperatorId]                  NVARCHAR (254) NOT NULL,
    [OperationDate]               DATETIME       CONSTRAINT [DF_MappingApplicationVersionArchive_OperationDate] DEFAULT (getdate()) NOT NULL,
    CONSTRAINT [PK_MappingApplicationVersionArchive] PRIMARY KEY CLUSTERED ([ApplicationVersionArchiveId] ASC) ON [HISTORIC]
);


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'A version archive of the mapping table for Application identifiers across multiple systems.', @level0type = N'SCHEMA', @level0name = N'Mapping', @level1type = N'TABLE', @level1name = N'ApplicationVersionArchive';

