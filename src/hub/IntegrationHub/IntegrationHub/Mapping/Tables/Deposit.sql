﻿CREATE TABLE [Mapping].[Deposit] (
    [DepositId]        BIGINT         IDENTITY (1, 1) NOT NULL,
    [AQDepositId]      INT            NULL,
    [ERxDepositId]     CHAR (18)      NULL,
    [ERxApplicationId] CHAR (18)      NULL,
    [LastModified]     DATETIME       CONSTRAINT [DF_MappingDeposit_LastModified] DEFAULT (getdate()) NOT NULL,
    [LastModifier]     NVARCHAR (254) NOT NULL,
    CONSTRAINT [PK_MappingDeposit] PRIMARY KEY CLUSTERED ([DepositId] ASC)
);
GO

CREATE UNIQUE NONCLUSTERED INDEX [AK_Mapping_Deposit_AQDepositId]
    ON [Mapping].[Deposit]([AQDepositId] ASC) WHERE ([AQDepositId] IS NOT NULL);
GO

CREATE UNIQUE NONCLUSTERED INDEX [AK_Mapping_Deposit_ERxDepositId]
    ON [Mapping].[Deposit]([ERxDepositId] ASC) WHERE ([ERxDepositId] IS NOT NULL);
GO

CREATE TRIGGER [Mapping].[DepositDeleteTrigger] ON [Mapping].[Deposit]
FOR DELETE
AS
DECLARE @OperatorId NVARCHAR(254)
BEGIN
	SELECT  @OperatorId = LastModifier FROM DELETED
	INSERT INTO Mapping.DepositVersionArchive 
	([DepositId], [AQDepositId], [ERxDepositId], [ERxApplicationId], [LastModified], [LastModifier], Operation, OperatorId, OperationDate)
	SELECT  [DepositId], [AQDepositId], [ERxDepositId], [ERxApplicationId], [LastModified], [LastModifier], 'Delete', @OperatorId, CURRENT_TIMESTAMP
	FROM    DELETED
END
GO

CREATE TRIGGER [Mapping].[DepositInsertTrigger] ON [Mapping].[Deposit]
FOR INSERT
AS
DECLARE @OperatorId NVARCHAR(254)
BEGIN
	SELECT  @OperatorId = LastModifier FROM INSERTED
	INSERT INTO Mapping.DepositVersionArchive
	([DepositId], [AQDepositId], [ERxDepositId], [ERxApplicationId], [LastModified], [LastModifier], Operation, OperatorId, OperationDate)
	SELECT  [DepositId], [AQDepositId], [ERxDepositId], [ERxApplicationId], [LastModified], [LastModifier], 'Insert', @OperatorId, CURRENT_TIMESTAMP
	FROM    INSERTED
END
GO

CREATE TRIGGER [Mapping].[DepositUpdateTrigger] ON [Mapping].[Deposit]
FOR UPDATE
AS
DECLARE @OperatorId NVARCHAR(254)
BEGIN
	SELECT  @OperatorId = LastModifier FROM INSERTED
	INSERT INTO Mapping.DepositVersionArchive
	([DepositId], [AQDepositId], [ERxDepositId], [ERxApplicationId], [LastModified], [LastModifier], Operation, OperatorId, OperationDate)
	SELECT  [DepositId], [AQDepositId], [ERxDepositId], [ERxApplicationId], [LastModified], [LastModifier], 'Update', @OperatorId, CURRENT_TIMESTAMP
	FROM    DELETED
END
GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Ensure uniqueness of Aqueduct Deposit identifiers.', @level0type = N'SCHEMA', @level0name = N'Mapping', @level1type = N'TABLE', @level1name = N'Deposit', @level2type = N'INDEX', @level2name = N'AK_Mapping_Deposit_AQDepositId';
GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Ensure uniqueness of Enrollment Rx Deposit identifiers.', @level0type = N'SCHEMA', @level0name = N'Mapping', @level1type = N'TABLE', @level1name = N'Deposit', @level2type = N'INDEX', @level2name = N'AK_Mapping_Deposit_ERxDepositId';
GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'A mapping table for Deposit identifiers across multiple systems.', @level0type = N'SCHEMA', @level0name = N'Mapping', @level1type = N'TABLE', @level1name = N'Deposit';
GO
