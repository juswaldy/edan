﻿CREATE TABLE [Mapping].[Note] (
    [NoteId]       BIGINT         IDENTITY (1, 1) NOT NULL,
    [AQNoteId]     INT            NULL,
    [ERxTaskId]    CHAR (18)      NULL,
    [CvwTaskId]    CHAR (18)      NULL,
    [LastModified] DATETIME       CONSTRAINT [DF_MappingNote_LastModified] DEFAULT (getdate()) NOT NULL,
    [LastModifier] NVARCHAR (254) NOT NULL,
    CONSTRAINT [PK_MappingNote] PRIMARY KEY CLUSTERED ([NoteId] ASC)
);
GO

CREATE UNIQUE NONCLUSTERED INDEX [AK_Mapping_Note_AQNoteId]
    ON [Mapping].[Note]([AQNoteId] ASC) WHERE ([AQNoteId] IS NOT NULL);
GO

CREATE UNIQUE NONCLUSTERED INDEX [AK_Mapping_Note_CvwTaskId]
    ON [Mapping].[Note]([CvwTaskId] ASC) WHERE ([CvwTaskId] IS NOT NULL);
GO

CREATE UNIQUE NONCLUSTERED INDEX [AK_Mapping_Note_ERxTaskId]
    ON [Mapping].[Note]([ERxTaskId] ASC) WHERE ([ERxTaskId] IS NOT NULL);
GO

CREATE TRIGGER [Mapping].[NoteDeleteTrigger] ON [Mapping].[Note]
FOR DELETE
AS
DECLARE @OperatorId NVARCHAR(254)
BEGIN
	SELECT  @OperatorId = LastModifier FROM DELETED
	INSERT INTO Mapping.NoteVersionArchive 
	([NoteId], [AQNoteId], [ERxTaskId], [CvwTaskId], [LastModified], [LastModifier], Operation, OperatorId, OperationDate)
	SELECT  [NoteId], [AQNoteId], [ERxTaskId], [CvwTaskId], [LastModified], [LastModifier], 'Delete', @OperatorId, CURRENT_TIMESTAMP
	FROM    DELETED
END
GO

CREATE TRIGGER [Mapping].[NoteInsertTrigger] ON [Mapping].[Note]
FOR INSERT
AS
DECLARE @OperatorId NVARCHAR(254)
BEGIN
	SELECT  @OperatorId = LastModifier FROM INSERTED
	INSERT INTO Mapping.NoteVersionArchive
	([NoteId], [AQNoteId], [ERxTaskId], [CvwTaskId], [LastModified], [LastModifier], Operation, OperatorId, OperationDate)
	SELECT  [NoteId], [AQNoteId], [ERxTaskId], [CvwTaskId], [LastModified], [LastModifier], 'Insert', @OperatorId, CURRENT_TIMESTAMP
	FROM    INSERTED
END
GO

CREATE TRIGGER [Mapping].[NoteUpdateTrigger] ON [Mapping].[Note]
FOR UPDATE
AS
DECLARE @OperatorId NVARCHAR(254)
BEGIN
	SELECT  @OperatorId = LastModifier FROM INSERTED
	INSERT INTO Mapping.NoteVersionArchive
	([NoteId], [AQNoteId], [ERxTaskId], [CvwTaskId], [LastModified], [LastModifier], Operation, OperatorId, OperationDate)
	SELECT  [NoteId], [AQNoteId], [ERxTaskId], [CvwTaskId], [LastModified], [LastModifier], 'Update', @OperatorId, CURRENT_TIMESTAMP
	FROM    DELETED
END
GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Ensure uniqueness of Aqueduct Note identifiers.', @level0type = N'SCHEMA', @level0name = N'Mapping', @level1type = N'TABLE', @level1name = N'Note', @level2type = N'INDEX', @level2name = N'AK_Mapping_Note_AQNoteId';
GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Ensure uniqueness of Causeview Task identifiers.', @level0type = N'SCHEMA', @level0name = N'Mapping', @level1type = N'TABLE', @level1name = N'Note', @level2type = N'INDEX', @level2name = N'AK_Mapping_Note_CvwTaskId';
GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Ensure uniqueness of Enrollment Rx Task identifiers.', @level0type = N'SCHEMA', @level0name = N'Mapping', @level1type = N'TABLE', @level1name = N'Note', @level2type = N'INDEX', @level2name = N'AK_Mapping_Note_ERxTaskId';
GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'A mapping table for Note identifiers across multiple systems.', @level0type = N'SCHEMA', @level0name = N'Mapping', @level1type = N'TABLE', @level1name = N'Note';
GO
