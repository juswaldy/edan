﻿CREATE TABLE [Mapping].[PersonVersionArchive] (
    [PersonVersionArchiveId] BIGINT         IDENTITY (1, 1) NOT NULL,
    [PersonId]               BIGINT         NOT NULL,
    [AQPersonId]             INT            NULL,
    [ERxContactId]           CHAR (18)      NULL,
    [CvwIndividualId]        CHAR (18)      NULL,
    [Name]                   NVARCHAR (254) NULL,
    [LastModified]           DATETIME       NOT NULL,
    [LastModifier]           NVARCHAR (254) NOT NULL,
    [Operation]              VARCHAR (7)    NOT NULL,
    [OperatorId]             NVARCHAR (254) NOT NULL,
    [OperationDate]          DATETIME       CONSTRAINT [DF_MappingPersonVersionArchive_OperationDate] DEFAULT (getdate()) NOT NULL,
    CONSTRAINT [PK_MappingPersonVersionArchive] PRIMARY KEY CLUSTERED ([PersonVersionArchiveId] ASC) ON [HISTORIC]
);


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'A version archive of the mapping table for Person identifiers across multiple systems.', @level0type = N'SCHEMA', @level0name = N'Mapping', @level1type = N'TABLE', @level1name = N'PersonVersionArchive';

