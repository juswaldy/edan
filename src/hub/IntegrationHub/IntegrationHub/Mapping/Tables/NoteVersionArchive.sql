﻿CREATE TABLE [Mapping].[NoteVersionArchive] (
    [NoteVersionArchiveId] BIGINT         IDENTITY (1, 1) NOT NULL,
    [NoteId]               BIGINT         NOT NULL,
    [AQNoteId]             INT            NULL,
    [ERxTaskId]            CHAR (18)      NULL,
    [CvwTaskId]            CHAR (18)      NULL,
    [LastModified]         DATETIME       NOT NULL,
    [LastModifier]         NVARCHAR (254) NOT NULL,
    [Operation]            VARCHAR (7)    NOT NULL,
    [OperatorId]           NVARCHAR (254) NOT NULL,
    [OperationDate]        DATETIME       CONSTRAINT [DF_MappingNoteVersionArchive_OperationDate] DEFAULT (getdate()) NOT NULL,
    CONSTRAINT [PK_MappingNoteVersionArchive] PRIMARY KEY CLUSTERED ([NoteVersionArchiveId] ASC) ON [HISTORIC]
);
GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'A version archive of the mapping table for Note identifiers across multiple systems.', @level0type = N'SCHEMA', @level0name = N'Mapping', @level1type = N'TABLE', @level1name = N'NoteVersionArchive';
GO
