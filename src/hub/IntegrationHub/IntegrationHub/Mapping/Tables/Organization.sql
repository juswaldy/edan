﻿CREATE TABLE [Mapping].[Organization] (
    [OrganizationId]   BIGINT         IDENTITY (1, 1) NOT NULL,
    [AQOrganizationId] INT            NULL,
    [ERxSchoolId]      CHAR (18)      NULL,
    [CvwAccountId]     CHAR (18)      NULL,
    [Name]             NVARCHAR (254) NULL,
    [LastModified]     DATETIME       CONSTRAINT [DF_MappingOrganization_LastModified] DEFAULT (getdate()) NOT NULL,
    [LastModifier]     NVARCHAR (254) NOT NULL,
    CONSTRAINT [PK_MappingOrganization] PRIMARY KEY CLUSTERED ([OrganizationId] ASC)
);


GO
CREATE UNIQUE NONCLUSTERED INDEX [AK_Mapping_Organization_AQOrganizationId]
    ON [Mapping].[Organization]([AQOrganizationId] ASC) WHERE ([AQOrganizationId] IS NOT NULL);


GO
CREATE UNIQUE NONCLUSTERED INDEX [AK_Mapping_Organization_CvwAccountId]
    ON [Mapping].[Organization]([CvwAccountId] ASC) WHERE ([CvwAccountId] IS NOT NULL);


GO
CREATE UNIQUE NONCLUSTERED INDEX [AK_Mapping_Organization_ERxSchoolId]
    ON [Mapping].[Organization]([ERxSchoolId] ASC) WHERE ([ERxSchoolId] IS NOT NULL);


GO

CREATE TRIGGER [Mapping].[OrganizationDeleteTrigger] ON [Mapping].[Organization]
FOR DELETE
AS
DECLARE @OperatorId NVARCHAR(254)
BEGIN
	SELECT  @OperatorId = LastModifier FROM DELETED
	INSERT INTO Mapping.OrganizationVersionArchive 
	([OrganizationId], [AQOrganizationId], [ERxSchoolId], [CvwAccountId], [Name], [LastModified], [LastModifier], Operation, OperatorId, OperationDate)
	SELECT  [OrganizationId], [AQOrganizationId], [ERxSchoolId], [CvwAccountId], [Name], [LastModified], [LastModifier], 'Delete', @OperatorId, CURRENT_TIMESTAMP
	FROM    DELETED
END
GO

CREATE TRIGGER [Mapping].[OrganizationInsertTrigger] ON [Mapping].[Organization]
FOR INSERT
AS
DECLARE @OperatorId NVARCHAR(254)
BEGIN
	SELECT  @OperatorId = LastModifier FROM INSERTED
	INSERT INTO Mapping.OrganizationVersionArchive
	([OrganizationId], [AQOrganizationId], [ERxSchoolId], [CvwAccountId], [Name], [LastModified], [LastModifier], Operation, OperatorId, OperationDate)
	SELECT  [OrganizationId], [AQOrganizationId], [ERxSchoolId], [CvwAccountId], [Name], [LastModified], [LastModifier], 'Insert', @OperatorId, CURRENT_TIMESTAMP
	FROM    INSERTED
END
GO

CREATE TRIGGER [Mapping].[OrganizationUpdateTrigger] ON [Mapping].[Organization]
FOR UPDATE
AS
DECLARE @OperatorId NVARCHAR(254)
BEGIN
	SELECT  @OperatorId = LastModifier FROM INSERTED
	INSERT INTO Mapping.OrganizationVersionArchive
	([OrganizationId], [AQOrganizationId], [ERxSchoolId], [CvwAccountId], [Name], [LastModified], [LastModifier], Operation, OperatorId, OperationDate)
	SELECT  [OrganizationId], [AQOrganizationId], [ERxSchoolId], [CvwAccountId], [Name], [LastModified], [LastModifier], 'Update', @OperatorId, CURRENT_TIMESTAMP
	FROM    DELETED
END
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Ensure uniqueness of Aqueduct Organization identifiers.', @level0type = N'SCHEMA', @level0name = N'Mapping', @level1type = N'TABLE', @level1name = N'Organization', @level2type = N'INDEX', @level2name = N'AK_Mapping_Organization_AQOrganizationId';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Ensure uniqueness of Causeview Account identifiers.', @level0type = N'SCHEMA', @level0name = N'Mapping', @level1type = N'TABLE', @level1name = N'Organization', @level2type = N'INDEX', @level2name = N'AK_Mapping_Organization_CvwAccountId';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Ensure uniqueness of Enrollment Rx School identifiers.', @level0type = N'SCHEMA', @level0name = N'Mapping', @level1type = N'TABLE', @level1name = N'Organization', @level2type = N'INDEX', @level2name = N'AK_Mapping_Organization_ERxSchoolId';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'A mapping table for Organization identifiers across multiple systems.', @level0type = N'SCHEMA', @level0name = N'Mapping', @level1type = N'TABLE', @level1name = N'Organization';

