﻿CREATE TABLE [Mapping].[OrganizationVersionArchive] (
    [OrganizationVersionArchiveId] BIGINT         IDENTITY (1, 1) NOT NULL,
    [OrganizationId]               BIGINT         NOT NULL,
    [AQOrganizationId]             INT            NULL,
    [ERxSchoolId]                  CHAR (18)      NULL,
    [CvwAccountId]                 CHAR (18)      NULL,
    [Name]                         NVARCHAR (254) NULL,
    [LastModified]                 DATETIME       NOT NULL,
    [LastModifier]                 NVARCHAR (254) NOT NULL,
    [Operation]                    VARCHAR (7)    NOT NULL,
    [OperatorId]                   NVARCHAR (254) NOT NULL,
    [OperationDate]                DATETIME       CONSTRAINT [DF_MappingOrganizationVersionArchive_OperationDate] DEFAULT (getdate()) NOT NULL,
    CONSTRAINT [PK_MappingOrganizationVersionArchive] PRIMARY KEY CLUSTERED ([OrganizationVersionArchiveId] ASC) ON [HISTORIC]
);


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'A version archive of the mapping table for Organization identifiers across multiple systems.', @level0type = N'SCHEMA', @level0name = N'Mapping', @level1type = N'TABLE', @level1name = N'OrganizationVersionArchive';

