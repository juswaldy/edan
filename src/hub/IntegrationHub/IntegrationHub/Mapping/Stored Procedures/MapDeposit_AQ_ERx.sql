﻿-- =============================================================================
-- Author:      Juswaldy Jusman
-- Create date: 2016-09-02
-- Description: Create or update a mapping between Aqueduct and ERx Deposit.
-- Returns:     Aqueduct Deposit Id, ERx Deposit Id, Return Code.
--              Return Code 0 means nothing was changed.
--              Return Code 1 means a new mapping was created.
--              Return Code 2 means an existing mapping was updated.
-- =============================================================================
CREATE PROCEDURE Mapping.MapDeposit_AQ_ERx
	@AQDepositId      INT,
	@ERxApplicationId VARCHAR(18),
	@ERxDepositId     VARCHAR(18),
	@LastModifier     NVARCHAR(254)
AS
BEGIN
	-- GUID: {C1E2C0F0-6ED6-49C6-A8D2-C8CAC8C92270}

	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	-- SET XACT_ABORT ON to ensure any existing transaction
	-- and not just the current statement is rolled back if
	-- there is a run-time error.
	SET XACT_ABORT ON;

	DECLARE	@Now               DATETIME,
			@ReturnCode        INT = 0, -- Default to success.
			@ErrorNumber       INT = NULL,
			@ErrorMessage      VARCHAR(MAX) = NULL,
			@NumberOfMatches   INT = 0,

			@_DepositId        BIGINT,
			@_AQDepositId      INT,
			@_ERxApplicationId VARCHAR(18),
			@_ERxDepositId     VARCHAR(18)
			;

	SET @Now = GETUTCDATE();

	BEGIN TRY
		DECLARE @hasOuterTransaction BIT = CASE WHEN @@TRANCOUNT > 0 THEN 1 ELSE 0 END;
		DECLARE @rollbackPoint NCHAR(32) = REPLACE(CONVERT(NCHAR(36), NEWID()), N'-', N'');

		IF @hasOuterTransaction = 1
		BEGIN
			SAVE TRANSACTION @rollbackPoint;
		END
		ELSE
		BEGIN
			BEGIN TRANSACTION @rollbackPoint;
		END;

		-- Trim and convert blank strings to null.
		IF RTRIM(@ERxApplicationId) = '' SET @ERxApplicationId = NULL ELSE SET @ERxApplicationId = RTRIM(LTRIM(@ERxApplicationId));
		IF RTRIM(@ERxDepositId) = '' SET @ERxDepositId = NULL ELSE SET @ERxDepositId = RTRIM(LTRIM(@ERxDepositId));
		IF RTRIM(@LastModifier) = '' SET @LastModifier = NULL ELSE SET @LastModifier = RTRIM(LTRIM(@LastModifier));

		-- Validate parameters.
		IF @AQDepositId IS NULL AND @ERxDepositId IS NULL
			THROW 50001, N'Bad mapping: Both ID''s are null', 1;

		IF @LastModifier IS NULL
			THROW 50001, N'Bad mapping: LastModifier parameter is null', 1;

		-- Count the number of ID matches.
		SELECT @NumberOfMatches = COUNT(*)
		FROM Mapping.Deposit
		WHERE AQDepositId = @AQDepositId
		OR ERxDepositId = @ERxDepositId;

		-- If more than one matches.
		IF @NumberOfMatches > 1
		BEGIN
			-- If more than one matches, throw error.
			THROW 50002, N'Bad mapping: Matched more than one ID''s', 1;
		END

		-- If no mapping exists.
		IF @NumberOfMatches = 0
		BEGIN
			-- If no mapping exists, then insert a new mapping row.
			INSERT INTO Mapping.Deposit
				(AQDepositId, ERxDepositId, ERxApplicationId, LastModified, LastModifier)
			VALUES
				(@AQDepositId, @ERxDepositId, @ERxApplicationId, @Now, @LastModifier);

			-- Return Code 1 means a new mapping was created.
			SET @ReturnCode = 1;
		END
		ELSE IF @NumberOfMatches = 1
		BEGIN
			-- If a single mapping exists, retrieve it.
			SELECT
				@_DepositId = DepositId,
				@_AQDepositId = AQDepositId,
				@_ERxDepositId = ERxDepositId,
				@_ERxApplicationId = ERxApplicationId
			FROM Mapping.Deposit
			WHERE AQDepositId = @AQDepositId
			OR ERxDepositId = @ERxDepositId;

			-- Diff with input params.
			IF COALESCE(@_AQDepositId, 0) = COALESCE(@AQDepositId, 0)
				AND COALESCE(@_ERxDepositId, '') = COALESCE(@ERxDepositId, '')
			BEGIN
				-- If no changes, do nothing.
				SET @ReturnCode = 0 -- Return Code 0 means nothing was changed.
			END
			ELSE
			-- Otherwise, one of these conditions has to be true:
			-- 1. Both the ID's must match, or
			-- 2. One of the database ID's must be null, or
			-- 3. One of the supplied ID's must be null
			IF (@_AQDepositId = @AQDepositId AND @_ERxDepositId = @ERxDepositId)
				OR @_AQDepositId IS NULL
				OR @_ERxDepositId IS NULL
				OR @AQDepositId IS NULL
				OR @ERxDepositId IS NULL
			BEGIN
				-- Set null parameters to db values.
				IF @AQDepositId IS NULL SET @AQDepositId = @_AQDepositId
				IF @ERxDepositId IS NULL SET @ERxDepositId = @_ERxDepositId

				-- Update the mapping table.
				UPDATE Mapping.Deposit SET
					AQDepositId = @AQDepositId,
					ERxDepositId = @ERxDepositId,
					ERxApplicationId = @ERxApplicationId,
					LastModified = @Now,
					LastModifier = @LastModifier
				WHERE DepositId = @_DepositId;

				-- Return Code 2 means an existing mapping was updated.
				SET @ReturnCode = 2;
			END
			ELSE
			BEGIN
				-- If one or more ID's don't match, throw error.
				THROW 50003, N'Bad mapping: ID values have changed', 1;
			END
		END
		ELSE
		BEGIN
			-- The number of matches is negative, throw error.
			THROW 50004, N'Bad mapping: Negative number of matches found', 1;
		END

		IF @hasOuterTransaction = 0
		BEGIN
			COMMIT TRANSACTION;
		END;
	END TRY
	BEGIN CATCH
		IF XACT_STATE() = 1
		BEGIN
			ROLLBACK TRANSACTION @rollbackPoint;
		END;

		-- Execute standard error handler
		SET @ErrorNumber = ERROR_NUMBER() + 50000;
		SET @ErrorMessage = 'MapDeposit_AQ_ERx: ' + ERROR_MESSAGE();
		THROW @ErrorNumber, @ErrorMessage, 1;
	END CATCH;

	-- Return result set.
	SELECT @AQDepositId, @ERxDepositId, @ReturnCode;
END
GO
