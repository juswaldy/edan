﻿-- =============================================================================
-- Author:      Juswaldy Jusman
-- Create date: 2016-06-09
-- Description: Create or update a mapping between Aqueduct and ERx Application.
-- Returns:     Aqueduct ApplicationId, CandidacyId, ERx Application Id, Return Code.
--              Return Code 0 means nothing was changed.
--              Return Code 1 means a new mapping was created.
--              Return Code 2 means an existing mapping was updated.
-- Changes:
-- 2016-08-02 JJJ Accept CandidacyId as parameter, disregard Name parameter if null.
-- =============================================================================
CREATE PROCEDURE Mapping.MapApplication_AQ_ERx
	@AQApplicationId  INT,
	@AQCandidacyId    INT,
	@ERxApplicationId VARCHAR(18),
	@ERxApplicantId   VARCHAR(18),
	@Name             NVARCHAR(254),
	@LastModifier     NVARCHAR(254)
AS
BEGIN
	-- GUID: {DE4E460C-CC8B-4393-B436-0F5E86BC465F}

	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	-- SET XACT_ABORT ON to ensure any existing transaction
	-- and not just the current statement is rolled back if
	-- there is a run-time error.
	SET XACT_ABORT ON;

	DECLARE	@Now               DATETIME,
			@ReturnCode        INT = 0, -- Default to success.
			@ErrorNumber       INT = NULL,
			@ErrorMessage      VARCHAR(MAX) = NULL,
			@NumberOfMatches   INT = 0,

			@_ApplicationId    BIGINT,
			@_AQApplicationId  INT,
			@_AQCandidacyId    INT,
			@_ERxApplicationId VARCHAR(18),
			@_ERxApplicantId   VARCHAR(18),
			@_Name             VARCHAR(254)
			;

	SET @Now = GETUTCDATE();

	BEGIN TRY
		DECLARE @hasOuterTransaction BIT = CASE WHEN @@TRANCOUNT > 0 THEN 1 ELSE 0 END;
		DECLARE @rollbackPoint NCHAR(32) = REPLACE(CONVERT(NCHAR(36), NEWID()), N'-', N'');

		IF @hasOuterTransaction = 1
		BEGIN
			SAVE TRANSACTION @rollbackPoint;
		END
		ELSE
		BEGIN
			BEGIN TRANSACTION @rollbackPoint;
		END;

		-- Trim and convert blank strings to null.
		IF RTRIM(@ERxApplicationId) = '' SET @ERxApplicationId = NULL ELSE SET @ERxApplicationId = RTRIM(LTRIM(@ERxApplicationId));
		IF RTRIM(@ERxApplicantId) = '' SET @ERxApplicantId = NULL ELSE SET @ERxApplicantId = RTRIM(LTRIM(@ERxApplicantId));
		IF RTRIM(@Name) = '' SET @Name = NULL ELSE SET @Name = RTRIM(LTRIM(@Name));
		IF RTRIM(@LastModifier) = '' SET @LastModifier = NULL ELSE SET @LastModifier = RTRIM(LTRIM(@LastModifier));

		-- Validate parameters.
		IF @AQApplicationId IS NULL AND @ERxApplicationId IS NULL AND @AQCandidacyId IS NULL
			THROW 50001, N'Bad mapping: Both Application and Candidacy ID''s are null', 1;

		IF @LastModifier IS NULL
			THROW 50001, N'Bad mapping: LastModifier parameter is null', 1;

		-- Count the number of ID matches.
		SELECT @NumberOfMatches = COUNT(*)
		FROM Mapping.Application
		WHERE AQApplicationId = @AQApplicationId
		OR AQCandidacyId = @AQCandidacyId
		OR ERxApplicationId = @ERxApplicationId;

		-- If more than one matches.
		IF @NumberOfMatches > 1
		BEGIN
			-- If more than one matches, throw error.
			THROW 50002, N'Bad mapping: Matched more than one ID''s', 1;
		END

		-- If no mapping exists.
		IF @NumberOfMatches = 0
		BEGIN
			-- If no mapping exists, then insert a new mapping row.
			INSERT INTO Mapping.Application
				(AQApplicationId, AQCandidacyId, ERxApplicationId, ERxApplicantId, Name, LastModified, LastModifier)
			VALUES
				(@AQApplicationId, @AQCandidacyId, @ERxApplicationId, @ERxApplicantId, @Name, @Now, @LastModifier);

			-- Return Code 1 means a new mapping was created.
			SET @ReturnCode = 1;
		END
		ELSE IF @NumberOfMatches = 1
		BEGIN
			-- If a single mapping exists, retrieve it.
			SELECT
				@_ApplicationId = ApplicationId,
				@_AQApplicationId = AQApplicationId,
				@_AQCandidacyId = AQCandidacyId,
				@_ERxApplicationId = ERxApplicationId,
				@_ERxApplicantId = ERxApplicantId,
				@_Name = Name
			FROM Mapping.Application
			WHERE AQApplicationId = @AQApplicationId
			OR AQCandidacyId = @AQCandidacyId
			OR ERxApplicationId = @ERxApplicationId;

			-- Diff with input params.
			IF COALESCE(@_AQApplicationId, 0) = COALESCE(@AQApplicationId, 0)
				AND COALESCE(@_AQCandidacyId, 0) = COALESCE(@AQCandidacyId, 0)
				AND COALESCE(@_ERxApplicationId, '') = COALESCE(@ERxApplicationId, '')
				AND COALESCE(@_ERxApplicantId, '') = COALESCE(@ERxApplicantId, '')
				AND COALESCE(@_Name, '') = COALESCE(@Name, '')
			BEGIN
				-- If no changes, do nothing.
				-- Return Code 0 means nothing was changed.
				SET @ReturnCode = 0
			END
			ELSE
			-- Otherwise, one of these conditions has to be true:
			-- 1. All the ID's must match (existing mapping), or
			-- 2. One of the database ID's must be null (parameters will override database), or
			-- 3. One of the supplied ID's must be null (value will be retrieved from database and returned)
			IF (@_AQApplicationId = @AQApplicationId AND @_ERxApplicationId = @ERxApplicationId AND @_AQCandidacyId = @AQCandidacyId)
				OR @_AQApplicationId IS NULL
				OR @_AQCandidacyId IS NULL
				OR @_ERxApplicationId IS NULL
				OR @AQApplicationId IS NULL
				OR @AQCandidacyId IS NULL
				OR @ERxApplicationId IS NULL
			BEGIN
				-- Set null parameters to db values.
				IF @AQApplicationId IS NULL SET @AQApplicationId = @_AQApplicationId
				IF @AQCandidacyId IS NULL SET @AQCandidacyId = @_AQCandidacyId
				IF @ERxApplicationId IS NULL SET @ERxApplicationId = @_ERxApplicationId
				IF @ERxApplicantId IS NULL SET @ERxApplicantId = @_ERxApplicantId
				IF @Name IS NULL SET @Name = @_Name

				-- Update the mapping table.
				UPDATE Mapping.Application SET
					AQApplicationId = @AQApplicationId,
					AQCandidacyId = @AQCandidacyId,
					ERxApplicationId = @ERxApplicationId,
					ERxApplicantId = @ERxApplicantId,
					Name = @Name,
					LastModified = @Now,
					LastModifier = @LastModifier
				WHERE ApplicationId = @_ApplicationId;

				-- Return Code 2 means an existing mapping was updated.
				SET @ReturnCode = 2;
			END
			ELSE
			BEGIN
				-- If one or more ID's don't match, throw error.
				THROW 50003, N'Bad mapping: ID values have changed', 1;
			END
		END
		ELSE
		BEGIN
			-- The number of matches is negative, throw error.
			THROW 50004, N'Bad mapping: Negative number of matches found', 1;
		END

		IF @hasOuterTransaction = 0
		BEGIN
			COMMIT TRANSACTION;
		END;
	END TRY
	BEGIN CATCH
		IF XACT_STATE() = 1
		BEGIN
			ROLLBACK TRANSACTION @rollbackPoint;
		END;

		-- Execute standard error handler
		SET @ErrorNumber = ERROR_NUMBER() + 50000;
		SET @ErrorMessage = 'MapApplication_AQ_ERx: ' + ERROR_MESSAGE();
		THROW @ErrorNumber, @ErrorMessage, 1;
	END CATCH;

	-- Return result set.
	SELECT @AQApplicationId, @AQCandidacyId, @ERxApplicationId, @ReturnCode;
END
