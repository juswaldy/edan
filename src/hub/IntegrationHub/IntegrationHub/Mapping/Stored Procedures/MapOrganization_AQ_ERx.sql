﻿-- =============================================================================
-- Author:      Juswaldy Jusman
-- Create date: 2016-06-07
-- Description: Create or update a mapping between Aqueduct Organization and ERx School.
-- Returns:     Aqueduct Organization Id, ERx School Id, Return Code.
--              Return Code 0 means nothing was changed.
--              Return Code 1 means a new mapping was created.
--              Return Code 2 means an existing mapping was updated.
-- =============================================================================
CREATE PROCEDURE Mapping.MapOrganization_AQ_ERx
	@AQOrganizationId INT,
	@ERxSchoolId      VARCHAR(18),
	@Name             NVARCHAR(254),
	@LastModifier     NVARCHAR(254)
AS
BEGIN
	-- GUID: {5D29E9C9-3D2C-4DB2-A19D-3B6FCB8DF696}

	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	-- SET XACT_ABORT ON to ensure any existing transaction
	-- and not just the current statement is rolled back if
	-- there is a run-time error.
	SET XACT_ABORT ON;

	DECLARE	@Now               DATETIME,
			@ReturnCode        INT = 0, -- Default to success.
			@ErrorNumber       INT = NULL,
			@ErrorMessage      VARCHAR(MAX) = NULL,
			@NumberOfMatches   INT = 0,

			@_OrganizationId   BIGINT,
			@_AQOrganizationId INT,
			@_ERxSchoolId      VARCHAR(18),
			@_Name             VARCHAR(254)
			;

	SET @Now = GETUTCDATE();

	BEGIN TRY
		DECLARE @hasOuterTransaction BIT = CASE WHEN @@TRANCOUNT > 0 THEN 1 ELSE 0 END;
		DECLARE @rollbackPoint NCHAR(32) = REPLACE(CONVERT(NCHAR(36), NEWID()), N'-', N'');

		IF @hasOuterTransaction = 1
		BEGIN
			SAVE TRANSACTION @rollbackPoint;
		END
		ELSE
		BEGIN
			BEGIN TRANSACTION @rollbackPoint;
		END;

		-- Trim and convert blank strings to null.
		IF RTRIM(@ERxSchoolId) = '' SET @ERxSchoolId = NULL ELSE SET @ERxSchoolId = RTRIM(LTRIM(@ERxSchoolId));
		IF RTRIM(@Name) = '' SET @Name = NULL ELSE SET @Name = RTRIM(LTRIM(@Name));
		IF RTRIM(@LastModifier) = '' SET @LastModifier = NULL ELSE SET @LastModifier = RTRIM(LTRIM(@LastModifier));

		-- Validate parameters.
		IF @AQOrganizationID IS NULL AND @ERxSchoolID IS NULL
			THROW 50001, N'Bad mapping: Both ID''s are null', 1;

		IF @LastModifier IS NULL
			THROW 50001, N'Bad mapping: LastModifier parameter is null', 1;

		-- Count the number of ID matches.
		SELECT @NumberOfMatches = COUNT(*)
		FROM Mapping.Organization
		WHERE AQOrganizationId = @AQOrganizationId
		OR ERxSchoolId = @ERxSchoolId;

		-- If more than one matches.
		IF @NumberOfMatches > 1
		BEGIN
			-- If more than one matches, throw error.
			THROW 50002, N'Bad mapping: Matched more than one ID''s', 1;
		END

		-- If no mapping exists.
		IF @NumberOfMatches = 0
		BEGIN
			-- If no mapping exists, then insert a new mapping row.
			INSERT INTO Mapping.Organization
				(AQOrganizationId, ERxSchoolId, Name, LastModified, LastModifier)
			VALUES
				(@AQOrganizationId, @ERxSchoolId, @Name, @Now, @LastModifier);

			-- Return Code 1 means a new mapping was created.
			SET @ReturnCode = 1;
		END
		ELSE IF @NumberOfMatches = 1
		BEGIN
			-- If a single mapping exists, retrieve it.
			SELECT
				@_OrganizationId = OrganizationId,
				@_AQOrganizationId = AQOrganizationId,
				@_ERxSchoolId = ERxSchoolId,
				@_Name = Name
			FROM Mapping.Organization
			WHERE AQOrganizationId = @AQOrganizationId
			OR ERxSchoolId = @ERxSchoolId;

			-- Diff with input params.
			IF COALESCE(@_AQOrganizationId, 0) = COALESCE(@AQOrganizationId, 0)
				AND COALESCE(@_ERxSchoolId, '') = COALESCE(@ERxSchoolId, '')
				AND COALESCE(@_Name, '') = COALESCE(@Name, '')
			BEGIN
				-- If no changes, do nothing.
				SET @_AQOrganizationId = @AQOrganizationId
			END
			ELSE
			-- Otherwise, one of these conditions has to be true:
			-- 1. Both the ID's must match, or
			-- 2. One of the database ID's must be null, or
			-- 3. One of the supplied ID's must be null
			IF (@_AQOrganizationId = @AQOrganizationId AND @_ERxSchoolId = @ERxSchoolId)
				OR @_AQOrganizationId IS NULL
				OR @_ERxSchoolId IS NULL
				OR @AQOrganizationId IS NULL
				OR @ERxSchoolId IS NULL
			BEGIN
				-- Set null parameters to db values.
				IF @AQOrganizationId IS NULL SET @AQOrganizationId = @_AQOrganizationId
				IF @ERxSchoolId IS NULL SET @ERxSchoolId = @_ERxSchoolId

				-- Update the mapping table.
				UPDATE Mapping.Organization SET
					AQOrganizationId = @AQOrganizationId,
					ERxSchoolId = @ERxSchoolId,
					Name = @Name,
					LastModified = @Now,
					LastModifier = @LastModifier
				WHERE OrganizationId = @_OrganizationId;

				-- Return Code 2 means an existing mapping was updated.
				SET @ReturnCode = 2;
			END
			ELSE
			BEGIN
				-- If one or more ID's don't match, throw error.
				THROW 50003, N'Bad mapping: ID values have changed', 1;
			END
		END
		ELSE
		BEGIN
			-- The number of matches is negative, throw error.
			THROW 50004, N'Bad mapping: Negative number of matches found', 1;
		END

		IF @hasOuterTransaction = 0
		BEGIN
			COMMIT TRANSACTION;
		END;
	END TRY
	BEGIN CATCH
		IF XACT_STATE() = 1
		BEGIN
			ROLLBACK TRANSACTION @rollbackPoint;
		END;

		-- Execute standard error handler
		SET @ErrorNumber = ERROR_NUMBER() + 50000;
		SET @ErrorMessage = 'MapOrganization_AQ_ERx: ' + ERROR_MESSAGE();
		THROW @ErrorNumber, @ErrorMessage, 1;
	END CATCH;

	-- Return result set.
	SELECT @AQOrganizationId, @ERxSchoolId, @ReturnCode;
END