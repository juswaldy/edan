﻿CREATE TABLE [Messaging].[MessageDeadLetter] (
    [DeadLetterId]  BIGINT        IDENTITY (1, 1) NOT NULL,
    [Id]            BIGINT        NOT NULL,
    [CorrelationId] BIGINT        NOT NULL,
    [Due]           DATETIME      NOT NULL,
    [Expires]       DATETIME      NOT NULL,
    [Deceased]      DATETIME      CONSTRAINT [DF_MessagingMessageDeadLetter_Deceased] DEFAULT (getutcdate()) NOT NULL,
    [Topic]         VARCHAR (254) NULL,
    [Payload]       XML           NULL,
    CONSTRAINT [PK_MessagingDeadLetterId] PRIMARY KEY CLUSTERED ([DeadLetterId] ASC) ON [HISTORIC]
) TEXTIMAGE_ON [HISTORIC];


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Store for data synchronization messages that expired or otherwise could not be processed', @level0type = N'SCHEMA', @level0name = N'Messaging', @level1type = N'TABLE', @level1name = N'MessageDeadLetter';

