﻿CREATE TABLE [Messaging].[MessageCompleted] (
    [CompletedId]   BIGINT         IDENTITY (1, 1) NOT NULL,
    [Id]            BIGINT         NOT NULL,
    [CorrelationId] BIGINT         NOT NULL,
    [Due]           DATETIME       NOT NULL,
    [Expires]       DATETIME       NOT NULL,
    [Locked]        DATETIME       NOT NULL,
    [Ended]         DATETIME       CONSTRAINT [DF_MessagingMessageCompleted_Ended] DEFAULT (getutcdate()) NOT NULL,
    [RunId]         VARCHAR (255)  NULL,
    [ErrorNumber]   INT            NULL,
    [ErrorMessage]  NVARCHAR (MAX) NULL,
    [Topic]         VARCHAR (255)  NULL,
    [Payload]       XML            NULL,
    CONSTRAINT [PK_MessagingMessageCompleted] PRIMARY KEY CLUSTERED ([CompletedId] ASC) ON [HISTORIC]
) TEXTIMAGE_ON [HISTORIC];


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Message store for data synchronization messages from/to connected systems that have been handled', @level0type = N'SCHEMA', @level0name = N'Messaging', @level1type = N'TABLE', @level1name = N'MessageCompleted';
GO

CREATE NONCLUSTERED INDEX [IX_Messaging_MessageCompleted_Ended_ErrorNumber]
    ON [Messaging].[MessageCompleted]([Ended] DESC, [ErrorNumber] ASC);


GO
