﻿CREATE TABLE [Messaging].[MessageQueue] (
    [Id]            BIGINT        IDENTITY (1, 1) NOT NULL,
    [CorrelationId] BIGINT        NOT NULL,
    [Due]           DATETIME      CONSTRAINT [DF_MessagingMessageQueue_Due] DEFAULT (getutcdate()) NOT NULL,
    [Expires]       DATETIME      CONSTRAINT [DF_MessagingMessageQueue_Expires] DEFAULT (dateadd(day,(1),getutcdate())) NOT NULL,
    [Topic]         VARCHAR (254) NULL,
    [Payload]       XML           NULL
);


GO
CREATE CLUSTERED INDEX [IX_MessagingMessageQueue_Due_Topic]
    ON [Messaging].[MessageQueue]([Due] ASC, [Topic] ASC);


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Message queue for pending data synchronization messages from/to connected systems', @level0type = N'SCHEMA', @level0name = N'Messaging', @level1type = N'TABLE', @level1name = N'MessageQueue';

