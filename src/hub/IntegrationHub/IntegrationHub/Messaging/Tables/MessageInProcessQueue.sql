﻿CREATE TABLE [Messaging].[MessageInProcessQueue] (
    [Id]            BIGINT        NOT NULL,
    [CorrelationId] BIGINT        NOT NULL,
    [Due]           DATETIME      NOT NULL,
    [Expires]       DATETIME      NOT NULL,
    [Locked]        DATETIME      CONSTRAINT [DF_MessagingMessageInProcessQueue_Locked] DEFAULT (getutcdate()) NOT NULL,
    [RunId]         VARCHAR (254) NULL,
    [Topic]         VARCHAR (254) NULL,
    [Payload]       XML           NULL
);


GO
CREATE CLUSTERED INDEX [IX_MessagingMessageInProcessQueue_Locked_Topic]
    ON [Messaging].[MessageInProcessQueue]([Locked] ASC, [Topic] ASC);


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Message queue for data synchronization messages from/to connected systems that are currently being handled', @level0type = N'SCHEMA', @level0name = N'Messaging', @level1type = N'TABLE', @level1name = N'MessageInProcessQueue';

