﻿-- =============================================
-- Author:      Andrew Menary
-- Create date: 2016-05-20
-- Description: Push a new data synchronization message onto the queue.
-- =============================================
CREATE PROCEDURE Messaging.MessageEnqueue
	@Topic         VARCHAR(254),
	@Payload       XML,
	@Due           DATETIME = NULL,
	@Expires       DATETIME = NULL,
	@CorrelationId BIGINT = 0
AS
BEGIN
	-- GUID: {F37A7614-3947-4D3A-AC8C-B39A9839F83E}

	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	-- SET XACT_ABORT ON to ensure any existing transaction
	-- and not just the current statement is rolled back if
	-- there is a run-time error.
	SET XACT_ABORT ON;

	DECLARE @Id           BIGINT,
	        @ReturnCode   INT = 0, -- Default to success.
	        @ErrorNumber  INT = NULL,
	        @ErrorMessage VARCHAR(MAX) = NULL;

	-- Validate parameters
	IF @Due IS NULL
		SET @Due = GETUTCDATE();

	IF @Expires IS NULL
		SET @Expires = DATEADD(day, 1, GETUTCDATE());

	BEGIN TRY
		DECLARE @hasOuterTransaction BIT = CASE WHEN @@TRANCOUNT > 0 THEN 1 ELSE 0 END;
		DECLARE @rollbackPoint NCHAR(32) = REPLACE(CONVERT(NCHAR(36), NEWID()), N'-', N'');

		IF @hasOuterTransaction = 1
		BEGIN
			SAVE TRANSACTION @rollbackPoint;
		END
		ELSE
		BEGIN
			BEGIN TRANSACTION @rollbackPoint;
		END;

		INSERT INTO Messaging.MessageQueue (
		        CorrelationId,
		        Due,
		        Expires,
		        Topic,
		        Payload
		) VALUES (
		        @CorrelationId,
		        @Due,
		        @Expires,
		        @Topic,
		        @Payload
		);

		SELECT  @Id = SCOPE_IDENTITY();

		IF @CorrelationId = 0
		BEGIN
			SET @CorrelationId = @Id;

			UPDATE  Messaging.MessageQueue
			SET     CorrelationId = @CorrelationId
			WHERE   Due = @Due
			AND     ISNULL(Topic, 'N/A') = ISNULL(@Topic, 'N/A')
			AND     Id = @Id;
		END

		SELECT  @ReturnCode, @Id, @CorrelationId;

		IF @hasOuterTransaction = 0
		BEGIN
			COMMIT TRANSACTION;
		END;
	END TRY
	BEGIN CATCH
		IF XACT_STATE() = 1
		BEGIN
			ROLLBACK TRANSACTION @rollbackPoint;
		END;

		-- Execute standard error handler
		SET @ErrorNumber = ERROR_NUMBER() + 50000;
		SET @ErrorMessage = ERROR_MESSAGE();
		THROW @ErrorNumber, @ErrorMessage, 1;
	END CATCH;
END
