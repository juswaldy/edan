﻿-- =============================================
-- Author:      Andrew Menary
-- Create date: 2016-05-20
-- Description: Acknowledge that a data synchronization message has been handled.
-- =============================================
CREATE PROCEDURE Messaging.MessageAckDone
	@Id            BIGINT,
	@CorrelationId BIGINT,
	@ErrorNumber   INTEGER = 0,
	@ErrorMessage  NVARCHAR(MAX) = ''
AS
BEGIN
	-- GUID: {E6AB3D66-BC39-4770-9AED-49C61ADD8AC0}

	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	-- SET XACT_ABORT ON to ensure any existing transaction
	-- and not just the current statement is rolled back if
	-- there is a run-time error.
	SET XACT_ABORT ON;

	DECLARE @Now          DATETIME,
	        @ReturnCode   INT = 0; -- Default to success.

	SET @Now = GETUTCDATE();

	BEGIN TRY
		DECLARE @hasOuterTransaction BIT = CASE WHEN @@TRANCOUNT > 0 THEN 1 ELSE 0 END;
		DECLARE @rollbackPoint NCHAR(32) = REPLACE(CONVERT(NCHAR(36), NEWID()), N'-', N'');

		IF @hasOuterTransaction = 1
		BEGIN
			SAVE TRANSACTION @rollbackPoint;
		END
		ELSE
		BEGIN
			BEGIN TRANSACTION @rollbackPoint;
		END;

		WITH cte AS (
			SELECT  TOP(1) Id,
			        CorrelationId,
			        Due,
			        Expires,
			        Locked,
			        RunId,
			        Topic,
			        Payload
			FROM    Messaging.MessageInProcessQueue WITH (ROWLOCK, READPAST)
			WHERE   Id = @Id
			AND     CorrelationId = @CorrelationId
		)
		DELETE FROM cte
			OUTPUT  deleted.Id,
			        deleted.CorrelationId,
			        deleted.Due,
			        deleted.Expires,
			        deleted.Locked,
			        @Now AS Ended,
			        deleted.RunId,
			        @ErrorNumber AS ErrorNumber,
			        @ErrorMessage AS ErrorMessage,
			        deleted.Topic,
			        deleted.Payload
			INTO    Messaging.MessageCompleted
			OUTPUT  @ReturnCode AS ReturnCode

		IF @hasOuterTransaction = 0
		BEGIN
			COMMIT TRANSACTION;
		END;
	END TRY
	BEGIN CATCH
		IF XACT_STATE() = 1
		BEGIN
			ROLLBACK TRANSACTION @rollbackPoint;
		END;

		-- Execute standard error handler
		SET @ErrorNumber = ERROR_NUMBER() + 50000;
		SET @ErrorMessage = ERROR_MESSAGE();
		THROW @ErrorNumber, @ErrorMessage, 1;
	END CATCH;
END
