﻿CREATE PROCEDURE [Messaging].[RunCloverGraph]
@graphName NVARCHAR (128), @graphSandbox NVARCHAR(128) = NULL, @graphEndpoint NVARCHAR (128) = NULL, @username NVARCHAR (128) = NULL, @password NVARCHAR (128) = NULL, @runId INT OUTPUT
AS EXTERNAL NAME [IntegrationHub].[StoredProcedures].[RunCloverGraph]
