﻿-- =============================================
-- Author:      Andrew Menary
-- Create date: 2016-05-20
-- Description: Examine data synchronization message queues to find stuck
--              or expired messages and deal with them appropriately.
-- Change History:
-- Date        Init Description
-- 2019-04-25  AWM  Add @Then to report on errors between @Then and @Now
-- 2019-04-26  AWM  Add logging levels and show highest level reached on subject line
-- 2019-04-29  AWM  Replaced GUID
-- =============================================
CREATE PROCEDURE [Messaging].[MonitorMessageQueues]
	@MaxActiveInterval INT = 15,  -- The maximum time in minutes that queued messages can be considered active
	@DelayDue INT = 5  -- The time in minutes that the start time is delayed for resubmitted messages
AS
BEGIN
	-- GUID: {91899CBE-4EFF-4CD1-A7BA-1D7CEE4A22EA}

	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	-- SET XACT_ABORT ON to ensure any existing transaction
	-- and not just the current statement is rolled back if
	-- there is a run-time error.
	SET XACT_ABORT ON;

	DECLARE @Then                   DATETIME,
	        @Now                    DATETIME,
	        @ReturnCode             INT = 0, -- Default to success.
	        @Id                     BIGINT,
	        @CorrelationId          BIGINT,
	        @Topic                  VARCHAR(254),
	        @Payload                XML,
	        @Due                    DATETIME = NULL,
	        @Expires                DATETIME = NULL,
	        @ErrorNumber            INTEGER = 0,
	        @ErrorMessage           NVARCHAR(MAX) = ''
	
	DECLARE @LogLevel_Trace         TINYINT = 0;  --most detailed messages, may contain sensitive data
	DECLARE @LogLevel_Debug         TINYINT = 1;  --for interactive investigation during development
	DECLARE @LogLevel_Information   TINYINT = 2;  --track the general flow of the application
	DECLARE @LogLevel_Warning       TINYINT = 3;  --highlight an abnormal or unexpected event in the application flow, but do not otherwise cause the process to stop
	DECLARE @LogLevel_Error         TINYINT = 4;  --the current flow of execution is stopped due to a failure
	DECLARE @LogLevel_Critical      TINYINT = 5;  --an unrecoverable application or system crash, or a catastrophic failure that requires immediate attention
	DECLARE @LogLevel_None          TINYINT = 6;  --specifies that the system should not write any messages
	DECLARE @LogLevel               TINYINT = @LogLevel_Warning;  --default level for logging in production

	-- Report
	DECLARE @Report TABLE (
	        [Id]       INTEGER IDENTITY(1,1) NOT NULL PRIMARY KEY,
	        [LogLevel] TINYINT,
	        [Message]  NVARCHAR(MAX)
	);

	INSERT INTO @Report ([LogLevel], [Message])
	SELECT @LogLevel_Information, 'Beginning execution of [Messaging].[MonitorMessageQueues] with @MaxActiveInterval = ' + CONVERT(NVARCHAR, @MaxActiveInterval) + ' and @DelayDue = ' + CONVERT(NVARCHAR, @DelayDue);

	SET @Now = GETUTCDATE();

	INSERT INTO @Report ([LogLevel], [Message])
	SELECT @LogLevel_Trace, 'The variable @Now = ' + CONVERT(NVARCHAR, @Now, 121);

	BEGIN TRY
		DECLARE @hasOuterTransaction BIT = CASE WHEN @@TRANCOUNT > 0 THEN 1 ELSE 0 END;
		INSERT INTO @Report ([LogLevel], [Message])
		SELECT @LogLevel_Trace, 'The variable @hasOuterTransaction = ' + CONVERT(NVARCHAR, @hasOuterTransaction);

		DECLARE @rollbackPoint NCHAR(32) = REPLACE(CONVERT(NCHAR(36), NEWID()), N'-', N'');
		INSERT INTO @Report ([LogLevel], [Message])
		SELECT @LogLevel_Trace, 'The variable @rollbackPoint = ' + @rollbackPoint;

		INSERT INTO @Report ([LogLevel], [Message])
		SELECT @LogLevel_Information, 'Beginning Transaction';

		IF @hasOuterTransaction = 1
		BEGIN
			SAVE TRANSACTION @rollbackPoint;

			INSERT INTO @Report ([LogLevel], [Message])
			SELECT @LogLevel_Debug, 'Execute SAVE TRANSACTION with @rollbackPoint = ' + @rollbackPoint;
		END
		ELSE
		BEGIN
			BEGIN TRANSACTION @rollbackPoint;

			INSERT INTO @Report ([LogLevel], [Message])
			SELECT @LogLevel_Debug, 'Execute BEGIN TRANSACTION with @rollbackPoint = ' + @rollbackPoint;
		END;

		INSERT INTO @Report ([LogLevel], [Message])
		SELECT @LogLevel_Information, 'Collecting dead letters';

		-- Remove Dead Letters
		SELECT  Id,
		        CorrelationId
		INTO    #DeadLettersPending
		FROM    Messaging.MessageQueue
		WHERE   Expires < @Now

		SELECT  Id,
		        CorrelationId
		INTO    #DeadLettersProcessing
		FROM    Messaging.MessageInProcessQueue
		WHERE   Expires < @Now

		SELECT  Id,
		        CorrelationId
		INTO    #DeadLettersUnmapped
		FROM    Messaging.MessageQueue
		WHERE   LEFT(Topic, 8) = 'Unmapped'

		DECLARE deadLetterCursor CURSOR LOCAL FAST_FORWARD
			FOR SELECT Id, CorrelationId FROM #DeadLettersPending
			    UNION
			    SELECT Id, CorrelationId FROM #DeadLettersProcessing
			    UNION
			    SELECT Id, CorrelationId FROM #DeadLettersUnmapped;
		OPEN deadLetterCursor;
		FETCH NEXT FROM deadLetterCursor
		INTO @Id, @CorrelationId;

		WHILE @@FETCH_STATUS = 0
		BEGIN
			EXECUTE Messaging.MessageDiscardDeadLetter @Id = @Id, @CorrelationId = @CorrelationId;

			INSERT INTO @Report ([LogLevel], [Message])
			SELECT @LogLevel_Trace, 'Discarding dead letter with @Id = ' + CONVERT(NVARCHAR, @Id) + ' and @CorrelationId = ' + CONVERT(NVARCHAR, @CorrelationId);

			FETCH NEXT FROM deadLetterCursor
			INTO @Id, @CorrelationId;
		END

		CLOSE deadLetterCursor;
		DEALLOCATE deadLetterCursor;

		INSERT INTO @Report ([LogLevel], [Message])
		SELECT @LogLevel_Information, 'Done collecting dead letters';

		INSERT INTO @Report ([LogLevel], [Message])
		SELECT @LogLevel_Information, 'Collecting stuck letters';

		-- Resubmit Stuck Messages
		-- (when they have been stuck for more than @MaxActiveInterval minutes)
		SELECT  Id,
		        CorrelationId,
		        Expires,
		        Topic,
		        Payload
		INTO    #StuckMessages
		FROM    Messaging.MessageInProcessQueue
		WHERE   DATEDIFF(minute, Locked, @Now) > @MaxActiveInterval

		DECLARE stuckMessageCursor CURSOR LOCAL FAST_FORWARD
			FOR SELECT Id, CorrelationId, Expires, Topic, Payload FROM #StuckMessages;
		OPEN stuckMessageCursor;
		FETCH NEXT FROM stuckMessageCursor
		INTO @Id, @CorrelationId, @Expires, @Topic, @Payload;

		WHILE @@FETCH_STATUS = 0
		BEGIN
			SELECT  @Due = DATEADD(minute, @DelayDue, @Now);
			EXECUTE Messaging.MessageEnqueue @Topic = @Topic, @Payload = @Payload, @Due = @Due, @Expires = @Expires, @CorrelationId = @CorrelationId;
			EXECUTE Messaging.MessageAckDone @Id = @Id, @CorrelationId = @CorrelationId, @ErrorMessage = 'Message was stuck in processing queue and has been re-enqueued.';

			INSERT INTO @Report ([LogLevel], [Message])
			SELECT @LogLevel_Trace, 'Unsticking stuck letter with @Id = ' + CONVERT(NVARCHAR, @Id) + ' and @CorrelationId = ' + CONVERT(NVARCHAR, @CorrelationId);

			FETCH NEXT FROM stuckMessageCursor
			INTO @Id, @CorrelationId, @Expires, @Topic, @Payload;
		END

		CLOSE stuckMessageCursor;
		DEALLOCATE stuckMessageCursor;

		INSERT INTO @Report ([LogLevel], [Message])
		SELECT @LogLevel_Information, 'Done collecting stuck letters';

		IF @hasOuterTransaction = 0
		BEGIN
			COMMIT TRANSACTION;

			INSERT INTO @Report ([LogLevel], [Message])
			SELECT @LogLevel_Debug, 'Execute COMMIT TRANSACTION';
		END;

		INSERT INTO @Report ([LogLevel], [Message])
		SELECT @LogLevel_Information, 'Collecting issues to report';

		IF EXISTS(SELECT 1 FROM sys.databases WHERE database_id = DB_ID(DB_NAME()) AND service_broker_guid IS NOT NULL AND is_broker_enabled <> 1)
		BEGIN
			INSERT  @Report ([LogLevel], [Message])
			SELECT  @LogLevel_Critical, 'Service Broker is not enabled in ' + DB_NAME() + ' database'
		END
		ELSE
		BEGIN
			INSERT  @Report ([LogLevel], [Message])
			SELECT  @LogLevel_Information, 'Service Broker is enabled in ' + DB_NAME() + ' database'
		END;

		IF EXISTS(SELECT 1 FROM sys.service_queues WHERE is_activation_enabled = 1 AND (is_enqueue_enabled = 0 OR is_receive_enabled = 0))
		BEGIN
			INSERT  @Report ([LogLevel], [Message])
			SELECT  @LogLevel_Critical, 'At least one activated queue is disabled for enqueue or for receive in ' + DB_NAME() + '.  This likely means the activator procedure is throwing errors'
		END
		ELSE
		BEGIN
			INSERT  @Report ([LogLevel], [Message])
			SELECT  @LogLevel_Information, 'All required queues are enabled for enqueue and for receive in ' + DB_NAME() + ' database'
		END;

		IF EXISTS(SELECT * FROM #DeadLettersPending)
		BEGIN
			INSERT  @Report ([LogLevel], [Message])
			SELECT  @LogLevel_Warning, 'There were ' + CAST(COUNT(*) AS VARCHAR) + ' new dead letters in the pending queue'
			FROM    #DeadLettersPending
		END
		ELSE
		BEGIN
			INSERT  @Report ([LogLevel], [Message])
			SELECT  @LogLevel_Information, 'There were no new dead letters in the pending queue'
		END;

		IF EXISTS(SELECT * FROM #DeadLettersProcessing)
		BEGIN
			INSERT  @Report ([LogLevel], [Message])
			SELECT  @LogLevel_Warning, 'There were ' + CAST(COUNT(*) AS VARCHAR) + ' new dead letters in the processing queue'
			FROM    #DeadLettersProcessing
		END
		ELSE
		BEGIN
			INSERT  @Report ([LogLevel], [Message])
			SELECT  @LogLevel_Information, 'There were no new dead letters in the processing queue'
		END;

		IF EXISTS(SELECT * FROM #StuckMessages)
		BEGIN
			INSERT  @Report ([LogLevel], [Message])
			SELECT  @LogLevel_Warning, 'There were ' + CAST(COUNT(*) AS VARCHAR) + ' stuck messages in the processing queue'
			FROM    #StuckMessages
		END
		BEGIN
			INSERT  @Report ([LogLevel], [Message])
			SELECT  @LogLevel_Information, 'There were no stuck messages in the processing queue'
		END;

		INSERT INTO @Report ([LogLevel], [Message])
		SELECT @LogLevel_Information, 'Done collecting issues to report';

		INSERT INTO @Report ([LogLevel], [Message])
		SELECT @LogLevel_Information, 'Collecting errors to report';

		-- Report on Recent Errors (which is to say, errors since the last time this job ran successfully)
		IF NOT EXISTS(SELECT 1 FROM Configuring.ApplicationSetting WHERE [Name] = 'monitorReportErrorsSince')
		BEGIN
			INSERT INTO Configuring.ApplicationSetting ([Name], [Value])
			SELECT 'monitorReportErrorsSince', CONVERT(NVARCHAR, DATEADD(minute, -60, @Now), 121);

			INSERT INTO @Report ([LogLevel], [Message])
			SELECT @LogLevel_Debug, 'Creating monitorReportErrorsSince setting in Configuring.ApplicationSetting';
		END;

		SELECT  @Then = CONVERT(DATETIME, ISNULL([Value], DATEADD(minute, -60, @Now)), 121)
		FROM    Configuring.ApplicationSetting
		WHERE   [Name] = 'monitorReportErrorsSince'

		INSERT INTO @Report ([LogLevel], [Message])
		SELECT @LogLevel_Trace, 'Retrieved monitorReportErrorsSince setting from Configuring.ApplicationSetting, Value is ' + CONVERT(NVARCHAR, @Then, 121);

		IF EXISTS(SELECT 1 FROM [Messaging].[MessageCompleted] WHERE [Ended] BETWEEN @Then AND @Now AND [ErrorNumber] <> 0)
		BEGIN
			INSERT  @Report ([LogLevel], [Message])
			SELECT  @LogLevel_Error, 'ERRORS: The following errors have occurred since UTC:' + CONVERT(NVARCHAR, @Then, 121) + '!!!'

			INSERT  @Report ([LogLevel], [Message])
			SELECT  @LogLevel_Error, CONCAT(CONVERT(NVARCHAR, [Id]), '  ', CONVERT(NVARCHAR, [Ended], 120), '  ', [Topic], ': "', REPLACE([ErrorMessage], 'One or more errors encountered:' + CHAR(10), ''), '"')
			FROM    [Messaging].[MessageCompleted]
			WHERE   [Ended] BETWEEN @Then AND @Now
			AND     [ErrorNumber] <> 0
			ORDER BY [CompletedId] DESC
		END
		BEGIN
			INSERT  @Report ([LogLevel], [Message])
			SELECT  @LogLevel_Information, 'There were no errors encountered between ' + CONVERT(NVARCHAR, @Then, 121) + ' and ' + CONVERT(NVARCHAR, @Now, 121);
		END;

		INSERT INTO @Report ([LogLevel], [Message])
		SELECT @LogLevel_Information, 'Done collecting errors to report';

		INSERT INTO @Report ([LogLevel], [Message])
		SELECT @LogLevel_Information, 'Ending execution of [Messaging].[MonitorMessageQueues] and sending Message Queue Monitor Report';

		IF EXISTS(SELECT * FROM @Report WHERE [LogLevel] >= @LogLevel)
		BEGIN
			DECLARE @ReportHTML NVARCHAR(MAX);
			DECLARE @ReportSubject NVARCHAR(MAX);

			SET @ReportHTML =
				N'<h1>Message Queue Monitor Report</h1>' +
				N'<table border="0">' +
				CAST ((
					SELECT  td = [Message]
					FROM    @Report
					WHERE   [LogLevel] >= @LogLevel
					FOR XML PATH('tr'), TYPE
				) AS NVARCHAR(MAX)) +
				N'</table>';

			SELECT  @ReportSubject =
			            CASE MAX([LogLevel])
			                WHEN @LogLevel_Trace THEN 'Trace'
			                WHEN @LogLevel_Debug THEN 'Debug'
			                WHEN @LogLevel_Information THEN 'Information'
			                WHEN @LogLevel_Warning THEN 'WARNING'
			                WHEN @LogLevel_Error THEN 'ERROR'
			                WHEN @LogLevel_Critical THEN 'CRITICAL'
			            END + ': IntegrationHub Message Queue Monitor Report'
			FROM    @Report

			BEGIN TRY
				EXEC msdb.dbo.sp_send_dbmail
					@profile_name = 'TWU',
					@recipients = 'dev@twu.ca',
					@subject = @ReportSubject,
					@body = @ReportHTML,
					@body_format = 'HTML';
			END TRY
			BEGIN CATCH
				-- Eat it, so we don't rollback for the sake of an undeliverable report
			END CATCH;
		END

		UPDATE  Configuring.ApplicationSetting
		SET     [Value] = CONVERT(NVARCHAR, @Now, 121)
		WHERE   [Name] = 'monitorReportErrorsSince';
	END TRY
	BEGIN CATCH
		IF XACT_STATE() = 1
		BEGIN
			ROLLBACK TRANSACTION @rollbackPoint;
		END;

		-- Execute standard error handler
		SET @ErrorNumber = ERROR_NUMBER() + 50000;
		SET @ErrorMessage = ERROR_MESSAGE();
		THROW @ErrorNumber, @ErrorMessage, 1;
	END CATCH;
END
GO

