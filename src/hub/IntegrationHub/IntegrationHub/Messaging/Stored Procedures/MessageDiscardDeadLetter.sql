﻿-- =============================================
-- Author:      Andrew Menary
-- Create date: 2016-05-20
-- Description: Discard a data synchronization message that has not been handled.
-- =============================================
CREATE PROCEDURE Messaging.MessageDiscardDeadLetter
	@Id            BIGINT,
	@CorrelationId BIGINT
AS
BEGIN
	-- GUID: {16FC1BC8-DB5C-4014-8678-A096010A650A}

	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	-- SET XACT_ABORT ON to ensure any existing transaction
	-- and not just the current statement is rolled back if
	-- there is a run-time error.
	SET XACT_ABORT ON;

	DECLARE @Now          DATETIME,
	        @ReturnCode   INT = 0, -- Default to success.
	        @ErrorNumber  INT = NULL,
	        @ErrorMessage VARCHAR(MAX) = NULL;

	SET @Now = GETUTCDATE();

	BEGIN TRY
		DECLARE @hasOuterTransaction BIT = CASE WHEN @@TRANCOUNT > 0 THEN 1 ELSE 0 END;
		DECLARE @rollbackPoint NCHAR(32) = REPLACE(CONVERT(NCHAR(36), NEWID()), N'-', N'');

		IF @hasOuterTransaction = 1
		BEGIN
			SAVE TRANSACTION @rollbackPoint;
		END
		ELSE
		BEGIN
			BEGIN TRANSACTION @rollbackPoint;
		END;

		IF EXISTS (
			SELECT  *
			FROM    Messaging.MessageInProcessQueue WITH (NOLOCK)
			WHERE   Id = @Id
			AND     CorrelationId = @CorrelationId
		)
		BEGIN --Source is In-Process Message Queue
			WITH cte AS (
				SELECT  TOP(1) Id,
						CorrelationId,
						Due,
						Expires,
						Topic,
						Payload
				FROM    Messaging.MessageInProcessQueue WITH (ROWLOCK, READPAST)
				WHERE   Id = @Id
				AND     CorrelationId = @CorrelationId
			)
			DELETE FROM cte
				OUTPUT  deleted.Id,
				        deleted.CorrelationId,
				        deleted.Due,
				        deleted.Expires,
				        @Now AS Deceased,
				        deleted.Topic,
				        deleted.Payload
				INTO    Messaging.MessageDeadLetter
				OUTPUT  @ReturnCode AS ReturnCode;
		END
		ELSE
		BEGIN --Source is Incoming Message Queue
			WITH cte AS (
				SELECT  TOP(1) Id,
				        CorrelationId,
				        Due,
				        Expires,
				        Topic,
				        Payload
				FROM    Messaging.MessageQueue WITH (ROWLOCK, READPAST)
				WHERE   Id = @Id
				AND     CorrelationId = @CorrelationId
			)
			DELETE FROM cte
				OUTPUT  deleted.Id,
				        deleted.CorrelationId,
				        deleted.Due,
				        deleted.Expires,
				        @Now AS Deceased,
				        deleted.Topic,
				        deleted.Payload
				INTO    Messaging.MessageDeadLetter
				OUTPUT  @ReturnCode AS ReturnCode;
		END

		IF @hasOuterTransaction = 0
		BEGIN
			COMMIT TRANSACTION;
		END;
	END TRY
	BEGIN CATCH
		IF XACT_STATE() = 1
		BEGIN
			ROLLBACK TRANSACTION @rollbackPoint;
		END;

		-- Execute standard error handler
		SET @ErrorNumber = ERROR_NUMBER() + 50000;
		SET @ErrorMessage = ERROR_MESSAGE();
		THROW @ErrorNumber, @ErrorMessage, 1;
	END CATCH;
END
