using System;
using System.Data;
using System.Data.SqlClient;
using System.Data.SqlTypes;
using Microsoft.SqlServer.Server;
using System.Net;
using System.IO;

/// <summary>
/// A class for implementing SQLCLR Stored Procedures.
/// </summary>
public partial class StoredProcedures
{
    /// <summary>
    /// Gets the value of an application setting from the database.
    /// </summary>
    /// <param name="name">The name of the application setting to retrieve.</param>
    /// <param name="sqlConnection">The database connection.</param>
    /// <returns>The value of the application setting as a string.</returns>
    /// <exception cref="ArgumentNullException">
    /// Error: a name parameter for GetApplicationSetting is required.
    /// or
    /// Error: a connection parameter for GetApplicationSetting is required.
    /// </exception>
    private static string GetApplicationSetting(string name, SqlConnection sqlConnection)
    {
        string result = "";

        // Guard clauses
        if (String.IsNullOrEmpty(name))
        {
            throw new ArgumentNullException("Error: a name parameter for GetApplicationSetting is required.");
        }
        if (sqlConnection == null)
        {
            throw new ArgumentNullException("Error: a connection parameter for GetApplicationSetting is required.");
        }

        // Request the value for the named parameter
        using (SqlCommand sqlCommand = new SqlCommand("SELECT Value FROM Configuring.ApplicationSetting WHERE Name = @name", sqlConnection))
        {
            try
            {
                sqlCommand.Parameters.Add("@name", SqlDbType.NVarChar);
                sqlCommand.Parameters["@name"].Value = name;
                object value = sqlCommand.ExecuteScalar();
                result = value != null ? (string)value : result;
            }
            catch (Exception)
            {
                // Ignore any errors and assume the requested value does not exist
            }
        }

        return result;
    }

    /// <summary>
    /// Runs a Clover ETL graph using the SimpleHTTP API.
    /// </summary>
    /// <param name="graphName">Name of the graph.</param>
    /// <param name="graphSandbox">Name of the Clover sandbox containing the graph.</param>
    /// <param name="graphEndpoint">The Clover API endpoint (base URI).</param>
    /// <param name="username">The username for authenticating to the Clover API.</param>
    /// <param name="password">The password for authenticating to the Clover API.</param>
    [Microsoft.SqlServer.Server.SqlProcedure]
    public static void RunCloverGraph(SqlString graphName, SqlString graphSandbox, SqlString graphEndpoint, SqlString username, SqlString password, out SqlInt32 runId)
    {
        // Declare variables
        string gName = graphName.IsNull ? "" : graphName.Value.Trim();
        string gSandbox = graphSandbox.IsNull ? "" : graphSandbox.Value.Trim();
        string gEndpoint = graphEndpoint.IsNull ? "" : graphEndpoint.Value.Trim();
        string gUsername = username.IsNull ? "" : username.Value.Trim();
        string gPassword = password.IsNull ? "" : password.Value.Trim();
        runId = 0;

        // Guard clauses
        if (gName == String.Empty)
        {
            SqlContext.Pipe.Send("RunCloverGraph: Parameter graphName is required.");
        }
        else if (gSandbox == String.Empty || gEndpoint == String.Empty || gUsername == String.Empty || gPassword == String.Empty)
        {
            // If required parameters are empty, then try to fetch default configuration values from the database
            using (SqlConnection sqlConnection = new SqlConnection("context connection = true"))
            {
                sqlConnection.Open();

                gSandbox = gSandbox == String.Empty ? GetApplicationSetting("graphSandbox", sqlConnection) : gSandbox;
                gEndpoint = gEndpoint == String.Empty ? GetApplicationSetting("graphEndpoint", sqlConnection) : gEndpoint;
                gUsername = gUsername == String.Empty ? GetApplicationSetting("graphUsername", sqlConnection) : gUsername;
                gPassword = gPassword == String.Empty ? GetApplicationSetting("graphPassword", sqlConnection) : gPassword;

                if (gSandbox == String.Empty)
                {
                    SqlContext.Pipe.Send("RunCloverGraph: Parameter graphSandbox is required and a default configuration value was not found in the database.");
                }
                if (gEndpoint == String.Empty)
                {
                    SqlContext.Pipe.Send("RunCloverGraph: Parameter graphEndpoint is required and a default configuration value was not found in the database.");
                }
                if (gUsername == String.Empty)
                {
                    SqlContext.Pipe.Send("RunCloverGraph: Parameter graphUsername is required and a default configuration value was not found in the database.");
                }
                if (gPassword == String.Empty)
                {
                    SqlContext.Pipe.Send("RunCloverGraph: Parameter graphPassword is required and a default configuration value was not found in the database.");
                }

                sqlConnection.Close();
            }
        }

        // If we are missing required parameters then throw an exception
        if (gName == String.Empty || gSandbox == String.Empty || gEndpoint == String.Empty || gUsername == String.Empty || gPassword == String.Empty)
        {
            throw new ArgumentException(String.Format("RunCloverGraph cannot proceed because required information was not provided. (Code={0}{1}{2}{3}{4})", gName == String.Empty ? 1 : 0, gSandbox == String.Empty ? 1 : 0, gEndpoint == String.Empty ? 1 : 0, gUsername == String.Empty ? 1 : 0, gPassword == String.Empty ? 1 : 0));
        }
        else  // Otherwise try to launch the Clover ETL graph
        {
            string graphUri = String.Format("{0}?sandbox={1}&graphID=graph/{2}.grf", gEndpoint, gSandbox, gName);

            using (WebClient client = new WebClient())
            {
                client.Headers.Add("X-Requested-By", "RunCloverGraph");
                client.Credentials = new NetworkCredential(gUsername, gPassword);

                Uri url = new Uri(graphUri);
                try
                {
                    using (Stream data = client.OpenRead(url))
                    {
                        using (StreamReader reader = new StreamReader(data))
                        {
                            string s = reader.ReadToEnd();
                            //TODO: do something with s which should be the graph run Id
                            int i = 0;
                            if (int.TryParse(s, out i))
                            {
                                runId = i;
                            }
                            SqlContext.Pipe.Send(String.Format("Started graph, got back: '{0}'.", s));
                        }
                    }
                }
                catch (Exception ex)
                {
                    string error = String.Format("Error: Attempt to run Clover graph '{0}' in sandbox '{1}' failed with Exception: '{2}'.  Request URL was '{3}'.", gName, gSandbox, ex.Message, graphUri);
                    SqlContext.Pipe.Send(error);
                    throw new ApplicationException(error, ex);
                }
            }
        }

        return;
    }
}
