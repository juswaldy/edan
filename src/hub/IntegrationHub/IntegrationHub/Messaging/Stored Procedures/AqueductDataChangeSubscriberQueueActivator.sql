﻿-- =============================================
-- Author:      Andrew Menary
-- Create date: 2016-06-16
-- Description: An internal activation procedure to
--              receive messages on a service broker
--              queue and handle them.
-- =============================================
CREATE PROCEDURE [Messaging].[AqueductDataChangeSubscriberQueueActivator]
AS
BEGIN
    -- GUID: {2A19CC01-0E40-41D1-BB76-7F20E1DC35DC}

    SET NOCOUNT ON;

    DECLARE @message_body XML;
    DECLARE @message_type_name SYSNAME;
    DECLARE @dialog UNIQUEIDENTIFIER;

    WHILE (1 = 1)
    BEGIN
        BEGIN TRANSACTION;
        SAVE TRANSACTION UndoReceive;

        -- Receive the next available message from the queue

        WAITFOR (
            RECEIVE top(1)  -- just handle one message at a time
                @message_type_name = message_type_name,      -- the type of message received
                @message_body = CONVERT(XML, message_body),  -- the message contents
                @dialog = CONVERSATION_HANDLE                -- the identifier of the dialog this message was received on
            FROM [Messaging].[AqueductDataChangeSubscriberQueue]
        ), TIMEOUT 1000;  -- if the queue is empty for one second, give UPDATE and go away

        -- If we didn't get anything, bail out
        IF (@@ROWCOUNT = 0)
        BEGIN
            ROLLBACK TRANSACTION;
            BREAK;
        END

        -- Check for the AqueductDataChange message.
        IF (@message_type_name = N'//www.twu.ca/integration/messages/AqueductDataChange')
        BEGIN
            DECLARE @response_message_body NVARCHAR(MAX) = N'<Reply><Status>Ok</Status><Request>' + CONVERT(NVARCHAR(MAX), @message_body) + '</Request></Reply>';
            DECLARE @topic VARCHAR(254);

            BEGIN TRY
                -- Get topic
                SELECT  @topic = CONVERT(VARCHAR(254), @message_body.query('data(/AqueductDataChange/Topic)'));

                -- Verify that if the message relates to Persons that it is one that needs to be handled
                IF (LEFT(@topic, 15) = 'Aqueduct.Person')
                BEGIN
                    DECLARE @Person TABLE (
                        Id INT NOT NULL PRIMARY KEY
                    );

                    INSERT INTO @Person
                    SELECT DISTINCT m.value('.', 'INT')
                    FROM   @message_body.nodes('//PersonId') t(m);

                    IF (NOT EXISTS(SELECT 1 FROM Mapping.Person p1 JOIN @Person p2 ON p1.AQPersonId = p2.Id))
                        SELECT  @topic = 'Unmapped.' + @topic;  -- Ignore Person messages which do not contain any mapped Id's
                END

                -- Verify that if the message relates to Organizations that it is one that needs to be handled
                IF (LEFT(@topic, 21) = 'Aqueduct.Organization')
                BEGIN
                    DECLARE @Organization TABLE (
                        Id INT NOT NULL PRIMARY KEY
                    );

                    INSERT INTO @Organization
                    SELECT DISTINCT m.value('.', 'INT')
                    FROM   @message_body.nodes('//OrganizationId') t(m);

                    IF (NOT EXISTS(SELECT 1 FROM Mapping.Organization o1 JOIN @Organization o2 ON o1.AQOrganizationId = o2.Id))
                        SELECT  @topic = 'Unmapped.' + @topic;  -- Ignore Organization messages which do not contain any mapped Id's
                END

                -- If we got to here then enqueue the message
                EXECUTE [Messaging].[MessageEnqueue] @Topic = @topic, @Payload = @message_body;
                IF (@@ERROR <> 0)
                BEGIN
                    ROLLBACK TRANSACTION UndoReceive;
                    EXEC Logging.TrackServiceBrokerMessage @dialog, N'Aqueduct', @message_type_name, @message_body;
                    BREAK;
                END;

                IF (LEFT(@topic, 8) <> 'Unmapped')
                BEGIN
                    -- Notify Clover that there is a queued message waiting
                    DECLARE  @runId INT = NULL;
                    EXEC Messaging.RunCloverGraph @graphName = @topic, @runId = @runId;
                    IF (@@ERROR <> 0)
                    BEGIN
                        ROLLBACK TRANSACTION UndoReceive;
                        EXEC Logging.TrackServiceBrokerMessage @dialog, N'Aqueduct', @message_type_name, @message_body;
                        BREAK;
                    END;
                END;

                -- Send the response message back to the sender.
                SEND ON CONVERSATION @dialog  -- send it back on the dialog we received the message on
                    MESSAGE TYPE [//www.twu.ca/integration/messages/AqueductDataChangeStatus] -- Must always supply a message type
                    (@response_message_body);  -- the message contents (a NVARCHAR(MAX) blob)

                -- To end the dialog you use END CONVERSATION here on the Target
                -- which sends a message of type 'http://schemas.microsoft.com/SQL/ServiceBroker/EndDialog'
                -- back to the Initiator.
                END CONVERSATION @dialog;

                EXEC Logging.ClearTrackingServiceBrokerMessage @dialog;
            END TRY
            BEGIN CATCH
                ROLLBACK TRANSACTION UndoReceive;
                EXEC Logging.TrackServiceBrokerMessage @dialog, N'Aqueduct', @message_type_name, @message_body;
            END CATCH
        END

        -- For error messages, end the conversation and clear the tracking.
        IF (@message_type_name = N'http://schemas.microsoft.com/SQL/ServiceBroker/Error')
        BEGIN
            END CONVERSATION @dialog;
            EXEC Logging.ClearTrackingServiceBrokerMessage @dialog;
        END

        -- For end dialog messages, clear the tracking.
        -- We don't end the conversation because that should have already happened
        IF (@message_type_name = N'http://schemas.microsoft.com/SQL/ServiceBroker/EndDialog')
        BEGIN
            EXEC Logging.ClearTrackingServiceBrokerMessage @dialog;
        END

        -- Commit the transaction.  At any point before this, we could roll
        -- back - the received message would be back on the queue and the response
        -- wouldn't be sent.
        COMMIT TRANSACTION;
    END
END
