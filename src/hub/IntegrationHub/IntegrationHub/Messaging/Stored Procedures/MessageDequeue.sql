﻿-- =============================================
-- Author:      Andrew Menary
-- Create date: 2016-05-20
-- Description: Pop a data synchronization message off the queue.
-- =============================================
CREATE PROCEDURE Messaging.MessageDequeue
	@Topic         VARCHAR(254),
	@RunId         VARCHAR(254) = NULL
AS
BEGIN
	-- GUID: {16BA49B3-71AF-4541-A465-F1E4C298246B}

	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	-- SET XACT_ABORT ON to ensure any existing transaction
	-- and not just the current statement is rolled back if
	-- there is a run-time error.
	SET XACT_ABORT ON;

	DECLARE @Now          DATETIME,
	        @ReturnCode   INT = 0, -- Default to success.
	        @ErrorNumber  INT = NULL,
	        @ErrorMessage VARCHAR(MAX) = NULL;

	SET @Now = GETUTCDATE();

	BEGIN TRY
		DECLARE @hasOuterTransaction BIT = CASE WHEN @@TRANCOUNT > 0 THEN 1 ELSE 0 END;
		DECLARE @rollbackPoint NCHAR(32) = REPLACE(CONVERT(NCHAR(36), NEWID()), N'-', N'');

		IF @hasOuterTransaction = 1
		BEGIN
			SAVE TRANSACTION @rollbackPoint;
		END
		ELSE
		BEGIN
			BEGIN TRANSACTION @rollbackPoint;
		END;

		WITH cte AS (
			SELECT  TOP(1) Id,
			        CorrelationId,
			        Due,
			        Expires,
			        Topic,
			        Payload
			FROM    Messaging.MessageQueue WITH (ROWLOCK, READPAST)
			WHERE   Due < @Now
			AND     ISNULL(Topic, 'N/A') = ISNULL(@Topic, 'N/A')
			ORDER BY Due, Topic
		)
		DELETE FROM cte
			OUTPUT  deleted.Id,
			        deleted.CorrelationId,
			        deleted.Due,
			        deleted.Expires,
			        @Now AS Locked,
			        @RunId AS RunId,
			        deleted.Topic,
			        deleted.Payload
			INTO    Messaging.MessageInProcessQueue
			OUTPUT  @ReturnCode AS ReturnCode,
			        deleted.Id,
			        deleted.CorrelationId,
			        deleted.Topic,
			        deleted.Payload

		IF @hasOuterTransaction = 0
		BEGIN
			COMMIT TRANSACTION;
		END;
	END TRY
	BEGIN CATCH
		IF XACT_STATE() = 1
		BEGIN
			ROLLBACK TRANSACTION @rollbackPoint;
		END;

		-- Execute standard error handler
		SET @ErrorNumber = ERROR_NUMBER() + 50000;
		SET @ErrorMessage = ERROR_MESSAGE();
		THROW @ErrorNumber, @ErrorMessage, 1;
	END CATCH;
END
