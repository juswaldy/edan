﻿GRANT
    ALTER,
    DELETE,
    EXECUTE,
    INSERT,
    REFERENCES,
    SELECT,
    UPDATE,
    VIEW DEFINITION
ON SCHEMA::[Configuring]
    TO [Developers]
