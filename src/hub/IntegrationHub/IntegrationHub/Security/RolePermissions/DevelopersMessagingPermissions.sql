﻿GRANT
    ALTER,
    DELETE,
    EXECUTE,
    INSERT,
    REFERENCES,
    SELECT,
    UPDATE,
    VIEW DEFINITION
ON SCHEMA::[Messaging]
    TO [Developers]
