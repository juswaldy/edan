﻿GRANT
    ALTER,
    DELETE,
    EXECUTE,
    INSERT,
    REFERENCES,
    SELECT,
    UPDATE,
    VIEW DEFINITION
ON SCHEMA::[Mapping]
    TO [Developers]
