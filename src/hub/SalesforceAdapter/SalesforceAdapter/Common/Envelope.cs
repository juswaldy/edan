﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Xml.Serialization;

namespace SalesforceAdapter.Common
{
	[XmlRootAttribute("Envelope", Namespace = "http://schemas.xmlsoap.org/soap/envelope/", IsNullable = false)]
	public class Envelope<T> where T : class
	{
		public class Body<T>
		{
			[XmlElementAttribute("notifications", Order = 0, Namespace = "http://soap.sforce.com/2005/09/outbound")]
			public T Notifications { get; set; }

			public Body() { }
			public Body(T _type)
			{
				if (_type != null)
					Notifications = _type;
			}
		}

		[XmlElementAttribute("Body", Order = 0)]
		public Body<T> body { get; set; }

		public Envelope() { }
		public Envelope(T _type)
		{
			body = new Body<T>(_type);
		}

		public string SerializeToXml()
		{
			XmlSerializer serializer = new XmlSerializer(typeof(Envelope<T>));
			TextWriter writer = new StringWriter();
			XmlSerializerNamespaces ns = new XmlSerializerNamespaces();
			ns.Add("soapenv", "http://schemas.xmlsoap.org/soap/envelope/");
			ns.Add("xsd", "http://www.w3.org/2001/XMLSchema");
			ns.Add("xsi", "http://www.w3.org/2001/XMLSchema-instance");
			ns.Add("sf", "urn:sobject.enterprise.soap.sforce.com");
			serializer.Serialize(writer, this, ns);
			return writer.ToString();
		}
	}
}