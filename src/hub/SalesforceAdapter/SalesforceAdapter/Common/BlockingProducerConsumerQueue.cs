﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Web;

namespace SalesforceAdapter.Common
{
	/// <summary>
	/// A ProducerConsumerQueue built on BlockingCollection<T> which waits (blocks) for
	/// items to become available on the queue before trying to process them.
	/// See: http://www.albahari.com/threading/part5.aspx#_BlockingCollectionT
	/// </summary>
	public class BlockingProducerConsumerQueue: IDisposable
	{
		class WorkItem
		{
			public readonly TaskCompletionSource<object> TaskSource;
			public readonly Action Action;
			public readonly CancellationToken? CancelToken;

			public WorkItem(TaskCompletionSource<object> taskSource, Action action, CancellationToken? cancelToken)
			{
				TaskSource = taskSource;
				Action = action;
				CancelToken = cancelToken;
			}
		}

		BlockingCollection<WorkItem> _taskQueue = new BlockingCollection<WorkItem>();

		public BlockingProducerConsumerQueue(int workerCount)
		{
			// Create and start a separate Task for each consumer:
			for (int i = 0; i < workerCount; i++)
				Task.Factory.StartNew(Consume);
		}

		public void Dispose()
		{
			_taskQueue.CompleteAdding();
		}

		public Task EnqueueTask(Action action, CancellationToken? cancelToken)
		{
			var tcs = new TaskCompletionSource<object>();
			_taskQueue.Add(new WorkItem(tcs, action, cancelToken));
			return tcs.Task;
		}

		void Consume()
		{
			foreach (WorkItem workItem in _taskQueue.GetConsumingEnumerable())
			{
				if (workItem.CancelToken.HasValue && workItem.CancelToken.Value.IsCancellationRequested)
				{
					workItem.TaskSource.SetCanceled();
				}
				else
				{
					try
					{
						workItem.Action();
						workItem.TaskSource.SetResult(null);  //Indicate completion
					}
					catch (OperationCanceledException ex)
					{
						if (ex.CancellationToken == workItem.CancelToken)
							workItem.TaskSource.SetCanceled();
						else
							workItem.TaskSource.SetException(ex);
					}
					catch (Exception ex)
					{
						workItem.TaskSource.SetException(ex);
					}
				}
			}
		}
	}
}