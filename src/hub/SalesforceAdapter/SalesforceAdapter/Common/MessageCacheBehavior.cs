﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.ServiceModel.Channels;
using System.ServiceModel.Description;
using System.ServiceModel.Dispatcher;
using System.Web;

namespace SalesforceAdapter.Common
{
	/// <summary>
	/// Attach a Service Behavior to inspect incoming soap messages and make a
	/// copy so that the raw message can later by accessed from within a typed
	/// service method
	/// 
	/// See: http://blogs.microsoft.co.il/sasha/2008/06/15/obtaining-an-untyped-wcf-message-from-a-typed-service-operation/
	///      http://weblogs.asp.net/paolopia/writing-a-wcf-message-inspector
	/// </summary>
	[AttributeUsage(AttributeTargets.Class)]
	public class MessageCacheBehavior : Attribute, IServiceBehavior
	{
		public void AddBindingParameters(ServiceDescription serviceDescription, ServiceHostBase serviceHostBase, System.Collections.ObjectModel.Collection<ServiceEndpoint> endpoints, BindingParameterCollection bindingParameters)
		{
		}

		public void ApplyDispatchBehavior(ServiceDescription serviceDescription, ServiceHostBase serviceHostBase)
		{
			foreach (ChannelDispatcher chDisp in serviceHostBase.ChannelDispatchers)
			{
				foreach (EndpointDispatcher epDisp in chDisp.Endpoints)
				{
					epDisp.DispatchRuntime.MessageInspectors.Add(new MessageCacheInspector());
				}
			}
		}

		public void Validate(ServiceDescription serviceDescription, ServiceHostBase serviceHostBase)
		{
		}
	}
}