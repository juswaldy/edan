﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading;
using System.Web;

namespace SalesforceAdapter.Common
{
	/// <summary>
	/// Callback interface providing an external method to handle events consumed in the ProducerConsumerQueue
	/// </summary>
	/// <typeparam name="T"></typeparam>
	public interface ITaskHandler<T>
	{
		void Handle(T task);
	}

	/// <summary>
	/// Generic producer/consumer queue for throttling typed messages
	/// See: http://www.albahari.com/threading/part2.aspx#_WaitHandle_Producer_Consumer_Queue
	/// </summary>
	/// <typeparam name="T"></typeparam>
	public class ProducerConsumerQueue<T> : IDisposable where T : class
	{
		EventWaitHandle _wait = new AutoResetEvent(false);
		Thread _worker;
		readonly Object _locker = new Object();
		Queue<T> _tasks = new Queue<T>();
		readonly ITaskHandler<T> _handler;

		public ProducerConsumerQueue(ITaskHandler<T> handler)
		{
			if (handler == null)
				throw new ArgumentNullException("handler");
			_handler = handler;
			_worker = new Thread(Work);
			_worker.Start();
			Trace.TraceInformation("Started ProducerConsumerQueue worker thread.");
		}

		public void EnqueueTask(T task)
		{
			Trace.TraceInformation("Enqueued task on ProducerConsumerQueue");
			lock (_locker) _tasks.Enqueue(task);
			_wait.Set();
		}

		public void Dispose()
		{
			Trace.TraceInformation("Disposing ProducerConsumerQueue.");
			Trace.Flush();
			EnqueueTask(null);  //Signal the consumer to exit
			_worker.Join();     //Wait for the consumer's thread to finish
			_wait.Close();      //Release any OS resources
		}

		private void Work()
		{
			while (true)
			{
				T task = null;
				lock (_locker)
					if (_tasks.Count > 0)
					{
						task = _tasks.Dequeue();
						if (task == null) return;
					}
				if (task != null)
				{
					Trace.TraceInformation("ProducerConsumerQueue is calling out to external method to handle queued task.");
					_handler.Handle(task);  //Handle task (message) here
				}
				else
					_wait.WaitOne();  //No more tasks - wait for a signal
			}
		}
	}
}