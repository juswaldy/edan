﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.ServiceModel.Channels;
using System.ServiceModel.Dispatcher;
using System.Web;

namespace SalesforceAdapter.Common
{
	/// <summary>
	/// Make a copy of an incoming WCF message so that the raw message
	/// can later by accessed from within a typed service method
	/// 
	/// See: http://blogs.microsoft.co.il/sasha/2008/06/15/obtaining-an-untyped-wcf-message-from-a-typed-service-operation/
	///      http://weblogs.asp.net/paolopia/writing-a-wcf-message-inspector
	/// </summary>
	public sealed class MessageCacheInspector : IDispatchMessageInspector
	{
		public object AfterReceiveRequest(ref Message request, IClientChannel channel, InstanceContext instanceContext)
		{
			MessageBuffer copy = request.CreateBufferedCopy(int.MaxValue);
			MessageCacheProperty.Install(copy.CreateMessage());
			request = copy.CreateMessage();
			copy.Close();
			return null;
		}

		//The operation is one-way,
		//so this won’t be called anyway
		public void BeforeSendReply(ref Message reply, object correlationState)
		{
		}
	}
}