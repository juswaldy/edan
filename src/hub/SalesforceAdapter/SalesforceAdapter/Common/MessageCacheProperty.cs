﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.ServiceModel.Channels;
using System.Web;

namespace SalesforceAdapter.Common
{
	/// <summary>
	/// A message property to cache a copy of the incoming message.
	/// Message properties are transient and do not get serialized
	/// into subsequent message calls.
	/// 
	/// See: http://blogs.microsoft.co.il/sasha/2008/06/15/obtaining-an-untyped-wcf-message-from-a-typed-service-operation/
	///      http://weblogs.asp.net/paolopia/writing-a-wcf-message-inspector
	/// </summary>
	public sealed class MessageCacheProperty
	{
		public const string Name = "MessageCacheProperty";

		public Message Message { get; private set; }

		public MessageCacheProperty(Message message)
		{
			Message = message;
		}

		public static Message GetContextMessage()
		{
			OperationContext ctx = OperationContext.Current;
			MessageCacheProperty messageProperty = (MessageCacheProperty)ctx.IncomingMessageProperties[Name];
			return messageProperty.Message;
		}

		public static void Install(Message message)
		{
			OperationContext ctx = OperationContext.Current;
			ctx.IncomingMessageProperties.Add(Name, new MessageCacheProperty(message));
		}
	}
}