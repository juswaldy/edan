﻿using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.IO;
using System.Net;
using System.ServiceModel;
using System.ServiceModel.Channels;
using System.Xml;
using System.Xml.XPath;
using SalesforceAdapter.Common;
using SalesforceAdapter.Properties;

namespace SalesforceAdapter.Prototype5
{
	//TODO: Provide a check to make sure the database is reachable before responding to requests
	//TODO: Provide a config to enable/disable the listening for notifications, ie. for system maintenance
	//TODO: Disable listening for notifications whenever the database is unreachable
	//TODO: Provide a periodic retry for writing to the database after a write failure
	//TODO: If the database becomes available re-enable disabled notification listening

	// NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "Notification" in code, svc and config file together.
	// NOTE: In order to launch WCF Test Client for testing this service, please select Notification.svc or Notification.svc.cs at the Solution Explorer and start debugging.	

	/// <summary>
	/// Receives/Handles any Salesforce SOAP Outbound Notification Message
	/// </summary>
	[MessageCacheBehavior]
	[ServiceBehavior(Namespace = "http://soap.sforce.com/2005/09/outbound")]
	public class Notification : INotification, ITaskHandler<Message>
	{
		static readonly private Object cacheLock = new Object();
		static readonly private Object queueLock = new Object();
		private ProducerConsumerQueue<Message> myQueue;

		/// <summary>
		/// Receives and replies to any SOAP notification request message from Salesforce Outbound Messaging.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns>A SOAP notification response message with "<Ack>true</Ack>".</returns>
		public notificationsResponse1 notifications(notificationsRequest request)
		{
			if (myQueue == null)
				lock (queueLock)
				{
					//Double-check pattern
					if (myQueue == null) myQueue = new ProducerConsumerQueue<Message>(this);
				}

			//Get raw message
			//NOTE: this requires [MessageCacheBehavior] attribute on the Notification class
			Message message = MessageCacheProperty.GetContextMessage();

			//TODO: log info enqueuing and acknowledging notifications
			//Trace.TraceInformation("Enqueuing message: '{0}'.", myMessage.GetBody<notifications>());
			myQueue.EnqueueTask(message);

			//Send acknowledgement back to Salesforce that the notification was handled
			notificationsResponse response = new notificationsResponse();
			response.Ack = true;

			return new notificationsResponse1() { notificationsResponse = response };
		}

		/// <summary>
		/// Handles the specified message.
		/// </summary>
		/// <param name="message">The message.</param>
		public void Handle(Message message)
		{
			//Create a copy of the message because once we read it we cannot read it again
			MessageBuffer copy = message.CreateBufferedCopy(int.MaxValue);
			Message request = copy.CreateMessage();
			message = copy.CreateMessage();
			copy.Close();

			//Read the message to determine the Topic
			string Entity = GetEntityFromRequest(request);
			string Topic = String.Format("Salesforce.{0}.Notify", Entity);

			//Push the message onto the queue for external processing
			EnqueueMessage(message, Topic);

			//Launch the Clover ETL graph to process the queued message
			RunGraph(Topic);
		}

		/// <summary>
		/// Enqueues the message.
		/// </summary>
		/// <param name="message">The message.</param>
		/// <param name="topic">The topic.</param>
		private static void EnqueueMessage(Message message, string topic)
		{
			ConnectionStringSettings connectionString = ConfigurationManager.ConnectionStrings["SalesForceAdapter"];
			using (SqlConnection cn = new SqlConnection(connectionString.ConnectionString))
			{
				using (SqlCommand command = new SqlCommand("Messaging.MessageEnqueue", cn))
				{
					command.CommandType = CommandType.StoredProcedure;

					SqlParameter _topic = new SqlParameter("@Topic", SqlDbType.VarChar);
					_topic.Value = topic;
					command.Parameters.Add(_topic);

					SqlParameter payload = new SqlParameter("@Payload", SqlDbType.Xml);
					using (XmlDictionaryReader reader = message.GetReaderAtBodyContents())
					{
						payload.Value = reader.ReadOuterXml();
					}
					command.Parameters.Add(payload);

					cn.Open();
					int rowsAffected = command.ExecuteNonQuery();
					if (rowsAffected > 0)
					{
						//Success!
						Trace.TraceInformation("Message was successfully enqueued to database.");
					}
					else
					{
						//Failure!
						Trace.TraceError("Message was not enqueued to database!:");
					}
				}
			}
		}

		/// <summary>
		/// Gets the Salesforce entity from the Outbound Message request.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns>A string with the Salesforce entity name or "Unknown" if the entity is not found</returns>
		private string GetEntityFromRequest(Message request)
		{
			string result = "Unknown";

			if (!request.IsEmpty)
			{
				//Load the request message into a MessageBuffer so it is easier to get at the message XML body.
				MessageBuffer copy = request.CreateBufferedCopy(int.MaxValue);

				//Setup XPath objects to query the message XML body
				XPathNavigator navigator = copy.CreateNavigator();
				XmlNamespaceManager manager = new XmlNamespaceManager(navigator.NameTable);
				manager.AddNamespace("s", "http://schemas.xmlsoap.org/soap/envelope/");
				manager.AddNamespace("xsd", "http://www.w3.org/2001/XMLSchema");
				manager.AddNamespace("xsi", "http://www.w3.org/2001/XMLSchema-instance");
				manager.AddNamespace("notifications", "http://soap.sforce.com/2005/09/outbound");
				manager.AddNamespace("sf", "urn:sobject.enterprise.soap.sforce.com");
				XPathExpression query = navigator.Compile("//notifications:sObject/@xsi:type");
				query.SetContext(manager);

				//Find the first sObject element and read its type attribute
				XPathNavigator node = navigator.SelectSingleNode(query);
				string type = node.Value;
				if (!String.IsNullOrEmpty(type))
					result = type.Split(':')[1];  //Remove the namespace prefix from start of string
				copy.Close();
			}

			return result;
		}

		/// <summary>
		/// Signal Clover ETL to run a graph.
		/// </summary>
		/// <param name="graphName">Name of the graph.</param>
		private void RunGraph(String graphName)
		{
			string graphUri = String.Format("{0}?sandbox=SalesforceSync_Test&graphID=graph/{1}.grf&nodeID=node1", Settings.Default.CloverETLGraphRunURL, graphName);

			using (WebClient client = new WebClient())
			{
				client.Credentials = new NetworkCredential(Settings.Default.CloverETLUser, Settings.Default.CloverETLPass);

				Uri url = new Uri(graphUri);
				try
				{
					using (Stream data = client.OpenRead(url))
					{
						using (StreamReader reader = new StreamReader(data))
						{
							string s = reader.ReadToEnd();
							//TODO: do something with s which should be the graph run Id
							Trace.TraceInformation("Started graph, got back: '{0}'.", s);
						}
					}
				}
				catch (Exception ex)
				{
					Trace.TraceError("Attempt to run graph failed with Exception: '{0}'.", ex.Message);
					//throw;
				}
			}
		}
	}
}
