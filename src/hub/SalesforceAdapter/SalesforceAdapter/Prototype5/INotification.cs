﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Serialization;

namespace SalesforceAdapter.Prototype5
{
	[ServiceContractAttribute(Namespace = "http://soap.sforce.com/2005/09/outbound", ConfigurationName = "Notification")]
	public interface INotification
	{
		[OperationContractAttribute(Action = "")]
		[XmlSerializerFormatAttribute()]
		[ServiceKnownTypeAttribute(typeof(Object))]
		notificationsResponse1 notifications(notificationsRequest request);
	}

	/// <remarks/>
	[SerializableAttribute()]
	[DebuggerStepThroughAttribute()]
	[DesignerCategoryAttribute("code")]
	[XmlTypeAttribute(AnonymousType = true, Namespace = "http://soap.sforce.com/2005/09/outbound")]
	public partial class notifications
	{
		private string organizationIdField;
		private string actionIdField;
		private string sessionIdField;
		private string enterpriseUrlField;
		private string partnerUrlField;
		private ObjectNotification[] notificationField;

		/// <remarks/>
		[XmlElementAttribute(Order = 0)]
		public string OrganizationId
		{
			get
			{
				return this.organizationIdField;
			}
			set
			{
				this.organizationIdField = value;
			}
		}

		/// <remarks/>
		[XmlElementAttribute(Order = 1)]
		public string ActionId
		{
			get
			{
				return this.actionIdField;
			}
			set
			{
				this.actionIdField = value;
			}
		}

		/// <remarks/>
		[XmlElementAttribute(IsNullable = true, Order = 2)]
		public string SessionId
		{
			get
			{
				return this.sessionIdField;
			}
			set
			{
				this.sessionIdField = value;
			}
		}

		/// <remarks/>
		[XmlElementAttribute(Order = 3)]
		public string EnterpriseUrl
		{
			get
			{
				return this.enterpriseUrlField;
			}
			set
			{
				this.enterpriseUrlField = value;
			}
		}

		/// <remarks/>
		[XmlElementAttribute(Order = 4)]
		public string PartnerUrl
		{
			get
			{
				return this.partnerUrlField;
			}
			set
			{
				this.partnerUrlField = value;
			}
		}

		/// <remarks/>
		[XmlElementAttribute("Notification", Order = 5)]
		public ObjectNotification[] Notification
		{
			get
			{
				return this.notificationField;
			}
			set
			{
				this.notificationField = value;
			}
		}
	}

	/// <remarks/>
	[SerializableAttribute()]
	[DebuggerStepThroughAttribute()]
	[DesignerCategoryAttribute("code")]
	[XmlTypeAttribute(Namespace = "http://soap.sforce.com/2005/09/outbound")]
	public partial class ObjectNotification
	{
		private string idField;
		private Object sObjectField;

		/// <remarks/>
		[XmlElementAttribute(Order = 0)]
		public string Id
		{
			get
			{
				return this.idField;
			}
			set
			{
				this.idField = value;
			}
		}

		/// <remarks/>
		[XmlElementAttribute(Order = 1)]
		[XmlAnyElement]
		public Object sObject
		{
			get
			{
				return this.sObjectField;
			}
			set
			{
				this.sObjectField = value;
			}
		}
	}

	/// <remarks/>
	//[XmlIncludeAttribute(typeof(Contact))]
	[XmlIncludeAttribute(typeof(AggregateResult))]
	[SerializableAttribute()]
	[DebuggerStepThroughAttribute()]
	[DesignerCategoryAttribute("code")]
	[XmlTypeAttribute(Namespace = "urn:sobject.enterprise.soap.sforce.com")]
	public partial class sObject
	{
		private string[] fieldsToNullField;
		private string idField;

		/// <remarks/>
		[XmlElementAttribute("fieldsToNull", IsNullable = true, Order = 0)]
		public string[] fieldsToNull
		{
			get
			{
				return this.fieldsToNullField;
			}
			set
			{
				this.fieldsToNullField = value;
			}
		}

		/// <remarks/>
		[XmlElementAttribute(IsNullable = true, Order = 1)]
		public string Id
		{
			get
			{
				return this.idField;
			}
			set
			{
				this.idField = value;
			}
		}
	}

	/// <remarks/>
	[SerializableAttribute()]
	[DebuggerStepThroughAttribute()]
	[DesignerCategoryAttribute("code")]
	[XmlTypeAttribute(Namespace = "urn:sobject.enterprise.soap.sforce.com")]
	public partial class AggregateResult : sObject
	{
		private XmlElement[] anyField;

		/// <remarks/>
		[XmlAnyElementAttribute(Namespace = "urn:sobject.enterprise.soap.sforce.com", Order = 0)]
		public XmlElement[] Any
		{
			get
			{
				return this.anyField;
			}
			set
			{
				this.anyField = value;
			}
		}
	}

	/// <remarks/>
	[SerializableAttribute()]
	[DebuggerStepThroughAttribute()]
	[DesignerCategoryAttribute("code")]
	[XmlTypeAttribute(AnonymousType = true, Namespace = "http://soap.sforce.com/2005/09/outbound")]
	public partial class notificationsResponse
	{
		private bool ackField;

		/// <remarks/>
		[XmlElementAttribute(Order = 0)]
		public bool Ack
		{
			get
			{
				return this.ackField;
			}
			set
			{
				this.ackField = value;
			}
		}
	}

	[DebuggerStepThroughAttribute()]
	[EditorBrowsableAttribute(EditorBrowsableState.Advanced)]
	[MessageContractAttribute(IsWrapped = false)]
	public partial class notificationsRequest
	{
		[MessageBodyMemberAttribute(Namespace = "http://soap.sforce.com/2005/09/outbound", Order = 0)]
		public notifications notifications;

		public notificationsRequest()
		{
		}

		public notificationsRequest(notifications notifications)
		{
			this.notifications = notifications;
		}
	}

	[DebuggerStepThroughAttribute()]
	[EditorBrowsableAttribute(EditorBrowsableState.Advanced)]
	[MessageContractAttribute(IsWrapped = false)]
	public partial class notificationsResponse1
	{
		[MessageBodyMemberAttribute(Namespace = "http://soap.sforce.com/2005/09/outbound", Order = 0)]
		public notificationsResponse notificationsResponse;

		public notificationsResponse1()
		{
		}

		public notificationsResponse1(notificationsResponse notificationsResponse)
		{
			this.notificationsResponse = notificationsResponse;
		}
	}
}
