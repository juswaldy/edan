﻿using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.IO;
using System.Net;
using System.ServiceModel;
using System.ServiceModel.Channels;
using System.Threading.Tasks;
using System.Web;
using System.Xml;
using System.Xml.XPath;
using SalesforceAdapter.Common;
using SalesforceAdapter.Properties;

namespace SalesforceAdapter.v1
{
	//TODO: Provide a check to make sure the database is reachable before responding to requests
	//TODO: Provide a config to enable/disable the listening for notifications, ie. for system maintenance
	//TODO: Disable listening for notifications whenever the database is unreachable
	//TODO: Provide a periodic retry for writing to the database after a write failure
	//TODO: If the database becomes available re-enable disabled notification listening

	// NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "Notification" in code, svc and config file together.
	// NOTE: In order to launch WCF Test Client for testing this service, please select Notification.svc or Notification.svc.cs at the Solution Explorer and start debugging.	

	/// <summary>
	/// Receives/Handles any Salesforce SOAP Outbound Notification Message
	/// </summary>
	[MessageCacheBehavior]
	[ServiceBehavior(Namespace = "http://soap.sforce.com/2005/09/outbound")]
	public class Notification : INotification
	{
		static readonly private Object cacheLock = new Object();
		static readonly private Object queueLock = new Object();
		private BlockingProducerConsumerQueue bQueue;

		/// <summary>
		/// Receives and replies to any SOAP notification request message from Salesforce Outbound Messaging.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns>A SOAP notification response message with "<Ack>true</Ack>".</returns>
		public notificationsResponse1 notifications(notificationsRequest request)
		{
			if (bQueue == null)
				lock (queueLock)
				{
					//Double-check pattern
					if (bQueue == null) bQueue = new BlockingProducerConsumerQueue(1);
				}

			//Only handle requests containing notification messages that we haven't seen before
			if (HasUnhandledNotifications(request))
			{
				//Get raw message
				//NOTE: this requires [MessageCacheBehavior] attribute on the Notification class
				Message message = MessageCacheProperty.GetContextMessage();

				//Create a copy of the message because once we read it we cannot read it again
				MessageBuffer copy = message.CreateBufferedCopy(int.MaxValue);
				message = copy.CreateMessage();
				
				//Read a copy of the message to get the Topic
				string Entity = GetEntityFromRequest(copy.CreateMessage());
				string Topic = String.Format("Salesforce.{0}.Notify", Entity);
				copy.Close();

				//TODO: log info enqueuing and acknowledging notifications
				Trace.TraceInformation("Enqueuing message: '{0}'.", Topic);
				Task enqueueTask = bQueue.EnqueueTask(() => { EnqueueMessage(message, Topic); }, null);
				try
				{
					enqueueTask.Wait();

					//Add Id of saved notifications to cache
					foreach (ObjectNotification notification in request.notifications.Notification)
					{
						lock (cacheLock)
						{
							//NOTE: the cache only lasts as long as the App Pool
							HttpRuntime.Cache.Insert(notification.Id, notification.Id, null, System.Web.Caching.Cache.NoAbsoluteExpiration, TimeSpan.FromHours(2));
							//TODO: log info processing record
						}
					}

					//Run Clover ETL graph
					Task graphTask = bQueue.EnqueueTask(() => { RunGraph(Topic); }, null);
				}
				catch (AggregateException ex)
				{
					Trace.TraceError("Error: Failed to Enqueue Task with Topic '{0}'.  Error was: '{1}'.", Topic, ex.InnerException.Message);
				}
			}

			//Send acknowledgement back to Salesforce that the notification was handled
			notificationsResponse response = new notificationsResponse() { Ack = true };

			return new notificationsResponse1(response);
		}

		/// <summary>
		/// Determines whether the specified request contains unhandled notifications.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
		private static bool HasUnhandledNotifications(notificationsRequest request)
		{
			bool result = false;

			foreach (ObjectNotification notification in request.notifications.Notification)
			{
				if (HttpRuntime.Cache[notification.Id] == null)
				{
					result = true;
				}
			}

			return result;
		}

		/// <summary>
		/// Enqueues the message.
		/// </summary>
		/// <param name="message">The message.</param>
		/// <param name="topic">The topic.</param>
		private static void EnqueueMessage(Message message, string topic)
		{
			ConnectionStringSettings connectionString = ConfigurationManager.ConnectionStrings["V1"];
			using (SqlConnection cn = new SqlConnection(connectionString.ConnectionString))
			{
				using (SqlCommand command = new SqlCommand("Messaging.MessageEnqueue", cn))
				{
					command.CommandType = CommandType.StoredProcedure;

					SqlParameter _topic = new SqlParameter("@Topic", SqlDbType.VarChar);
					_topic.Value = topic;
					command.Parameters.Add(_topic);

					SqlParameter payload = new SqlParameter("@Payload", SqlDbType.Xml);
					using (XmlDictionaryReader reader = message.GetReaderAtBodyContents())
					{
						payload.Value = reader.ReadOuterXml();
					}
					command.Parameters.Add(payload);

					cn.Open();
					try
					{
						int rowsAffected = command.ExecuteNonQuery();

						//Success!
						Trace.TraceInformation("Message was successfully enqueued to database.");
					}
					catch (Exception ex)
					{
						//Failure!
						Trace.TraceError("Message was not enqueued to database!  Exception: '{0}'.", ex.Message);
					}
				}
			}
		}

		/// <summary>
		/// Gets the Salesforce entity from the Outbound Message request.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns>A string with the Salesforce entity name or "Unknown" if the entity is not found</returns>
		private static string GetEntityFromRequest(Message request)
		{
			string result = "Unknown";

			if (!request.IsEmpty)
			{
				//Load the request message into a MessageBuffer so it is easier to get at the message XML body.
				MessageBuffer copy = request.CreateBufferedCopy(int.MaxValue);

				//Setup XPath objects to query the message XML body
				XPathNavigator navigator = copy.CreateNavigator();
				XmlNamespaceManager manager = new XmlNamespaceManager(navigator.NameTable);
				manager.AddNamespace("s", "http://schemas.xmlsoap.org/soap/envelope/");
				manager.AddNamespace("xsd", "http://www.w3.org/2001/XMLSchema");
				manager.AddNamespace("xsi", "http://www.w3.org/2001/XMLSchema-instance");
				manager.AddNamespace("notifications", "http://soap.sforce.com/2005/09/outbound");
				manager.AddNamespace("sf", "urn:sobject.enterprise.soap.sforce.com");
				XPathExpression query = navigator.Compile("//notifications:sObject/@xsi:type");
				query.SetContext(manager);

				//Find the first sObject element and read its type attribute
				XPathNavigator node = navigator.SelectSingleNode(query);
				string type = node.Value;
				if (!String.IsNullOrEmpty(type))
					result = type.Split(':')[1];  //Remove the namespace prefix from start of string
				copy.Close();
			}

			return result;
		}

		/// <summary>
		/// Signal Clover ETL to run a graph.
		/// </summary>
		/// <param name="graphName">Name of the graph.</param>
		private static void RunGraph(String graphName)
		{
			string graphUri = String.Format("{0}?sandbox={1}&graphID=graph/{2}.grf&nodeID=node1", Settings.Default.CloverETLGraphRunURL, Settings.Default.CloverETLSandboxName, graphName);

			using (WebClient client = new WebClient())
			{
				client.Credentials = new NetworkCredential(Settings.Default.CloverETLUser, Settings.Default.CloverETLPass);

				Uri url = new Uri(graphUri);
				try
				{
					using (Stream data = client.OpenRead(url))
					{
						using (StreamReader reader = new StreamReader(data))
						{
							string s = reader.ReadToEnd();
							//TODO: do something with s which should be the graph run Id
							Trace.TraceInformation("Started graph, got back: '{0}'.", s);
						}
					}
				}
				catch (Exception ex)
				{
					Trace.TraceError("Attempt to run graph failed with Exception: '{0}'.", ex.Message);
					//throw;
				}
			}
		}
	}
}
