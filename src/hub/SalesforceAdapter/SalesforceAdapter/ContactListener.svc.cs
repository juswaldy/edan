﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Net;
using System.Threading;
using System.Web;
using SalesforceAdapter.Properties;

namespace SalesforceAdapter
{
	//TODO: Provide a check to make sure the database is reachable before responding to requests
	//TODO: Provide a config to enable/disable the listening for notifications, ie. for system maintenance
	//TODO: Disable listening for notifications whenever the database is unreachable
	//TODO: Provide a periodic retry for writing to the database after a write failure
	//TODO: If the database becomes available re-enable disabled notification listening

	// NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "ContactListener" in code, svc and config file together.
	// NOTE: In order to launch WCF Test Client for testing this service, please select ContactListener.svc or ContactListener.svc.cs at the Solution Explorer and start debugging.
	public class ContactListener : IContactListener
	{
		/// <summary>
		/// Producer/Consumer queue for throttling Contact upsert notifications
		/// </summary>
		public class ContactQueue : IDisposable
		{
			EventWaitHandle _wait = new AutoResetEvent(false);
			Thread _worker;
			readonly Object _locker = new Object();
			Queue<notifications> _tasks = new Queue<notifications>();

			public ContactQueue()
			{
				_worker = new Thread(Work);
				_worker.Start();
			}

			public void EnqueueTask(notifications task)
			{
				lock (_locker) _tasks.Enqueue(task);
				_wait.Set();
			}

			public void Dispose()
			{
				EnqueueTask(null);  //Signal the consumer to exit
				_worker.Join();     //Wait for the consumer's thread to finish
				_wait.Close();      //Release any OS resources
			}

			private void Work(object obj)
			{
				while (true)
				{
					notifications task = null;
					lock (_locker)
						if (_tasks.Count > 0)
						{
							task = _tasks.Dequeue();
							if (task == null) return;
						}
					if (task != null)
					{
						doWork(task);  //Handle notification task (message) here
					}
					else
						_wait.WaitOne();  //No more tasks - wait for a signal
				}
			}
		}

		static readonly private Object cacheLock = new Object();
		static readonly private Object queueLock = new Object();
		private ContactQueue myQueue;

		public notificationsResponse1 notifications(notificationsRequest request)
		{
			if (myQueue == null)
				lock (queueLock)
				{
					//Double-check pattern
					if (myQueue == null) myQueue = new ContactQueue();
				}
			//TODO: log info enqueuing and acknowledging notifications
			myQueue.EnqueueTask(request.notifications);
			notificationsResponse response = new notificationsResponse();
			response.Ack = true;

			return new notificationsResponse1() { notificationsResponse = response };
		}

		static public void doWork(notifications task)
		{
			ConnectionStringSettings connectionString = ConfigurationManager.ConnectionStrings["SalesForceAdapter"];

			ContactNotification[] contactNotifications = task.Notification;
			int notificationRecordId;

			using (SqlConnection cn = new SqlConnection(connectionString.ConnectionString))
			{
				using (SqlCommand command = new SqlCommand("Inbound.NotificationInsert", cn))
				{
					command.CommandType = CommandType.StoredProcedure;

					SqlParameter notificationType = new SqlParameter("@NotificationType", SqlDbType.NVarChar);
					notificationType.Value = "CONTACT_UPSERT";
					command.Parameters.Add(notificationType);

					SqlParameter organizationId = new SqlParameter("@organizationId", SqlDbType.NVarChar);
					organizationId.Value = task.OrganizationId;
					command.Parameters.Add(organizationId);

					SqlParameter actionId = new SqlParameter("@actionId", SqlDbType.NVarChar);
					actionId.Value = task.ActionId;
					command.Parameters.Add(actionId);

					SqlParameter sessionId = new SqlParameter("@sessionId", SqlDbType.NVarChar);
					sessionId.Value = task.SessionId;
					command.Parameters.Add(sessionId);

					SqlParameter enterpriseUrl = new SqlParameter("@enterpriseUrl", SqlDbType.NVarChar);
					enterpriseUrl.Value = task.EnterpriseUrl;
					command.Parameters.Add(enterpriseUrl);

					SqlParameter partnerUrl = new SqlParameter("@partnerUrl", SqlDbType.NVarChar);
					partnerUrl.Value = task.PartnerUrl;
					command.Parameters.Add(partnerUrl);

					cn.Open();
					Object oValue = command.ExecuteScalar();
					if (int.TryParse(oValue.ToString(), out notificationRecordId))
					{
						//Success!
					}
				}
			}

			foreach (ContactNotification contactNotification in contactNotifications)
			{
				Contact contact = (Contact)contactNotification.sObject;
				try
				{
					//have we seen this notification before?
					//if (HttpRuntime.Cache[contactNotification.Id] != null)
					//{
					//	//TODO: log warn skipping record
					//	continue;
					//}
					//lock (cacheLock)
					//{
					//	//NOTE: the cache only lasts as long as the App Pool
					//	HttpRuntime.Cache.Insert(contactNotification.Id, contact.Id, null, System.Web.Caching.Cache.NoAbsoluteExpiration, TimeSpan.FromHours(2));
					//	//TODO: log info processing record
					//}

					//handle message
					using (SqlConnection cn = new SqlConnection(connectionString.ConnectionString))
					{
						using (SqlCommand command = new SqlCommand("Inbound.ContactInsert", cn))
						{
							command.CommandType = CommandType.StoredProcedure;

							SqlParameter NotificationRecordId = new SqlParameter("@NotificationRecordId", SqlDbType.NVarChar);
							NotificationRecordId.Value = notificationRecordId;
							command.Parameters.Add(NotificationRecordId);

							SqlParameter Id = new SqlParameter("@Id", SqlDbType.NVarChar);
							Id.Value = contact.Id;
							command.Parameters.Add(Id);

							SqlParameter AQ_ID__c = new SqlParameter("@AQ_ID__c", SqlDbType.Int);
							AQ_ID__c.Value = contact.AQ_ID__c;
							command.Parameters.Add(AQ_ID__c);

							SqlParameter Birth_Last_Name__c = new SqlParameter("@Birth_Last_Name__c", SqlDbType.NVarChar);
							Birth_Last_Name__c.Value = contact.Birth_Last_Name__c;
							command.Parameters.Add(Birth_Last_Name__c);

							SqlParameter Birthdate = new SqlParameter("@Birthdate", SqlDbType.DateTime);
							Birthdate.Value = contact.Birthdate;
							command.Parameters.Add(Birthdate);

							SqlParameter CreatedById = new SqlParameter("@CreatedById", SqlDbType.NVarChar);
							CreatedById.Value = contact.CreatedById;
							command.Parameters.Add(CreatedById);

							SqlParameter CreatedDate = new SqlParameter("@CreatedDate", SqlDbType.DateTime);
							CreatedDate.Value = contact.CreatedDate;
							command.Parameters.Add(CreatedDate);

							SqlParameter FirstName = new SqlParameter("@FirstName", SqlDbType.NVarChar);
							FirstName.Value = contact.FirstName;
							command.Parameters.Add(FirstName);

							SqlParameter LastName = new SqlParameter("@LastName", SqlDbType.NVarChar);
							LastName.Value = contact.LastName;
							command.Parameters.Add(LastName);

							SqlParameter RecordTypeId = new SqlParameter("@RecordTypeId", SqlDbType.NVarChar);
							RecordTypeId.Value = contact.RecordTypeId;
							command.Parameters.Add(RecordTypeId);

							SqlParameter Religion__c = new SqlParameter("@Religion__c", SqlDbType.NVarChar);
							Religion__c.Value = contact.Religion__c;
							command.Parameters.Add(Religion__c);

							SqlParameter cx__Deceased__c = new SqlParameter("@cx__Deceased__c", SqlDbType.NVarChar);
							cx__Deceased__c.Value = contact.cx__Deceased__c;
							command.Parameters.Add(cx__Deceased__c);

							SqlParameter cx__Gender__c = new SqlParameter("@cx__Gender__c", SqlDbType.NVarChar);
							cx__Gender__c.Value = contact.cx__Gender__c;
							command.Parameters.Add(cx__Gender__c);

							SqlParameter cx__Marital_Status__c = new SqlParameter("@cx__Marital_Status__c", SqlDbType.NVarChar);
							cx__Marital_Status__c.Value = contact.cx__Marital_Status__c;
							command.Parameters.Add(cx__Marital_Status__c);

							SqlParameter cx__Middle_Name__c = new SqlParameter("@cx__Middle_Name__c", SqlDbType.NVarChar);
							cx__Middle_Name__c.Value = contact.cx__Middle_Name__c;
							command.Parameters.Add(cx__Middle_Name__c);

							SqlParameter cx__Nickname__c = new SqlParameter("@cx__Nickname__c", SqlDbType.NVarChar);
							cx__Nickname__c.Value = contact.cx__Nickname__c;
							command.Parameters.Add(cx__Nickname__c);

							SqlParameter cx__Suffix__c = new SqlParameter("@cx__Suffix__c", SqlDbType.NVarChar);
							cx__Suffix__c.Value = contact.cx__Suffix__c;
							command.Parameters.Add(cx__Suffix__c);

							foreach (SqlParameter parameter in command.Parameters)
							{
								if (parameter.Value == null)
								{
									parameter.Value = DBNull.Value;
								}
							}

							cn.Open();
							Object oValue = command.ExecuteScalar();
						}
					}
				}
				catch (Exception ex)
				{
					//TODO: log error processing record
					throw ex;
				}
			}

			//if (notificationRecordId > 0)
			//{
			//	using (SqlConnection cn = new SqlConnection(connectionString.ConnectionString))
			//	{
			//		using (SqlCommand command = new SqlCommand("UPDATE Inbound.Notification SET StatusCode = 'PENDING' WHERE RecordId = " + notificationRecordId.ToString(), cn))
			//		{
			//			command.CommandType = CommandType.Text;
			//			cn.Open();
			//			command.ExecuteNonQuery();
			//		}
			//	}
			//}

			//Signal Clover ETL graph to start
			using (WebClient client = new WebClient())
			{
				client.Credentials = new NetworkCredential(Settings.Default.CloverETLUser, Settings.Default.CloverETLPass);

				Uri url = new Uri(Settings.Default.CloverETLGraphRunURL + "?sandbox=SalesforceSync_Test&graphID=graph/ProcessNewSFContact.grf&nodeID=node1");
				Stream data = client.OpenRead(url);
				StreamReader reader = new StreamReader(data);
				string s = reader.ReadToEnd();
				//TODO: do something with s which should contain the graph run Id
				data.Close();
				reader.Close();
			}
		}

		//public System.Threading.Tasks.Task<notificationsResponse1> notificationsAsync(notificationsRequest request)
		//{
		//	throw new NotImplementedException();
		//}
	}
}
