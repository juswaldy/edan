﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.IO;
using System.Net;
using System.ServiceModel;
using System.ServiceModel.Channels;
using System.Threading.Tasks;
using System.Web;
using System.Xml;
using System.Xml.XPath;
using SalesforceAdapter.Common;
using SalesforceAdapter.Properties;

namespace SalesforceAdapter.v1_1
{
	//TODO: Provide a check to make sure the database is reachable before responding to requests
	//TODO: Provide a config to enable/disable the listening for notifications, ie. for system maintenance
	//TODO: Disable listening for notifications whenever the database is unreachable
	//TODO: Provide a periodic retry for writing to the database after a write failure
	//TODO: If the database becomes available re-enable disabled notification listening

	// NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "Notification" in code, svc and config file together.
	// NOTE: In order to launch WCF Test Client for testing this service, please select Notification.svc or Notification.svc.cs at the Solution Explorer and start debugging.	

	/// <summary>
	/// Receives/Handles any Salesforce SOAP Outbound Notification Message
	/// </summary>
	[MessageCacheBehavior]
	[ServiceBehavior(Namespace = "http://soap.sforce.com/2005/09/outbound")]
	public class Notification : INotification
	{
		static readonly private Object cacheLock = new Object();
		static readonly private Object queueLock = new Object();
		private BlockingProducerConsumerQueue bQueue;

		/// <summary>
		/// The data for the following 2 dictionaries come from an array of triplets in the Settings with the format:
		/// </summary>

		/// <summary>
		/// A dictionary that maps a Salesforce OrgId to its InstanceName and Clover Sandboxname.
		/// The data for this dictionary come from an array of triplets in the Settings with the format:
		/// <Salesforce Org Id>|<Instance Name aka the Source Name for the Message Topic>|<Clover Sandbox Name>
		/// Each item in the triplet is separated by the '|' pipe symbol, like this:
		/// 00D550000006Pik|Causeview|SalesforceSync_Dev
		/// 00Dm00000008jZH|ERx|ERxSync_Dev
		/// </summary>
		static private Dictionary<string, Tuple<string, string>> _SalesforceInstances;
		static private Dictionary<string, Tuple<string, string>> SalesforceInstances
		{
			get
			{
				if (_SalesforceInstances == null)
				{
					// Create a new instance of the Dictionary.
					_SalesforceInstances = new Dictionary<string, Tuple<string, string>>();
					foreach (string instance in Settings.Default.SalesforceInstances)
					{
						string[] s = instance.Split('|');
						Tuple<string, string> t = new Tuple<string, string>(
							s[1],  // Item1 is the Instance name.
							s[2]); // Item2 is the Clover Sandbox name.
						_SalesforceInstances[s[0]] = t;
					}
				}
				return _SalesforceInstances;
			}
		}

		/// <summary>
		/// Receives and replies to any SOAP notification request message from Salesforce Outbound Messaging.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns>A SOAP notification response message with "<Ack>true</Ack>".</returns>
		public notificationsResponse1 notifications(notificationsRequest request)
		{
			if (bQueue == null)
				lock (queueLock)
				{
					//Double-check pattern
					if (bQueue == null) bQueue = new BlockingProducerConsumerQueue(1);
				}

			//Only handle requests containing notification messages that we haven't seen before
			if (HasUnhandledNotifications(request))
			{
				//Get raw message
				//NOTE: this requires [MessageCacheBehavior] attribute on the Notification class
				Message message = MessageCacheProperty.GetContextMessage();

				//Create a copy of the message because once we read it we cannot read it again
				MessageBuffer copy = message.CreateBufferedCopy(int.MaxValue);
				message = copy.CreateMessage();
				
				// Get the Sandbox, Source, Entity and make a Topic.
				string Sandbox = GetSandboxFromRequest(copy.CreateMessage());
				string Source = GetSourceFromRequest(copy.CreateMessage());
				string Entity = GetEntityFromRequest(copy.CreateMessage());
				string Topic = String.Format("{0}.{1}.Notify", Source, Entity);
				copy.Close();

				//TODO: log info enqueuing and acknowledging notifications
				Trace.TraceInformation("Enqueuing message: '{0}'.", Topic);
				Task enqueueTask = bQueue.EnqueueTask(() => { EnqueueMessage(message, Topic); }, null);
				try
				{
					enqueueTask.Wait();

					//Add Id of saved notifications to cache
					foreach (ObjectNotification notification in request.notifications.Notification)
					{
						lock (cacheLock)
						{
							//NOTE: the cache only lasts as long as the App Pool
							HttpRuntime.Cache.Insert(notification.Id, notification.Id, null, System.Web.Caching.Cache.NoAbsoluteExpiration, TimeSpan.FromHours(2));
							//TODO: log info processing record
						}
					}

					//Run Clover ETL graph
					Task graphTask = bQueue.EnqueueTask(() => { RunGraph(Sandbox, Topic); }, null);
				}
				catch (AggregateException ex)
				{
					Trace.TraceError("Error: Failed to Enqueue Task with Topic '{0}'.  Error was: '{1}'.", Topic, ex.InnerException.Message);
				}
			}

			//Send acknowledgement back to Salesforce that the notification was handled
			notificationsResponse response = new notificationsResponse() { Ack = true };

			return new notificationsResponse1(response);
		}

		/// <summary>
		/// Determines whether the specified request contains unhandled notifications.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
		private static bool HasUnhandledNotifications(notificationsRequest request)
		{
			bool result = false;

			foreach (ObjectNotification notification in request.notifications.Notification)
			{
				if (HttpRuntime.Cache[notification.Id] == null)
				{
					result = true;
				}
			}

			return result;
		}

		/// <summary>
		/// Enqueues the message.
		/// </summary>
		/// <param name="message">The message.</param>
		/// <param name="topic">The topic.</param>
		private static void EnqueueMessage(Message message, string topic)
		{
			ConnectionStringSettings connectionString = ConfigurationManager.ConnectionStrings["V1.1"];
			using (SqlConnection cn = new SqlConnection(connectionString.ConnectionString))
			{
				using (SqlCommand command = new SqlCommand("Messaging.MessageEnqueue", cn))
				{
					command.CommandType = CommandType.StoredProcedure;

					SqlParameter _topic = new SqlParameter("@Topic", SqlDbType.VarChar);
					_topic.Value = topic;
					command.Parameters.Add(_topic);

					SqlParameter payload = new SqlParameter("@Payload", SqlDbType.Xml);
					using (XmlDictionaryReader reader = message.GetReaderAtBodyContents())
					{
						payload.Value = reader.ReadOuterXml();
					}
					command.Parameters.Add(payload);

					cn.Open();
					try
					{
						int rowsAffected = command.ExecuteNonQuery();

						//Success!
						Trace.TraceInformation("Message was successfully enqueued to database.");
					}
					catch (Exception ex)
					{
						//Failure!
						Trace.TraceError("Message was not enqueued to database!  Exception: '{0}'.", ex.Message);
					}
				}
			}
		}

		/// <summary>
		/// Uses the provided xpath query to select a single node in the given Message.
		/// </summary>
		/// <param name="request">The Message.</param>
		/// <param name="xpath">The xpath query string.</param>
		/// <returns>The XML Node in the form of an XPathNavigator.</returns>
		private static XPathNavigator GetSingleNodeFromRequest(Message request, string xpath)
		{
			XPathNavigator result = null;

			if (!request.IsEmpty && xpath.Trim().Length > 0)
			{
				//Load the request message into a MessageBuffer so it is easier to get at the message XML body.
				MessageBuffer copy = request.CreateBufferedCopy(int.MaxValue);

				//Setup XPath objects to query the message XML body
				XPathNavigator navigator = copy.CreateNavigator();
				XmlNamespaceManager manager = new XmlNamespaceManager(navigator.NameTable);
				manager.AddNamespace("s", "http://schemas.xmlsoap.org/soap/envelope/");
				manager.AddNamespace("xsd", "http://www.w3.org/2001/XMLSchema");
				manager.AddNamespace("xsi", "http://www.w3.org/2001/XMLSchema-instance");
				manager.AddNamespace("notifications", "http://soap.sforce.com/2005/09/outbound");
				manager.AddNamespace("sf", "urn:sobject.enterprise.soap.sforce.com");
				XPathExpression query = navigator.Compile(xpath);
				query.SetContext(manager);

				//Find the first node specified by the xpath query.
				result = navigator.SelectSingleNode(query);

				//Finish working with the buffer.
				copy.Close();
			}

			return result;
		}

		/// <summary>
		/// Gets the Salesforce entity from the Outbound Message request.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns>A string with the Salesforce entity name or "Unknown" if the entity is not found</returns>
		private static string GetEntityFromRequest(Message request)
		{
			string result = "Unknown";

			//Find the first sObject element and read its type attribute
			XPathNavigator node = GetSingleNodeFromRequest(request, "//notifications:sObject/@xsi:type");
			if (node != null)
			{
				try
				{
					string type = node.Value;
					if (!String.IsNullOrEmpty(type))
						result = type.Split(':')[1];  //Remove the namespace prefix from start of string
				}
				catch
				{
					// Do nothing. Let this function return "Unknown".
				}
			}

			return result;
		}

		/// <summary>
		/// Gets the 15-character Salesforce Org Id from the Outbound Message request.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns>A 15 character string of the Salesforce Org Id name or "Unknown" if the OrgId element is not found</returns>
		private static string GetOrgidFromRequest(Message request)
		{
			string result = "Unknown";

			//Find the OrganizationId element.
			XPathNavigator node = GetSingleNodeFromRequest(request, "//notifications:OrganizationId");
			if (node != null)
			{
				try
				{
					// Use only the first 15 digits from the Salesforce OrgId.
					string instance = node.Value;
					if (!String.IsNullOrEmpty(instance))
						result = instance.Substring(0,15);
				}
				catch
				{
					// Do nothing. Let this function return "Unknown".
				}
			}

			return result;
		}

		/// <summary>
		/// Gets the Salesforce instance name from the Outbound Message request.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns>A string with the Salesforce instance name or "Unknown" if the instance is not found</returns>
		private static string GetSourceFromRequest(Message request)
		{
			string result = "Unknown";

			try
			{
				// Lookup the instance name for the OrgId.
				string orgId = GetOrgidFromRequest(request);
				if (!String.IsNullOrEmpty(orgId))
					result = SalesforceInstances[orgId].Item1;
			}
			catch
			{
				// Do nothing. Let this function return "Unknown".
			}

			return result;
		}

		/// <summary>
		/// Gets the Clover sandbox from the Salesforce instance found in the Outbound Message request.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns>A string with the Clover sandbox name or the default Clover sandbox name if the instance is not found</returns>
		private static string GetSandboxFromRequest(Message request)
		{
			string result = Settings.Default.CloverETLSandboxName;

			try
			{
				// Look up the clover sandbox for the OrgId.
				string orgId = GetOrgidFromRequest(request);
				if (!String.IsNullOrEmpty(orgId))
					result = SalesforceInstances[orgId].Item2;
			}
			catch
			{
				// Do nothing. Let this function return the default Clover sandbox.
			}

			return result;
		}

		/// <summary>
		/// Signal Clover ETL to run a graph.
		/// </summary>
		/// <param name="sandboxName">Name of the sandbox.</param>
		/// <param name="graphName">Name of the graph.</param>
		private static void RunGraph(String sandboxName, String graphName)
		{
			string graphUri = String.Format("{0}?sandbox={1}&graphID=graph/{2}.grf&nodeID=node1", Settings.Default.CloverETLGraphRunURL, sandboxName, graphName);

			using (WebClient client = new WebClient())
			{
				client.Credentials = new NetworkCredential(Settings.Default.CloverETLUser, Settings.Default.CloverETLPass);

				Uri url = new Uri(graphUri);
				try
				{
					using (Stream data = client.OpenRead(url))
					{
						using (StreamReader reader = new StreamReader(data))
						{
							string s = reader.ReadToEnd();
							//TODO: do something with s which should be the graph run Id
							Trace.TraceInformation("Started graph, got back: '{0}'.", s);
						}
					}
				}
				catch (Exception ex)
				{
					Trace.TraceError("Attempt to run graph failed with Exception: '{0}'.", ex.Message);
					//throw;
				}
			}
		}
	}
}
