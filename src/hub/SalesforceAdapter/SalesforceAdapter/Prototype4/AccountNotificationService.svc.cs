﻿using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.IO;
using System.Net;
using SalesforceAdapter.Properties;
using SalesforceAdapter.Common;

namespace SalesforceAdapter.Prototype4.Account
{
	// NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "AccountNotificationService" in code, svc and config file together.
	// NOTE: In order to launch WCF Test Client for testing this service, please select AccountNotificationService.svc or AccountNotificationService.svc.cs at the Solution Explorer and start debugging.
	public class AccountNotificationService : IAccountNotificationService, ITaskHandler<notifications>
	{
		static readonly private Object cacheLock = new Object();
		static readonly private Object queueLock = new Object();
		private ProducerConsumerQueue<notifications> myQueue;

		public notificationsResponse1 notifications(notificationsRequest request)
		{
			if (myQueue == null)
				lock (queueLock)
				{
					//Double-check pattern
					if (myQueue == null) myQueue = new ProducerConsumerQueue<notifications>(this);
				}
			//TODO: log info enqueuing and acknowledging notifications
			Trace.TraceInformation("Enqueuing task with parameter: '{0}'.", request.notifications.ToString());
			myQueue.EnqueueTask(request.notifications);
			notificationsResponse response = new notificationsResponse();
			response.Ack = true;

			return new notificationsResponse1() { notificationsResponse = response };
		}

		public void Handle(notifications task)
		{
			string Topic = "Salesforce.Account.Notify";
			Envelope<notifications> envelope = new Envelope<notifications>(task);  //Wrap the notification in an Envelope class which becomes the root when serialized to XML
			ConnectionStringSettings connectionString = ConfigurationManager.ConnectionStrings["SalesForceAdapter"];

			using (SqlConnection cn = new SqlConnection(connectionString.ConnectionString))
			{
				using (SqlCommand command = new SqlCommand("Messaging.MessageEnqueue", cn))
				{
					command.CommandType = CommandType.StoredProcedure;

					SqlParameter topic = new SqlParameter("@Topic", SqlDbType.VarChar);
					topic.Value = Topic;
					command.Parameters.Add(topic);

					SqlParameter payload = new SqlParameter("@payload", SqlDbType.Xml);
					payload.Value = envelope.SerializeToXml();
					command.Parameters.Add(payload);

					cn.Open();
					int rowsAffected = command.ExecuteNonQuery();
					if (rowsAffected > 0)
					{
						//Success!
						Trace.TraceInformation("Message was successfully enqueued to database.");
					}
				}
			}

			//Signal Clover ETL graph to start
			using (WebClient client = new WebClient())
			{
				client.Credentials = new NetworkCredential(Settings.Default.CloverETLUser, Settings.Default.CloverETLPass);

				Uri url = new Uri(Settings.Default.CloverETLGraphRunURL + "?sandbox=SalesforceSync_Test&graphID=graph/" + Topic + ".grf&nodeID=node1");
				try
				{
					using (Stream data = client.OpenRead(url))
					{
						using (StreamReader reader = new StreamReader(data))
						{
							string s = reader.ReadToEnd();
							//TODO: do something with s which should contain the graph run Id
							Trace.TraceInformation("Started graph, got back: '{0}'.", s);
						}
					}
				}
				catch (Exception ex)
				{
					Trace.TraceError("Attempt to run graph failed with Exception: '{0}'.", ex.Message);
					//throw;
				}
			}
		}
	}
}
