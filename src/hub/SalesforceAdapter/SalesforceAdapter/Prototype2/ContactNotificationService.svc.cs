﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Net;
using System.ServiceModel.Channels;
using System.Threading;
using System.Web;
using System.Xml.Serialization;
using SalesforceAdapter.Properties;

namespace SalesforceAdapter.Prototype2
{
	//TODO: Provide a check to make sure the database is reachable before responding to requests
	//TODO: Provide a config to enable/disable the listening for notifications, ie. for system maintenance
	//TODO: Disable listening for notifications whenever the database is unreachable
	//TODO: Provide a periodic retry for writing to the database after a write failure
	//TODO: If the database becomes available re-enable disabled notification listening

	// NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "ContactNotificationService" in code, svc and config file together.
	// NOTE: In order to launch WCF Test Client for testing this service, please select ContactNotificationService.svc or ContactNotificationService.svc.cs at the Solution Explorer and start debugging.
	public class ContactNotificationService : IContactNotificationService
	{
		/// <summary>
		/// Producer/Consumer queue for throttling Contact upsert notifications
		/// </summary>
		public class ContactQueue : IDisposable
		{
			EventWaitHandle _wait = new AutoResetEvent(false);
			Thread _worker;
			readonly Object _locker = new Object();
			Queue<notifications> _tasks = new Queue<notifications>();

			public ContactQueue()
			{
				_worker = new Thread(Work);
				_worker.Start();
			}

			public void EnqueueTask(notifications task)
			{
				lock (_locker) _tasks.Enqueue(task);
				_wait.Set();
			}

			public void Dispose()
			{
				EnqueueTask(null);  //Signal the consumer to exit
				_worker.Join();     //Wait for the consumer's thread to finish
				_wait.Close();      //Release any OS resources
			}

			private void Work(object obj)
			{
				while (true)
				{
					notifications task = null;
					lock (_locker)
						if (_tasks.Count > 0)
						{
							task = _tasks.Dequeue();
							if (task == null) return;
						}
					if (task != null)
					{
						doWork(task);  //Handle notification task (message) here
					}
					else
						_wait.WaitOne();  //No more tasks - wait for a signal
				}
			}
		}

		static readonly private Object cacheLock = new Object();
		static readonly private Object queueLock = new Object();
		private ContactQueue myQueue;

		public notificationsResponse1 notifications(notificationsRequest request)
		{
			if (myQueue == null)
				lock (queueLock)
				{
					//Double-check pattern
					if (myQueue == null) myQueue = new ContactQueue();
				}
			//TODO: log info enqueuing and acknowledging notifications
			myQueue.EnqueueTask(request.notifications);
			notificationsResponse response = new notificationsResponse();
			response.Ack = true;

			return new notificationsResponse1() { notificationsResponse = response };
		}

		static public void doWork(notifications task)
		{
			string Topic = "Salesforce.Contact.New";
			ConnectionStringSettings connectionString = ConfigurationManager.ConnectionStrings["SalesForceAdapter"];

			//ContactNotification[] contactNotifications = task.Notification;

			using (SqlConnection cn = new SqlConnection(connectionString.ConnectionString))
			{
				using (SqlCommand command = new SqlCommand("Messaging.NotificationRequestEnqueue", cn))
				{
					command.CommandType = CommandType.StoredProcedure;

					SqlParameter topic = new SqlParameter("@Topic", SqlDbType.VarChar);
					topic.Value = Topic;
					command.Parameters.Add(topic);

					//Message notification = MessageCacheProperty.GetContextMessage();
					XmlSerializer serializer = new XmlSerializer(typeof(Envelope));
					TextWriter writer = new StringWriter();
					XmlSerializerNamespaces ns = new XmlSerializerNamespaces();
					ns.Add("soapenv", "http://schemas.xmlsoap.org/soap/envelope/");
					ns.Add("xsd", "http://www.w3.org/2001/XMLSchema");
					ns.Add("xsi", "http://www.w3.org/2001/XMLSchema-instance");
					ns.Add("sf", "urn:sobject.enterprise.soap.sforce.com");
					serializer.Serialize(writer, new Envelope(task), ns);
					SqlParameter payload = new SqlParameter("@payload", SqlDbType.Xml);
					//payload.Value = notification.ToString();
					payload.Value = writer.ToString();
					command.Parameters.Add(payload);

					cn.Open();
					int rowsAffected = command.ExecuteNonQuery();
					if (rowsAffected > 0)
					{
						//Success!
					}
				}
			}

			//Signal Clover ETL graph to start
			using (WebClient client = new WebClient())
			{
				client.Credentials = new NetworkCredential(Settings.Default.CloverETLUser, Settings.Default.CloverETLPass);

				Uri url = new Uri(Settings.Default.CloverETLGraphRunURL + "?sandbox=SalesforceSync_Test&graphID=graph/" + Topic + ".grf&nodeID=node1");
				Stream data = client.OpenRead(url);
				StreamReader reader = new StreamReader(data);
				string s = reader.ReadToEnd();
				//TODO: do something with s which should contain the graph run Id
				data.Close();
				reader.Close();
			}
		}
	}
}
