﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml;
using System.Xml.Serialization;

namespace SalesforceAdapter.Prototype2
{
	[XmlRootAttribute("Envelope", Namespace = "http://schemas.xmlsoap.org/soap/envelope/", IsNullable = false)]
	public class Envelope
	{
		[XmlElementAttribute(Order = 0)]
		public Body Body { get; set; }

		public Envelope() { }
		public Envelope(notifications _notifications)
		{
			Body = new Body(_notifications);
		}
	}

	public class Body
	{
		[XmlElementAttribute(Order = 0, Namespace = "http://soap.sforce.com/2005/09/outbound")]
		public notifications notifications { get; set; }

		public Body() { }
		public Body(notifications _notifications)
		{
			if (_notifications != null)
				notifications = _notifications;
		}
	}
}