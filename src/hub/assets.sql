USE master;
GO

CREATE DATABASE JUS_ODS;
GO

USE JUS_ODS;
GO

CREATE SCHEMA Meta;
GO

CREATE TABLE Meta.Source (
	SourceId          BIGINT        IDENTITY (1, 1)           NOT NULL,
	SourceName        VARCHAR(256)                            NOT NULL,
	SourceHostUrl     VARCHAR(256)                            NOT NULL,
	SourceInstance    VARCHAR(256)                            NOT NULL,
	SourceSchema      VARCHAR(256)                            NOT NULL
);

INSERT INTO Meta.Source ( SourceName, SourceHostUrl, SourceInstance, SourceSchema ) VALUES
( 'Telus', 'file://cloveretl/IT/Prod/Import', '', '' );
GO


CREATE TABLE Meta.Entity (
	EntityId           BIGINT        IDENTITY (1, 1)           NOT NULL,
	EntitySourceId     INT                                     NOT NULL,
	EntityName         VARCHAR(256)                            NOT NULL
);
GO

CREATE TABLE Meta.Attribute (
	AttributeId        BIGINT        IDENTITY (1, 1)           NOT NULL,
	EntityId           INT                                     NOT NULL,
	AttributeName      VARCHAR(256)                            NOT NULL,
	DatatypeSQL        VARCHAR(256)                            NOT NULL,
	DatatypeClover     VARCHAR(256)                            NOT NULL
);
GO

CREATE SCHEMA Assets;
GO

CREATE TABLE Assets.Telus (
	SubscriberName        VARCHAR(256),
	BAN                   VARCHAR(256),
	PhoneNumber           VARCHAR(256),
	Status                VARCHAR(256),
	RatePlan              VARCHAR(256),
	ServiceCategory       VARCHAR(256),
	DeviceName            VARCHAR(256),
	IMEI                  VARCHAR(256),
	SimSerialNumber       VARCHAR(256),
	UpgradeEligible       VARCHAR(256),
	DeviceBalanceDue      VARCHAR(256),
	DomesticUsageMB       VARCHAR(256),
	DomesticAllowanceMB   VARCHAR(256),
	DomesticOverageMB     VARCHAR(256),
	DomesticOverageCharge VARCHAR(256),
	RoamingUsageMB        VARCHAR(256),
	RoamingAllowanceMB    VARCHAR(256),
	RoamingOverageMB      VARCHAR(256),
	RoamingOverageCharge  VARCHAR(256),
	DaysLeft              VARCHAR(256),
	ContractEndDate       VARCHAR(256)
);
GO

CREATE TABLE Assets.TeamDynamix (
	ID                       VARCHAR(256),
	AppID                    VARCHAR(256),
	AppName                  VARCHAR(256),
	FormID                   VARCHAR(256),
	FormName                 VARCHAR(256),
	ProductModelID           VARCHAR(256),
	ProductModelName         VARCHAR(256),
	ManufacturerID           VARCHAR(256),
	ManufacturerName         VARCHAR(256),
	SupplierID               VARCHAR(256),
	SupplierName             VARCHAR(256),
	StatusID                 VARCHAR(256),
	StatusName               VARCHAR(256),
	LocationID               VARCHAR(256),
	LocationName             VARCHAR(256),
	LocationRoomID           VARCHAR(256),
	LocationRoomName         VARCHAR(256),
	Tag                      VARCHAR(256),
	SerialNumber             VARCHAR(256),
	Name                     VARCHAR(256),
	PurchaseCost             VARCHAR(256),
	AcquisitionDate          VARCHAR(256),
	ExpectedReplacementDate  VARCHAR(256),
	RequestingCustomerID     VARCHAR(256),
	RequestingCustomerName   VARCHAR(256),
	RequestingDepartmentID   VARCHAR(256),
	RequestingDepartmentName VARCHAR(256),
	OwningCustomerID         VARCHAR(256),
	OwningCustomerName       VARCHAR(256),
	OwningDepartmentID       VARCHAR(256),
	OwningDepartmentName     VARCHAR(256),
	ParentID                 VARCHAR(256),
	ParentSerialNumber       VARCHAR(256),
	ParentName               VARCHAR(256),
	ParentTag                VARCHAR(256),
	MaintenanceScheduleID    VARCHAR(256),
	MaintenanceScheduleName  VARCHAR(256),
	ConfigurationItemID      VARCHAR(256),
	CreatedDate              VARCHAR(256),
	CreatedUid               VARCHAR(256),
	CreatedFullName          VARCHAR(256),
	ModifiedDate             VARCHAR(256),
	ModifiedUid              VARCHAR(256),
	ModifiedFullName         VARCHAR(256),
	ExternalID               VARCHAR(256),
	ExternalSourceID         VARCHAR(256),
	ExternalSourceName       VARCHAR(256),
	Uri                      VARCHAR(256)
);
GO

USE JUS_Hub;

CREATE SCHEMA Staging;
GO

CREATE TABLE Staging.TelusAssets (
	SubscriberName        VARCHAR(256),
	BAN                   VARCHAR(256),
	PhoneNumber           VARCHAR(256),
	Status                VARCHAR(256),
	RatePlan              VARCHAR(256),
	ServiceCategory       VARCHAR(256),
	DeviceName            VARCHAR(256),
	IMEI                  VARCHAR(256),
	SimSerialNumber       VARCHAR(256),
	UpgradeEligible       VARCHAR(256),
	DeviceBalanceDue      VARCHAR(256),
	DomesticUsageMB       VARCHAR(256),
	DomesticAllowanceMB   VARCHAR(256),
	DomesticOverageMB     VARCHAR(256),
	DomesticOverageCharge VARCHAR(256),
	RoamingUsageMB        VARCHAR(256),
	RoamingAllowanceMB    VARCHAR(256),
	RoamingOverageMB      VARCHAR(256),
	RoamingOverageCharge  VARCHAR(256),
	DaysLeft              VARCHAR(256),
	ContractEndDate       VARCHAR(256)
);
GO

CREATE TABLE Staging.TeamDynamixAssets (
	ID                       VARCHAR(256),
	AppID                    VARCHAR(256),
	AppName                  VARCHAR(256),
	FormID                   VARCHAR(256),
	FormName                 VARCHAR(256),
	ProductModelID           VARCHAR(256),
	ProductModelName         VARCHAR(256),
	ManufacturerID           VARCHAR(256),
	ManufacturerName         VARCHAR(256),
	SupplierID               VARCHAR(256),
	SupplierName             VARCHAR(256),
	StatusID                 VARCHAR(256),
	StatusName               VARCHAR(256),
	LocationID               VARCHAR(256),
	LocationName             VARCHAR(256),
	LocationRoomID           VARCHAR(256),
	LocationRoomName         VARCHAR(256),
	Tag                      VARCHAR(256),
	SerialNumber             VARCHAR(256),
	Name                     VARCHAR(256),
	PurchaseCost             VARCHAR(256),
	AcquisitionDate          VARCHAR(256),
	ExpectedReplacementDate  VARCHAR(256),
	RequestingCustomerID     VARCHAR(256),
	RequestingCustomerName   VARCHAR(256),
	RequestingDepartmentID   VARCHAR(256),
	RequestingDepartmentName VARCHAR(256),
	OwningCustomerID         VARCHAR(256),
	OwningCustomerName       VARCHAR(256),
	OwningDepartmentID       VARCHAR(256),
	OwningDepartmentName     VARCHAR(256),
	ParentID                 VARCHAR(256),
	ParentSerialNumber       VARCHAR(256),
	ParentName               VARCHAR(256),
	ParentTag                VARCHAR(256),
	MaintenanceScheduleID    VARCHAR(256),
	MaintenanceScheduleName  VARCHAR(256),
	ConfigurationItemID      VARCHAR(256),
	CreatedDate              VARCHAR(256),
	CreatedUid               VARCHAR(256),
	CreatedFullName          VARCHAR(256),
	ModifiedDate             VARCHAR(256),
	ModifiedUid              VARCHAR(256),
	ModifiedFullName         VARCHAR(256),
	ExternalID               VARCHAR(256),
	ExternalSourceID         VARCHAR(256),
	ExternalSourceName       VARCHAR(256),
	Uri                      VARCHAR(256)
);
GO

CREATE TABLE Staging.TeamDynamixUsers (
	UID VARCHAR(256),
	BEID VARCHAR(256),
	BEIDInt VARCHAR(256),
	IsActive VARCHAR(256),
	IsConfidential VARCHAR(256),
	UserName VARCHAR(256),
	FullName VARCHAR(256),
	FirstName VARCHAR(256),
	LastName VARCHAR(256),
	MiddleName VARCHAR(256),
	Salutation VARCHAR(256),
	Nickname VARCHAR(256),
	DefaultAccountID VARCHAR(256),
	DefaultAccountName VARCHAR(256),
	PrimaryEmail VARCHAR(256),
	AlternateEmail VARCHAR(256),
	ExternalID VARCHAR(256),
	AlternateID VARCHAR(256),
	SecurityRoleName VARCHAR(256),
	SecurityRoleID VARCHAR(256),
	OrgApplications VARCHAR(256),
	PrimaryClientPortalApplicationID VARCHAR(256),
	ReferenceID VARCHAR(256),
	AlertEmail VARCHAR(256),
	ProfileImageFileName VARCHAR(256),
	Company VARCHAR(256),
	Title VARCHAR(256),
	HomePhone VARCHAR(256),
	PrimaryPhone VARCHAR(256),
	WorkPhone VARCHAR(256),
	Pager VARCHAR(256),
	OtherPhone VARCHAR(256),
	MobilePhone VARCHAR(256),
	Fax VARCHAR(256),
	DefaultPriorityID VARCHAR(256),
	DefaultPriorityName VARCHAR(256),
	AboutMe VARCHAR(256),
	WorkAddress VARCHAR(256),
	WorkCity VARCHAR(256),
	WorkState VARCHAR(256),
	WorkZip VARCHAR(256),
	WorkCountry VARCHAR(256),
	HomeAddress VARCHAR(256),
	HomeCity VARCHAR(256),
	HomeState VARCHAR(256),
	HomeZip VARCHAR(256),
	HomeCountry VARCHAR(256),
	LocationID VARCHAR(256),
	LocationName VARCHAR(256),
	LocationRoomID VARCHAR(256),
	LocationRoomName VARCHAR(256),
	DefaultRate VARCHAR(256),
	CostRate VARCHAR(256),
	IsEmployee VARCHAR(256),
	WorkableHours VARCHAR(256),
	IsCapacityManaged VARCHAR(256),
	ReportTimeAfterDate VARCHAR(256),
	EndDate VARCHAR(256),
	ShouldReportTime VARCHAR(256),
	ReportsToUID VARCHAR(256),
	ReportsToFullName VARCHAR(256),
	ResourcePoolID VARCHAR(256),
	ResourcePoolName VARCHAR(256),
	TZID VARCHAR(256),
	TZName VARCHAR(256),
	TypeID VARCHAR(256),
	AuthenticationUserName VARCHAR(256),
	AuthenticationProviderID VARCHAR(256),
	IMProvider VARCHAR(256),
	IMHandle VARCHAR(256)
);
GO

CREATE TABLE Staging.TeamDynamixAttributesChoices (
	ID VARCHAR(256),
	Name VARCHAR(256),
	IsActive VARCHAR(256),
	DateCreated VARCHAR(256),
	DateModified VARCHAR(256),
	[Order] VARCHAR(256)
);
GO

CREATE TABLE Staging.TeamDynamixAssetsVendors (
	ID VARCHAR(256),
	AppID VARCHAR(256),
	AppName VARCHAR(256),
	Name VARCHAR(256),
	Description VARCHAR(256),
	IsActive VARCHAR(256),
	AccountNumber VARCHAR(256),
	IsContractProvider VARCHAR(256),
	IsManufacturer VARCHAR(256),
	IsSupplier VARCHAR(256),
	CompanyInformation_ID VARCHAR(256),
	CompanyInformation_AddressLine1 VARCHAR(256),
	CompanyInformation_AddressLine2 VARCHAR(256),
	CompanyInformation_AddressLine3 VARCHAR(256),
	CompanyInformation_AddressLine4 VARCHAR(256),
	CompanyInformation_City VARCHAR(256),
	CompanyInformation_State VARCHAR(256),
	CompanyInformation_PostalCode VARCHAR(256),
	CompanyInformation_Country VARCHAR(256),
	CompanyInformation_Url VARCHAR(256),
	CompanyInformation_Phone VARCHAR(256),
	CompanyInformation_Fax VARCHAR(256),
	ContactName VARCHAR(256),
	ContactTitle VARCHAR(256),
	ContactDepartment VARCHAR(256),
	ContactEmail VARCHAR(256),
	PrimaryContactInformation_ID VARCHAR(256),
	PrimaryContactInformation_AddressLine1 VARCHAR(256),
	PrimaryContactInformation_AddressLine2 VARCHAR(256),
	PrimaryContactInformation_AddressLine3 VARCHAR(256),
	PrimaryContactInformation_AddressLine4 VARCHAR(256),
	PrimaryContactInformation_City VARCHAR(256),
	PrimaryContactInformation_State VARCHAR(256),
	PrimaryContactInformation_PostalCode VARCHAR(256),
	PrimaryContactInformation_Country VARCHAR(256),
	PrimaryContactInformation_Url VARCHAR(256),
	PrimaryContactInformation_Phone VARCHAR(256),
	PrimaryContactInformation_Fax VARCHAR(256),
	ContractsCount VARCHAR(256),
	ProductModelsCount VARCHAR(256),
	AssetsSuppliedCount VARCHAR(256),
	CreatedDate VARCHAR(256),
	CreatedUid VARCHAR(256),
	CreatedFullName VARCHAR(256),
	ModifiedDate VARCHAR(256),
	ModifiedUid VARCHAR(256),
	ModifiedFullName VARCHAR(256)
);
GO

CREATE TABLE Staging.TeamDynamixAssetsModels (
	ID VARCHAR(256),
	AppID VARCHAR(256),
	AppName VARCHAR(256),
	Name VARCHAR(256),
	Description VARCHAR(1024),
	IsActive VARCHAR(256),
	ManufacturerID VARCHAR(256),
	ManufacturerName VARCHAR(256),
	ProductTypeID VARCHAR(256),
	ProductTypeName VARCHAR(256),
	PartNumber VARCHAR(256),
	CreatedDate VARCHAR(256),
	CreatedUid VARCHAR(256),
	CreatedFullName VARCHAR(256),
	ModifiedDate VARCHAR(256),
	ModifiedUid VARCHAR(256),
	ModifiedFullName VARCHAR(256)
);
GO

CREATE TABLE Staging.KaseyaAgents (
	AgentId VARCHAR(256) NULL,
	AgentName VARCHAR(256) NULL,
	Online VARCHAR(256) NULL,
	OSType VARCHAR(256) NULL,
	OSInfo VARCHAR(256) NULL,
	OrgId VARCHAR(256) NULL,
	MachineGroupId VARCHAR(256) NULL,
	MachineGroup VARCHAR(256) NULL,
	ComputerName VARCHAR(256) NULL,
	IPv6Address VARCHAR(256) NULL,
	IPAddress VARCHAR(256) NULL,
	MacAddr VARCHAR(256) NULL,
	OperatingSystem VARCHAR(256) NULL,
	OSVersion VARCHAR(256) NULL,
	LastLoggedInUser VARCHAR(256) NULL,
	LastRebootTime VARCHAR(256) NULL,
	LastCheckInTime VARCHAR(256) NULL,
	Country VARCHAR(256) NULL,
	CurrentUser VARCHAR(256) NULL,
	Contact VARCHAR(256) NULL,
	TimeZone VARCHAR(256) NULL,
	RamMBytes VARCHAR(256) NULL,
	CpuCount VARCHAR(256) NULL,
	CpuSpeed VARCHAR(256) NULL,
	CpuType VARCHAR(256) NULL,
	DomainWorkgroup VARCHAR(256) NULL,
	AgentFlags VARCHAR(256) NULL,
	AgentVersion VARCHAR(256) NULL,
	ToolTipNotes VARCHAR(256) NULL,
	ShowToolTip VARCHAR(256) NULL,
	DefaultGateway VARCHAR(256) NULL,
	DNSServer1 VARCHAR(256) NULL,
	DNSServer2 VARCHAR(256) NULL,
	DHCPServer VARCHAR(256) NULL,
	PrimaryWINS VARCHAR(256) NULL,
	SecondaryWINS VARCHAR(256) NULL,
	ConnectionGatewayIP VARCHAR(256) NULL,
	FirstCheckIn VARCHAR(256) NULL,
	PrimaryKServer VARCHAR(256) NULL,
	SecondaryKServer VARCHAR(256) NULL,
	CreationDate VARCHAR(256) NULL,
	OneClickAccess VARCHAR(256) NULL,
	AdminContact VARCHAR(256) NULL,
	SystemSerialNumber VARCHAR(256) NULL,
	ContactPhone VARCHAR(256) NULL,
	ContactEmail VARCHAR(256) NULL,
	Attributes VARCHAR(256) NULL
);
GO

CREATE TABLE Staging.ADComputers (
	BadLogonCount VARCHAR(256) NULL,
	CanonicalName VARCHAR(256) NULL,
	CN VARCHAR(256) NULL,
	Created VARCHAR(256) NULL,
	createTimeStamp VARCHAR(256) NULL,
	Description VARCHAR(256) NULL,
	DisplayName VARCHAR(256) NULL,
	DistinguishedName VARCHAR(256) NULL,
	DNSHostName VARCHAR(256) NULL,
	Enabled VARCHAR(256) NULL,
	IPv4Address VARCHAR(256) NULL,
	IPv6Address VARCHAR(256) NULL,
	isCriticalSystemObject VARCHAR(256) NULL,
	LastBadPasswordAttempt VARCHAR(256) NULL,
	lastLogon VARCHAR(256) NULL,
	LastLogonDate VARCHAR(256) NULL,
	lastLogonTimestamp VARCHAR(256) NULL,
	logonCount VARCHAR(256) NULL,
	Modified VARCHAR(256) NULL,
	modifyTimeStamp VARCHAR(256) NULL,
	Name VARCHAR(256) NULL,
	ObjectClass VARCHAR(256) NULL,
	ObjectGUID VARCHAR(256) NULL,
	objectSid VARCHAR(256) NULL,
	OperatingSystem VARCHAR(256) NULL,
	OperatingSystemServicePack VARCHAR(256) NULL,
	OperatingSystemVersion VARCHAR(256) NULL,
	PasswordLastSet VARCHAR(256) NULL,
	PasswordNeverExpires VARCHAR(256) NULL,
	PasswordNotRequired VARCHAR(256) NULL,
	PrimaryGroup VARCHAR(256) NULL,
	primaryGroupID VARCHAR(256) NULL,
	ProtectedFromAccidentalDeletion VARCHAR(256) NULL,
	pwdLastSet VARCHAR(256) NULL,
	SamAccountName VARCHAR(256) NULL,
	SID VARCHAR(256) NULL,
	TrustedForDelegation VARCHAR(256) NULL,
	TrustedToAuthForDelegation VARCHAR(256) NULL,
	userAccountControl VARCHAR(256) NULL,
	UserPrincipalName VARCHAR(256) NULL,
	uSNChanged VARCHAR(256) NULL,
	uSNCreated VARCHAR(256) NULL,
	whenChanged VARCHAR(256) NULL,
	whenCreated VARCHAR(256) NULL
);
GO

CREATE TABLE Staging.ExtronAssets (
	DeviceId VARCHAR(256) NULL,
	LocationName VARCHAR(256) NULL,
	RoomName VARCHAR(256) NULL,
	DeviceTypeName VARCHAR(256) NULL,
	Manufacturer VARCHAR(256) NULL,
	Model VARCHAR(256) NULL,
	DeviceName VARCHAR(256) NULL
);
GO

CREATE TABLE Staging.Telus_TD (
	SubscriberName           VARCHAR(256),
	PhoneNumber              VARCHAR(256),
	DeviceName               VARCHAR(256),
	IMEI                     VARCHAR(256),
	ContractEndDate          VARCHAR(256),
	ID                       VARCHAR(256),
	AppID                    VARCHAR(256),
	OwningCustomerID         VARCHAR(256),
	OwningCustomerName       VARCHAR(256),
	ProductModelID           VARCHAR(256),
	ManufacturerID           VARCHAR(256),
	SupplierID               VARCHAR(256),
	StatusID                 VARCHAR(256),
	AcquisitionDate          VARCHAR(256)
);
GO
