;WITH base AS (
    SELECT o.OpType, UPPER(o.OpSchema) OpSchema, o.OpName, t.TokenOpId, t.TokenSequence, t.TokenInstance
    FROM Metacoder.Token t
    JOIN Metacoder.Op o ON t.TokenOpId = o.OpId
    WHERE t.TokenMode = 'W'
    AND o.OpName NOT IN ('GENERATE_CSV', 'GENERATE_CSV_SCRIPT')
),
starget AS (
    SELECT *
    FROM base
    WHERE TokenInstance IN ('FROM', 'JOIN', 'LEFT JOIN', 'LEFT  JOIN', 'LEFT OUTER JOIN', 'CROSS JOIN', 'INNER JOIN', 'EXEC', 'INTO', 'UPDATE')
),
stargetx AS (
    SELECT s1.OpType, s1.OpSchema, s1.OpName, s1.TokenInstance, REPLACE(REPLACE(CONCAT(s2.TokenInstance, s3.TokenInstance, s4.TokenInstance), '[', ''), ']', '') continuation, REPLACE(REPLACE(CONCAT(s5.TokenInstance, s6.TokenInstance, s7.TokenInstance, s8.TokenInstance), '[', ''), ']', '') cont2
    FROM starget s1
    LEFT JOIN base s2 ON s1.TokenOpId = s2.TokenOpId AND s1.TokenSequence = s2.TokenSequence-1
    LEFT JOIN base s3 ON s1.TokenOpId = s3.TokenOpId AND s1.TokenSequence = s3.TokenSequence-2
    LEFT JOIN base s4 ON s1.TokenOpId = s4.TokenOpId AND s1.TokenSequence = s4.TokenSequence-3
    LEFT JOIN base s5 ON s1.TokenOpId = s5.TokenOpId AND s1.TokenSequence = s5.TokenSequence-4
    LEFT JOIN base s6 ON s1.TokenOpId = s6.TokenOpId AND s1.TokenSequence = s6.TokenSequence-5
    LEFT JOIN base s7 ON s1.TokenOpId = s7.TokenOpId AND s1.TokenSequence = s7.TokenSequence-6
    LEFT JOIN base s8 ON s1.TokenOpId = s8.TokenOpId AND s1.TokenSequence = s8.TokenSequence-7
),
stargety AS (
    SELECT *, CASE WHEN cont2 LIKE '.%.%' THEN CONCAT(continuation, cont2) ELSE continuation END y
    FROM stargetx
    WHERE continuation LIKE '%.%'
),
plusreverse AS (
    SELECT y.OpId, x.*, REVERSE(y) y_reverse
    FROM stargety x
    LEFT JOIN Metacoder.Op y ON x.y = CONCAT(y.OpSchema, '.', y.OpName)
),
final AS (
    SELECT *, UPPER(SUBSTRING(y, 1, CHARINDEX('.', y)-1)) firstpart, REVERSE(SUBSTRING(y_reverse, 1, CHARINDEX('.', y_reverse)-1)) lastpart
    FROM plusreverse
),
nonops AS (
    SELECT DISTINCT firstpart schemaname, lastpart objname, 'T' type
    FROM final
    WHERE OpId IS NULL
),
forwarddeps AS (
    SELECT DISTINCT OpSchema sourceschema, OpName sourcename, firstpart targetschema, lastpart targetname
    FROM final
    WHERE TokenInstance IN ('FROM', 'JOIN', 'LEFT JOIN', 'LEFT  JOIN', 'LEFT OUTER JOIN', 'CROSS JOIN', 'INNER JOIN', 'EXEC')
),
backdeps AS (
    SELECT DISTINCT firstpart sourceschema, lastpart sourcename, OpSchema targetschema, OpName targetname
    FROM final
    WHERE TokenInstance IN ('INTO', 'UPDATE')
),
deps AS (
    SELECT * FROM forwarddeps
    UNION
    SELECT * FROM backdeps
),
therest AS (
    SELECT sourceschema, sourcename FROM deps
    UNION
    SELECT targetschema, targetname FROM deps
)
--SELECT * FROM nonops
--SELECT DISTINCT * FROM therest
SELECT DISTINCT sourcename, targetname FROM deps
---*/

