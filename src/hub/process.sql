-- Adapted from https://dba.stackexchange.com/questions/165143/how-to-get-the-sql-server-activity-monitors-output-using-t-sql

;WITH waiting_tasks AS (
	-- Sort waiting tasks for each thread, descending so that we can pick the longest ones later.
	 SELECT
	 	*,
	 	ROW_NUMBER() OVER (PARTITION BY waiting_task_address ORDER BY wait_duration_ms DESC) AS row_num 
	FROM sys.dm_os_waiting_tasks
)
SELECT
	s.session_id,
	t.task_state,
	r.command,
	SUBSTRING(st.text,
		(r.statement_start_offset / 2) + 1, -- Unicode = 2 bytes.
		((
			CASE r.statement_end_offset
			WHEN -1 THEN DATALENGTH(st.text)
			ELSE r.statement_end_offset
			END
			-
			r.statement_start_offset
		) / 2) + 1 -- Unicode = 2 bytes.
	) running_statement,

	REPLACE(CONCAT(
		DB_NAME(st.dbid),
		N'.',
		OBJECT_SCHEMA_NAME(st.objectid, st.dbid),
		N'.',
		OBJECT_NAME(st.objectid, st.dbid)
	), N'..', N'') proc_name,

	s.host_name,
	s.program_name,
	DB_NAME(r.database_id) db_name,

	w.blocking_session_id,

	-- True if session is blocking other(s), and is either idle or has no blocker.
	-- False otherwise.
	CASE
	WHEN r2.session_id IS NOT NULL -- Session is blocking other(s).
		AND
		(
			r.blocking_session_id = 0 -- Session is not blocked by anyone else.
			OR r.session_id IS NULL -- Session is idle.
		) THEN 1
	ELSE 0
	END head_blocker,

	r.cpu_time,
	r.total_elapsed_time / 1000.0 elapsed_sec,
	w.wait_type,
	w.wait_duration_ms,
	w.resource_description,
	(s.reads + s.writes) * 8 / 1024 total_io_mb,
	s.memory_usage * 8192 / 1024 total_memory_kb,
	r.open_transaction_count,
	s.is_user_process,
	s.login_name,
	s.login_time,
	s.last_request_start_time,
	c.client_net_address, 
	t.exec_context_id,
	r.request_id,
	st.text

FROM sys.dm_exec_sessions s
LEFT JOIN sys.dm_exec_connections c ON s.session_id = c.session_id
LEFT JOIN sys.dm_exec_requests r ON s.session_id = r.session_id
LEFT JOIN sys.dm_os_tasks t ON r.session_id = t.session_id AND r.request_id = t.request_id
LEFT JOIN waiting_tasks w ON t.session_id = w.session_id AND w.row_num = 1 -- Select the longest waiting thread for each task.
LEFT JOIN sys.dm_exec_requests r2 ON r.session_id = r2.blocking_session_id
OUTER APPLY sys.dm_exec_sql_text(r.sql_handle) AS st 

WHERE s.session_id > 50 -- Ignore system sessions.
AND t.task_state IS NOT NULL
ORDER BY s.session_id;
