
import argparse
import re
import requests
import json
import time
import pyodbc
from tdconfig import *
from dbconfig import *
from pydantic_settings import BaseSettings
import datetime
from dateutil.relativedelta import relativedelta
from bs4 import BeautifulSoup
from markdownify import markdownify as md

DEBUGGING = False

class Settings(BaseSettings):
    app_id : str = '366' # Tickets: 366 = IT, 1220 = Registrar - Request
    modelid : str = str(None)
    entity_id : str = str(None)
    attribute_id : str = str(None)
    entity_name : str = str(None)
    function : str = str(None)
    api_call_path : str = str(None)
    method : str = str(None)
    infile : str = str(None)
    outfile : str = str(None)

settings = Settings()

## Clean up data value.
def cleanup(x):
    # Replace whitespace with a single space.
    y = re.sub(r'\s+', ' ', x)
    # Replace single quotes with two single quotes.
    y = re.sub(r"'", "''", y)
    # Replace 0001-01-01 dates with blank.
    y = re.sub(r'0001-01-01T.*', '', y)
    return y

## Format the bearer token for the header.
def bearer(auth_token):
    return 'Bearer {}'.format(auth_token)

## Login to TeamDynamix.
def login():
    url = '{}/auth/loginadmin'.format(urlbase)
    headers = {'Content-Type': 'application/json'}
    login_data = {'BEID': TD_BEID, 'WebServicesKey': TD_WebServicesKey}
    auth_token = requests.post(url, headers=headers, data=json.dumps(login_data))
    return auth_token

## Save auth token to the file.
def save_auth(auth_token):
    jwt = open(auth_token_file, 'w')
    jwt.write(auth_token)
    jwt.close()

## Save TeamDynamix response to file.
def save_response(response, file_name):
    response_file = open(file_name, 'w')
    response_file.write(json.dumps(response, indent=4))
    response_file.close()

## Ensure we have a valid auth token.
def ensure_auth():
    # Read auth token from the file.
    auth_token = open(auth_token_file).read()

    # Verify auth token by calling getuser.
    url = '{}/auth/getuser'.format(urlbase)
    headers = {'Content-Type': 'application/json', 'Authorization': bearer(auth_token)}
    response = requests.get(url, headers=headers)
    response = json.loads(response.text)

    # If failed verification, login, get a new auth token, and save it to the file.
    if response.get('UID', '') == '':
        auth_token = login()
        auth_token = auth_token.text
        save_auth(auth_token)

    return bearer(auth_token)

## Call the TeamDynamix API and return response text as JSON.
def call_api(method, params, data=None, sentinel=0.0, limit=1.0):
    # Read auth token from the file.
    auth_token = open(auth_token_file).read()

    # Sleep if we may be over limit.
    time_diff = time.process_time() - sentinel
    if time_diff < limit:
        time.sleep(limit - time_diff)

    # Get the method to call.
    requests_method = getattr(requests, method)

    # Call the TeamDynamix API.
    headers = {'Content-Type': 'application/json', 'Authorization': bearer(auth_token)}
    url = params['api_call_path']
    print(headers, params, data) if DEBUGGING else None
    response = requests_method(url, headers=headers, json=data)
    print('Response: {}'.format(response.text)) if DEBUGGING else None
    # response = {} if response.text in [ '', '[]' ] else json.loads(response.text)
    response = {} if response.text in [ '', '[]' ] or response.json()['ID'] == -1 else json.loads(response.text)
    return (response, time.process_time())

## Get generic object from TeamDynamix.
def get_object():
    params = { 'api_call_path': settings.api_call_path }
    print('Pulling {} at {}'.format(settings.entity_name, time.strftime('%Y-%m-%d %H:%M:%S')), flush=True)
    obj = call_api(settings.method, params)
    save_response(obj, f'{settings.entity_name}.json')

## Get applications from TeamDynamix.
def get_applications():
    params = { 'api_call_path': '{}/applications'.format(urlbase) }
    print('Pulling applications at {}'.format(time.strftime('%Y-%m-%d %H:%M:%S')), flush=True)
    applications = call_api('get', params)
    save_response(applications, 'applications.json')

## Pull object headers from TeamDynamix.
def pull_object_headers(apiname, tablename):
    ## Prepare db connection and cursor.
    conn = pyodbc.connect(hub_PROD, autocommit=True)
    cursor = conn.cursor()

    # Setup parameters.
    params = { 'api_call_path': '{}/{}/{}/search'.format(urlbase, settings.app_id, apiname) }
    sentinel = 0
    max_results = 2000
    batch_size = 100
    date_format = '%Y-%m-%dT%H:%M:%S'
    date_today = datetime.date.today()
    date_from = datetime.date(2024, 2, 1)

    print(params) if DEBUGGING else None

    # Get objects one month at a time.
    while date_from < date_today:
        date_to = date_from + relativedelta(months=1)
        data = {
            'MaxResults': max_results,
            'CreatedDateFrom': date_from.strftime(date_format),
            'CreatedDateTo': date_to.strftime(date_format)
        }
        data = {
            'ReturnCount' : max_results
        }
        objects, sentinel = call_api('post', params, data, sentinel=sentinel, limit=2.0)
        print(f'{date_from.strftime(date_format)} = {len(objects)}')
        with open(f'kba-{settings.app_id}.json', 'w') as outfile:
            json.dump(objects, outfile, indent=4)

        # If we reached max results, quit. We need to split up this month to more than one call.
        if len(objects) == max_results:
            raise Exception(f'Reached max results of {max_results}!')
        
        # # Save object IDs to the database, batch_size at a time.
        # for i in range(0, len(objects), batch_size):
        #     object_ids = [ f"('{object['AppID']}','{object['ID']}')" for object in objects[i:i+batch_size]]
        #     sql = f"INSERT INTO {tablename} (AppID, ID) VALUES {','.join(object_ids)}"
        #     cursor.execute(sql)
        #     cursor.commit()

        # Prepare for the next month.
        date_from = date_to

## Meta #####################################################################
## Get metadata from TD type page.
def get_metadata() -> str:
    import requests
    from bs4 import BeautifulSoup
    
    # Default to the Ticket type page.
    typepage = settings.api_call_path if settings.api_call_path else 'https://trinitywestern.teamdynamix.com/TDWebApi/Home/type/TeamDynamix.Api.Tickets.Ticket'

    # Prepare the output file and metadata name.
    metaname = typepage.split('/')[-1]
    outputfile = f'{metaname}.json'
    with open(outputfile, 'w') as outfile:

        # Request the metadata page.
        response = requests.get(typepage)
        soup = BeautifulSoup(response.text, 'html.parser')

        # Get the metadata properties table.
        table = soup.find('table', {'id': 'properties-table'})

        # Read table header and make an attribute list.
        attributes = [ th.text.strip() for th in table.find('thead').find_all('th') ]

        # Convert question mark to 'Is'.
        attributes = [ re.sub(r'(.*)\?$', r'Is\1', attribute) for attribute in attributes ]
        print(attributes) if DEBUGGING else None

        # Prepare the field list and populate it from the table. Skip the header.
        fields = []
        for row in table.find_all('tr')[1:]:
            cols = row.find_all('td')
            col_values = [
                cols[0].text.strip(),
                True if cols[1].find('span', {'class': 'glyphicon-pencil'}) else False,
                True if cols[2].find('span', {'class': 'glyphicon-ok-circle'}) else False,
                cols[3].text.strip(),
                True if cols[4].find('span', {'class': 'glyphicon-ok-circle'}) else False,
                cols[5].text.strip()
            ]
            fields.append(dict(zip(attributes, col_values)))
        print(fields) if DEBUGGING else None

        # Make a final dictionary and write it to the output file.
        metadata = {}
        metadata[metaname] = fields
        json.dump(metadata, outfile, indent=4)

    return outputfile

## Convert metadata to SQL.
def metadata_to_sql():
    infile = f'{settings.entity_name}.json'
    outfile = f'{settings.entity_name}.json.sql'
    tablename = '_'.join(settings.entity_name.split('.')[-2:])
    with open(infile, 'r') as infile, open(outfile, 'w') as outfile:
        meta = json.load(infile)
        sqlfields = []
        for f in meta[settings.entity_name]:
            name = f['Name']
            if '[]' not in f['Type']:
                sqltype = TD2SQL_TYPEMAP[f['Type']] if f['Type'] in TD2SQL_TYPEMAP else 'NVARCHAR(MAX)'
                sqlfields.append(f'    [{name}] {sqltype}')
        sql = f'CREATE TABLE [TD].[{tablename}] (\n'
        sql += ',\n'.join(sqlfields)
        sql += '\n);'
        outfile.write(sql)

## Tickets #####################################################################
## Get a single ticket from TeamDynamix.
def get_ticket():
    params = { 'api_call_path': '{}/{}/tickets/{}'.format(urlbase, settings.modelid, settings.entity_id) }
    print('Pulling ticket for app_id: {}, ticket_id: {} at {}'.format(settings.modelid, settings.entity_id, time.strftime('%Y-%m-%d %H:%M:%S')), flush=True)
    ticket, _ = call_api('get', params)
    if ticket.get('ID', '') != '':
        save_response(ticket, 'ticket.json')
        print(ticket['Attributes']) if DEBUGGING else None

## Pull ticket headers from TeamDynamix.
def pull_ticket_headers():
    settings.app_id = 3210
    pull_object_headers(apiname='tickets', tablename='TD.Tickets_Ticket')

## Pull ticket details from TeamDynamix.
def pull_ticket_details():
    entity_name = 'Ticket'

    # Get the list of tickets to process.
    conn = pyodbc.connect(hub_PROD, autocommit=True)
    cursor = conn.cursor()
    cursor.execute("""
        SELECT t.AppID, t.ID
        FROM Staging.TDTicket t
        WHERE COALESCE(t.AppID, '') != '' AND COALESCE(t.ID, '') != ''
        AND t.Title IS NULL
        ORDER BY t.AppID, t.ID
    """)
    rows = cursor.fetchall()

    # Pull each ticket.
    sentinel = 0
    for row in rows:
        app_id = row.AppID
        ticket_id = row.ID
        params = { 'api_call_path': '{}/{}/tickets/{}'.format(urlbase, app_id, ticket_id) }
        print('Pulling ticket for app_id: {}, ticket_id: {} at {}'.format(app_id, ticket_id, time.strftime('%Y-%m-%d %H:%M:%S')), flush=True)
        ticket, sentinel = call_api('get', params, sentinel=sentinel, limit=0.6)
        if ticket.get('ID', '') != '':
            save_response(ticket, 'Ticket.json')
            for f in FIELDS[entity_name].keys():
                FIELDS[entity_name][f] = cleanup(str(ticket.get(f, '')))
            sql = """
                UPDATE Staging.TD{} SET
                {}
                WHERE AppID = '{}' AND ID = '{}'
            """.format(entity_name, ', '.join(['{} = N\'{}\''.format(k, v) for k, v in FIELDS[entity_name].items()]), app_id, ticket_id)
            print(sql) if DEBUGGING else None
            cursor.execute(sql)
            cursor.commit()

            # Get the ticket's custom attributes.
            subentity_name = 'Attribute'
            for a in ticket[f'{subentity_name}s']:
                for f in FIELDS[subentity_name].keys():
                    FIELDS[subentity_name][f] = '' if 'ID' in f else cleanup(str(a.get(f, '')))
                sql = """
                    INSERT INTO Staging.TD{} VALUES
                    ({}, {}, {})
                """.format('Attribute', ticket['ID'], a['ID'], ', '.join(['N\'{}\''.format(v) for v in FIELDS[subentity_name].values()][2:]))
                print(sql) if DEBUGGING else None
                cursor.execute(sql)
                cursor.commit()

## Pull ticket feeds from TeamDynamix.
def pull_ticketfeeds():
    # Get the list of tickets to process.
    conn = pyodbc.connect(hub_PROD, autocommit=True)
    cursor = conn.cursor()
    cursor.execute("""
        SELECT t.AppID, t.ID
        FROM Staging.TDTicket t
        LEFT JOIN Staging.TDFeed f ON t.ID = f.ItemID
        WHERE COALESCE(t.ID, '') != '' AND f.ID IS NULL
        ORDER BY t.AppID, t.ID DESC
    """)
    rows = cursor.fetchall()

    # Pull feeds for each ticket.
    sentinel_tf = 0
    for row in rows:
        app_id = row.AppID
        ticket_id = row.ID
        params = { 'api_call_path': '{}/{}/tickets/{}/feed'.format(urlbase, app_id, ticket_id) }
        print('Pulling ticket feeds for app_id: {}, ticket_id: {} at {}'.format(app_id, ticket_id, time.strftime('%Y-%m-%d %H:%M:%S')))
        ticket_feeds, sentinel_tf = call_api('get', params, sentinel_tf, 1.0)
        save_response(ticket_feeds, 'TicketFeeds.json')
        entity_name = 'Feed'
        feedbody = []
        sentinel = 0
        for f in ticket_feeds:
            feed_id = f['ID']
            params = { 'api_call_path': '{}/feed/{}'.format(urlbase, feed_id) }
            feed, sentinel = call_api('get', params, sentinel, 1.0)
            save_response(feed, 'Feed.json')
            feedbody.append(feed['Body'])
            for reply in feed['Replies']:
                feedbody.append(reply['Body'])
            for f in FIELDS[entity_name].keys():
                x = str(feed.get(f, ''))
                # Replace single quotes with two single quotes.
                x = re.sub(r"'", "''", x)
                FIELDS[entity_name][f] = x
            print('Feed body: {}'.format('\n'.join(feedbody))) if DEBUGGING else None
            # Replace feed in database.
            cursor.execute("DELETE Staging.TD{} WHERE ID = '{}'".format(entity_name, feed_id))
            sql = """
                INSERT INTO Staging.TD{} ({}) VALUES ({})
            """.format(entity_name, ', '.join(FIELDS[entity_name].keys()), ', '.join(['\'{}\''.format(v) for v in FIELDS[entity_name].values()]))
            cursor.execute(sql)
            cursor.commit()

## KB Articles #################################################################
## Pull KB Article headers from TeamDynamix.
def pull_kba_headers():
    # 2990 Admissions- Portal   
    # 2640 Business MBA - Portal
    # 2545 Facilities - Portal  
    # 2786 Finance - Portal     
    # 3077 Financial Aid        
    # 2542 GLOBAL - Portal      
    # 2636 Housing - Portal     
    # 2546 HR - Portal
    # 2550 IT - Portal
    # 2669 Library - Portal     
    # 2547 Marketing - Portal   
    # 3021 Outfitters - Portal  
    # 2980 Payroll - Portal     
    # 2548 Registrar - Portal   
    # 1904 TWU Client Portal    
    # 2168 z Test Portal        
    settings.app_id = 1904
    pull_object_headers(apiname='knowledgebase', tablename='Staging.TDKBA')

## Pull KB Article details from TeamDynamix.
def pull_kba_details():
    entity_name = 'KBA'

    # Get the list of kbas to process.
    conn = pyodbc.connect(hub_PROD, autocommit=True)
    cursor = conn.cursor()
    cursor.execute("""
        SELECT x.AppID, x.ID
        FROM Staging.TDKBA x
        WHERE COALESCE(x.AppID, '') != '' AND COALESCE(x.ID, '') != ''
        AND x.Subject IS NULL
        ORDER BY x.AppID, x.ID
    """)
    rows = cursor.fetchall()

    # Pull each kba.
    sentinel = 0
    for row in rows:
        app_id = row.AppID
        kba_id = row.ID
        params = { 'api_call_path': '{}/{}/knowledgebase/{}'.format(urlbase, app_id, kba_id) }
        print('Pulling kba for app_id: {}, kba_id: {} at {}'.format(app_id, kba_id, time.strftime('%Y-%m-%d %H:%M:%S')), flush=True)
        kba, sentinel = call_api('get', params, sentinel=sentinel, limit=0.6)
        if kba.get('ID', '') != '':
            save_response(kba, 'KBA.json')
            for f in FIELDS[entity_name].keys():
                FIELDS[entity_name][f] = cleanup(str(kba.get(f, '')))
            sql = """
                UPDATE Staging.TD{} SET
                {}
                WHERE AppID = '{}' AND ID = '{}'
            """.format(entity_name, ', '.join(['{} = N\'{}\''.format(k, v) for k, v in FIELDS[entity_name].items()]), app_id, kba_id)
            print(sql) if DEBUGGING else None
            cursor.execute(sql)
            cursor.commit()

## Convert KB Article Body to markdown and text.
def reformat_kbabody():
    # Get the list of kbas to process.
    conn = pyodbc.connect(hub_PROD, autocommit=True)
    cursor = conn.cursor()
    cursor.execute("""
        SELECT x.AppID, x.ID, x.Body
        FROM Staging.TDKBA x
        WHERE BodyText IS NULL OR BodyMarkdown IS NULL
        ORDER BY x.AppID, x.ID
    """)
    rows = cursor.fetchall()

    for row in rows:
        app_id = row.AppID
        kba_id = row.ID
        print('Processing kba body for app_id: {}, kba_id: {} at {}'.format(app_id, kba_id, time.strftime('%Y-%m-%d %H:%M:%S')), flush=True)
        html = BeautifulSoup(row.Body, features='lxml')
        bodytext = html.get_text().replace("'", "''")
        bodymarkdown = md(str(html)).replace("'", "''")
        sql = "UPDATE Staging.TDKBA SET BodyText = N'{}', BodyMarkdown = N'{}' WHERE AppID = '{}' AND ID = '{}'".format(bodytext, bodymarkdown, app_id, kba_id)
        print(sql) if DEBUGGING else None
        cursor.execute(sql)
        cursor.commit()

################################################################################

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Call TeamDynamix API')
    parser.add_argument('-m', '--modelid', help='Model ID', required=False)
    parser.add_argument('-e', '--entityid', help='Entity ID', required=False)
    parser.add_argument('-a', '--attributeid', help='Attribute ID', required=False)
    parser.add_argument('-n', '--entityname', help='Entity Name', required=False)
    parser.add_argument('-f', '--function', help='Function name to call', required=False)
    parser.add_argument('-p', '--apicallpath', help='Path to endpoint', required=False)
    parser.add_argument('-t', '--method', default='function', help='API request method: get, post, put, or function', required=True)
    parser.add_argument('-i', '--infile', help='Path to input file', required=False)
    parser.add_argument('-o', '--outfile', help='Path to output file', required=False)
    args = parser.parse_args()

    # Get parameters.
    settings.modelid = args.modelid
    settings.entity_id = args.entityid
    settings.attribute_id = args.attributeid
    settings.entity_name = args.entityname
    settings.function = args.function
    settings.api_call_path = args.apicallpath
    settings.method = args.method
    settings.infile = args.infile
    settings.outfile = args.outfile

    # Login and save auth token to the file.
    auth_token = login()
    auth_token = auth_token.text
    save_auth(auth_token)

    if args.method == 'function':
        exec("{}()".format(args.function))
    else:
        params = { 'api_call_path': f'{urlbase}/{args.apicallpath}' }
        print('Pulling {} at {}'.format(args.entityname, time.strftime('%Y-%m-%d %H:%M:%S')), flush=True)
        obj = call_api(args.method, params)
        save_response(obj, f'{args.entityname}.json')
