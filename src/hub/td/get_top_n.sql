DECLARE @n INT = 20;
DECLARE @sortorder VARCHAR(8) = 'DESC';
DECLARE @basetable VARCHAR(256) = 'TDTicket'
DECLARE @fieldlist VARCHAR(MAX) = 'TypeName,SlaName';
DECLARE @script_populate NVARCHAR(MAX);
DECLARE @script_aggregate NVARCHAR(MAX);
DECLARE @script NVARCHAR(MAX);

-- Create temp table #topN with id column.
IF OBJECT_ID('tempdb.dbo.#topN', 'U') IS NOT NULL DROP TABLE #topN;
CREATE TABLE #topN (id INT IDENTITY(1, 1));

-- Add required columns.
SELECT @script = SUBSTRING((SELECT CONCAT(';ALTER TABLE #topN ADD ', element, ' VARCHAR(256)') AS [text()] FROM dbo.twu_fn_string_split(@fieldlist, ',') ORDER BY idx FOR XML PATH ('')), 2, 8000);
EXEC sp_executesql @script;

-- Prepare populate script.
SET @script_populate = '
    WITH counts AS ( SELECT {fieldlist}, COUNT(*) Count FROM Staging.{basetable} GROUP BY {fieldlist} ) SELECT * FROM counts ORDER BY Count {sortorder};
    WITH counts AS ( SELECT {fieldlist}, COUNT(*) Count FROM Staging.{basetable} GROUP BY {fieldlist} ),
    topN AS ( SELECT TOP {n} {fieldlist} FROM counts ORDER BY Count DESC )
    INSERT INTO #topN SELECT * FROM topN;
';

-- Prepare aggregate script.
SET @script_aggregate = '
WITH base AS (
    SELECT {fieldlist}, CONCAT(CONVERT(VARCHAR(4), DATEPART(YEAR, CreatedDate)), ''-'', RIGHT(CONCAT(''00'', CONVERT(VARCHAR(2), DATEPART(MONTH, CreatedDate))), 2)) ym
    FROM Staging.{basetable} x
    JOIN #topN y ON {joinlist}
)
SELECT {fieldlist}, ym, COUNT(*) count
FROM base x
GROUP BY {fieldlist}, ym
'

-- Get top N fieldname.
DELETE FROM #topN;
SET @script = REPLACE(@script_populate, '{basetable}', @basetable);
SET @script = REPLACE(@script, '{fieldlist}', @fieldlist);
SET @script = REPLACE(@script, '{sortorder}', @sortorder);
SET @script = REPLACE(@script, '{n}', @n);
EXEC sp_executesql @script;

-- Get aggregated results.
SET @script = REPLACE(@script_aggregate, '{basetable}', @basetable);
SET @script = REPLACE(@script, '{fieldlist}', (SELECT SUBSTRING((SELECT CONCAT(',x.', element) AS [text()] FROM dbo.twu_fn_string_split(@fieldlist, ',') ORDER BY idx FOR XML PATH ('')), 2, 8000)));
SET @script = REPLACE(@script, '{joinlist}', (SELECT SUBSTRING((SELECT CONCAT(' AND x.', element, ' = y.', element) AS [text()] FROM dbo.twu_fn_string_split(@fieldlist, ',') ORDER BY idx FOR XML PATH ('')), 6, 8000)));
EXEC sp_executesql @script