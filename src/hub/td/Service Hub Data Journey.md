# Service Hub Data Journey

## Data Discussion #23

Fri, 16 Sep 2022

### Conceptual Dependency Graph

```mermaid
graph BT
    OneNote(OneNote)
    Tickets(Tickets)
    KB(KB Articles)
    Tickets --> OneNote
    KB --> OneNote
```

### Model Training Strategy

```mermaid
graph LR
    OneNote --> Tree
    Tickets --> XGBoost
    Tree --> XGBoost
    XGBoost --> Categories
    Tree --> TopicModeling
    KB --> TopicModeling
    TopicModeling --> Suggestion
```

## Service Hub Data

Fri, 23 Sep 2022