# Service Hub Tickets

## Loading TD tables

```mermaid
graph
    td -- Download<br>metadata --> html
    html -- Parse<br>metadata --> metadata
    metadata -- Normalize --> normalized
    normalized -- Generate --> sql
    sql[CREATE TABLE<br>sql] -- Execute --> table
    td -- API Get --> json
    json -- Parse<br>data --> data
    data --> joined
    normalized --> joined
    joined -- Load --> table
```

## Generic Process `source --> table`

```mermaid
graph
    source -- Download<br>metadata --> exformat
    exformat -- Parse<br>metadata --> preformat
    preformat -- Normalize --> informat
    informat -- Generate --> sql
    sql[CREATE TABLE<br>sql] -- Execute --> table
    source -- API Get --> json
    json -- Parse<br>data --> data
    data --> joined
    informat --> joined
    joined -- Load --> table
```