--------------------------------------------------------------------------------
-- Author:      Juswaldy Jusman
-- Date:        2022-06-22
-- Description: Given a table and 2 sets of columns p ⊂ q, return the counts of
--              unique q that are not unique in p.
--              If uniqueness of p = q, then return a null set.
--------------------------------------------------------------------------------
CREATE PROCEDURE uniq
	@table VARCHAR(256),
	@p VARCHAR(MAX),
	@q VARCHAR(MAX)
AS
BEGIN
	DECLARE @script NVARCHAR(MAX) = '
	WITH base AS ( SELECT DISTINCT {q} FROM {table} )
	SELECT {p}, COUNT(*) Count FROM base GROUP BY {p} HAVING COUNT(*) > 1
	';
	SET @script = REPLACE(@script, '{table}', @table);
	SET @script = REPLACE(@script, '{p}', @p);
	SET @script = REPLACE(@script, '{q}', @q);
	EXEC sp_executesql @script;
END
GO
