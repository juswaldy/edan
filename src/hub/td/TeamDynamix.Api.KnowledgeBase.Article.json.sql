CREATE TABLE [Staging].[TDKBA] (
    [ID] INT,
    [AppID] INT,
    [AppName] NVARCHAR(MAX),
    [CategoryID] INT,
    [CategoryName] NVARCHAR(MAX),
    [Subject] NVARCHAR(MAX),
    [Body] NVARCHAR(MAX),
    [Summary] NVARCHAR(MAX),
    [Status] NVARCHAR(MAX),
    [ReviewDateUtc] DATETIME,
    [Order] FLOAT,
    [IsPublished] BIT,
    [IsPublic] BIT,
    [WhitelistGroups] BIT,
    [InheritPermissions] BIT,
    [NotifyOwner] BIT,
    [RevisionID] INT,
    [RevisionNumber] INT,
    [DraftStatus] NVARCHAR(MAX),
    [CreatedDate] DATETIME,
    [CreatedUid] UNIQUEIDENTIFIER,
    [CreatedFullName] NVARCHAR(MAX),
    [ModifiedDate] DATETIME,
    [ModifiedUid] UNIQUEIDENTIFIER,
    [ModifiedFullName] NVARCHAR(MAX),
    [OwnerUid] UNIQUEIDENTIFIER,
    [OwnerFullName] NVARCHAR(MAX),
    [OwningGroupID] INT,
    [OwningGroupName] NVARCHAR(MAX),
    [Uri] NVARCHAR(MAX)
);

ALTER TABLE Staging.TDKBA ALTER COLUMN OwningGroupID VARCHAR(MAX);
ALTER TABLE Staging.TDKBA ALTER COLUMN OwnerUid VARCHAR(MAX);
ALTER TABLE Staging.TDKBA ALTER COLUMN ReviewDateUtc VARCHAR(MAX);
ALTER TABLE Staging.TDKBA ADD BodyText VARCHAR(MAX);
ALTER TABLE Staging.TDKBA ADD BodyMarkdown VARCHAR(MAX);
