TD_ENTITIES = {
	"OrgApplication" => { :url => "https://api.teamdynamix.com/TDWebApi/Home/type/TeamDynamix.Api.Apps.OrgApplication" },
	"User" => { :url => "https://api.teamdynamix.com/TDWebApi/Home/type/TeamDynamix.Api.Users.User" },
	"Group" => { :url => "https://api.teamdynamix.com/TDWebApi/Home/type/TeamDynamix.Api.Users.Group" },
	"Location" => { :url => "https://api.teamdynamix.com/TDWebApi/Home/type/TeamDynamix.Api.Locations.Location" },
	"Asset" => { :url => "https://api.teamdynamix.com/TDWebApi/Home/type/TeamDynamix.Api.Assets.Asset" },
	"Vendor" => { :url => "https://api.teamdynamix.com/TDWebApi/Home/type/TeamDynamix.Api.Assets.Vendor" },
	"ProductModel" => { :url => "https://api.teamdynamix.com/TDWebApi/Home/type/TeamDynamix.Api.Assets.ProductModel" },
	"ProductType" => { :url => "https://api.teamdynamix.com/TDWebApi/Home/type/TeamDynamix.Api.Assets.ProductType" },
	"AssetStatus" => { :url => "https://api.teamdynamix.com/TDWebApi/Home/type/TeamDynamix.Api.Assets.AssetStatus" },
	"ReportInfo" => { :url => "https://api.teamdynamix.com/TDWebApi/Home/type/TeamDynamix.Api.Reporting.ReportInfo" },
	"Report" => { :url => "https://api.teamdynamix.com/TDWebApi/Home/type/TeamDynamix.Api.Reporting.Report" },
	"Project" => { :url => "https://api.teamdynamix.com/TDWebApi/Home/type/TeamDynamix.Api.Projects.Project" },
	"Plan" => { :url => "https://api.teamdynamix.com/TDWebApi/Home/type/TeamDynamix.Api.Plans.Plan" },
	"Task" => { :url => "https://api.teamdynamix.com/TDWebApi/Home/type/TeamDynamix.Api.Plans.Task" }
}

TELUS_METADATA = {
	"Subscriber" => [
		{
			"Name" => "SubscriberName",
			"Datatype" => "String",
			"Editable" => true,
			"Required" => false,
			"Nullable" => true,
			"Summary" => "",
			"URL" => ""
		},
		{
			"Name" => "BAN",
			"Datatype" => "String",
			"Editable" => true,
			"Required" => false,
			"Nullable" => true,
			"Summary" => "",
			"URL" => ""
		},
		{
			"Name" => "PhoneNumber",
			"Datatype" => "String",
			"Editable" => true,
			"Required" => false,
			"Nullable" => true,
			"Summary" => "",
			"URL" => ""
		},
		{
			"Name" => "Status",
			"Datatype" => "String",
			"Editable" => true,
			"Required" => false,
			"Nullable" => true,
			"Summary" => "",
			"URL" => ""
		},
		{
			"Name" => "RatePlan",
			"Datatype" => "String",
			"Editable" => true,
			"Required" => false,
			"Nullable" => true,
			"Summary" => "",
			"URL" => ""
		},
		{
			"Name" => "ServiceCategory",
			"Datatype" => "String",
			"Editable" => true,
			"Required" => false,
			"Nullable" => true,
			"Summary" => "",
			"URL" => ""
		},
		{
			"Name" => "DeviceName",
			"Datatype" => "String",
			"Editable" => true,
			"Required" => false,
			"Nullable" => true,
			"Summary" => "",
			"URL" => ""
		},
		{
			"Name" => "IMEI",
			"Datatype" => "String",
			"Editable" => true,
			"Required" => false,
			"Nullable" => true,
			"Summary" => "",
			"URL" => ""
		},
		{
			"Name" => "SimSerialNumber",
			"Datatype" => "String",
			"Editable" => true,
			"Required" => false,
			"Nullable" => true,
			"Summary" => "",
			"URL" => ""
		},
		{
			"Name" => "UpgradeEligible",
			"Datatype" => "String",
			"Editable" => true,
			"Required" => false,
			"Nullable" => true,
			"Summary" => "",
			"URL" => ""
		},
		{
			"Name" => "DeviceBalanceDue",
			"Datatype" => "String",
			"Editable" => true,
			"Required" => false,
			"Nullable" => true,
			"Summary" => "",
			"URL" => ""
		},
		{
			"Name" => "DomesticUsageMB",
			"Datatype" => "String",
			"Editable" => true,
			"Required" => false,
			"Nullable" => true,
			"Summary" => "",
			"URL" => ""
		},
		{
			"Name" => "DomesticAllowanceMB",
			"Datatype" => "String",
			"Editable" => true,
			"Required" => false,
			"Nullable" => true,
			"Summary" => "",
			"URL" => ""
		},
		{
			"Name" => "DomesticOverageMB",
			"Datatype" => "String",
			"Editable" => true,
			"Required" => false,
			"Nullable" => true,
			"Summary" => "",
			"URL" => ""
		},
		{
			"Name" => "DomesticOverageCharge",
			"Datatype" => "String",
			"Editable" => true,
			"Required" => false,
			"Nullable" => true,
			"Summary" => "",
			"URL" => ""
		},
		{
			"Name" => "RoamingUsageMB",
			"Datatype" => "String",
			"Editable" => true,
			"Required" => false,
			"Nullable" => true,
			"Summary" => "",
			"URL" => ""
		},
		{
			"Name" => "RoamingAllowanceMB",
			"Datatype" => "String",
			"Editable" => true,
			"Required" => false,
			"Nullable" => true,
			"Summary" => "",
			"URL" => ""
		},
		{
			"Name" => "RoamingOverageMB",
			"Datatype" => "String",
			"Editable" => true,
			"Required" => false,
			"Nullable" => true,
			"Summary" => "",
			"URL" => ""
		},
		{
			"Name" => "RoamingOverageCharge",
			"Datatype" => "String",
			"Editable" => true,
			"Required" => false,
			"Nullable" => true,
			"Summary" => "",
			"URL" => ""
		},
		{
			"Name" => "DaysLeft",
			"Datatype" => "String",
			"Editable" => true,
			"Required" => false,
			"Nullable" => true,
			"Summary" => "",
			"URL" => ""
		},
		{
			"Name" => "ContractEndDate",
			"Datatype" => "String",
			"Editable" => true,
			"Required" => false,
			"Nullable" => true,
			"Summary" => "",
			"URL" => ""
		}
	]
}
