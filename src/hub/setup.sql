USE master;
GO

CREATE DATABASE JUS_Hub;
GO

USE JUS_Hub;
GO

CREATE SCHEMA meta;
GO

CREATE TABLE meta.datatype (
    datatype_id        INT           IDENTITY (1, 1)           NOT NULL,
    datatype_name      VARCHAR(256)                            NOT NULL,
    source_type        VARCHAR(256)                            NOT NULL,
    description        NVARCHAR(MAX)                           NOT NULL,
    last_modified_by   VARCHAR(512)  DEFAULT SYSTEM_USER       NOT NULL,
    last_modified_time DATETIME      DEFAULT CURRENT_TIMESTAMP NOT NULL,
    PRIMARY KEY (datatype_id)
);
GO

CREATE UNIQUE INDEX ix_meta_datatype ON meta.datatype (source_type ASC, datatype_name ASC);
GO

CREATE TABLE meta.host (
    host_id            BIGINT        IDENTITY (1, 1)           NOT NULL,
    host_name          VARCHAR(256)                            NOT NULL,
    ip                 VARCHAR(256)                            NOT NULL,
    description        NVARCHAR(MAX)                           NOT NULL,
    last_modified_by   VARCHAR(512)  DEFAULT SYSTEM_USER       NOT NULL,
    last_modified_time DATETIME      DEFAULT CURRENT_TIMESTAMP NOT NULL,
    PRIMARY KEY (host_id)
);
GO

CREATE UNIQUE INDEX ix_meta_host ON meta.host (host_name ASC, ip ASC);
GO

CREATE TABLE meta.source (
    source_id          BIGINT        IDENTITY (1, 1)           NOT NULL,
    host_id            BIGINT                                  NOT NULL,
    source_name        VARCHAR(256)                            NOT NULL,
    source_type        VARCHAR(256)                            NOT NULL,
    description        NVARCHAR(MAX)                           NOT NULL,
    last_modified_by   VARCHAR(512)  DEFAULT SYSTEM_USER       NOT NULL,
    last_modified_time DATETIME      DEFAULT CURRENT_TIMESTAMP NOT NULL,
    PRIMARY KEY (source_id),
    FOREIGN KEY (host_id) REFERENCES meta.host(host_id) ON UPDATE CASCADE ON DELETE CASCADE
);
GO

CREATE UNIQUE INDEX ix_meta_source ON meta.source (host_id ASC, source_type ASC, source_name ASC);
GO

CREATE TABLE meta.entity (
    entity_id          BIGINT        IDENTITY (1, 1)           NOT NULL,
    source_id          BIGINT                                  NOT NULL,
    entity_name        VARCHAR(256)                            NOT NULL,
    entity_type        VARCHAR(256)                            NOT NULL,
    description        NVARCHAR(MAX)                           NOT NULL,
    last_modified_by   VARCHAR(512)  DEFAULT SYSTEM_USER       NOT NULL,
    last_modified_time DATETIME      DEFAULT CURRENT_TIMESTAMP NOT NULL,
    PRIMARY KEY (entity_id),
    FOREIGN KEY (source_id) REFERENCES meta.source(source_id) ON UPDATE CASCADE ON DELETE CASCADE
);
GO

CREATE UNIQUE INDEX ix_meta_entity ON meta.entity (source_id ASC, entity_type ASC, entity_name ASC);
GO

CREATE TABLE meta.attribute (
    attribute_id       BIGINT        IDENTITY (1, 1)           NOT NULL,
    entity_id          BIGINT                                  NOT NULL,
    attribute_name     VARCHAR(256)                            NOT NULL,
    attribute_type     VARCHAR(256)                            NOT NULL,
    datatype_id        INT                                     NOT NULL,
    description        NVARCHAR(MAX)                           NOT NULL,
    last_modified_by   VARCHAR(512)  DEFAULT SYSTEM_USER       NOT NULL,
    last_modified_time DATETIME      DEFAULT CURRENT_TIMESTAMP NOT NULL,
    PRIMARY KEY (attribute_id),
    FOREIGN KEY (datatype_id) REFERENCES meta.datatype(datatype_id) ON UPDATE CASCADE ON DELETE CASCADE,
    FOREIGN KEY (entity_id) REFERENCES meta.entity(entity_id) ON UPDATE CASCADE ON DELETE CASCADE
);
GO

CREATE UNIQUE INDEX ix_meta_attribute ON meta.attribute (entity_id ASC, datatype_id ASC, attribute_type ASC, attribute_name ASC);
GO

CREATE SCHEMA sentinel;
GO

CREATE TABLE sentinel.code_version (
    code_version_id    BIGINT        IDENTITY(1,1)             NOT NULL,
    code_version_time  DATETIME      DEFAULT CURRENT_TIMESTAMP NOT NULL,
    source_id          BIGINT                                  NOT NULL,
    object_id          BIGINT                                  NOT NULL,
    object_name        VARCHAR(512)                            NULL,
    definition         NVARCHAR(MAX)                           NOT NULL,
    last_modified_by   VARCHAR(512)  DEFAULT SYSTEM_USER       NOT NULL,
    last_modified_time DATETIME      DEFAULT CURRENT_TIMESTAMP NOT NULL,
    PRIMARY KEY (code_version_id),
    FOREIGN KEY (source_id) REFERENCES meta.source(source_id) ON UPDATE CASCADE ON DELETE CASCADE
);
GO

CREATE UNIQUE INDEX ix_sentinel_code_version ON sentinel.code_version (source_id ASC, object_id ASC, object_name ASC, code_version_time ASC);
GO

IF NOT EXISTS(SELECT name FROM sys.database_principals WHERE name = 'cloveretl') CREATE USER [cloveretl] FOR LOGIN [cloveretl];
GO

ALTER ROLE [db_datareader] ADD MEMBER [cloveretl];
GO

ALTER ROLE [db_datawriter] ADD MEMBER [cloveretl];
GO

INSERT INTO meta.datatype (source_type, datatype_name, description) VALUES
('SQL Server', 'bigint', 'bigint'),
('SQL Server', 'int', 'int'),
('SQL Server', 'smallint', 'smallint'),
('SQL Server', 'tinyint', 'tinyint'),
('SQL Server', 'bit', 'bit'),
('SQL Server', 'decimal', 'decimal'),
('SQL Server', 'numeric', 'numeric'),
('SQL Server', 'money', 'money'),
('SQL Server', 'smallmoney', 'smallmoney'),
('SQL Server', 'float', 'float'),
('SQL Server', 'real', 'real'),
('SQL Server', 'time', 'time'),
('SQL Server', 'date', 'date'),
('SQL Server', 'smalldatetime', 'smalldatetime'),
('SQL Server', 'datetime', 'datetime'),
('SQL Server', 'datetime2', 'datetime2'),
('SQL Server', 'datetimeoffset', 'datetimeoffset'),
('SQL Server', 'char', 'char'),
('SQL Server', 'varchar', 'varchar'),
('SQL Server', 'text', 'text'),
('SQL Server', 'nchar', 'nchar'),
('SQL Server', 'nvarchar', 'nvarchar'),
('SQL Server', 'ntext', 'ntext'),
('SQL Server', 'binary', 'binary'),
('SQL Server', 'varbinary', 'varbinary'),
('SQL Server', 'image', 'image'),
('SQL Server', 'uniqueidentifier', 'uniqueidentifier'),
('SQL Server', 'xml', 'xml');
GO

INSERT INTO meta.host (host_name, ip, description) VALUES
('jenzabarsrv.twu.ca', '10.10.118.156', 'Jenzabar server');

DECLARE @host_id BIGINT = IDENT_CURRENT(N'meta.host');
INSERT INTO meta.source (host_id, source_type, source_name, description) VALUES
(@host_id, 'SQL Server', 'TmsEPrd', 'Jenzabar database');

