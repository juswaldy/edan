USE JUS_ODS;
GO

IF EXISTS (
	SELECT *
	FROM INFORMATION_SCHEMA.VIEWS
	WHERE TABLE_SCHEMA = N'Metacoder'
	AND TABLE_NAME = N'ViewOpTable'
)
	DROP VIEW Metacoder.ViewOpTable;
GO

IF EXISTS (
	SELECT *
	FROM INFORMATION_SCHEMA.VIEWS
	WHERE TABLE_SCHEMA = N'Metacoder'
	AND TABLE_NAME = N'ViewOpTableDetail'
)
	DROP VIEW Metacoder.ViewOpTableDetail;
GO

IF EXISTS (
	SELECT *
	FROM INFORMATION_SCHEMA.TABLES
	WHERE TABLE_SCHEMA = N'Metacoder'
	AND TABLE_NAME = N'OpAccess'
)
	DROP TABLE Metacoder.OpAccess;
GO

IF EXISTS (
	SELECT *
	FROM INFORMATION_SCHEMA.TABLES
	WHERE TABLE_SCHEMA = N'Metacoder'
	AND TABLE_NAME = N'Token'
)
	DROP TABLE Metacoder.Token;
GO

IF EXISTS (
	SELECT *
	FROM INFORMATION_SCHEMA.TABLES
	WHERE TABLE_SCHEMA = N'Metacoder'
	AND TABLE_NAME = N'Op'
)
	DROP TABLE Metacoder.Op;
GO

IF EXISTS (
	SELECT *
	FROM INFORMATION_SCHEMA.TABLES
	WHERE TABLE_SCHEMA = N'Metacoder'
	AND TABLE_NAME = N'Source'
)
	DROP TABLE Metacoder.Source;
GO

DROP SCHEMA Metacoder;
GO

CREATE SCHEMA Metacoder;
GO

CREATE TABLE Metacoder.Source (
	SourceId          BIGINT        IDENTITY (1, 1)           NOT NULL,
	SourceType        VARCHAR(256)                            NOT NULL,
	SourceName        VARCHAR(256)                            NOT NULL,
	SourceHostUrl     VARCHAR(256)                            NOT NULL,
	SourceInstance    VARCHAR(256)                            NOT NULL,
	PRIMARY KEY CLUSTERED (SourceId ASC)
);

INSERT INTO Metacoder.Source ( SourceType, SourceName, SourceHostUrl, SourceInstance ) VALUES
( 'SQL', 'Jenzabar', 'db6.dev.twu.ca:1443', 'TmsEPrd' ),
( 'SQL', 'Aqueduct', 'db6.dev.twu.ca:1443', 'Aqueduct' ),
( 'SQL', 'FinancialReporting', 'db1.test.twu.ca:1443', 'FinancialReporting' ),
( 'SQL', 'JICS', 'jenzabarsrvtest.twu.ca:1443', 'ICS_NET' ),
( 'SQL', 'AQ Report', 'db6.dev.twu.ca:1443', 'Aqueduct.Reporting.Report' ),
( 'SQL', 'FR Datacubes', 'db1.test.twu.ca:1443', 'FinancialReporting.META.DATACUBE' ),
( 'SQL', 'FR Staging', 'db1.test.twu.ca:1443', 'FinancialReporting.META.STAGING_TABLE' );
GO

CREATE TABLE Metacoder.Op (
	OpId              BIGINT        IDENTITY (1, 1)           NOT NULL,
	OpSourceId        BIGINT                                  NOT NULL,
	OpType            VARCHAR(256)                            NOT NULL,
	OpSubtype         VARCHAR(256)                            NULL,
	OpSchema          VARCHAR(256)                            NOT NULL,
	OpName            VARCHAR(256)                            NOT NULL,
	PRIMARY KEY CLUSTERED (OpId ASC)
);
GO

CREATE TABLE Metacoder.Token (
	TokenId            BIGINT        IDENTITY (1, 1)          NOT NULL,
	TokenOpId          BIGINT                                 NOT NULL,
	TokenMode          VARCHAR(256)                           NOT NULL,
	TokenSequence      BIGINT                                 NOT NULL,
	TokenType          VARCHAR(256)                           NOT NULL,
	TokenInstance      VARCHAR(MAX)                           NOT NULL,
	PRIMARY KEY CLUSTERED (TokenId ASC)
);
GO

CREATE NONCLUSTERED INDEX ix_Metacoder_Token ON Metacoder.Token (
	TokenOpId ASC,
	TokenMode ASC,
	TokenSequence ASC,
	TokenType ASC
);
GO

CREATE TABLE Metacoder.OpAccess (
	OpAccessId         BIGINT        IDENTITY (1, 1)          NOT NULL,
	OpId               BIGINT                                 NOT NULL,
	OpSegment          BIGINT                                 NOT NULL,
	OpAccessType       VARCHAR(256)                           NOT NULL,
	Tablename          VARCHAR(256)                           NOT NULL,
	Fieldname          VARCHAR(256)                           NULL,
	PRIMARY KEY CLUSTERED (OpAccessId ASC)
);
GO

CREATE NONCLUSTERED INDEX ix_Metacoder_OpAccess ON Metacoder.OpAccess (
	OpId ASC,
	OpSegment ASC,
	OpAccessType ASC,
	Tablename ASC
) INCLUDE (
	Fieldname
);
GO

CREATE VIEW Metacoder.ViewOpTableDetail AS
WITH cleaned AS (
    SELECT
        OpAccessId,
        Tablename,
        REPLACE(REPLACE(Tablename, '[', ''), ']', '') CleanTablename
    FROM Metacoder.OpAccess
),
dots AS (
    SELECT
        *,
        LEN(CleanTablename) - LEN(REPLACE(CleanTablename, '.', '')) NumDots,
        CHARINDEX('.', CleanTablename) FirstDot,
        CHARINDEX('.', CleanTablename, CHARINDEX('.', CleanTablename)+1) SecondDot
    FROM cleaned
),
dst AS (
    SELECT
        *,
        CASE
        WHEN NumDots < 2 THEN NULL
        ELSE SUBSTRING(CleanTablename, 1, FirstDot-1)
        END DbSegment,
        CASE NumDots
        WHEN 0 THEN NULL
        WHEN 1 THEN SUBSTRING(CleanTablename, 1, FirstDot-1)
        WHEN 2 THEN SUBSTRING(CleanTablename, FirstDot+1, SecondDot-FirstDot-1)
        END SchemaSegment,
        CASE NumDots
        WHEN 0 THEN CleanTablename
        WHEN 1 THEN SUBSTRING(CleanTablename, FirstDot+1, LEN(CleanTablename))
        WHEN 2 THEN SUBSTRING(CleanTablename, SecondDot+1, LEN(CleanTablename))
        END TableSegment
    FROM dots
)
SELECT
    a.OpId,
    a.OpSegment,
    o.OpType,
    s.SourceInstance,
    o.OpSchema,
    o.OpName,
    a.OpAccessType,
    d.DbSegment,
    d.SchemaSegment,
    d.TableSegment
FROM dst d
JOIN Metacoder.OpAccess a ON d.OpAccessId = a.OpAccessId
JOIN Metacoder.Op o ON a.OpId = o.OpId
JOIN Metacoder.Source s ON o.OpSourceId = s.SourceId;
GO

CREATE VIEW Metacoder.ViewOpTable AS
SELECT DISTINCT
    SourceInstance,
    OpSchema,
    OpName,
    OpAccessType,
    DbSegment,
    SchemaSegment,
    TableSegment
FROM Metacoder.ViewOpTableDetail;
GO
