# Master Data

This module provides master data management functionalities.

- Data quality and cleaning
  - [OpenRefine](https://github.com/OpenRefine/OpenRefine)
  - [openrefine-client](https://github.com/opencultureconsulting/openrefine-client)
- Models and entities
- Methods and exceptions

