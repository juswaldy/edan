# EDAN

Enterprise Data Analysis N00bificator 2000



```mermaid
graph LR
	subgraph Producers
		Cloud
		RDBMS
		File
	end
	subgraph Hub
		Shipping
		Staging
		Receiving
	end
	subgraph Warehouse
		Meta
		ODS
		Master
		Cubes
	end
	subgraph Outlets
		Budgeting
		Enrollment
		Research
		AQReports
	end
	subgraph Consumers
		Tableau
		Vena
		Pandas
		Emily
	end

	Cloud --> Receiving
	RDBMS --> Receiving
	File --> Receiving
	Receiving --> Staging

	Staging --> Shipping
	
	Meta --> ODS
	ODS --> Master
	Meta --> Master

	Staging --> ODS
	
	Shipping --> Research
	Shipping --> Enrollment
	Shipping --> Budgeting
	Shipping --> AQReports
	AQReports --> Pandas
	AQReports --> Emily

	Curator --> Meta

	Enrollment --> Tableau
	Research --> Tableau

	Research --> Vena
	Budgeting --> Vena
	
	Master --> Shipping
	Meta --> Staging

	Master --> Cubes
	Cubes --> Shipping
	Meta --> Cubes
```

